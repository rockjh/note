monitor对象可以简单理解为一种同步工具，也可以说是一种同步机制，在java中每个对象仅有一个，多线程中互斥访问

线程等待synchronized锁时，进入MONITOR状态，调用Thread.interrupt()，不会中断，isInterrupted()为true

wait/notify/notifyAll只能用于synchronized方法或者语句块中，并且object.wait()和synchronized锁定的为同一个对象，否则会抛出异常。wait()释放当前锁使线程进入WAIT状态，该WAIT状态在被调用了Thread.interrupt()之后会抛出InterruptedException异常

LockSupport.park()使线程进入WAIT状态，调用unpark()可以恢复，调用Thread.interrupt()也可以恢复并且不会抛出异常,isInterrupted()为true

java.util.concurrent包中，基于Lock锁都是采用的LockSupport.park()的形式使线程进入WAIT状态

suspend/resume，阻塞和恢复线程，不会释放锁，已弃用。弃用原因，因为不释放锁，很容易导致死锁的发生如果用suspend挂起一个持有一个锁的线程，那么该锁在线程恢复之前是不可用的，这时候如果调用suspend方法的线程试图获得同一个锁，就会发生死锁

yield()向调度器发起一个示意，表示可以放弃当前使用的处理器，去处理其他任务，调度器可以忽略，在处理时一般会根据调度器的优先级来处理，也就是说可能获得使用权的还是当前线程。一般使用调试代码，它有助于重现因竞争条件而产生的错误

sleep(1000)线程休眠，不释放锁，可被Thread.interrupt()打断，抛出InterruptedException异常。关于sleep(0)作用，放弃当前cpu的调度，各个线程重新竞争cpu时间片

join是将两个线程从异步转为同步执行，比如main线程中在A线程开始之后，调用了A.join()，那么main线程就进入WAIT状态，直到A线程执行完毕，main线程恢复RUNNABLE状态。内部原理是在join方法中调用了wait()方法，因为当前执行线程是main，所以main方法进入WAIT状态，直到A线程执行完毕之后会调用A.notifyAll()，main线程恢复执行

退出一个线程的办法有哪些？1.直接使用stop()方法，已弃用，不推荐，因为stop是粗暴的直接终止线程，可能会导致数据不一致等问题，比如共享变量被修改，又无法还原。2.使用标记，定义一个boolean变量，线程运行为true,终止为false。3.使用interrupt()标记可以中断线程，之后使用isInterrupted()判断线程是否可以中断，如果可以直接return。
