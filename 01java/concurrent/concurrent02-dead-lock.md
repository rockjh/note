发生死锁的原因

- 互斥条件，类似java中的Monitor都是独占，要么我用，要么你用
- 互斥条件是长期持有的，在使用结束前，自己不会释放，也不能被其他线程抢占
- 循环依赖关系，两个或者多个个体之间出现了锁的链条环

避免死锁方法

1. 尽量避免使用多个锁，并且只有需要时才持有锁，否则即使是非常精通并发编程的工程师，也难免掉进坑里，嵌套的synchronized和lock非常容易出问题。因此如果我们赋予了一段程序太多的职责，出现了既要...又要...的情况，可能就需要我们审视下设计思路或目的是否合理了。
2. 如果必须使用多个锁，尽量设计好锁的获取顺序，参考银行家算法。将对象（方法）和锁之间的关系用图形化的方式分别抽象出来；根据对象之间的组合，调用关系对比和组合，考虑可能调用时序；按照可能的时序合并，发现可能死锁的场景
3. 使用带超时的方法，为程序带来更多的可控性，我们完全可以假定该锁不一定会获得，指定超时时间，并未无法获得锁时准备退出逻辑

银行家算法原理

- 当一个顾客对资金的最大需求量不超过银行家现有资金时就可接纳顾客
- 顾客可以分期贷款，但贷款总数不能超过最大需求量、
- 当银行家现有资金不满足顾客尚需的贷款数额时，对顾客的贷款可推迟支付，但总能使顾客在有限的时间里的得到贷款
- 当顾客得到所需全部资金后，一定能在有限的时间里归还所有资金

死锁示例代码

```
public class DeadLockSample extends Thread {
    private String first;
    private String second;

    public DeadLockSample(String name, String first, String second) {
        super(name);
        this.first = first;
        this.second = second;
    }

    public void run() {
        synchronized (first) {
            System.out.println(this.getName() + "obtained:" + first);
            try {
                Thread.sleep(1000L);
                synchronized (second) {
                    System.out.println(this.getName() + "obtained:" + second);
                }
            } catch (InterruptedException e) {
                //Donothing
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        String lockA = "lockA";
        String lockB = "lockB";
        DeadLockSample t1 = new DeadLockSample("Thread1", lockA, lockB);
        DeadLockSample t2 = new DeadLockSample("Thread2", lockB, lockA);
        t1.start();
        t2.start();
        t1.join();
        t2.join();
    }
}
```
分析死锁的来源，方法一

使用jstack pid打印当前进程的线程堆栈快照，如下图

![线程堆栈快照](http://img2.ph.126.net/LZUbA1bGzVX2iFyz7oTm7A==/1986650385724268945.png)

分析死锁的来源，方法二

使用java提供的标准apiThreadMXBean接口，其提供了方法findDeadlockedThreads()可以直接找出死锁，修改后的main方法如下

```
public static void main(String[] args) throws InterruptedException {
    ThreadMXBean mbean = ManagementFactory.getThreadMXBean();
    Runnable dlCheck = () -> {
        long[] threadIds = mbean.findDeadlockedThreads();
        if (threadIds != null) {
            ThreadInfo[] threadInfos = mbean.getThreadInfo(threadIds);
            System.out.println("Detected deadlock threads:");
            for (ThreadInfo threadInfo : threadInfos) {
            	//可以对死锁线程的信息进行处理
                Arrays.stream(threadInfo.getStackTrace()).forEach(item-> System.out.println(item));
                System.out.println(threadInfo.getThreadName());
            }
        }
    };
    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    // 稍等 5 秒，然后每 10 秒进行一次死锁扫描
    scheduler.scheduleAtFixedRate(dlCheck, 5L, 10L, TimeUnit.SECONDS);


    String lockA = "lockA";
    String lockB = "lockB";
    DeadLockSample t1 = new DeadLockSample("Thread1", lockA, lockB);
    DeadLockSample t2 = new DeadLockSample("Thread2", lockB, lockA);
    t1.start();
    t2.start();
    t1.join();
    t2.join();
}
```
jstack命令补充。jstack <pid>

线程状态

- Deadlock，死锁。表明已经发生了死锁，需要处理
- Runnable，运行中。如果多打印几次都处理这个状态，表明线程忙碌，可能处理死循环
- Waiting on condition，等待资源。等待的什么资源是否和其他线程的一些资源冲突
- Waiting on monitor entry，等待获取监视器。，等待的什么资源是否和其他线程的一些资源冲突
- Suspended，暂停
- Object.wait()或TIMED_WAITING，对象等待中。这里可能是性能的瓶颈，比如IO,网络等操作
- Blocked，阻塞。这里为等待资源超时，可能是死锁
- Parked，停止

jstack使用场景可以实时的打印jvm堆栈信息，定位死循环，死锁，配合性能监测工具测试性能问题，这些都需要执行几次jstack来定位问题

关于死循环问题，在linux系统中直接使用top命令找出占用cpu最高的java进程，然后再使用top -Hp <pid>找出该进程中占用cpu最高的线程，记住该线程的pid，转换为十六进制，然后多次使用jstack <进程的pid>，查看打印出来的nid=线程pid十六进制值的线程卡在什么位置，状态，就可以定位死循环位置