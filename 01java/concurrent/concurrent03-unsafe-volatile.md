### Unsafe是什么
 - Unsafe类是在sun.misc包下，不属于Java标准。但是很多Java的基础类库，包括一些被广泛使用的高性能开发库都是基于Unsafe类开发的，比如Netty、Cassandra、Hadoop、Kafka等。Unsafe类在提升Java运行效率，增强Java语言底层操作能力方面起了很大的作用
 - 它拥有直接操作内存空间的能力，它的底层cpu指令是轻量级的，因此速度快，但因为他直接操作内存，慎用。
 
### Unsafe用途

- 标准的cas实现，可以实现无锁编程，实例java8的ConCurrentHashMap，下面我也有一个自己的实现

```
public static void main(String[] args) throws Exception{
    Constructor<Unsafe> constructor = Unsafe.class.getDeclaredConstructor();
    constructor.setAccessible(true);
    //通过反射得到unsafe
    Unsafe unsafe = constructor.newInstance();
    Class<?> ak = int[].class;
    //数组开始偏移值
    long ABASE = unsafe.arrayBaseOffset(ak);
    int scale = unsafe.arrayIndexScale(ak);
    //数组每个元素占位
    int ASHIFT = 31 - Integer.numberOfLeadingZeros(scale);
    //字段value额偏移值
    long valueOffset = unsafe.objectFieldOffset(MyObject.class.getDeclaredField("value"));

    MyObject myObject = new MyObject();
    myObject.setValue(1);
    //变更value值
    unsafe.compareAndSwapInt(myObject,valueOffset,1,2);
    //变更arrays元素索引为1的值
    unsafe.compareAndSwapInt(myObject.getArrays(),((long) 1 << ASHIFT) + ABASE, 2,1);
    //打印变更后arrays元素索引为1的值
    System.out.println(unsafe.getInt(myObject.getArrays(),((long) 1 << ASHIFT) + ABASE));
}

static class MyObject {
    private volatile int value;
    private volatile int[] arrays = new int[]{1,2,3,4,5,6,7};
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int[] getArrays() {
        return arrays;
    }
}
```

> 注意我使用的基本数据类型int，如果对象为Integer等对象那么compareAndSwapInt变为compareAndSwapObject，getInt变为getObject

- 调用park/unpark阻塞/恢复线程，具体见Thread 锁

- 还有其他的实现用到时候再详解
 
### volatile解析

#### 历史背景

    随着cpu的发展，内存的读写速度逐渐跟不上cpu，因此cpu增加了缓存，在多核cpu的情况下不同的cpu执行不同的线程，他们用到的同一个变量在各自的cpu缓存中可能不一致，这就导致了数据不一致的问题

#### 解决方案

    各个处理器遵循一些协议保证数据的一致性，同时在硬件层面增加内存屏障。内存屏障分为读屏障和写屏障

#### 内存屏障

1. 使用方式，通过在读写操作前后加入内存屏障
2. 作用，可以阻止屏障两侧执行指令重排序；强制把写缓冲区/高速缓存中的脏数据等写回主内存，让缓存中相应的数据失效
3. 指令重排的目的是为了在不改变单线程执行结果的前提下，优化程序的运行效率，在多线程中可能会导致得不到自己想要的效果
    ```
    //下面的执行逻辑是，B线程等待A线程执行完毕赋值之后再执行
    //当指令重排下面两行代码交换顺序时，多线程情况下会有问题
    context = loadContext();
    contextReady = true;
    
    
    boolean contextReady = false;
    //在线程A中执行
    context = loadContext();
    contextReady = true;
    //在线程B中执行
    while( ! contextReady ){
        sleep(200);
    }
    doAfterContextReady (context);
    ```

#### volatile原理

1. 不同的硬件对于内存屏障的实现方式是不同的，java屏蔽掉这些差异，通过jvm生成内存屏障指令
2. 一个变量被修饰后，就会拥有内存屏障的两个特性。不被指令重排；对其他线程可见。
3. 对于volatile变量的读写拥有原子性，单纯的读写都不会受到干扰，i++是两个操作，因此不属于原子操作


### 易错知识点

1. 内存结构是方法区、堆、虚拟机栈、本地方法栈、程序计数器
2. 内存模型是，cpu有L1、L2、L3多级缓存，也有内存屏障，通过volatile和synchronized处理并发
3. synchronized的作用，互斥、禁止代码重排、cpu缓存失效
4. volatile作用，cpu缓存失效，禁止代码重排