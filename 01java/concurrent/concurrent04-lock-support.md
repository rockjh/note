LockSupport中的核心方法都是基于

```
//阻塞当前线程
UNSAFE.park(boolean isAbsolute, long time);
//恢复thread线程的阻塞
UNSAFE.uppark(Thread thread);
```
它的虚拟机内部实现阻塞和解除阻塞是基于一个permit作为关联，permit相当于一个信号量（0,1）默认0。

调用park则会检测permit是否为1，如果为1则置为0且立即返回；如果为0则阻塞线程，直到permit变为1

调用unpark，如果没有可用线程，则给定许可（permit置为1），不会累计；如果有线程被阻塞，解除阻塞状态，同时park返回；如果给定线程没有启动，该操作不能保证有任何效果

因此park/unpark可以颠倒顺序调用，不必遵从类似wait/notify的顺序，也可以达到相同的效果，如果先调用unpark，则调用park会立即返回


关键代码

```
//blocker对象在线程受阻塞时被记录
//以允许监视工具和诊断工具确定线程受阻塞的原因
public static void park(Object blocker) {
    Thread t = Thread.currentThread();
    //使用UNSAFE设置blocker值
    setBlocker(t, blocker);
    UNSAFE.park(false, 0L);
    //park返回阻塞结束后，设置回null
    setBlocker(t, null);
}
```

> 在park时调用Thread.interrupt()可以中断阻塞，并且返回true

参考：[LockSupport解析与使用](https://blog.csdn.net/secsf/article/details/78560013)