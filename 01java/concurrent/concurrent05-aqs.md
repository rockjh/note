## 概述

AQS，抽象队列式的同步器，AQS定义了一套多线程访问共享资源的同步器框架，许多同步类实现都依赖于它，如常用的ReentrantLock/Semaphore/CountDownLatch

## 框架

![clh队列](http://qba5tosiw.bkt.clouddn.com/721070-20170504110246211-10684485.png)

1. 他维护了一个volatile in state（代表共享资源）和一个FIFO线程等待队列（多线程征用资源被阻塞时会进入此队列），这里volatile是核心关键词，state的访问方式有三种
    - getState
    - setState
    - compareAndSetState
2. AQS定义两种资源共享方式
    - exclusive，独占，只有一个线程能执行，如ReentrantLock
    - share，共享，多个线程可以同时执行，如Semaphore、CountDownLatch
3. 不同的自定义同步器争用共享资源的方式也不同，自定义同步器在实现时只需要实现共享资源state的获取与释放方式即可，至于具体线程等待队列的维护（如获取资源失败、入队、唤醒出队等），AQS已经在顶层实现好了，自定义同步器实现时主要实现以下几种方法
    - isHeldExclusively()，该线程是否正在独占资源，只有用到condition才需要实现它
    - tryAcquire(int)，独占方式，尝试获取资源，成功返回true，失败false
    - tryRelease(int)，独占方式，尝试释放资源，成功返回true，失败false
    - tryAcquireShared(int)，共享方式，尝试获取资源，负数表示失败，0或者正数表示成功，且剩余资源数为返回值
    - tryReleaseShared(int)，共享方式，尝试释放资源，如果释放后允许唤醒后续等待节点返回true，否则返回false
4. 示例
    - 以ReentrantLock为例，state初始化为0，表示未锁定状态。A线程lock()时，会调用tryAcquire()独占该锁并将state+1。此后，其他线程再tryAcquire()时就会失败，直到A线程unlock()到state=0（即释放锁）为止，其它线程才有机会获取该锁。当然，释放锁之前，A线程自己是可以重复获取此锁的（state会累加），这就是可重入的概念。但要注意，获取多少次就要释放多么次，这样才能保证state是能回到零态的
    - 再以CountDownLatch以例，任务分为N个子线程去执行，state也初始化为N（注意N要与线程个数一致）。这N个子线程是并行执行的，每个子线程执行完后countDown()一次，state会CAS减1。等到所有子线程都执行完后(即state=0)，会unpark()主调用线程，然后主调用线程就会从await()函数返回，继续后余动作
    - 一般来说，自定义同步器要么是独占方法，要么是共享方式，他们也只需实现tryAcquire-tryRelease、tryAcquireShared-tryReleaseShared中的一种即可。但AQS也支持自定义同步器同时实现独占和共享两种方式，如ReentrantReadWriteLock

## 源码详解

### acquire独占锁流程

1. Node节点是对每一个等待获取资源的线程的封装，其包含了同步线程本身及其等待状态，如是否被阻塞，是否等待唤醒，是否已经被取消等，变量waitStatus则表示当前Node节点的等待状态，共有5种取值
    - CANCELLED(1)，表示当前结点已取消调度，当timeout或被中断（响应中断的情况下），会出发变更为此状态，进入该状态后的结点将不会再变化
    - SIGNAL(-1)，表示后继结点在等待当前节点唤醒，后继结点入队时，会将前继结点的状态更新为SIGNAL
    - CONDITION(-2)，表示结点等待在condition上，当其他线程调用了condition的signal方法后，CONDITION状态的结点将从等待队列转移到同步队列中，等待获取同步锁
    - PROPAGATE(-3)，共享模式下，前继结点不仅会唤醒其后继结点，同时也可能会唤醒后继的后继结点
    - 0，新结点入队时的默认状态
    > 负值表示结点处于有效等待状态，而正值表示结点已被取消，所以源码中很多地方用>0、<0来判断结点的状态是否正常
2. acquire(int)，此方法是独占模式下县城获取共享资源的顶层入口，如果获取到资源，线程直接返回，否则进入等待队列，直到获取到资源为止，且整个过程忽略中断的影响，代码、执行流程如下
    ```
    public final void acquire(int arg) {
        if (!tryAcquire(arg) &&
            acquireQueued(addWaiter(Node.EXCLUSIVE), arg))
            selfInterrupt();
    }
    ```
    - tryacquire尝试直接获取资源，如果成功则直接返回（这里体现了非公平锁，每个线程获取锁时会尝试直接抢占加塞一次，而CLH队列中可能还有别的线程在等待）
    - addWaiter将该线程加入等待队列的尾部，并标记为独占模式
    - acquireQueued使线程阻塞在等待队列中获取资源，一直获取到资源后才返回，如果在整个过程中被中断过，则返回true，否则返回false
    - 如果线程在等待过程中被中断过，他是不响应的，只有获取资源后才再进行自我中断selfInterrupt，将中断补上

3. tryAcquire(int)，此方法尝试获取独占锁，获取成功返回true，否则false
    ```
    protected boolean tryAcquire(int arg) {
        throw new UnsupportedOperationException();
    }
    ```
    - 直接抛出异常，因为AQS只是一个框架，具体的资源获取/释放方式交由自定义同步器去实现
    - 具体的资源获取交由自定义同步器去实现（通过state的get/set/CAS），能不能重入，加塞都是自己定义实现
    - 之所以没有定义为abstract，因为独占模式只用实现tryAcquire-tryRelease，而共享模式只用tryAcquireShared-tryReleaseShared

4. addWaiter(Node)，此方法用于将当前线程加入到等待队列的队尾，并返回当前节点所在的结点
    ```
    private Node addWaiter(Node mode) {
        //以给定模式构造结点。mode有两种：EXCLUSIVE（独占）和SHARED（共享）
        Node node = new Node(Thread.currentThread(), mode);
        //尝试快速方式直接放到队尾
        Node pred = tail;
        if (pred != null) {
            node.prev = pred;
            if (compareAndSetTail(pred, node)) {
                pred.next = node;
                return node;
            }
        }
        //上一步失败则通过enq入队
        enq(node);
        return node;
    }
    ```

    ```
    private Node enq(final Node node) {
        //CAS"自旋"，直到成功加入队尾
        for (;;) {
            Node t = tail;
            if (t == null) { // 队列为空，创建一个空的标志结点作为head结点，并将tail也指向它。
                if (compareAndSetHead(new Node()))
                    tail = head;
            } else {//正常流程，放入队尾
                node.prev = t;
                if (compareAndSetTail(t, node)) {
                    t.next = node;
                    return t;
                }
            }
        }
    }
    ```

5. acquireQueued(Node,int)，已经被放入等待队列尾部了，进入等待状态休息，直到其他线程彻底释放资源后唤醒自己
    ```
    final boolean acquireQueued(final Node node, int arg) {
        boolean failed = true;//标记是否成功拿到资源
        try {
            boolean interrupted = false;//标记等待过程中是否被中断过
            for (;;) {
                final Node p = node.predecessor();//拿到前驱
                //如果前驱是head，即该结点已成老二，那么便有资格去尝试获取资源（可能是老大释放完资源唤醒自己的，当然也可能被interrupt了）。
                if (p == head && tryAcquire(arg)) {
                    setHead(node);//拿到资源后，将head指向该结点。所以head所指的标杆结点，就是当前获取到资源的那个结点或null。
                    p.next = null; // setHead中node.prev已置为null，此处再将head.next置为null，就是为了方便GC回收以前的head结点。也就意味着之前拿完资源的结点出队了！
                    failed = false; // 成功获取资源
                    return interrupted;//返回等待过程中是否被中断过
                }

                //如果自己可以休息了，就通过park()进入waiting状态，直到被unpark()。如果不可中断的情况下被中断了，那么会从park()中醒过来，发现拿不到资源，从而继续进入park()等待。
                if (shouldParkAfterFailedAcquire(p, node) &&
                    parkAndCheckInterrupt())
                    interrupted = true;//如果等待过程中被中断过，哪怕只有那么一次，就将interrupted标记为true
            }
        } finally {
            if (failed) // 如果等待过程中没有成功获取资源（如timeout，或者可中断的情况下被中断了），那么取消结点在队列中的等待。
                cancelAcquire(node);
        }
    }
    ```
    ```
    private static boolean shouldParkAfterFailedAcquire(Node pred, Node node) {
        int ws = pred.waitStatus;//拿到前驱的状态
        if (ws == Node.SIGNAL)
            //如果已经告诉前驱拿完号后通知自己一下，那就可以安心休息了
            return true;
        if (ws > 0) {
            /*
            * 如果前驱放弃了，那就一直往前找，直到找到最近一个正常等待的状态，并排在它的后边。
            * 注意：那些放弃的结点，由于被自己“加塞”到它们前边，它们相当于形成一个无引用链，稍后就会被保安大叔赶走了(GC回收)！
            */
            do {
                node.prev = pred = pred.prev;
            } while (pred.waitStatus > 0);
            pred.next = node;
        } else {
            //如果前驱正常，那就把前驱的状态设置成SIGNAL，告诉它拿完号后通知自己一下。有可能失败，人家说不定刚刚释放完呢！
            compareAndSetWaitStatus(pred, ws, Node.SIGNAL);
        }
        return false;
    }
    ```
    ```
    private final boolean parkAndCheckInterrupt() {
        LockSupport.park(this);
        return Thread.interrupted();
    }
    ```
    - 结点进入队尾后，检查状态，找到安全休息点（一个正常的结点后面，最好的情况是head后面，等待head释放锁）
    - 调用park进入waiting状态，等待unpark或者interrupt唤醒，interrupt唤醒的话，只是加一个标识，然后继续进入阻塞
    - 被唤醒后，看自己是不是有资格能拿到号。如果拿到，head指向当前结点，并返回从入队到拿到号的整个过程中是否被中断过；如果没拿到，继续流程1。

总结

1. 调用自定义同步器的tryAcquire()尝试直接去获取资源，如果成功则直接返回；
2. 没成功，则addWaiter()将该线程加入等待队列的尾部，并标记为独占模式；
3. acquireQueued()使线程在等待队列中休息，有机会时（轮到自己，会被unpark()）会去尝试获取资源。获取到资源后才返回。如果在整个等待过程中被中断过，则返回true，否则返回false。
4. 如果线程在等待过程中被中断过，它是不响应的。只是获取资源后才再进行自我中断selfInterrupt()，将中断补上

![acquire-process](http://qba5tosiw.bkt.clouddn.com/aqs-main-process.png)

### release独占锁流程

1. release(int)，释放指定量的资源，如果彻底释放了即status=0，他会唤醒等待队列中的其他线程来获取资源
    ```
    public final boolean release(int arg) {
        if (tryRelease(arg)) {
            Node h = head;//找到头结点
            if (h != null && h.waitStatus != 0)
                unparkSuccessor(h);//唤醒等待队列里的下一个线程
            return true;
        }
        return false;
    }
    ```
    ```
    private void unparkSuccessor(Node node) {
        //这里，node一般为当前线程所在的结点。
        int ws = node.waitStatus;
        if (ws < 0)//置零当前线程所在的结点状态，允许失败。
            compareAndSetWaitStatus(node, ws, 0);

        Node s = node.next;//找到下一个需要唤醒的结点s
        if (s == null || s.waitStatus > 0) {//如果为空或已取消
            s = null;
            for (Node t = tail; t != null && t != node; t = t.prev) // 从后向前找。
                if (t.waitStatus <= 0)//从这里可以看出，<=0的结点，都是还有效的结点。
                    s = t;
        }
        if (s != null)
            LockSupport.unpark(s.thread);//唤醒
    }
    ```

总结

release()是独占模式下线程释放共享资源的顶层入口。它会释放指定量的资源，如果彻底释放了（即state=0）,它会唤醒等待队列里的其他线程来获取资源。

### acquireShared共享锁流程

1. acquireShared(int)，共享模式下线程获取共享资源的顶层入口，他会获取指定量的资源，获取成功则直接返回，获取失败进入等待队列
    ```
    public final void acquireShared(int arg) {
        if (tryAcquireShared(arg) < 0)
            doAcquireShared(arg);
    }
    ```
2. doAcquireShared(int)，将当前线程加入等待队列尾部休息，直到其他线程释放资源唤醒自己
    ```
    private void doAcquireShared(int arg) {
        final Node node = addWaiter(Node.SHARED);//加入队列尾部
        boolean failed = true;//是否成功标志
        try {
            boolean interrupted = false;//等待过程中是否被中断过的标志
            for (;;) {
                final Node p = node.predecessor();//前驱
                if (p == head) {//如果到head的下一个，因为head是拿到资源的线程，此时node被唤醒，很可能是head用完资源来唤醒自己的
                    int r = tryAcquireShared(arg);//尝试获取资源
                    if (r >= 0) {//成功
                        setHeadAndPropagate(node, r);//将head指向自己，还有剩余资源可以再唤醒之后的线程
                        p.next = null; // help GC
                        if (interrupted)//如果等待过程中被打断过，此时将中断补上。
                            selfInterrupt();
                        failed = false;
                        return;
                    }
                }

                //判断状态，寻找安全点，进入waiting状态，等着被unpark()或interrupt()
                if (shouldParkAfterFailedAcquire(p, node) &&
                    parkAndCheckInterrupt())
                    interrupted = true;
            }
        } finally {
            if (failed)
                cancelAcquire(node);
        }
    }
    ```
    ```
    private void setHeadAndPropagate(Node node, int propagate){
        Node h = head;
        setHead(node);//head指向自己
        //如果还有剩余量，继续唤醒下一个邻居线程
        if (propagate > 0 || h == null || h.waitStatus < 0) {
            Node s = node.next;
            if (s == null || s.isShared())
                doReleaseShared();
        }
    }
    ```
    - 当前剩余资源数为3，第二个结点需要资源数5，第三个结点需要资源数为2，队列也会继续park，不会唤醒第三个结点

总结

1. tryAcquireShared()尝试获取资源，成功则直接返回
2. 失败则通过doAcquireShared()进入等待队列park()，直到被unpark()/interrupt()并成功获取到资源才返回。整个等待过程也是忽略中断的。其实跟acquire()的流程大同小异，只不过多了个自己拿到资源后，还会去唤醒后继队友的操作（这才是共享嘛）

### releaseShared共享锁流程

1. releaseShared(int)，共享模式下线程释放共享资源的顶层入口。它会释放指定量的资源，如果成功释放且允许唤醒等待线程，它会唤醒等待队列里的其他线程来获取资源

    ```
    public final boolean releaseShared(int arg) {
        if (tryReleaseShared(arg)) {//尝试释放资源
            doReleaseShared();//唤醒后继结点
            return true;
        }
        return false;
    }
    ```
    ```
    private void doReleaseShared() {
        for (;;) {
            Node h = head;
            if (h != null && h != tail) {
                int ws = h.waitStatus;
                if (ws == Node.SIGNAL) {
                    if (!compareAndSetWaitStatus(h, Node.SIGNAL, 0))
                        continue;
                    unparkSuccessor(h);//唤醒后继
                }
                else if (ws == 0 &&
                        !compareAndSetWaitStatus(h, 0, Node.PROPAGATE))
                    continue;
            }
            if (h == head)// head发生变化
                break;
        }
    }
    ```

### 使用的设计思想

1. 模板设计模式
2. 使用自旋，然后采用park阻塞，这样就不会消耗cpu，唤醒后，也可以执行流程