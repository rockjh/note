### 写在前面的

1. java内置锁synchronized，他利用jvm每一个对象都存在的monitor互斥访问的属性，达到加锁的目的。出了锁区域，jvm自动释放锁。synchronized的可重入是因为：当前线程持monitor，再次进入synchronized时，会在monitor对象中的Nest字段计数。synchronized在现代jdk版本中有偏斜锁（默认），轻量级锁，重量级锁，根据竞争情况采用不同的锁级别，jvm可以根据不同的竞争情况自动的升降级锁
    - 偏斜锁，锁不存在竞争而且总是由同一线程多次获得，为了让线程获得锁的代价更低而引用了偏向锁，无竞争时不需要CAS操作来加锁和解锁
    - 轻量级锁，通过CAS操作来加锁和解锁
    - 重量级锁，一个线程获得锁，其他线程阻塞

2. java在concurrent包中实现Lock，常用的有ReentrantLock，ReentrantReadWriteLock，需要手动释放锁，他的实现主要依赖于AbstractQueuedSynchronizer抽象类。AbstractQueuedSynchronizer抽象类加锁主要使用内置的一个锁状态属性status，status初始化为0，独占锁获取之后会加1，独占锁释放之后会减1，释放锁之前status是可以累加的（可重入），当status=0时，当前线程就已经释放锁了,唤醒下一个等待线程。共享锁status初始化为n（n>0），当有线程需要m个资源时，status-m，直到status=0，还需要资源的线程进入等待队列。其中线程的WAIT状态使用LockSupport类中的sun.misc.Unsafe的park方法处理。

### 常用的锁

1. CountDownLatch是一个同步类实例代码如下
    ```
    //main线程需要等到thread-1,thread-2,thread-3都执行完毕之后才会执行
    public static void main(String[] args) throws InterruptedException {
        //3为等待线程数
        CountDownLatch countDownLatch = new CountDownLatch(3);
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName());
            //计数减一
            countDownLatch.countDown();
        }, "thread-1").start();
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName());
            countDownLatch.countDown();
        }, "thread-2").start();
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName());
            countDownLatch.countDown();
        }, "thread-3").start();
        //进入WAIT状态，直到计数=0被唤醒
        countDownLatch.await();
        System.out.println(Thread.currentThread().getName());
    }
    ```

2. ReentrantReadWriteLock读写锁，实现方式是两个读写内部类共用一个AQS实例，基本逻辑如下：同一个线程获得写锁可重入读锁；读锁为共享锁，写锁为独占锁；读锁被占用时，无法获得写锁，如果同一个线程中先获得读锁，再请求获得写锁，会导致死锁的发生
3. ReentrantLock互斥重入锁，实例化，lock，unlock
4. Semaphore基于共享锁，初始化n个信号量，表明可以有n个信号量可用，用完之后进入队列等待，可选公平不公平锁
    ```
    public static void main(String[] args) throws InterruptedException {
        //初始化信号量为3，表示共有3个信号量被分配，如果再有多的需排队等待
        Semaphore semaphore = new Semaphore(3);
        //响应中断的获取信号量
        semaphore.acquire();
        //不响应中断获取信号量
        //semaphore.acquireUninterruptibly();
        //释放信号量
        semaphore.release();
        //减少信号量，protected方法，可被继承
        //semaphore.reducePermits(1);
    }
```
6. StampedLock是Java8引入的一种新的所机制,简单的理解,可以认为它是读写锁的一个改进版本,读写锁虽然分离了读和写的功能,使得读与读之间可以完全并发,但是读和写之间依然是冲突的,读锁会完全阻塞写锁,它使用的依然是悲观的锁策略.如果有大量的读线程,他也有可能引起写线程的饥饿。而StampedLock则提供了一种乐观的读策略,这种乐观策略的锁非常类似于无锁的操作,使得乐观锁完全不会阻塞写线程
    ```
    public class Point {
        private double x, y;//内部定义表示坐标点
        private final StampedLock s1 = new StampedLock();//定义了StampedLock锁,

        private void move(double deltaX, double deltaY) {
            long stamp = s1.writeLock();//这里的含义和distanceFormOrigin方法中 s1.readLock()是类似的
            try {
                x += deltaX;
                y += deltaY;
            } finally {
                s1.unlockWrite(stamp);//退出临界区,释放写锁
            }
        }

        private double distanceFormOrigin() {//只读方法
            long stamp = s1.tryOptimisticRead();  //试图尝试一次乐观读 返回一个类似于时间戳的邮戳整数stamp 这个stamp就可以作为这一个所获取的凭证
            double currentX = x, currentY = y;//读取x和y的值,这时候我们并不确定x和y是否是一致的
            if (!s1.validate(stamp)) {//判断这个stamp是否在读过程发生期间被修改过,如果stamp没有被修改过,责任无这次读取时有效的,因此就可以直接return了,反之,如果stamp是不可用的,则意味着在读取的过程中,可能被其他线程改写了数据,因此,有可能出现脏读,如果如果出现这种情况,我们可以像CAS操作那样在一个死循环中一直使用乐观锁,知道成功为止
                stamp = s1.readLock();//也可以升级锁的级别,这里我们升级乐观锁的级别,将乐观锁变为悲观锁, 如果当前对象正在被修改,则读锁的申请可能导致线程挂起.
                try {
                    currentX = x;
                    currentY = y;
                } finally {
                    s1.unlockRead(stamp);//退出临界区,释放读锁
                }
            }
            return Math.sqrt(currentX * currentX + currentY * currentY);
        }
    }
    ```
