### 新建线程方式

- 继承Thread类，实现run方法，无返回值
- 实现runnable接口，实现run方法，无返回值
- 实现callable接口，实现call方法，采用FutureTask包装，有返回值

### FutureTask包装

1. 新建线程代码如下
    ```
    public class CallableRandomDouble implements Callable<Double> {
        @Override
        public Double call() {
            return Math.random();
        }
    }
    ```
    ```
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<Double> task = new FutureTask<>(new CallableRandomDouble());
        new Thread(task , "有返回值的线程").start();
        System.out.println(task.get());
    }
    ```
    - 使用FutureTask将callable的实现类包装一下，将他的call方法，赋值进FutureTask中的callable局部变量中
    - 使用Thread启动，最终还是由Thread来管理线程的一些状态
    - FutureTask只是将callable进行了一个封装，加入了一些其他的状态，等待返回值
    - 执行完毕之后，通过get可以获取到返回值

### 源码解析

1. 当前线程的几个状态
    ```
    private volatile int state;
    private static final int NEW          = 0;
    private static final int COMPLETING   = 1;
    private static final int NORMAL       = 2;
    private static final int EXCEPTIONAL  = 3;
    private static final int CANCELLED    = 4;
    private static final int INTERRUPTING = 5;
    private static final int INTERRUPTED  = 6;
    ```
2. get()，获取执行结果，这个方法会产生阻塞，会一直等到任务执行完毕才返回

    ```
    public V get() throws InterruptedException, ExecutionException {
        int s = state;
        if (s <= COMPLETING)
            s = awaitDone(false, 0L);
        return report(s);
    }
    ```
    ```
    private int awaitDone(boolean timed, long nanos)
        throws InterruptedException {
        final long deadline = timed ? System.nanoTime() + nanos : 0L;
        WaitNode q = null;
        boolean queued = false;
        for (;;) {
            if (Thread.interrupted()) {
                removeWaiter(q);
                throw new InterruptedException();
            }

            int s = state;
            if (s > COMPLETING) {
                if (q != null)
                    q.thread = null;
                return s;
            }
            else if (s == COMPLETING) // cannot time out yet
                Thread.yield();
            else if (q == null)
                q = new WaitNode();
            else if (!queued)
                queued = UNSAFE.compareAndSwapObject(this, waitersOffset,
                                                     q.next = waiters, q);
            else if (timed) {
                nanos = deadline - System.nanoTime();
                if (nanos <= 0L) {
                    removeWaiter(q);
                    return state;
                }
                LockSupport.parkNanos(this, nanos);
            }
            else
                LockSupport.park(this);
        }
    }
    ```
    - 另外一个线程去获取某个线程执行结果，如果线程没有执行完毕，会加入到WaitNode，一个链表的等待队列，然后将waiters换成最新的等待
    - 使用park阻塞当前线程

3. run()方法，Thread要执行的方法
    ```
    public void run() {
        if (state != NEW ||
            !UNSAFE.compareAndSwapObject(this, runnerOffset,
                                         null, Thread.currentThread()))
            return;
        try {
            Callable<V> c = callable;
            if (c != null && state == NEW) {
                V result;
                boolean ran;
                try {
                    result = c.call();
                    ran = true;
                } catch (Throwable ex) {
                    result = null;
                    ran = false;
                    setException(ex);
                }
                if (ran)
                    set(result);
            }
        } finally {
            runner = null;
            int s = state;
            if (s >= INTERRUPTING)
                handlePossibleCancellationInterrupt(s);
        }
    }
    ```
    ```
    protected void set(V v) {
        if (UNSAFE.compareAndSwapInt(this, stateOffset, NEW, COMPLETING)) {
            outcome = v;
            UNSAFE.putOrderedInt(this, stateOffset, NORMAL);
            finishCompletion();
        }
    }
    ```
    ```
    private void finishCompletion() {
        for (WaitNode q; (q = waiters) != null;) {
            if (UNSAFE.compareAndSwapObject(this, waitersOffset, q, null)) {
                for (;;) {
                    Thread t = q.thread;
                    if (t != null) {
                        q.thread = null;
                        LockSupport.unpark(t);
                    }
                    WaitNode next = q.next;
                    if (next == null)
                        break;
                    q.next = null;
                    q = next;
                }
                break;
            }
        }
        done();
        callable = null;
    }
    ```
    - run方法主要是，执行总逻辑，调用了callable传递进来的call方法，顺利执行后调用set来设置返回值到局部变量outcome
    - set中调用了finishCompletion来唤醒WaitNode链表中的等待返回值的线程
    - finishCompletion方法，是根据WaitNode的链表来将他们一个一个的unpark唤醒