### 写在前面的

一般对于程序的异步处理，我们会采取新建线程的处理方式，但面对多个或者复杂任务时，我们通常采用的线程池，java中包含三种线程池ForkJoinPool、ThreadPoolExecutor、ScheduledThreadPoolExecutor，他们之间的关系如下图

![线程池关系图](http://img2.ph.126.net/anuTgCdkigxVJ2F1S4ek1g==/1680968561016572309.png)

### ThreadPoolExecutor解析

构造函数

```
    /**
     *
     * @param corePoolSize 核心线程数量，执行任务后在线程池中保留的数量，不受keepAliveTime时间的影响,除非allowCoreThreadTimeOut=true
     * @param maximumPoolSize 最大线程数量，可能会因为任务过多，线程池临时增加到这个数量，当线程空闲后受keepAliveTime时间影响，进行销毁
     * @param keepAliveTime 保存时间
     * @param unit 参数keepAliveTime的单位
     * @param workQueue 任务阻塞队列
     * @param threadFactory 线程工厂
     * @param handler 拒绝策略
     */
    public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory,
                              RejectedExecutionHandler handler) {
        if (corePoolSize < 0 ||
                maximumPoolSize <= 0 ||
                maximumPoolSize < corePoolSize ||
                keepAliveTime < 0)
            throw new IllegalArgumentException();
        if (workQueue == null || threadFactory == null || handler == null)
            throw new NullPointerException();
        this.acc = System.getSecurityManager() == null ?
                null :
                AccessController.getContext();
        this.corePoolSize = corePoolSize;
        this.maximumPoolSize = maximumPoolSize;
        this.workQueue = workQueue;
        this.keepAliveTime = unit.toNanos(keepAliveTime);
        this.threadFactory = threadFactory;
        this.handler = handler;
    }
```
主要我们注意以下几点

线程池是如何开始运行的？

在我们初始化ThreadPoolExecutor后，可以使用execute或者submit方法将任务放入线程池，最终他们都是调用的execute方法

在execute方法中，有以下几种选择

- 线程池中的数量少于核心线程数量，则调用addWorker(command, true)初始化一个线程，并将其放入线程池中
- 线程池中的数量大于等于核心线程数量，将任务放进队列中
- 线程池队列满，当前线程数小于最大线程数，创建新线程执行任务，直到等于最大线程数
- 线程池队列满，当前线程数等于最大线程数，采用拒绝策略

注意以下两点
1. 线程池中的线程使用的是内部类Worker，他继承了AbstractQueuedSynchronizer类实现了Runnable接口，关键属性thread为线程的属性，firstTask为需要执行的任务，这几者之间的关系是：thread的target=worker，worker的target=获取task，firstTask=获取的任务或者构造函数传入。因此，初始化之后启动这个工作线程只需要thread.start()就ok，在addWorker方法中就是这样处理的。
2. 线程池是如何销毁线程的？当worker线程启动之后，运行runWorker(this)方法，； 里面有一个while循环，while条件有一个是getTask()，在满足线程销毁条件时，任务队列不会阻塞，调用workQueue.poll(keepAliveTime, TimeUnit.NANOSECONDS)，如果在keepAliveTime时间内队列中没有任务，则返回null，线程终结

### ScheduledThreadPoolExecutor解析

定时任务线程池，继承ThreadPoolExecutor，实现ScheduledExecutorService，它主要是在ThreadPoolExecutor功能上面进行扩充一个定时循环执行的功能。自定义一个阻塞队列DelayedWorkQueue，它按照执行的时间点升序排列，最先执行的放在队列前面；自定义一个ScheduledFutureTask封装任务，根据任务执行周期，将任务放在队列中，如果是周期任务，执行之后会重新放进队列中。

ScheduledExecutorService结构定义如下
```
//执行一次，delay时间之后执行，单位unit
ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit);
//执行一次，delay时间之后执行，单位unit
<V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit);
//循环执行，initialDelay时间之后执行第一次，之后每隔period时间循环执行，单位unit
ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit);
//循环执行，initialDelay时间之后执行第一次，之后每隔period时间循环执行，单位unit
ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit);
```

注意以下两点
1. DelayedWorkQueue类中的leader属性表示当前等待队列出任务的线程，同一时刻只会有一个线程处于等待队列出任务，其他线程要么已经在做任务，要么阻塞
2. 排序方式使用的堆排序

### ForkJoinPool解析

ForkJoinPool具体思想是归并，先拆分，后归并结果集，当只需要执行的逻辑时，也可以不使用归并

```
public class Sum {

    public static void main(String[] args) throws Exception {
        ForkJoinPool pool = ForkJoinPool.commonPool();
        // 提交可分解的CalTask任务
        Future<Integer> future = pool.submit(new CalTask(0, 1000));
        System.out.println(future.get());
        pool.shutdown();
    }

    private static class CalTask extends RecursiveTask<Integer> {
        // 每个执行子任务最多只累加10个数
        private static final int THRESHOLD = 10;
        private int start;
        private int end;

        // 累加从start到end的数组元素
        public CalTask(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        protected Integer compute() {
            int sum = 0;
            //当end与start之间的差小于THRESHOLD时，开始进行实际累加
            if (end - start < THRESHOLD) {
                for (int i = start; i < end; i++) {
                    sum += i;
                }
                return sum;
            } else {
                // 如果当end与start之间的差大于THRESHOLD时，即要打印的数超过20个
                // 将大任务分解成两个小任务。
                int middle = (start + end) / 2;
                CalTask left = new CalTask(start, middle);
                CalTask right = new CalTask(middle, end);
                // 并行执行两个“小任务”
                left.fork();
                right.fork();
                // 把两个“小任务”累加的结果合并起来
                return left.join() + right.join();
            }
        }
    }
}
```

