### 概述

位于java.util.concurrent.atomic包下面的原子操作类，主要是依赖于CAS，它采用Unsafe中的比较交换来实现

### 单类型

AtomicLong是Long的原子操作类，Integer、Boolean，Reference（对象引用类型）也有相应的类型
```
public final boolean compareAndSet(long expect, long update) {
    return unsafe.compareAndSwapLong(this, valueOffset, expect, update);
}
```

### 数组类型

AtomicLongArray是Long数组的原子操作类，类似的Integer，Reference（对象引用类型）也有相应的类型
```
public final boolean compareAndSet(int i, long expect, long update) {
    return compareAndSetRaw(checkedByteOffset(i), expect, update);
}
```

### 解决ABA问题

使用AtomicStampedReference带版本号的解决ABA的问题，每次设置值时，不仅要比较旧值，还要比较版本号
```

private static class Pair<T> {
    final T reference;
    final int stamp;
    private Pair(T reference, int stamp) {
        this.reference = reference;
        this.stamp = stamp;
    }
    static <T> Pair<T> of(T reference, int stamp) {
        return new Pair<T>(reference, stamp);
    }
}
//比较交换的值
private volatile Pair<V> pair;
```
```
public boolean compareAndSet(V   expectedReference,
                                 V   newReference,
                                 int expectedStamp,
                                 int newStamp) {
    Pair<V> current = pair;
    //当值和版本号都相同时返回true
    //当比较交换Pair对象成功时，返回true
    return
        expectedReference == current.reference &&
        expectedStamp == current.stamp &&
        ((newReference == current.reference &&
            newStamp == current.stamp) ||
            casPair(current, Pair.of(newReference, newStamp)));
}
```
> AtomicMarkableReference类似，只是他没有使用版本号，使用的是一个boolean型，作为一个标记

