## BIO
传统的IO，阻塞串行IO，经典案例Socket网络编程

```
public class Server{
    private static ServerSocket serverSocket;
    public static void main(String[] args){
        try {
            serverSocket = new ServerSocket(10086);
            while (true) {
                Socket socket = serverSocket.accept();
                PrintWriter out = new PrintWriter(socket.getOutputStream());
                out.println("Hello World");
                out.flush();
                //如果流不关闭，那么这个socket的通信就不会结束，一直阻塞，所以一般正式环境在新客户端来时建立新线程，来处理相关操作
                //输入流或者输出流其中之一关闭，通信就结束，另外的一个流也会关闭
                out.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 }
```

```
public class Client {

    public static void main(String[] args){
        for (int i = 0; i <100 ; i++) {
            try (Socket client = new Socket(InetAddress.getLocalHost(), 10086)) {
                new BufferedReader(new InputStreamReader(client.getInputStream())).lines().forEach(line-> System.out.println(line));
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
```
![BIO类关系](http://img2.ph.126.net/ZW-_2WvWKdwbqUL9FYsiDQ==/1387953110260744886.png)

## NIO

NIO要素Buffer（缓冲）、Channel（通道）、Selector（选择器）

### Buffer（缓冲区）

#### 常用Buffer类型

ByteBuffer,MappedByteBuffer,CharBuffer,DoubleBuffer,FloatBuffer,IntBuffer,LongBuffer,ShortBuffer。对应了几大基本数据类型

> ByteBuffer可以选择实例化为DirectByteBuffer和HeapByteBuffer，如果数据量比较小的中小应用情况下，可以考虑使用heapBuffer；反之可以用directBuffer。一般来说DirectByteBuffer可以减少一次系统空间到用户空间的拷贝。但Buffer创建和销毁的成本更高，更不宜维护，通常会用内存池来提高性能。其他buffer类似

#### 利用Buffer读写数据步骤

- 将数据写入buffer

- 调用flip将写模式改为读模式

- 从buffer中读取数据，进行操作

- 调用clear()清除整个buffer数据或者调用compact()清空已读数据

#### buffer的属性

- capacity容量，该buffer最多存储的字节数

- position位置，写模式下，position从0到capacity-1，变更为读模式后，position归零，边读边移动

- limit限制，写模式下，代表我们能写的最大量为capacity，读模式下，变更为原position位置，即有数据的位置

![buffer_modes.png](http://upload-images.jianshu.io/upload_images/7446289-9e2df5fac3a189e0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


#### buffer api

- 通过allocate()方法为buffer分配内存大小，如开辟一个48字节的ByteBuffer buffer：ByteBuffer.allocate(48)

- 写数据可以通过通道写，如：FileChannel.read(buffer)；也可以通过put方法来写数据，如：buffer.put(127)

- flip()翻转方法，将写模式切换到读模式，position归零，设置limit为之前的position位置

- 读数据可以读到通道，如：FileChannel.write(buffer)；也可以调用get()方法读取，byte aByte=buffer.get()

- buffer.rewind()将position置0，limit不变，这样我们就可以重复读取数据啦

- buffer.clear()将position置为0，limit设置为capacity，这里并没有删除buffer里面的数据，只是把标记位置改了；

- buffer.compact()清除已读数据，这里也没有删除数据，将position设置为未读的个数，将后面几个未读的字节顺序的复制到前面的几个字节，limit设置为capacity，比如buffer容量3个字节，读取hello，在读取了2个字节后我就调用了compact()方法，那么此时position为1，limit为3，buffer内部存储的数据buff[0]='l',buff[1]='e',buff[2]='l'，因为有一个'l'没有读完，将'l'提取到最前面供下次读取

- mark()可以标记当前的position位置，通过reset来恢复mark位置，可以用来实现重复读取满足条件的数据块

- equals()两个buffer相等需满足，类型相同，buffer剩余（未读）字节数相同，所有剩余字节数相同

- compareTo()比较buffer中的剩余元素，只不过此方法适合排序

### Channel（通道）

#### Channel的重要实现

- FileChannel用于文件数据的读写，transferTo()方法可以将通道的数据传送至另外一个通道，完成数据的复制

- DatagramChannel用于UDP数据的读写

- SocketChannel用于TCP的数据读写，通常我们所说的客户端套接字通道

- ServerSocketChannel允许我们监听TCP链接请求，通常我们所说的服务端套接字通道，每一个请求都会创建一个SocketChannel

#### Scatter和Gather

- java nio在Channel实现类也实现了Scatter和Gather相关类

- Scatter.read()是从通道读取的操作能把数据写入多个buffer，即一个通道向多个buffer写数据的过程，但是read必须写满一个buffer后才会向后移动到下一个buffer，因此read不适合大小会动态改变的数据。代码如下：

```
ByteBuffer header = ByteBuffer.allocate(128);
ByteBuffer body = ByteBuffer.allocate(1024);
ByteBuffer[] bufferArray = { header, body };
channel.read(bufferArray);
```

- Gather.write()是从可以把多个buffer的数据写入通道，write是只会写position到limit之间的数据，因此写是可以适应大小动态改变的数据。代码如下：

```
ByteBuffer header = ByteBuffer.allocate(128);
ByteBuffer body = ByteBuffer.allocate(1024);
//write data into buffers
ByteBuffer[] bufferArray = { header, body };
channel.write(bufferArray);
```

> 在有些场景会非常有用，比如处理多份需要分开传输的数据，举例来说，假设一个消息包含了header和body，我们可能会把header和body分别放在不同的buffer

### Selector（选择器）

Selector用于检查一个或多个NIO Channel的状态是否可读可写，这样就可以实现单线程管理多个Channels，也就是可以管理多个网络连接，NIO非阻塞主要就是通过Selector注册事件监听，监听通道将数据就绪后，就进行实际的读写操作，因此前面说的IO两个阶段，一阶段NIO仅仅是异步监听，二阶段就是同步实际操作数据

#### Selector监听事件类别

- SelectionKey.OP_CONNECT是Channel和server连接成功后，连接就绪

- SelectionKey.OP_ACCEPT是server Channel接收请求连接就绪

- SelectionKey.OP_READ是Channel有数据可读时，处于读就绪

- SelectionKey.OP_WRITE是Channel可以进行数据写入是，写就绪

#### 使用Selector的步骤

- 创建一个Selector，Selector selector = Selector.open();

- 注册Channel到Selector上面，将Channel切换为非阻塞的：channel.configureBlocking(false)，然后绑定Selector：SelectionKey key = channel.register(selector, SelectionKey.OP_READ);

- SelectionKey.OP_READ为监听的事件类别，如果要监听多个事件，可利用位的或运算结合多个常量如：int interestSet = SelectionKey.OP_READ | SelectionKey.OP_WRITE;

> 需要使用Selector的Channel必须是非阻塞的，FileChannel不能切换为非租塞的，因此FileChannel不适用于Selector

下面是TCP的服务端和客户端的实现

```
public class TCPServer {

    private static final int bufferSize = 1024;
    private static final long timeOut = 3000;// 超时时间
    private static final int listenPort = 1993;// 本地监听端口

    public static void main(String[] args) throws Exception {
        Selector selector = Selector.open();
        ServerSocketChannel listenerChannel = ServerSocketChannel.open();// 创建监听通道，专门用来监听指定的本地端口
        listenerChannel.socket().bind(new InetSocketAddress(listenPort));// 将listenerChannel的socket绑定为本地服务器（IP+prot）绑定
        listenerChannel.configureBlocking(false);
        // 将选择器绑定到监听信道,只有非阻塞信道才可以注册选择器.并在注册过程中指出该信道可以进行Accept操作
        listenerChannel.register(selector, SelectionKey.OP_ACCEPT);

        while (true) {
            if (selector.select(timeOut) == 0) {// 监听注册的通道，当其中有注册的IO时该函数返回（3000ms没有反应返回0），操作可以进行，并添加对应的SelectorKey
                System.out.println("It haven't I/O now, please wait!");
                continue;
            }

            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                try {
                    if (key.isAcceptable()) {
                        handleAccept(key);
                    }
                    if (key.isReadable()) {
                        handleRead(key);
                    }
                } catch (IOException e) {
                    //抛出异常取消该客户端的通道在Selector上的注册
                    e.printStackTrace();
                    continue;
                }
            }
            //每次select之后就会把准备好的key放在selectedKeys中，操作完之后要删除，否则下一轮循环中他还会存在，但是又没有select会报NullPointException
            selector.selectedKeys().clear();
        }
    }

    public static void handleAccept(SelectionKey key) throws IOException {
        // 返回创建此键的通道，接受客户端建立连接的请求，并返回SocketChannel对象
        SocketChannel clientChannel = ((ServerSocketChannel) key.channel()).accept();
        clientChannel.configureBlocking(false);
        // 将clientChannel注册到服务端的selector中，这样才能和客户端进行通信
        clientChannel.register(key.selector(), SelectionKey.OP_READ, ByteBuffer.allocate(bufferSize));
    }

    public static void handleRead(SelectionKey key) throws IOException {
        // 获取客户端通信的通道
        SocketChannel clientChannel = (SocketChannel) key.channel();
        ByteBuffer buffer = (ByteBuffer) key.attachment();
        buffer.clear();
        // 从客户端通道读取信息到buffer缓冲区中(并返回读到信息的字节数)
        long bytesRead = clientChannel.read(buffer);
        if (bytesRead == -1) {
            clientChannel.close();
        } else {
            buffer.flip();
            // 将字节转化为为UTF-8的字符串
            String receivedString = Charset.forName("UTF-8").newDecoder().decode(buffer).toString();
            System.out.println("接收到来自：" + clientChannel.socket().getRemoteSocketAddress() + "发来的信息：" + receivedString);
            String msgSendToClient = "已接收到你的信息：" + receivedString + "正在处理中";
            buffer = ByteBuffer.wrap(msgSendToClient.getBytes("UTF-8"));
            clientChannel.write(buffer);
            // 设置为下一次读取或是写入做准备
            key.interestOps(SelectionKey.OP_READ);
            //如果写准备那么selector.select(timeOut)就会一直返回准备好写的100个key,就会cpu空转，写了之后就不会有这种问题
            //key.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
        }
    }
}
```

```
public class TCPClient {

    public static void main(String[] args) throws IOException {
        Client client = new Client("127.0.0.1",1993,100);
        //Client如果不是另起一个线程，那么下面的就不会执行
        client.start();
        //每个客户端通道都发送消息给服务器
        while (true) {
            Scanner scan = new Scanner(System.in);
            String msg = scan.next();
            Iterator<SelectionKey> iterator = client.selector.keys().iterator();
            while (iterator.hasNext()) {
                client.sendMsg((SocketChannel) iterator.next().channel(), msg);
            }
        }
    }

    //多线程，选择器监听时后续的可以继续执行
    static class Client extends Thread{
        // 通道选择器，用于管理客户端的通道
        private Selector selector;

        public Client(String host,int port,int clientNum) throws IOException {
            // 创建选择器，并把通道注册到选择器中
            selector = Selector.open();
            for (int i = 0; i <clientNum ; i++) {
                // 与服务器通信的通道
                SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress(host, port));
                socketChannel.configureBlocking(false);
                socketChannel.register(selector, SelectionKey.OP_READ);
                sendMsg(socketChannel,"test"+i);
            }
        }

        @Override
        public void run(){
            try {
                while (selector.select() > 0) {// select()方法只能使用一次，用了之后就会自动删除,每个连接到服务器的选择器都是独立的
                    // 遍历每个有IO操作Channel对应的SelectionKey
                    for (SelectionKey sk : selector.selectedKeys()) {
                        if (sk.isReadable()) {
                            // 使用NIO读取Channel中的数据
                            SocketChannel sc = (SocketChannel) sk.channel();
                            ByteBuffer buffer = ByteBuffer.allocate(1024);
                            sc.read(buffer);
                            buffer.flip();
                            String receivedString = Charset.forName("UTF-8").newDecoder().decode(buffer).toString();
                            System.out.println("接收到来自服务器：" + sc.socket().getRemoteSocketAddress() + "的信息：" + receivedString);
                            //重新设置关注点
                            sk.interestOps(SelectionKey.OP_READ);
                        } else if (sk.isAcceptable()) {
                            // TODO: 2018/9/27
                        } else if (sk.isConnectable()) {
                            // TODO: 2018/9/27
                        } else if (sk.isWritable()) {
                            // TODO: 2018/9/27
                        }
                    }
                    selector.selectedKeys().clear();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * 发送字符串到服务器
         * @param message
         * @param socketChannel
         * @throws IOException
         */
        public void sendMsg(SocketChannel socketChannel,String message) throws IOException{
            ByteBuffer writeBuffer = ByteBuffer.wrap(message.getBytes("UTF-8"));
            socketChannel.write(writeBuffer);
        }
    }
}
```

- selector.select()查看是否有就绪事件，有n个就绪事件就返回n，没有就阻塞
- Selector类的基本实现SelectorImpl，其中有selectedKeys（就绪的key集合），keys（key注册集合），publicKeys（外部访问注册key集合），publicSelectedKeys（外部访问就绪key集合），一般来说他们之间内部元素的关系keys=publicKeys，selectedKeys=publicSelectedKeys
- 如果有n个SelectionKey就绪那么selector.select()返回n，并且publicSelectedKeys的元素和selectedKeys的元素也为n
- selector.selectedKeys().clear()清除就绪的集合，下一次selector.select()时不会清除之前就绪的集合


## AIO