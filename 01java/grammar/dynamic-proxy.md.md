### jdk代理

参考博文:[jdk动态代理实现原理](http://rejoy.iteye.com/blog/1627405)
之前虽然会用jdk代理，但一直不知道是怎么回事，比如说：InvocationHandler的invoke方法是由谁来调用的，代理对象是怎么生成的。
先来看看jdk的动态代理是怎么用的吧

实现自己的InvocationHandler code
```
package com.geccocrawler.gecco.spider.render.html;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 实现自己的InvocationHandler
 *
 * @author zyb
 * @since 2012-8-9
 */
public class MyInvocationHandler implements InvocationHandler {

    // 目标对象   
    private Object target;

    /**
     * 构造方法
     *
     * @param target 目标对象
     */
    public MyInvocationHandler(Object target) {
        super();
        this.target = target;
    }


    /**
     * 执行目标对象的方法
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        // 在目标对象的方法执行之前简单的打印一下  
        System.out.println("------------------before------------------");

        // 执行目标对象的方法  
        Object result = method.invoke(target, args);

        // 在目标对象的方法执行之后简单的打印一下  
        System.out.println("-------------------after------------------");

        return result;
    }

    /**
     * 获取目标对象的代理对象
     *
     * @return 代理对象
     */
    public Object getProxy() {
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                target.getClass().getInterfaces(), this);
    }
}
```
目标对象实现的接口UserService code
```
package com.geccocrawler.gecco.spider.render.html;

import java.io.IOException;

/**
 * 目标对象实现的接口，用JDK来生成代理对象一定要实现一个接口
 *
 * @author zyb
 * @since 2012-8-9
 */
public interface UserService {

    /**
     * 目标方法
     */
    public abstract void add();

} 
```
目标对象UserServiceImpl code
```
package com.geccocrawler.gecco.spider.render.html;

import java.io.IOException;

/**
 * 目标对象
 *
 * @author zyb
 * @since 2012-8-9
 */
public class UserServiceImpl implements UserService {
  
    /* (non-Javadoc) 
     * @see dynamic.proxy.UserService#add() 
     */

    public void add() {
        System.out.println("--------------------add---------------");
    }
}
```
动态代理测试类ProxyTest code
```
package com.geccocrawler.gecco.spider.render.html;

/**
 * 动态代理测试类
 *
 * @author zyb
 * @since 2012-8-9
 */
public class ProxyTest {

    public static void main(String[] args) throws Throwable {
        // 实例化目标对象  
        UserService userService = new UserServiceImpl();

        // 实例化InvocationHandler
        MyInvocationHandler invocationHandler = new MyInvocationHandler(userService);

        // 根据目标对象生成代理对象  
        UserService proxy = (UserService) invocationHandler.getProxy();

        // 调用代理对象的方法  
        proxy.add();

    }
}
```
执行结果如下： 
------------------before------------------ 
--------------------add--------------- 
-------------------after------------------ 

用起来是很简单吧，其实这里基本上就是AOP的一个简单实现了，在目标对象的方法执行之前和执行之后进行了增强。Spring的AOP实现其实也是用了Proxy和InvocationHandler这两个东西的。

用起来是比较简单，但是如果能知道它背后做了些什么手脚，那就更好不过了。首先来看一下JDK是怎样生成代理对象的。既然生成代理对象是用的Proxy类的静态方newProxyInstance，那么我们就去它的源码里看一下它到底都做了些什么？
在内存中生成一个字节码文件，然后使用classload加载，反射实例化，调用MyInvocationHandler中的invoke方法实现代理

### cglib代理

参考博文：[cglib动态代理实现](http://songbo-mail-126-com.iteye.com/blog/968792)
JDK实现动态代理需要实现类通过接口定义业务方法，对于没有接口的类，如何实现动态代理呢，这就需要CGLib了。CGLib采用了非常底层的字节码技术，其原理是通过字节码技术为一个类创建子类，并在子类中采用方法拦截的技术拦截所有父类方法的调用，顺势织入横切逻辑。JDK动态代理与CGLib动态代理均是实现Spring AOP的基础
简单的实现举例：
这是一个需要被代理的类，也就是父类，通过字节码技术创建这个类的子类，实现动态代理。
```
public class SayHello {
    public void say() {
        System.out.println("hello everyone");
    }
}
```
该类实现了创建子类的方法与代理的方法。getProxy(SuperClass.class)方法通过入参即父类的字节码，通过扩展父类的class来创建代理对象。intercept()方法拦截所有目标类方法的调用，obj表示目标类的实例，method为目标类方法的反射对象，args为方法的动态入参，proxy为代理类实例。proxy.invokeSuper(obj, args)通过代理类调用父类中的方法。
```
public class CglibProxy implements MethodInterceptor {
    private Enhancer enhancer = new Enhancer();

    public Object getProxy(Class clazz) {
        //设置需要创建子类的类
        enhancer.setSuperclass(clazz);
        enhancer.setCallback(this);
        //通过字节码技术动态创建子类实例
        return enhancer.create();
    }

    //实现MethodInterceptor接口方法
    public Object intercept(Object obj, Method method, Object[] args,
                            MethodProxy proxy) throws Throwable {
        System.out.println("前置代理");
        //通过代理类调用父类中的方法
        Object result = proxy.invokeSuper(obj, args);
        System.out.println("后置代理");
        return result;
    }
}
```
具体实现类：
```
public class DoCGLib {
    public static void main(String[] args) {
        CglibProxy proxy = new CglibProxy();
        //通过生成子类的方式创建代理类
        SayHello proxyImp = (SayHello) proxy.getProxy(SayHello.class);
        proxyImp.say();
    }
}
```
输出结果：
前置代理
hello everyone
后置代理

CGLib创建的动态代理对象性能比JDK创建的动态代理对象的性能高不少，但是CGLib在创建代理对象时所花费的时间却比JDK多得多，所以对于单例的对象，因为无需频繁创建对象，用CGLib合适，反之，使用JDK方式要更为合适一些。同时，由于CGLib由于是采用动态创建子类的方法，对于final方法，无法进行代理

补充——————————————————

代理是为一个类设置代理，不是针对方法

jdk的代理需要接口是因为内存中动态生成的字节码需要实现那个接口，以便于强转类型不出现报错，即超类一致

cglib不需要是因为他是在需要代理的类的基础上生成的子类，也做到了超类一致，强转不会报错

可以专门为一个接口生成代理，类似mybatis

### 应用场景

日志、权限、事务、性能监测、错误信息监测