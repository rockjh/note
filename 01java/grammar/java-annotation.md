### 定义

注解通过@interface定义，可以被元注解@Retention，@Documented，@Target，@Inherited，@Repeatable和<font color=Blue>普通注解</font>修饰。注解可以理解为代码中的特殊标记，这些标记可以在编译、类加载，运行时被读取，并通过Annotation Processing Tool（APT）注解处理工具进行相应的处理，通过注解开发人员可以在不改变原有代码和逻辑的情况下在源代码中嵌入补充信息

### 元注解

1. @Retention解释了注解的存活时间，他的取值如下：
    - RetentionPolicy.SOURCE 注解只在源码阶段保留，在编译器进行编译时它将被丢弃忽视
    - RetentionPolicy.CLASS 注解只被保留到编译进行的时候，它并不会被加载到 JVM 中。注解默认使用这种方式
    - RetentionPolicy.RUNTIME 注解可以保留到程序运行的时候，它会被加载进入到 JVM 中，所以在程序运行时可以获取到它们。因此可以使用反射机制读取该注解的信息

2. @Documented这个注解和文档有关，他的作用是能够讲注解中的元素包含到javadoc中

3. @Target指定了注解运用的场景，修饰的元素，target有下面的取值
    - ElementType.ANNOTATION_TYPE 可以给一个注解进行注解
    - ElementType.CONSTRUCTOR 可以给构造方法进行注解
    - ElementType.FIELD 可以给属性进行注解
    - ElementType.LOCAL_VARIABLE 可以给局部变量进行注解
    - ElementType.METHOD 可以给方法进行注解
    - ElementType.PACKAGE 可以给一个包进行注解
    - ElementType.PARAMETER 可以给一个方法内的参数进行注解
    - ElementType.TYPE 可以给一个类型进行注解，比如类、接口、枚举，<font color=Blue>注解</font>

4. @Inherited是继承的意思，如果说一个超类被@Inherited修饰， 那么如果他的子类没有被任何注解应用的话， 这个子类就继承了这个超类的注解

5. @Repeatable是可重复的注解，如下代码@Schedules就是一个容器装下了重复的注解@Schedule，使用方法，就是可以在一个元素上面定义多个重复的注解，然后使用容器注解@Schedules判断，循环处理

    ```
    @Repeatable(Schedules.class)
    public @interface Schedule {
        String dayOfMonth() default "first";
        String dayOfWeek() default "Mon";
        int hour() default 12;
    }

    @Retention(RetentionPolicy.RUNTIME)
    public @interface Schedules {
        Schedule[] value();
    }
    ```
### java预置注解

1. @Deprecated标记过时的元素，过时的类，成员变量，方法

2. @Override提示子类中的方法是复写父类中的方法

3. @SuppressWarnings阻止编译器告警

4. @FunctionalInterface函数式接口注解，函数式接口就是一个只有一个方法的普通接口，如Runnable接口

5. @PostConstruct被修饰的方法，会在构造器执行函数执行完成之后执行，在spring中，执行顺序Constructor>@Autowore>@PostConstruct，所以@PostConstruct很适合用来加载一些初始化操作，比如初始化缓存、字典等

### 注解与反射

加上了注解，一般通过反射来获取注解信息，可以通过java.lang.Class,java.lang.reflect.Method,java.lang.reflect.Field来获取是否有注解，然后获取注解的属性来进行操作

### 注解的使用场景

比如JSON序列化时，序列化的字段名想换一个；某个数据类型的格式化。大致就是需要有一些补充操作的，可以加上注解