### jvm是如何对类进行加载的
1. 类加载的来源
    - 压缩文件（jar、war）
    - 其他文件中（jsp）
    - 网络中（以前的applet）
    - 运行时按照特定规则生成的二进制流（动态代理）
    - 数据库中读取
2. 什么时候会进行类加载
    - new、使用设置执行static变量或方法时
    - 使用java.lang.reflect包中的方法对类进行反射调用时
    - 当初始化一个类时，如果父类还没有进行初始化，会先初始化父类
    - 虚拟机启动，用户需要指定一个主类（main），虚拟机会先初始化主类
    - 使用jdk7的动态语言支持时
    > 如果是final的static变量在编译期间就已经被替换了，不会发生类加载通过子类访问父类的static变量，只会初始化父类
3. 类初始化顺序
    - 父类--静态变量、父类--静态初始化块
    - 子类--静态变量、子类--静态初始化块
    - 父类--变量、父类--初始化块、父类--构造器（没有显示调用，就调用默认无参构造）
    - 子类--变量、子类--初始化块，子类--构造器
4. 类加载步骤
    - 加载，可以在这一步自定义类加载器，进行解密等操作
        - 通过类的全限定名来获取定义类的二进制流
        - 在内存中生成一个代表这个类的java.lang.Class对象，作为方法区这个类的各种数据的访问入口
    - 验证，确保class文件的字节流包含的信息符号当前虚拟机的要求，会完成
        - 文件格式验证
        - 元数据验证
        - 字节码验证
        - 符号引用验证，会抛出NoSuchMethodError、NoSuchFieldError等错误
    - 准备
        - 为（static）类变量分配内存并设置类变量初始值（类型默认值，例如int=0）
        - 对于final static变量，编译时Javac将会为value生成ConstantValue属性，在准备阶段虚拟机就会根据ConstantValue的设置将value赋值为123
            ```
            public static final int value=123
            ```
    - 解析，虚拟机将常量池内的符号引用替换为直接引用
        - 符号引用，是类中的各种引用跳转
        - 直接引用，是直接指向内存中的具体指针，cpu可直接使用
    - 类初始化，执行类构造器＜clinit＞方法的过程，类构造器方法由下面两种合成
        - 编译器自动收集类中的所有static类变量的赋值动作
        - 静态语句块（static{}块）中的语句
        - 如果一个类中没有静态语句块，也没有对类变量的赋值操作，那么就不会生成类构造器方法
        - 虚拟机会保证一个类的＜clinit＞方法在多线程环境中被正确地加锁、同步，如果多个线程同时去初始化一个类，单例的静态内部类实现方式
    - 使用
    - 卸载
5. 如下一段程序是如何进行类加载的

    ```
    public class Kafka {

        public static void main(String[] args) {
            ReplicaManager replicaManager = new ReplicaManager();
        }
    }

    class ReplicaManager extends AbstractDataManager{

        public static int flushInterval = Configuration.getInt("replica.flush.interval");

        public static Map<String,Replica> replicaMap;

        static{
            loadReplicaFromDisk();
        }

        public static void loadReplicaFromDisk(){
            replicaMap = new HashMap<String,Replica>();
        }

    }
    ```
    ![类加载流程](http://assets.processon.com/chart_image/5f8e5c5807912906db3206a6.png?_=1603166391818)

### 类加载器

1. 类加载器
    - 启动类加载器（Bootstrap ClassLoader），加载jre安装目录的核心类jre/lib
    - 扩展类加载器（Extension ClassLoader），加载jre安装目录下的扩展类jre/lib/ext
    - 应用程序类加载器（Application ClassLoader），加载classpath环境变量中所指定的路径中的类，就是我们所写的java代码
    - 自定义类加载器，根据自己的需要加载类
2. 双亲委派模型，除了顶层的启动类加载器外，其余的类加载器都应当有自己的父类加载器
    - 定义，某个特定的类加载器在接到加载类的请求 时，首先将加载任务委托给父类加载器， 依次递归，如果父类加载器可以完成类加载任务，就成功返回，只有父类加载器无法完成此加载任务时，才自己去加载
    - 父类加载器和类加载器是采用组合的形式，就是有一个parent属性
    - Java类随着它的类加载器一起具备了一种带有优先级的层次关系。例如类java.lang.Object，它存放在rt.jar之中，无论哪一个类加载器要加载这个类
    - 比较两个类是否相同是类全名+类加载器

    ![双亲委派模型类加载器](http://assets.processon.com/chart_image/5f8e83f0e0b34d0711113bd7.png?_=1603176338508)
3. 自定义类加载器都是java.lang.ClassLoader的子类，有两个达到目的方式
    - 重写findClass，建议采用，就是获取class的字节数组
    - 重写loadClass，不建议采用，因为里面实现了双亲委派模型，重写的话会破坏这种模型

### 破坏双亲委派模型

1. tomocat是个web容器， 那么它要解决什么问题
    - web容器可能需要部署多个应用，不同的应用可能会依赖同一个三方类库的不同版本，所以需要保证每个应用的类库是独立的<font color='green'>WebappClassLoader，各个Webapp私有的类加载器，加载路径中的class只对当前Webapp可见</font>
    - 多个应用可能会共享同一份类库<font color='green'>sharedLoader，各个Webapp共享的类加载器，加载路径中的class对于所有Webapp可见，但是对于Tomcat容器不可见；</font>
    - web容器也有自己的类库，不能和应用类库混淆，应该隔离开来<font color='green'>catalinaLoader，Tomcat容器私有的类加载器，加载路径中的class对于Webapp不可见；</font>
    - <font color='green'>commonLoader，Tomcat最基本的类加载器，加载路径中的class可以被Tomcat容器本身以及各个Webapp访问</font>

    ![tomcat类加载](http://assets.processon.com/chart_image/5f8e8b27e0b34d07111160ef.png?_=1603336501201)

2. jdbc
    - DriverManager是通过启动类加载器加载的，他来管理jdbc的接口
    - 实现是各个厂商的jar包通过应用程序类加载器加载的
    - 双亲委派模型是应用类加载器--->扩展类加载器--->启动类加载器，父级加载器是访问不了子级加载的类的，这里需要DriverManager能够访问例如mysql驱动，所以破坏了双亲委派模型
    - 使用了contextClassLoader在启动类加载器中获取到了应用类加载器，然后通过spi机制使用应用类加载器加载实现类，在DriverManager就得到了实现类的Class对象，这里就完成了双亲委派模型的破坏
        - contextClassLoader可以通过set方法设置，默认父线程的
        - contextClassLoader，最开始默认的Application ClassLoader
    - 在DriverManager中再次加载实现类，仅仅是让启动类加载器为了获得实现类的Class对象

    ![spi-jdbc流程图](http://assets.processon.com/chart_image/5f8e942fe0b34d0711118f4b.png?_=1603182426395)


### 方法调用解析

1. 静态分派，在编译期间就已经确定了，简而言之就是表面可以看到的，例如在idea中ctrl点击，就可以确定
    ```
    public static void main(String[] args){
        Human man = new Man();
        Human woman = new Woman();
        //human
        sayHello(man);
        //human
        sayHello(woman);
    }

    public static void sayHello(Human human){
        System.out.println("human");
    }
    public static void sayHello(Man man){
        System.out.println("man");
    }
    public static void sayHello(Woman woman){
        System.out.println("woman");
    }

    static abstract class Human{}
    static class Man extends Human{}
    static class Woman extends Human{}
    ```
2. 动态分派，也叫做多态，每个类都有一个虚方法表（虚拟机维护、维护着各个方法的实际入口--->方法区的某个地址）