### 为什么需要垃圾回收

我们创建的对象其实是占用内存资源的，当在堆内存中某些对象没有被引用了，那么他就应该被回收

```
public static void main(String[] args) {
    ReplicaManager replicaManager = new ReplicaManager();
}
```
![jvm-gc](http://assets.processon.com/chart_image/5f91295307912906db3828a5.png?_=1603349174422)

### 如何计算一个对象，到底在jvm中占用的内存空间

1. 一个对象对内存空间的占用分为两个部分
    - 对象头的信息
    - 对象的实例信息
2. 对象头，在64位的linux系统中，会占用16字节；实例对象中有int会占用4个字节、long占用8个字节等
3. 另外还有一些对象头补齐机制、指针压缩机制等

### 方法区的类会被垃圾回收吗

满足下面的几个条件方法区中的类会被回收

1. 该类的所有实例对象都已经从java堆内存里被回收
2. 加载这个类的classloader已经被回收
3. 对该类的class对象没有任何引用

### 跟jvm内存相关的几个核心参数

1. -XSS，每个线程的栈内存大小
2. -Xms，java堆大小刚开始的大小
3. -Xmx，java堆内存的最大大小
4. -Xmn，java堆内存中新生代的大小，扣除新生代剩下的就是老年代内存的大小
5. -XX:PermSize，永久代大小，-XX:PermSize=100M
6. -XX:MaxPermSize永久代最大大小
7. 使用示例
    - -Xms和-Xmx通常设置为一样的-Xmx100M -Xmx100M
    - IDEA中一般在VM options中配置，多个参数空格间隔
    - 通过java启动jar包的配置指定参数
        ```
        java -Xmx100M -Xmx100M -XX:PermSize=100M -jar App.jar
        ```
    - 通过tomcat启动配置，在bin目录下，找到脚本catalina.sh，添加如下参数
        ```
        JAVA_OPTS="-Xmx100M -Xmx100M -XX:PermSize=100M"
        ```

### 判断对象的存活

1. 引用计数器
    - 定义，被引用一次就减一，引用失效一次就减一，为零可被回收
    - 优点，实时性，程序无需挂起，区域性（更新对象计数器，不会影响其他对象）
    - 缺点，不能解决循环引用，浪费cpu（内存足够也会计数）
2. 可达性分析
    - 定义，通过GC ROOTS的对象作为起始点，从这些节点向下搜索，搜索走过的路径称为搜索链，一个对象到GC ROOTS没有任何搜索链时，该对象可以被回收
    - 缺点，垃圾回收时，应用会挂起一定时间
    - 优点，解决了循环引用问题
    - GC ROOTS对象包括下面几种
        - 虚拟机栈（栈帧中的本地变量表）中引用的对象
        - 方法区中类静态属性引用的对象
        - 方法区中常量引用的对象
        - 本地方法栈中JNI（一般指native方法）引用的对象

### finalize拯救回收对象

```
public class ReplicaManager {

    public static ReplicaManager instance;

    @Override
    protected void finalize() throws Throwable {
        instance = this;
    }
}
```
1. 回收对象条件
    - 进行可达性分析
    - 此对象是否有必要执行finalize()，当finalize()被复写并且没有被虚拟机调用过，才认为有必要执行，finalize()只会被执行一次
2. 上面这个代码在调用GC清理之前会调用一次finalize()，又使用了gc root引用，因此就不会被清理掉

### 引用类型

1. 强引用，Object obj = new Object();属于强引用
2. 软引用，进行垃圾回收后，发现内存空间还是存放不了新对象，就会对软引用进行垃圾回收
    ```
    //引用队列，如果被回收，就会被放进引用队列
    ReferenceQueue<String> referenceQueue=new ReferenceQueue<>();
    //软引用
    SoftReference<String> softReference=new SoftReference<>(new String("123"),referenceQueue);
    ```
3. 弱引用，下一次垃圾回收时，一定会被回收
    ```
    //一个线程只有一个ThreadLocal.ThreadLocalMap，在ThreadLocalMap中可以存在多个{key,value}->{ThreadLocal的弱引用,value}
    //如果某个ThreadLocal作用域过期，强引用失效，根据Entry继承弱引用的特性，他存储的是ThreadLocal的弱引用
    //当该ThreadLocal的强引用失效，那么gc可以对这个键进行回收
    static class Entry extends WeakReference<ThreadLocal<?>> {
        /** The value associated with this ThreadLocal. */
        Object value;

        Entry(ThreadLocal<?> k, Object v) {
            //将ThreadLocal作为弱引用使用key
            super(k);
            value = v;
        }
    }
    //ThreadLocal为什么会在程序运行时失效？ThreadLocal定义为某个局部变量，范围过了自然失效
    //ThreadLocal无效了，对应的value什么时候清除？在set或者get的时候会调用清除
    ```
4. 虚引用（幻想引用）,虚引用必须和引用队列ReferenceQueue联合使用，当垃圾回收器准备回收一个对象时，如果发现他还有虚引用，就会再回收对象的内存之前，把这个虚引用加入到与之关联的引用队列中，程序可以通过判断引用队列中是否已经加入了虚引用，来了解被引用的对象是否将要被垃圾回收，如果程序发现某个虚引用已经被加入到引用队列，那么就可以在所引用的对象的内存被回收之前才去一些程序行动
    ```
    //幻想引用
    //创建一个对象
    String str = new String("JAVA讲义");
    //创建一个引用队列
    ReferenceQueue<String> rq = new ReferenceQueue<>();
    //创建一个虚引用，指定引用对象.不能单独使用必须关联引用队列
    PhantomReference pr = new PhantomReference(str, rq);
    System.out.println(pr.get());
    //切断强引用
    str = null;
    //试图取得虚引用对象,null
    System.out.println(pr.get());
    //垃圾回收
    System.gc();
    System.runFinalization();
    //取出引队列中的最先进入队列的引用与pr进行比较true
    //这里可以判断如果队列中有值，那么某一个虚引用被gc回收，我们可以做特定的操作
    System.out.println(rq.poll() == pr);
    ```

> 引用之间的状态是可以切换的，软引用，弱引用都可以通过get来获取值，然后再赋值给强引用，所以前面的引用可达性图他们是可以转换的

### 垃圾收集算法

1. 标记清除算法，算法分为标记和清除两个阶段，先标记出所有需要回收的对象，在标记完成后统一回收所有被标记的对象
    - 缺点，产生大量不连续的内存碎片，当需要分配较大对象时，无法找到足够的连续内存，而不得不触发一次GC
2. 复制算法，将可用内存按容量划分大小相等的AB两块，每次只使用其中一块，当A用完了，就将A还存活的对象复制到B上面，然后把A全部清理掉
    - 缺点，将原来的内存缩小为了原来的一半，会进行一次移动
    - 优点，简答高效，不用考虑内存碎片等复杂情况
3. 标记整理算法，首先标记出所有需要回收的对象，在标记完成后，让所有存活的对象都向一端移动，然后直接清理掉端边界以外的内存

### 分代收集算法

1. jvm都采用分代收集算法，根据对象的存活周期不同，将内存划分为几块，一般分为新生代、老年代，就可以根据各个年代的特点采用最适合的收集算法
2. 因为新生代的对象98%是朝生夕死，所以不需要按照1：1的比例，所以将内存划分为Eden:Survivor:Survivor=8:1:1的比例，每次使用Eden和其中一块Survivor[1]，当回收时，将Eden和Survivor[1]中还存活的对象复制到Survivor[2]中，我们没有办法保证每次回收都只有不多于10%的对象存活，当Survivor空间不够用时，需要依赖其他内存（这里指老年代）进行分配担保（Handle Promotion），新生代使用复制算法
3. 老年代因为对象存活率高，没有额外空间对他进行分配担保，必须使用标记清除或者标记整理算法