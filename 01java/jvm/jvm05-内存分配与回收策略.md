### 新生代

1. 新生代采用变种的复制算法
    - 将内存划分为Eden:Survivor:Survivor=8:1:1的比例，每次使用Eden和其中一块Survivor[1]，当回收时，将Eden和Survivor[1]中还存活的对象复制到Survivor[2]中
    - 只在Eden分配新对象，Survivor只存放上次存活的对象
    - -XX:SurvivorRatio=8调整Eden和Survivor的比例
2. 触发时机
    - 新生代的Eden和其中一块Survivor快要满的时候
    - 触发Full GC之前会先进行Minor GC
3. 图示
    ![新生代垃圾回收](http://assets.processon.com/chart_image/5f97858ff346fb06e1eebcb9.png?_=1603767509140)

### 老年代

1. 老年代采用标记整理算法
2. 触发时机
    - 有新生代对象进入老年代，老年代空间不足时
    - Minor GC之前，会有一个对老年代空间的检查，空间是否大于等于以往几次Minor GC后平均晋升老年代对象的大小
        - 大于等于时，在Minor GC之后如果发现老年代的空间不足会再次进行Full GC
        - 小于时，会在Minor GC之前进行一次Full GC

### 新生代晋升老年代
1. 经历过15次垃GC还存在的对象（默认），可以通过-XX:MaxTenuringThreshold调整
2. 动态对象年龄判断，年龄1+年龄2+年龄3+年龄n对象的总和超过了Survivor区域的50%，就会把年龄n以上的对象都放进老年代
3. 大对象直接进入老年代，如果不直接进入老年代
    - 导致内存有空间，还是需要提前进行垃圾回收获取连续空间来放他们
    - Minor GC会进行大量的内存复制
    - X:PretenureSizeThreshold 参数 ，大于这个数量直接在老年代分配，缺省为0 ，表示绝不会直接分配在老年代（只对Serial/ParNew有用）
4. 空间分配担保，新生代中有大量的对象存活，survivor空间不够，当出现大量对象在MinorGC后仍然存活的情况，就需要老年代进行分配担保，把所有存活的对象都转移到老年代中

### 新生代的配置

1. 新生代大小配置参数的优先级
    - 高，-XX:NewSize/MaxNewSize
    - 中间，-Xmn，（NewSize= MaxNewSize）
    - 低，-XX:NewRatio ，表示比例，例如=2，表示 新生代：老年代 = 1:2
2. -XX:SurvivorRatio，表示Eden和Survivor的比值，缺省为8表示 Eden:FromSurvivor:ToSurvivor=8:1:1
3. 同样的代码情况下
    - -Xms20M -Xmx20M -XX:+PrintGCDetails –Xmn2m -XX:SurvivorRatio=2没有垃圾回收数组都在老年代
    - -Xms20M -Xmx20M -XX:+PrintGCDetails -Xmn7m -XX:SurvivorRatio=2发生了垃圾回收新生代存了部分数组，老年代也保存了部分数组，发生了晋升现象
    - -Xms20M -Xmx20M -XX:+PrintGCDetails -Xmn15m -XX:SurvivorRatio=8新生代可以放下所有的数组老年代没放
    - -Xms20M -Xmx20M -XX:+PrintGCDetails -XX:NewRatio=2发生了垃圾回收出现了空间分配担保，而且发生了FullGC

### GC调优

1. 原因，GC时，我们的系统将会暂停运行（stop the world）所以需要GC时间够短，GC次数够少
2. 调休的原则
    - 大多数java应用不需要GC调优，需要调优的不是JVM参数的问题，而是代码引起的
    - GC调优之前应该先做一下这些操作
        - 选择合适的GC回收器
        - 选择合适的堆大小
        - 选择年轻代在堆中的比重
3. 调优的步骤
    - 监控GC的状态
    - 分析结果，判断是否需要调优，以下是阈值
        - minorGC时间<50MS，10s一次
        - FullGC执行时间<1S，频率10分钟以上
4. 阅读GC日志
    ```
    //YoungGC  [GC前年轻代80.08M->GC后0.37M(年轻代总大小90M)]
    //GC前堆179.98M->GC后堆100.3M(堆总大小190M) 堆=年轻代+老年代
    [GC (Allocation Failure) [PSYoungGen: 82004K->384K(92160K)] 184303K->102715K(194560K), 0.0035647 secs] [Times: user=0.00 sys=0.00, real=0.00 secs]
    ```

### JIT

1. 定义，Java程序最初是通过解释器（Interpreter）进行解释执行的，当虚拟机发现某个方法或代码块的运行特别频繁时，就会把这些代码认定为“热点代码”。为了提高热点代码的执行效率，在运行时，虚拟机将会把这些代码编译成与本地平台相关的机器码，并进行各种层次的优化，完成这个任务的编译器称为即时编译器（Just In Time Compiler，下文统称JIT编译器）
2. 参数配置
    - 使用java -version能够打印出虚拟机默认的配置
    - -Xint，禁用JIT
    - -XComp，所有字节码编译成本地代码再执行
    - -Xmixed，有选择的进行JIT编译（一般默认）
3. 即时编译后存放的空间
    - 配置-XX:ReservedCodeCacheSize=N
    - jdk8默认240m
    - 满了会jvm会抛出CodeCache is full
4. 热点代码编译的编译阈值，由下面两个相加
    - 方法调用计数器
    - 循环回边计数器，循环完成的次数


### 浅堆和深堆

1. 浅堆，是指一个对象所消耗的内存，不包括所引用的对象
2. 深堆，是指对象的保留集中所有的对象的浅堆大小之和（不包括被其他对象引用的对象）
3. 示例，对象A引用了C和D，对象B引用了C和E。那么对象A的浅堆大小只是A本身，不含C和D，而A的实际大小为A、C、D三者之和。而A的深堆大小为A与D之和，由于对象C还可以通过对象B访问到，因此不在对象A的深堆范围内

### 实战

1. 项目情况，一个日处理上亿数据的计算系统，这个系统会不停的从mysql数据库和其他数据源中提取大量的数据，加载到JVM中进行计算处理
2. 机器配置处理速度，每台机器每分钟执行100次数据提取和计算的任务，每次会提取大概1w条左右的数据到内存中计算，平均每次计算大概需要耗费10秒左右，机器4核8G，JVM分配4G，新生代1.5G，老年代1.5G，Eden1.2G，Survivor100M
3. 每条数据大约20个字段，可以认为每条数据1KB，1w数据大约10M，也就是每次就是10M，大约1分钟Eden就会占满
4. 一分钟大约会计算80次的数据提取，还有20次没有计算，也就是每分钟大约会有200M的对象不能被回收
5. 流程
    - 第一分钟到Eden满，老年代1.5G大于新生代的1.2G，新生代直接回收，有200M不能回收，又因为Survivor只有100M，启用新生代的空间担保机制，直接将这200M分配到老年代
    - 第二分钟到Eden满，老年代1.2G小于等于新生代的1.2G，默认开启-XX:-HandlePromotionFailure，上次平均进入老年代200M，说老年代空间时足够的，又转移200M到老年代
    - 直到第八分钟Eden满，因为老年代空间不足仅有100M，所以会进行Full GC，也就是说大约七八分钟就要进行一次Full GC，太频繁了
6. 问题，是因为每次进行Minor GC都会直接进入老年代
7. 解决方案，增加年轻代的大小，但是需要注意动态年龄判断的问题，如果内存空间不够，可以考虑修改Eden和Survivor之间的比例

> 1分钟100个任务计算，每个任务计算是10秒，在60秒到来那一瞬间，在50-60秒之间开始的任务是没有执行完毕的，也就是(1/6)*100=17，大约也就20个任务没有计算完毕
