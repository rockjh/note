### 了解GC和内存分配策略的目的

1. GC对应用性能是有影响的
2. 写代码有好处

### 判断对象的存活

1. 引用计数器
    - 定义，被引用一次就减一，引用失效一次就减一，为零可被回收
    - 优点，实时性，程序无需挂起，区域性（更新对象计数器，不会影响其他对象）
    - 缺点，不能解决循环引用，浪费cpu（内存足够也会计数）
2. 可达性分析
    - 定义，通过GC ROOTS的对象作为起始点，从这些节点向下搜索，搜索走过的路径称为搜索链，一个对象到GC ROOTS没有任何搜索链时，该对象可以被回收
    - 缺点，垃圾回收时，应用会挂起一定时间
    - 优点，解决了循环引用问题
    - GC ROOTS对象包括下面几种
        - 虚拟机栈（栈帧中的本地变量表）中引用的对象
        - 方法区中类静态属性引用的对象
        - 方法区中常量引用的对象
        - 本地方法栈中JNI（一般指native方法）引用的对象

### 引用类型

1. 强引用，Object obj = new Object();属于强引用
2. 软引用，系统将要发生OOM时，会将对象回收
    ```
    //引用队列，如果被回收，就会被放进引用队列
    ReferenceQueue<String> referenceQueue=new ReferenceQueue<>();
    //软引用
    SoftReference<String> softReference=new SoftReference<>(new String("123"),referenceQueue);
    ```
3. 弱引用，下一次垃圾回收时，一定会被回收
    ```
    //一个线程只有一个ThreadLocal.ThreadLocalMap，在ThreadLocalMap中可以存在多个{key,value}->{ThreadLocal的弱引用,value}
    //如果某个ThreadLocal作用域过期，强引用失效，根据Entry继承弱引用的特性，他存储的是ThreadLocal的弱引用
    //当该ThreadLocal的强引用失效，那么gc可以对这个键进行回收
    static class Entry extends WeakReference<ThreadLocal<?>> {
        /** The value associated with this ThreadLocal. */
        Object value;

        Entry(ThreadLocal<?> k, Object v) {
            //将ThreadLocal作为弱引用使用key
            super(k);
            value = v;
        }
    }
    //ThreadLocal为什么会在程序运行时失效？ThreadLocal定义为某个局部变量，范围过了自然失效
    //ThreadLocal无效了，对应的value什么时候清除？在set或者get的时候会调用清除
    ```
4. 虚引用（幻想引用）,虚引用必须和引用队列ReferenceQueue联合使用，当垃圾回收器准备回收一个对象时，如果发现他还有虚引用，就会再回收对象的内存之前，把这个虚引用加入到与之关联的引用队列中，程序可以通过判断引用队列中是否已经加入了虚引用，来了解被引用的对象是否将要被垃圾回收，如果程序发现某个虚引用已经被加入到引用队列，那么就可以在所引用的对象的内存被回收之前才去一些程序行动
    ```
    //幻想引用
    //创建一个对象
    String str = new String("JAVA讲义");
    //创建一个引用队列
    ReferenceQueue<String> rq = new ReferenceQueue<>();
    //创建一个虚引用，指定引用对象.不能单独使用必须关联引用队列
    PhantomReference pr = new PhantomReference(str, rq);
    System.out.println(pr.get());
    //切断强引用
    str = null;
    //试图取得虚引用对象,null
    System.out.println(pr.get());
    //垃圾回收
    System.gc();
    System.runFinalization();
    //取出引队列中的最先进入队列的引用与pr进行比较true
    //这里可以判断如果队列中有值，那么某一个虚引用被gc回收，我们可以做特定的操作
    System.out.println(rq.poll() == pr);
    ```

> 引用之间的状态是可以切换的，软引用，弱引用都可以通过get来获取值，然后再赋值给强引用，所以前面的引用可达性图他们是可以转换的

### 垃圾收集算法

1. 标记清除算法，算法分为标记和清除两个阶段，先标记出所有需要回收的对象，在标记完成后统一回收所有被标记的对象
    - 缺点，产生大量不连续的内存碎片，当需要分配较大对象时，无法找到足够的连续内存，而不得不触发一次GC
2. 复制算法，将可用内存按容量划分大小相等的AB两块，每次只使用其中一块，当A用完了，就将A还存活的对象复制到B上面，然后把A全部清理掉
    - 缺点，将原来的内存缩小为了原来的一半，会进行一次移动
    - 优点，简答高效，不用考虑内存碎片等复杂情况
3. 标记整理算法，首先标记出所有需要回收的对象，在标记完成后，让所有存活的对象都向一端移动，然后直接清理掉端边界以外的内存

### 分代收集算法

1. jvm都采用分代收集算法，根据对象的存活周期不同，将内存划分为几块，一般分为年轻代、老年代，就可以根据各个年代的特点采用最适合的收集算法
2. 因为年轻代的对象98%是朝生夕死，所以不需要按照1：1的比例，所以将内存划分为Eden:Survivor:Survivor=8:1:1的比例，每次使用Eden和其中一块Survivor[1]，当回收时，将Eden和Survivor[1]中还存活的对象复制到Survivor[2]中，我们没有办法保证每次回收都只有不多于10%的对象存活，当Survivor空间不够用时，需要依赖其他内存（这里指老年代）进行分配担保（Handle Promotion），新生代使用复制算法
3. 老年代因为对象存活率高，没有额外空间对他进行分配担保，必须使用标记清除或者标记整理算法

### 垃圾收集器定义

1. Serial
    - 新生代，复制算法，单线程
    - 进行垃圾收集时必须暂停所有工作线程，直到完成（stop the world）
    - 简单高效，适合内存不大的情况
2. ParNew
    - 新生代，复制算法，并行多线程收集器
    - ParNew垃圾收集器是Serial收集器的多线程版本
    - 搭配CMS垃圾回收器的首选
    - 并行，垃圾收集的多线程的同时进行
3. Parallel Scavenge（ParallerGC）吞吐量优先收集器
    - 新生代，复制算法，并行多线程收集器
    - 类似ParNew，更加关注吞吐量，达到一个可控制的吞吐量
    - 本身是Server级别多CPU机器上的默认GC方式，主要适合后台运算不需要太多交互的任务
    - 吞吐量，运行用户代码时间/(运行用户代码时间+垃圾收集总时间)
4. Serial Old
    - 老年代，标记整理算法，单线程
    - jdk7/8默认的老年代垃圾收集器
    - client模式下虚拟机使用
5. Parallel Old
    - 老年代，标记整理算法，并行的多线程收集器
    - Parallel Scacenge的老年代版本，为配合Parallel Scacenge的面向吞吐量的特性而开发的对应组合
    - 在注重吞吐量以及CPU资源敏感的场合采用
6. Concurrent Mark Sweep（CMS）
    - 老年代，标记清除算法，并行与并发收集器，尽可能缩短垃圾收集时用户线程停止的时间，缺点在于
        - 内存碎片
        - 需要更多的CPU资源
        - 浮动垃圾的问题，需要更大的堆空间
    - 重视服务的响应速度，系统停顿时间和用户体验的互联网网站或者B/S系统，互联网后端目前CMS是主流的垃圾回收器
    - 并行，垃圾收集的多线程和应用的多线程同时进行
    - 浮动垃圾，CMS并发清理阶段用户线程还在运行着，会产生新的垃圾，CMS无法在当次清理他们，只能留待下一次GC，他们称为浮动垃圾
7. G1
    - 跨新生代和老年代，标记整理+化整为零，并行与并发收集器
    - jdk1.7才正式引入，采用分区回收思维，基本不牺牲吞吐量的前提下完成低停顿的内存回收，可以预测停顿是其最大的优势
    - 面向服务端应用的垃圾回收器，目标为取代CMS

### 垃圾收集器特性

1. Serial/Serial Old，最古老的，单线程，独占式，成熟，适合单CPU服务器
2. ParNew，和Serial没啥区别，多了一个多线程、多CPU、停顿时间比Serial少
3. ParallerGC，关注吞吐量的垃圾收集器
    - -XX:MaxGCPauseMills，参数允许的值是一个大于0的毫秒数，收集器将尽可能地保证内存回收花费的时间不超过设定值，如果值太小，系统会把新生代调小来达到目的，这样吞吐量就下去了
    - -XX:GCTimeRatio，参数的值应当是一个大于0且小于100的整数，也就是垃圾收集时间占总时间的比率，相当于是吞吐量的倒数
    - -XX:+UseAdaptiveSizePolicy，虚拟机会根据当前系统的运行情况收集性能监控信息，动态调整这些参数以提供最合适的停顿时间或者最大的吞吐量，这种调节方式称为GC自适应的调节策略
4. CMS，一种以获取最短回收停顿时间为目标的收集器
    - 运行过程
        - 初始标记-短暂，仅仅只是标记一下GC Roots能直接关联到的对象，速度很快，会stop the world
        - 并发标记，和用户的应用程序同时进行，进行GC Roots Tracing的过程
        - 重新标记，处理并发标记过程中用户程序运行过程中导致标记产生变动，时间长于初始标记，远远低于并发标记，会stop the world
        - 并发清除，由于整个过程中耗时最长的并发标记和并发清除过程收集器线程都可以与用户线程一起工作
    - -XX:CMSInitialOccupyFraction（jdk1.6默认值92%），预留一部分空间提供并发收集时的程序运作使，要是CMS运行期间预留的内存无法满足程序需要，就会出现一次Concurrent Mode Failure失败，临时启用Serial Old收集器来重新进行老年代的垃圾收集
    - -XX:+UseCMSCompactAtFullCollection（默认开启），用于在CMS收集器顶不住要进行FullGC时开启内存碎片的合并整理过程，内存整理的过程是无法并发的，空间碎片问题没有了，但停顿时间不得不变长
    - -XX：CMSFullGCsBeforeCompaction（默认0），设置执行多少次不压缩的Full GC后，跟着来一次带压缩的（默认值为0，表示每次进入FullGC时都进行碎片整理）
5. G1，新版jdk9的默认实现，暂时不做了解

### 切换垃圾收集器

- -XX:+UseSerialGC，新生代和老年代都用串行收集器
- -XX:+UseParNewGC，新生代使用ParNew，老年代使用Serial Old
- -XX:+UseParallelGC，新生代使用ParallerGC，老年代使用Serial Old
- -XX:+UseConcMarkSweepGC，表示新生代使用ParNew，老年代的用CMS
- -XX:+UseG1GC，表示新生代，老年代使用G1

### 内存分配与回收策略

1. 对象优先在Eden分配，如果说Eden内存空间不足，就会发生Minor GC
2. 大对象直接进入老年代，需要大量连续内存空间的Java对象，比如很长的字符串和大型数组
    - 导致内存有空间，还是需要提前进行垃圾回收获取连续空间来放他们
    - 会进行大量的内存复制
    - XX:PretenureSizeThreshold 参数 ，大于这个数量直接在老年代分配，缺省为0 ，表示绝不会直接分配在老年代
3. 长期存活的对象将进入老年代，默认15岁，-XX:MaxTenuringThreshold调整
4. 动态对象年龄判定，如果在Survivor空间中相同年龄所有对象大小的总和大于Survivor空间的一半，年龄大于或等于该年龄的对象就可以直接进入老年代，无须等到MaxTenuringThreshold中要求的年龄
5. 空间分配担保，新生代中有大量的对象存活，survivor空间不够，当出现大量对象在MinorGC后仍然存活的情况，就需要老年代进行分配担保，把Survivor无法容纳的对象直接进入老年代

#### GC类别

##### MinorGC
1. 实现过程，采用复制算法
    - 首先，把Eden和ServivorFrom区域中存活的对象复制到ServicorTo区域（如果有对象的年龄以及达到了老年的标准，一般是15，则赋值到老年代区）
    - 同时把这些对象的年龄+1（如果ServicorTo不够位置了就放到老年区）
    - 然后，清空Eden和ServicorFrom中的对象；最后，ServicorTo和ServicorFrom互换，原ServicorTo成为下一次GC时的ServicorFrom区

2. 触发机制

    - 当年轻代满时就会触发Minor GC，这里的年轻代满指的是Eden代满，Survivor满不会引发GC。通过复制算法 ,回收垃圾。复制算法不会产生内存碎片

##### MajorGC
1. 实现过程，采用标记—清除算法
    - 首先扫描一次所有老年代，标记出存活的对象
    - 然后回收没有标记的对象
    - MajorGC的耗时比较长，因为要扫描再回收。MajorGC会产生内存碎片，为了减少内存损耗，我们一般需要进行合并或者标记出来方便下次直接分配，当老年代也满了装不下的时候，就会抛出OOM（Out of Memory）异常
2. 触发机制
    - Major GC清理老年代(old GC)

##### FullGC
1. 定义，是对新生代、老年代、永久代【jdk1.8后没有这个概念了】统一的回收
2. 触发机制
    - 调用System.gc时，系统建议执行Full GC，但是不必然执行
    - 老年代空间不足
    - 方法区空间不足
    - 通过Minor GC后进入老年代的平均大小大于老年代的可用内存
    - 由Eden区、survivor space1（From Space）区向survivor space2（To Space）区复制时，对象大小大于To Space可用内存，则把该对象转存到老年代，且老年代的可用内存小于该对象大小

### 新生代的配置

1. 新生代大小配置参数的优先级
    - 高，-XX:NewSize/MaxNewSize
    - 中间，-Xmn，（NewSize= MaxNewSize）
    - 低，-XX:NewRatio ，表示比例，例如=2，表示 新生代：老年代 = 1:2
2. -XX:SurvivorRatio，表示Eden和Survivor的比值，缺省为8表示 Eden:FromSurvivor:ToSurvivor=8:1:1
3. 同样的代码情况下
    - -Xms20M -Xmx20M -XX:+PrintGCDetails –Xmn2m -XX:SurvivorRatio=2没有垃圾回收数组都在老年代
    - -Xms20M -Xmx20M -XX:+PrintGCDetails -Xmn7m -XX:SurvivorRatio=2发生了垃圾回收新生代存了部分数组，老年代也保存了部分数组，发生了晋升现象
    - -Xms20M -Xmx20M -XX:+PrintGCDetails -Xmn15m -XX:SurvivorRatio=8新生代可以放下所有的数组老年代没放
    - -Xms20M -Xmx20M -XX:+PrintGCDetails -XX:NewRatio=2发生了垃圾回收出现了空间分配担保，而且发生了FullGC

### GC调优

1. 目的，GC时间够小，GC次数够少
2. 调休的原则
    - 大多数java应用不需要GC调优，需要调优的不是JVM参数的问题，而是代码引起的
    - GC调优之前应该先做一下这些操作
        - 选择合适的GC回收器
        - 选择合适的堆大小
        - 选择年轻代在堆中的比重
3. 调优的步骤
    - 监控GC的状态
    - 分析结果，判断是否需要调优，以下是阈值
        - minorGC时间<50MS，10s一次
        - FullGC执行时间<1S，频率10分钟以上
4. 阅读GC日志
    ```
    //YoungGC  [GC前年轻代80.08M->GC后0.37M(年轻代总大小90M)]
    //GC前堆179.98M->GC后堆100.3M(堆总大小190M) 堆=年轻代+老年代
    [GC (Allocation Failure) [PSYoungGen: 82004K->384K(92160K)] 184303K->102715K(194560K), 0.0035647 secs] [Times: user=0.00 sys=0.00, real=0.00 secs]
    ```

### JIT

1. 定义，Java程序最初是通过解释器（Interpreter）进行解释执行的，当虚拟机发现某个方法或代码块的运行特别频繁时，就会把这些代码认定为“热点代码”。为了提高热点代码的执行效率，在运行时，虚拟机将会把这些代码编译成与本地平台相关的机器码，并进行各种层次的优化，完成这个任务的编译器称为即时编译器（Just In Time Compiler，下文统称JIT编译器）
2. 参数配置
    - 使用java -version能够打印出虚拟机默认的配置
    - -Xint，禁用JIT
    - -XComp，所有字节码编译成本地代码再执行
    - -Xmixed，有选择的进行JIT编译（一般默认）
3. 即时编译后存放的空间
    - 配置-XX:ReservedCodeCacheSize=N
    - jdk8默认240m
    - 满了会jvm会抛出CodeCache is full
4. 热点代码编译的编译阈值，由下面两个相加
    - 方法调用计数器
    - 循环回边计数器，循环完成的次数


### 浅堆和深堆

1. 浅堆，是指一个对象所消耗的内存，不包括所引用的对象
2. 深堆，是指对象的保留集中所有的对象的浅堆大小之和（不包括被其他对象引用的对象）
3. 示例，对象A引用了C和D，对象B引用了C和E。那么对象A的浅堆大小只是A本身，不含C和D，而A的实际大小为A、C、D三者之和。而A的深堆大小为A与D之和，由于对象C还可以通过对象B访问到，因此不在对象A的深堆范围内