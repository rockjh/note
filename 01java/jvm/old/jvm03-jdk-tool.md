### jps

1. 定义，列出当前机器上正在运行的虚拟机进程
2. 参数解释
    - -p，仅仅显示VM标示，不显示jar、class、main参数等信息
    - -m，输出主函数传入的参数下，就是在执行程序时从命令行输入的参数
    - -l，输出应用程序主类完整package名称或jar完整名称
    - -v，列出jvm参数，-Xms20m -Xmx50m是启动程序指定的jvm参数

### jstat

1. 定义，是用于监视虚拟机各种运行状态信息的命令行工具。它可以显示本地或者远程虚拟机进程中的类装载、内存、垃圾收集、JIT编译等运行数据
2. 参数解释
    - -class，类加载器
    - -compiler，JIT
    - -gc，GC堆状态
    - -gccapacity，各区大小
    - -gccause，最近一次GC统计和原因
    - -gcnew，新区统计
    - -gcnewcapacity，新区大小
    - -gcold，老区统计
    - -gcoldcapacity，老区大小
    - -gcpermcapacity，永久区大小
    - -gcutil，GC统计汇总
    - -printcompilation，HotSpot编译统计
3. 示例，需要每250毫秒查询一次进程2764垃圾收集状况，一共查询20次命令如下
    ```
    jstat-gc 2764 250 20
    ```

### jinfo

1. 定义，查看和修改虚拟机的参数
2. 参数
    - –sysprops，可以查看由System.getProperties()取得的参数
    - –flag，未被显式指定的参数的系统默认值
    - –flags，（注意s）显示虚拟机的参数
    - –flag +[参数]，可以增加参数，但是仅限于由java -XX:+PrintFlagsFinal –version查询出来且为manageable的参数
    - –flag -[参数] 可以去除参数


### jmap

1. 定义，Java内存映像工具
2. 示例，jmap -dump:live,format=b,file=heap.bin 19116，生成某个进程的内存映像文件
3. 缺点，JVM会将整个heap的信息dump写入到一个文件，heap如果比较大的话，就会导致这个过程比较耗时，并且执行的过程中为了保证dump的信息是可靠的，所以会暂停应用

### jhat

1. 定义，来分析jmap生成的堆转储快照
2. 示例，jhat /Users/rockjh/heap.bin，会出现一个ready字样，然后就可以访问出现的端口查看内存映像
3. 缺点，因为这是命令行工具，不够具体化，一般不推荐在服务器中使用，可以拿到内存映像文件后，拿到可视化工具中查看

### jstack

1. 定义，用于生成虚拟机当前时刻的线程快照。线程快照就是当前虚拟机内每一条线程正在执行的方法堆栈的集合
2. 示例，jstack 19116，生成线程快照的主要目的是定位线程出现长时间停顿的原因，如线程间死锁、死循环、请求外部资源导致的长时间等待等都是导致线程长时间停顿的常见原因
3. 在代码中可以使用Thread--->getAllStackTraces方法用于获取虚拟机中所有线程的StackTraceElement对象

### visualvm，JConsole

1. JConsole，Java监视与管理控制台，图形化界面，大致内容是前面命令行的总和
2. Visualvm,多合一故障处理工具，需要根据不同的jdk版本下载插件
3. 管理远程进程需要在远程程序的启动参数中增加
    ```
    -Djava.rmi.server.hostname=…..
    -Dcom.sun.management.jmxremote
    -Dcom.sun.management.jmxremote.port=8888
    -Dcom.sun.management.jmxremote.authenticate=false
    -Dcom.sun.management.jmxremote.ssl=false
    ```
