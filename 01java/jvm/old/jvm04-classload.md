### 类加载的来源

1. 压缩文件（jar、war
2. 其他文件中（jsp）
3. 网络中（以前的applet）
4. 运行时按照特定规则生成的二进制流（动态代理）
5. 数据库中读取

### 触发类加载的场景

1. new、使用设置执行static变量或方法时
2. 使用java.lang.reflect包中的方法对类进行反射调用时
3. 当初始化一个类时，如果父类还没有进行初始化，会先初始化父类
4. 虚拟机启动，用户需要指定一个主类（main），虚拟机会先初始化主类
5. 使用jdk7的动态语言支持时
> 如果是final的static变量在编译期间就已经被替换了，不会发生类加载通过子类访问父类的static变量，只会初始化父类

### 类初始化顺序

1. 父类--静态变量、父类--静态初始化块
2. 子类--静态变量、子类--静态初始化块
3. 父类--变量、父类--初始化块、父类--构造器（没有显示调用，就调用默认无参构造）
4. 子类--变量、子类--初始化块，子类--构造器
> 1、2是类加载才会发生的

### 类加载步骤

1. 加载，可以在这一步自定义类加载器，进行解密等操作
    - 通过类的全限定名来获取定义类的二进制流
    - 在内存中生成一个代表这个类的java.lang.Class对象，作为方法区这个类的各种数据的访问入口
2. 验证，确保class文件的字节流包含的信息符号当前虚拟机的要求，会完成
    - 文件格式验证
    - 元数据验证
    - 字节码验证
    - 符号引用验证，会抛出NoSuchMethodError、NoSuchFieldError等错误
3. 准备
    - 为（static）类变量分配内存并设置类变量初始值（类型默认值，例如int=0）
    - 对于final static变量，编译时Javac将会为value生成ConstantValue属性，在准备阶段虚拟机就会根据ConstantValue的设置将value赋值为123
        ```
        public static final int value=123
        ```
4. 解析，虚拟机将常量池内的符号引用替换为直接引用
    - 符号引用，是类中的各种引用跳转
    - 直接引用，是直接指向内存中的具体指针，cpu可直接使用
5. 类初始化，执行类构造器＜clinit＞方法的过程，类构造器方法由下面两种合成
    - 编译器自动收集类中的所有static类变量的赋值动作
    - 静态语句块（static{}块）中的语句
    - 如果一个类中没有静态语句块，也没有对类变量的赋值操作，那么就不会生成类构造器方法
    - 虚拟机会保证一个类的＜clinit＞方法在多线程环境中被正确地加锁、同步，如果多个线程同时去初始化一个类，单例的静态内部类实现方式
6. 使用
7. 卸载

### 系统的类加载器

1. Bootstrap ClassLoader，启动类加载器，由c++实现，是虚拟机自身的一部分
    - 负责将存放在＜JAVA_HOME＞\lib目录中的或者被-Xbootclasspath参数所指定的路径中的，并且是虚拟机识别的（仅按照文件名识别，如rt.jar，名字不符合的类库即使放在lib目录中也不会被加载）类库加载到虚拟机内存中
    - 启动类加载器无法被Java程序直接引用，用户在编写自定义类加载器时，如果需要把加载请求委派给引导类加载器，那直接使用null
2. Extension ClassLoader，扩展类加载器，java.lang.ClassLoader的子类
    - 由sun.misc.Launcher$ExtClassLoader实现
    - 它负责加载＜JAVA_HOME＞\lib\ext目录中的，或者被java.ext.dirs系统变量所指定的路径中的所有类库，开发者可以直接使用扩展类加载器
3. Application ClassLoader，应用程序类加载器，java.lang.ClassLoader的子类
    - 由sun.misc.Launcher$AppClassLoader实现
    - 负责加载用户类路径（ClassPath）上所指定的类库，开发者可以直接使用这个类加载器，如果应用程序中没有自定义过自己的类加载器，一般情况下这个就是程序中默认的类加载器

### 双亲委派模型

1. 双亲委派模型，除了顶层的启动类加载器外，其余的类加载器都应当有自己的父类加载器
    - 定义，某个特定的类加载器在接到加载类的请求 时，首先将加载任务委托给父类加载器， 依次递归，如果父类加载器可以完成类加载任务，就成功返回，只有父类加载器无法完成此加载任务时，才自己去加载
    - 父类加载器和类加载器是采用组合的形式，就是有一个parent属性
    - Java类随着它的类加载器一起具备了一种带有优先级的层次关系。例如类java.lang.Object，它存放在rt.jar之中，无论哪一个类加载器要加载这个类
    - 比较两个类是否相同是类全名+类加载器
    - 自定义类加载器都是java.lang.ClassLoader的子类，有两个达到目的方式
        - 重写findClass，建议采用，就是获取class的字节数组
        - 重写loadClass，不建议采用，因为里面实现了双亲委派模型，重写的话会破坏这种模型
2. 破坏双亲委派模型
    - 自定义Classload，继承ClassLoader，重写loadClass方法破坏实现好的双亲委派模型
    - 存在一种情况，Bootstrap Classload通过DriverManager管理了jdbc接口，但是实现是各个厂商的jar包，Bootstrap Classload无法加载到这些具体的实现类，只能通过Thread类的contextClassLoader来加载具体的实现类
        - contextClassLoader可以通过set方法设置，默认父线程的contextClassLoader，最开始默认的Application ClassLoader
    - 由于用户对程序动态性的追求导致的，代码热替换、模块热部署等，例如OSGI

### 方法调用解析

1. 静态分派，在编译期间就已经确定了，简而言之就是表面可以看到的，例如在idea中ctrl点击，就可以确定
    ```
    public static void main(String[] args){
        Human man = new Man();
        Human woman = new Woman();
        //human
        sayHello(man);
        //human
        sayHello(woman);
    }

    public static void sayHello(Human human){
        System.out.println("human");
    }
    public static void sayHello(Man man){
        System.out.println("man");
    }
    public static void sayHello(Woman woman){
        System.out.println("woman");
    }

    static abstract class Human{}
    static class Man extends Human{}
    static class Woman extends Human{}
    ```
2. 动态分派，也叫做多态，每个类都有一个虚方法表（虚拟机维护、维护着各个方法的实际入口--->方法区的某个地址）