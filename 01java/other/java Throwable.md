### Throwable分类

只有继承Throwalble的类才可以被throws,catch，它是所有异常，错误的父类，常见的错误，异常类图如下

![Throwable及其子类](http://img1.ph.126.net/R46sOpdDluIYeRQ6uhdf_g==/6597278673868292128.png)

#### Error

Error是指在正常情况下，不大可能出现的情况，绝大部分的Error都会导致程序（比如jvm自身）处于非正常的，不可恢复的状态，既然是非正常情况，所以不便于也不需要捕获，常见Error如下

- OutOfMemoryError内存溢出，可能是程序内存泄漏导致，可能是jvm内存分配相对于程序来说太小等导致的错误
- NoClassDefFoundError编译期间能够找到改类，运行时找不到会报此类错误，或者类的静态初始化模块错误也会导致

#### Exception

异常分为运行时异常（继承RunTimeException）和可检查异常。运行时异常根据情况捕捉，可检查异常强制捕捉或者往外层抛出

关于自定义异常，一般来说如果异常导致了程序不可恢复那么可以定义为运行时异常比如IllegalArgumentException，如果程序可以恢复那么定义为可检查异常，捕捉后可尝试恢复程序如ConnectException，一般来说异常信息中不可包含具体的业务数据，不可将异常等信息展示到用户界面