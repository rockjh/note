### Hash碰撞的拒绝式服务攻击

 通过数以万计不等但hash相等的key，可以使得HashMap的算法由O(1)降为0(n)，也就是由哈希表降为链表，例如字符串rQ和qp的hash相等，那么如果我想弄1024个值不同，hash相等的key，就可以分别是：
 ```
 rQrQrQrQrQrQrQrQrQrQ
 qpqpqpqpqpqpqpqpqpqp
 rQrQrQrQrQrQrQrQrQqp
 rQrQrQrQrQrQrQrQqpqp
 ....
 ```
 java8在一定程度上解决了，当链式容量大于MIN_TREEIFY_CAPACITY时，会改造为红黑树
 
 ### jdk5,6版本中NIO的epoll空转bug
 
 selector.select()在没有key就绪的时候返回不等于0，也就是selector.selectedKeys().size==0是，selector.select()不阻塞，会一直循环导致cpu空转。
 
导致这样的原因主要是在各个操作系统平台中，poll和epoll对于突然中断的连接socket会对返回的eventSet事件集合置为POLLHUP，也可能是POLLERR，eventSet事件集合发生了变化，这就可能导致Selector会被唤醒

当时netty，jetty等框架临时解决方案是判断空转n次之后，就断定进入的bug模式，然后将Selector重新new一遍