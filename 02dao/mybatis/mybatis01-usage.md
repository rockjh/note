### 基础配置

springboot中路径配置
```yml
mybatis:
    mapper-locations: classpath:mapping/*.xml
    type-aliases-package: com.rockjh.study.bean
    #如果dao层加了@Mapper注解就可以不用下面的配置
    @MapperScan(basePackages = "com.rockjh.study.mapper")
```

### 基本标签

1. select，查询sql标签
2. update，更新sql标签
3. insert，插入sql标签
4. delete，删除sql标签
5. resultMap，自定义返回值映射
6. sql，sql语句段，可以在其他中引用，sql标签可以包含参数
    ```xml
    <sql id="userColumns">
        ${alias}.id,${alias}.username,${alias}.password 
    </sql>
    <select id="selectUsers" resultType="map">
        select
            <include refid="userColumns"><property name="alias" value="t1"/></include>,
            <include refid="userColumns"><property name="alias" value="t2"/></include>
        from some_table t1 cross join some_table t2
    </select>
    ```
7. include，引用sql标签，实例如上
8. insert获取主键的方式，自增主键和其他主键方式不同
    - 自增主键，可以 useGeneratedKeys = true配合keyProperty使用，这两个联合使用是下面这段代码的一个简写
        ```xml
        <selectkey keyproperty="id" order="after" resultType="int">
            select LAST_INSERT_ID()
        </selectkey>
        ```
    - 其他方式的获取主键，比如oracle不支持自增主键，获取主键的方式
        ```xml
        //order的意思是，获取id是在id生成之前，还是id生成之后获取
        <selectkey keyproperty="id" order="before" resultType="int">
            select SEQ_ID.nextval from dual
        </selectkey>
        ```

### 动态sql

1. if，根据判断来决定需不需要某条语句
    ```xml
    <insert id="insert2" useGeneratedKeys="true" keyProperty="id">
        INSERT INTO sys_user
          (id,user_name,user_password,
          <if test="userEmail!=null and uerEmail!=''">
              user_email,
          </if>
          user_info,head_img,create_time)
        VALUES
          (#{id},#{userName},#{userPassword},
          <if test="userEmail!=null and uerEmail!=''">
              #{userEmail},
          </if>
          #{userInfo},#{headImg,jdbcType=BLOB},#{createTime,jdbcType=TIMESTAMP})
    </insert>
    ```
2. choose (嵌套when，otherwise)，类似于java中的switch，按照顺序执行，只能进入一个
    ```xml
    <select id="dynamicChooseTest" parameterType="Blog" resultType="Blog">
        select * from t_blog where 1 = 1 
        <choose>
            <when test="title != null">
                and title = #{title}
            </when>
            <when test="content != null">
                and content = #{content}
            </when>
            <otherwise>
                and owner = "owner1"
            </otherwise>
        </choose>
    </select>
    ```
3. trim，用于拼接sql，比如if条件中多逗号，and等
    - prefix，给sql语句拼接的前缀
    - suffix，给sql语句拼接的后缀
    - prefixOverrides，去除sql语句前面的关键字或者字符
    - suffixOverrides，去除sql语句后面的关键字或者字符
    ```xml
    <select id="findActiveBlogLike" resultType="Blog">
    SELECT * FROM BLOG 
        <trim prefix="WHERE" prefixOverrides="AND">
            <if test="state != null">
                state = #{state}
            </if> 
            <if test="title != null">
                AND title like #{title}
            </if>
            <if test="author != null and author.name != null">
                AND author_name like #{author.name}
            </if>
        </trim>
    </select>
    ```
4. where，只会在有一个元素的时候才会加上where，也会自动去除多余的AND或OR
    ```xml
    <select id="findActiveBlogLike" resultType="Blog">
        SELECT * FROM BLOG 
        <where> 
            <if test="state != null">
                state = #{state}
            </if> 
            <if test="title != null">
                AND title like #{title}
            </if>
            <if test="author != null and author.name != null">
                AND author_name like #{author.name}
            </if>
        </where>
    </select>
    ```
5. set，用于更新操作，主要是在包含的语句前输出一个 set，然后如果包含的语句是以逗号结束的话将会把该逗号忽略
    ```xml
    <update id="dynamicSetTest" parameterType="Blog"> 
      	update t_blog
        <set>
            <if test="title != null">
                title = #{title},
            </if>
            <if test="content != null">
                content = #{content},
            </if>
            <if test="owner != null">
                owner = #{owner}
            </if>
        </set>
        where id = #{id}
    </update>
    ```
6. foreach，循环输出，参数列表示例代码如下
    - item，集合中元素迭代时的别名
    - index，在list和数组中,index是元素的序号，在map中，index是元素的key
    - open，整个循环内容的开头字符串
    - close，整个循环内容的结尾字符串
    - separator，每次循环的分隔符
    - collection，要做foreach的对象
        - 如果传入的是单参数且参数类型是一个List的时候，collection属性值为list
        - 如果传入的是单参数且参数类型是一个array数组的时候，collection的属性值为array
        - 如果传入的参数是多个的时候，我们就需要把它们封装成一个Map了

    
    ```xml
    //常规，传入的是list
    <select id="selectByIdList" resultType="tk.mybatis.simple.model.SysUser">
        select id,
          user_name userName,
          user_password userPassword,
        from sys_user
        where id in
        <foreach collection="list" open="(" close=")" separator=","
                 item="id" index="i">
            #{id}
        </foreach>
    </select>
    ```
    
    ```xml
    //批量插入
    <insert id="insertList">
        insert into sys_user(
        user_name,user_password,user_email,
        user_info,head_img,create_time)
        values
        <foreach collection="list" item="user" separator=",">
            (
            #{user.userName},#{user.userPassword},#{user.userEmail},
            #{user.userInfo},#{user.headImg,jdbcType=BLOB},
            #{user.createTime,jdbcType=TIMESTAMP}
            )
        </foreach>
    </insert>
    ```

    ```xml
    //当参数是Map类型的时候，foreach标签的index属性值对应的不是索引值，而是Map中的key
    <update id="updateByMap">
        update sys_user
        set
        <foreach collection="_parameter" item="val" index="key" separator=",">
            ${key}=#{val}
        </foreach>
        where id=#{id}
    </update>
    ```
7. bind，创建一个bind元素标签的变量后，就可以在下面直接使用，使用bind拼接字符串不仅可以避免因更换数据库而修改SQL，也能预防SQL 注入
    ```xml
    <!--使用bind元素进行模糊查询-->
    <select id="selectUserByBind" resultType="com.po.MyUser" parameterType= "com.po.MyUser">
        <!-- bind 中的 uname 是 com.po.MyUser 的属性名-->
        <bind name="paran_uname" value="'%' + uname + '%'"/>
        select * from user where uname like #{paran_uname}
    </select>
    ```

> 执行原理，根据表达式的值 完成逻辑判断并动态拼接sql的功能

### 映射

1. 参数映射

    - 多个参数的情况下xml中可以写#{0},#{1}，
    - 多个参数可以在dao中的参数前面注解@Param(alisName)
    - 多个参数可以封装成map，或者直接就是一个实体，那么xml中访问就是直接访问属性名

2. 返回值映射

    - 使用resultMap标签，在前面逐一定义数据库列名和对象属性名之间的映射关系
        ```xml
        <resultMap type="me.gacl.domain.Order">
            <id property="id" columm="order_id">
            <result property="orderNo" column="order_no"/>
        </resultMap>
        ```
    - 在查询语句中取别名，别名和实体中的字段一致

3. xml映射

    接口全限名+方法名拼接字符串作为key值，可唯一定位一个MapperStatement，在Mybatis中，每一个select、insert、update、delete标签，都会被解析为一个MapperStatement对象。Mapper 接口的工作原理是JDK动态代理，Mybatis运行时会使用JDK动态代理为Mapper接口生成代理对象proxy，代理对象会拦截接口方法，转而执行MapperStatement所代表的sql，然后将sql执行结果返回

### 分页

1. 在xml文件中，采用limit分页
2. 自定义拦截器，拦截固定名字后缀如byPage，自动在sql后面加上limit
3. 使用PageHelper分页，需要引入maven插件，配置，内部使用的是拦截器分页

### 缓存

1. 一级缓存（默认开启，每个查询都写上flushCache = true就相当于关闭了）

    - 在同一个SqlSession中，Mybatis会把执行的方法和参数通过算法生成缓存的键值，将键值和结果存放在一个Map 中，如果后续的键值一样，则直接从Map中获取数据
    - 不同的SqlSession之间的缓存是相互隔离的
    - flushCache清空一级和二级缓存
    - 在xml标签中加上flushCache = true会清空当前SqlSession中的所有缓存
    - UPDATE、INSERT、DELETE的flushCache默认为true,SELECT相反

2. 二级缓存（可配置总开关或xml文件中局部开关）

    - 二级缓存存在于SqlSessionFactory生命周期中
    - 二级缓存是namespace级别的，可以通过配置跨域，但是意义就不大了
        ```
        <cache-ref namespace="其他的命名空间">
        ```
    - 如果在一个namespace中刷新了缓存，另一个namespace中没有刷新，就会出现读到脏数据的情况。所以，推荐在一个Mapper里面只操作单表的情况使用
    - 在mapper.xml中添加cache标签
        ```xml
        <cache type="org.apache.ibatis.cache.impl.PerpetualCache"
           size="1024"
           eviction="LRU"
           flushInterval="120000"
           readOnly="false"/>
        ```
        - type，用于指定缓存的实现类型
        - eviction，对应的是回收策略，默认LRU（最近最少使用）
        - flushInterval，对应刷新间隔，单位毫秒，默认值不设置
        - size，对应为引用的数量，即最多的缓存对象数据，默认为1024
        - readOnly，为只读，默认false
            1. false: 可读写， 在创建对象时， 会通过反序列化得到缓存对象的拷贝。 因此在速度上会相对慢一点， 但重在安全。
            2. true: 只读， 只读的缓存会给所有调用者返回缓存对象的相同实例。 因此性能很好， 但如果修改了对象， 有可能会导致程序出问题

3. 缓存执行流程

    - 读取，二级缓存 —> 一级缓存 —> db

4. 第三方做缓存

    - mybatis本身的缓存不适合分布式，可以实现Cache接口，配合redis实现自己的缓存逻辑，实现分布式缓存

5. 其他

    - SqlSession是方法级别的生命周期，因此在一个方法中使用两次相同的查询，第二次会走缓存，或者说联合查询时，有时候会查询多次某个数据
    - 二级缓存，在事务不提交的情况下，不会生效

### 关联查询

1. 一对一映射
    ```xml
    <resultMap type="User" id="userResultMap">
        <!-- 属性名和数据库列名映射 -->
        <id property="id" column="user_id"  />
        <result property="userName" column="user_userName"  />
        <result property="userAge" column="user_userAge"  />
        <result property="userAddress" column="user_userAddress"  />
    </resultMap>
    
    <!-- User join Article进行联合查询  (一对一)-->    
    <resultMap id="articleResultMap" type="Article">
        <id property="id" column="article_id" />
        <result property="title" column="article_title" />
        <result property="content" column="article_content" />
        <!-- 将article的user属性映射到userResultMap -->
        <association property="user" javaType="User" resultMap="userResultMap"/>  
    </resultMap>
    
    <!-- 使用别名来映射匹配 -->
    <select id="getUserArticles" parameterType="int" resultMap="articleResultMap">
       select user.id user_id,user.userName user_userName,user.userAddress user_userAddress,
       article.id article_id,article.title article_title,article.content article_content 
       from user,article 
       where user.id=article.userid and user.id=#{id}
    </select>
    
    <!-- 另一种联合查询  (一对一)的实现，但是这种方式有“N+1”的问题 -->
   <resultMap id="articleResultMap" type="Article">
        <id property="id" column="article_id" />
        <result property="title" column="article_title" />
        <result property="content" column="article_content" />
        <!-- select可以引用其他mapper文件中的查询 -->
        <!-- 可以使用懒加载，fetchType="lazy" -->
        <!-- property的值是实体中的属性名 -->
        <association property="user" javaType="User" column="user_id" select="selectUser"/>  
    </resultMap>
    
    <select id="selectUser" parameterType="int" resultType="User">
        select * from user where id = #{id}
    </select>
   ```
2. 多对一
    ```xml
    <resultMap id="userResultMap" type="User">
        <id property="id" column="user_id"  />
        <result property="userName" column="user_userName"  />
        <result property="userAge" column="user_userAge"  />
        <result property="userAddress" column="user_userAddress"  />
    </resultMap>
    
    <resultMap id="articleResultMap" type="Article">
        <id property="id" column="article_id" />
        <result property="title" column="article_title" />
        <result property="content" column="article_content" />
        <association property="user" javaType="User" resultMap="userResultMap"/>  
    </resultMap>
    
    <resultMap id="blogResultMap" type="Blog">
        <id property="id" column="blog_id" />
        <result property="title" column="blog_title" />
        <!-- 将article list属性映射到collection -->
        <collection property="articles" ofType="Article" resultMap="articleResultMap"/>
    </resultMap>
    
    <!-- select语句 -->
    <select id="getBlogByID" parameterType="int" resultMap="blogResultMap">
       select user.id user_id,user.userName user_userName,user.userAddress user_userAddress,
       article.id article_id,article.title article_title,article.content article_content, 
       blog.id blog_id, blog.title blog_title
       from user,article,blog 
       where user.id=article.userid and blog.id=article.blogid and blog.id=#{id}
    </select>
    ```
### 其他

1. #{}和${}的区别是什么
- #{}是预编译处理，${}是字符串替换，#{}更加安全可以防止sql注入
    - 预编译的机制，提前对sql进行预编译，而后注入的参数不会在进行sql编译成语句
    - 字符替换是发生在预编译前，字符替换后，可能会发生sql注入问题
    
2. 注解和XML的对比

    - 注解，在sql简单时方便、简洁
    - xml在sql复杂时，灵活、实现了sql分离、好维护