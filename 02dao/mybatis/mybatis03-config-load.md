### 关键类

1. 使用建造者模式，分别会对mybatis-conf.xml和mapper.xml进行解析

    - XmlConfigBuilder，解析mybatis-config.xml，因为使用了spirng之后，该配置文件几乎不需要再使用，这里不做解释
    - XmlMapperBuilder，解析mapper.xml文件中的配置和sql语句节点
    - XmlStatementBuilder，解析mapper.xml文件中的sql语句节点

2. Configuration，mybatis启动初始化的核心就是将所有xml配置文件信息加载到Configuration对象中，Configuration是单例的，生命周期是应用级的
3. MapperRegistry，mapper接口动态代理工厂类的注册中心，在mybatis中，通过mapperProxy实现InvocationHandler接口，MapperProxyFactory用于生成动态代理的实例对象
4. ResultMap，用于解析mapper.xml中的resultMap节点，使用ReslutMapping来封装id，result等子元素
5. MappedStatement，用于存储mapper.xml文件中的select、insert、update、delete节点，同时还包含了这些节点的很多重要属性
6. SqlSource，mapper.xml文件中的sql语句会被解析成sqlSource对象，经过解析sqlSource包含的语句最终仅仅包含？占位符，可以直接提交给数据库执行

### 解析流程

1. XmlMapperBuilder#parse解析mapper.xml，一个xml文件对应一个XmlMapperBuilder对象
    ```java
    public void parse() {
        if (!configuration.isResourceLoaded(resource)) {
        configurationElement(parser.evalNode("/mapper"));
        configuration.addLoadedResource(resource);
        bindMapperForNamespace();
        }

        parsePendingResultMaps();
        parsePendingCacheRefs();
        parsePendingStatements();
    }
    ```
2. XmlMapperBuilder#configurationElement解析mapper.xml文件中的其他节点和sql节点
    ```java
    //获取namespace
    String namespace = context.getStringAttribute("namespace");
    if (namespace == null || namespace.equals("")) {
    throw new BuilderException("Mapper's namespace cannot be empty");
    }
    builderAssistant.setCurrentNamespace(namespace);
    //二级缓存引用
    cacheRefElement(context.evalNode("cache-ref"));
    //二级缓存的声明
    cacheElement(context.evalNode("cache"));
    //parameterMap
    parameterMapElement(context.evalNodes("/mapper/parameterMap"));
    //resultMap
    resultMapElements(context.evalNodes("/mapper/resultMap"));
    //sql标签的引用
    sqlElement(context.evalNodes("/mapper/sql"));
    //sql节点
    buildStatementFromContext(context.evalNodes("select|insert|update|delete"));
    ```
3. XmlMapperBuilder#buildStatementFromContext解析sql节点
    ```java
    for (XNode context : list) {
      final XMLStatementBuilder statementParser = new XMLStatementBuilder(configuration, builderAssistant, context, requiredDatabaseId);
      try {
        statementParser.parseStatementNode();
      } catch (IncompleteElementException e) {
        configuration.addIncompleteStatement(statementParser);
      }
    }
    ```
4. XMLStatementBuilder#parseStatementNode解析每一个sql节点存储为MapperStatement，解析出来以下参数，进行实例化
    ```java
    builderAssistant.addMappedStatement(
        id, sqlSource, statementType, sqlCommandType,
        fetchSize, timeout, parameterMap, parameterTypeClass, resultMap, resultTypeClass,
        resultSetTypeEnum, flushCache, useCache, resultOrdered, 
        keyGenerator, keyProperty, keyColumn, databaseId, langDriver, resultSets);
    ```
5. MapperBuilderAssistant#addMappedStatement构建实例化MapperStatement，里面包含sql的各种信息，最终将xml解析出来后，封装为的java-pojo，基本上就包括前面的那些参数

### binding

1. binding模块主要是mapper接口到xml文件之间的一个映射。回到第一步，XMLMapperBuilder#bindMapperForNamespace，将mapper接口放进Configuration中
    ```java
    Class<?> boundType = null;
    //获取到对应的接口
    boundType = Resources.classForName(namespace);
    configuration.addLoadedResource("namespace:" + namespace);
    //将接口进行一个缓存
    configuration.addMapper(boundType);
    ```
2. Configuration#addMapper
    ```java
    mapperRegistry.addMapper(type);
    ```
3. MapperRegistry#addMapper
    ```java
    //加入缓存，MapperProxyFactory是Mapper的实例工厂类
    knownMappers.put(type, new MapperProxyFactory<T>(type));
    //翻译接口中是否是采用的注解编程
    MapperAnnotationBuilder parser = new MapperAnnotationBuilder(config, type);
    parser.parse();
    ```
4. MapperProxyFactory是用来实例化MapperProxy的，正常情况下interface是不可以实例化的，也不能执行，这里采用了jdk的动态代理生成一个新的子类，子类就可以实例化
    ```java
    //MapperProxy实现了InvocationHandler接口，
    protected T newInstance(MapperProxy<T> mapperProxy) {
        return (T) Proxy.newProxyInstance(mapperInterface.getClassLoader(), new Class[] { mapperInterface }, mapperProxy);
    }
    ```
5. MapperProxy#invoke，因为是代理的接口，所以不会执行method.invoke方法
    ```java
    //不代理Object的方法和默认的方法
    if (Object.class.equals(method.getDeclaringClass())) {
        return method.invoke(this, args);
    } else if (isDefaultMethod(method)) {
        return invokeDefaultMethod(proxy, method, args);
    }
    //从缓存中获取MapperMethod，如果没有就是实例化
    final MapperMethod mapperMethod = cachedMapperMethod(method);
    //执行对应的方法
    return mapperMethod.execute(sqlSession, args);
    ```
6. MapperProxy#cachedMapperMethod，获取MapperMethod对象
    ```java
    private MapperMethod cachedMapperMethod(Method method) {
        MapperMethod mapperMethod = methodCache.get(method);
        if (mapperMethod == null) {
        mapperMethod = new MapperMethod(mapperInterface, method, sqlSession.getConfiguration());
        methodCache.put(method, mapperMethod);
        }
        return mapperMethod;
    }
    ```
7. 实例化MapperMethod，每一个sql节点都会实例化一个MapperMethod，MapperMethod基本上就包含了这个方法执行的所有的东西
    ```java
    public class MapperMethod {
        private final SqlCommand command;
        private final MethodSignature method;
    }

    public static class SqlCommand {
        //全称，namespace+id
        private final String name;
        //UNKNOWN, INSERT, UPDATE, DELETE, SELECT, FLUSH
        private final SqlCommandType type;
    }

    public static class MethodSignature {
        private final boolean returnsMany;
        private final boolean returnsMap;
        private final boolean returnsVoid;
        private final boolean returnsCursor;
        private final Class<?> returnType;
        private final String mapKey;
        private final Integer resultHandlerIndex;
        private final Integer rowBoundsIndex;
        private final ParamNameResolver paramNameResolver;
    }
    ```
8. MapperMethod#execute，根据SqlCommandType来选择使用sqlSession的什么方法来执行，并且获取到参数
    ```java
    method.convertArgsToSqlCommandParam(args);
    ```
9. MapperMethod#convertArgsToSqlCommandParam到ParamNameResolver#getNamedParams方法
    ```java
    public Object getNamedParams(Object[] args) {
        final int paramCount = names.size();
        //没有参数时，返回null
        if (args == null || paramCount == 0) {
            return null;
        //只有一个参数时，直接返回，包括map，对象实例
        } else if (!hasParamAnnotation && paramCount == 1) {
            return args[names.firstKey()];
        } else {
            //有多个参数时，names<序号，参数名>是在实例化MapperMethod的时候设置进去的
            //设置进去的时候会用到会识别@Param注解，没有这个注解的
            final Map<String, Object> param = new ParamMap<Object>();
            int i = 0;
            for (Map.Entry<Integer, String> entry : names.entrySet()) {
                param.put(entry.getValue(), args[entry.getKey()]);
                // add generic param names (param1, param2, ...)
                final String genericParamName = GENERIC_NAME_PREFIX + String.valueOf(i + 1);
                // 没有使用@Param的参数
                if (!names.containsValue(genericParamName)) {
                    param.put(genericParamName, args[entry.getKey()]);
                }
                i++;
            }
            return param;
        }
    }
    ```

### 设计模式

XmlConfigBuilder、XmlMapperBuilder、XmlStatementBuilder等使用了建造者模式