### 概述

mybatis之所以会建立一个反射基础组件，是因为jdk的反射工具类对于mybatis的一些需求，实现起来太繁琐，因此针对自己的一些业务实现了一些基础组件

### 反射核心类

1. ObjectFactory，mybatis每次创建结果对象的新实例时，他都会使用对象工厂ObjectFactory去构建POJO

2. ReflectorFactory，创建Reflector的工厂类，Reflectory是mybatis反射模块的基础，每个Reflector对象都对应一个类，在其中缓存了反射操作所需要的类元信息（一些方法，属性的缓存）

3. ObjectWrapper，对对象的包装，抽象了对象的属性信息，他定义了一系列查询对象属性信息的方法，以及更新属性的方法

4. ObjectWrapperFactory，ObjectWrapper的工厂类，用于创建ObjectWrapper

5. MetaClass主要是使用Reflector对象，所有的对定义了的bean都通过MetaCalss来设置和获取属性

6. MetaObject，封装了对象的元信息，包装了mybatis中的四个核心的反射类，也是提供给外部使用的反射工具类，可以利用它读取获取修改对象的属性信息

    ```java
    private final Object originalObject;
    private final ObjectWrapper objectWrapper;
    private final ObjectFactory objectFactory;
    private final ObjectWrapperFactory objectWrapperFactory;
    private final ReflectorFactory reflectorFactory;
    ```
### ObjectFactory

1. 默认实现DefaultObjectFactory，用于实例化接收结果集的对象，
    ```java
    public <T> T create(Class<T> type) {
        return create(type, null, null);
    }
    ```
2. 创建的时候可以传入构造方法的参数类型和实际参数
    ```java
    public <T> T create(Class<T> type, List<Class<?>> constructorArgTypes, List<Object> constructorArgs) {
        //用于修改List、Set、Map等的默认实例
        Class<?> classToCreate = resolveInterface(type);
        //如果后面两个参数为null，那么会参数默认的无参构造函数
        return (T) instantiateClass(classToCreate, constructorArgTypes, constructorArgs);
    }
    ```
3. 修改默认的集合创建类型
    ```java
    protected Class<?> resolveInterface(Class<?> type) {
        Class<?> classToCreate;
        if (type == List.class || type == Collection.class || type == Iterable.class) {
        classToCreate = ArrayList.class;
        } else if (type == Map.class) {
        classToCreate = HashMap.class;
        } else if (type == SortedSet.class) {
        classToCreate = TreeSet.class;
        } else if (type == Set.class) {
        classToCreate = HashSet.class;
        } else {
        classToCreate = type;
        }
        return classToCreate;
    }
    ```
### ReflectorFactory

1. 默认实现DefaultReflectorFactory，是创建Reflector的工厂类，DefaultReflectorFactory对Reflector进行了一个缓存，以类为单位存储在Map中
    ```java
    //存储缓存的Map
    private final ConcurrentMap<Class<?>, Reflector> reflectorMap = new ConcurrentHashMap<Class<?>, Reflector>();

    //寻找时，没有找到就new一个，如果允许缓存就放入Map缓存
    public Reflector findForClass(Class<?> type) {
        if (classCacheEnabled) {
            Reflector cached = reflectorMap.get(type);
            if (cached == null) {
                cached = new Reflector(type);
                reflectorMap.put(type, cached);
            }
            return cached;
        } else {
            return new Reflector(type);
        }
    }
    ```
2. Reflector是存储一个类需要直接进行反射的一些类信息，比如包含的什么方法、属性等，他主要是为MetaClass提供服务的，定义了bean的属性获取设置都是用MetaCalss对象
    ```java
    public Reflector(Class<?> clazz) {
        //存储的class类型
        type = clazz;
        //默认的无参构造函数
        addDefaultConstructor(clazz);
        //所有的get方法，会有一些判断
        //根据get方法名规则获取到属性，进行一些判断比较
        //最后处理出来一个属性只有一个get方法
        addGetMethods(clazz);
        //添加set方法，类似get的处理方式
        addSetMethods(clazz);
        //添加属性，有一个处理，有些属性没有写对应的get/set方法
        //自己封装了一个类，利用反射直接设置属性的值，相当于也就是get/set方法了
        //然后放在上面的get/set方法的集合里面
        addFields(clazz);
        //可读的属性名
        readablePropertyNames = getMethods.keySet().toArray(new String[getMethods.keySet().size()]);
        //可写的字段名
        writeablePropertyNames = setMethods.keySet().toArray(new String[setMethods.keySet().size()]);
        for (String propName : readablePropertyNames) {
            //忽略大小写的属性名
            caseInsensitivePropertyMap.put(propName.toUpperCase(Locale.ENGLISH), propName);
        }
        for (String propName : writeablePropertyNames) {
            //忽略大小写的属性名
            caseInsensitivePropertyMap.put(propName.toUpperCase(Locale.ENGLISH), propName);
        }
    }
    ```
### ObjectWrapper、ObjectWrapperFactory、MetaObject

1. ObjectWrapper它主要实现了兼容不同对象类型的查询属性接口和设值接口，有以下几个实现
    - BaseWrapper抽象类，其他Wrapper的实现基础，这里采用了模板模式，引用了MetaObject作为属性
    - BeanWrapper，专用Bean对象包装，里面特定引用了MetaClass
    - CollectionWrapper对Collection的一个包装
    - MapWrapper对Map的一个包装
2. ObjectWrapperFactory主要是为ObjectWrapper提供工厂，不过貌似没有使用起来
3. MetaObject中构造方法提供了一个简单的工厂模式，来生成不同的实体类Wrapper
    ```java
    if (object instanceof ObjectWrapper) {
      this.objectWrapper = (ObjectWrapper) object;
    } else if (objectWrapperFactory.hasWrapperFor(object)) {
      this.objectWrapper = objectWrapperFactory.getWrapperFor(this, object);
    } else if (object instanceof Map) {
      this.objectWrapper = new MapWrapper(this, (Map) object);
    } else if (object instanceof Collection) {
      this.objectWrapper = new CollectionWrapper(this, (Collection) object);
    } else {
      this.objectWrapper = new BeanWrapper(this, object);
    }
    ```
4. MetaObject中引用了reflectorFactory，因此在实例化BeanWrapper时，可以根据reflectorFactory获取到对应class的reflector
    ```java
    public BeanWrapper(MetaObject metaObject, Object object) {
        super(metaObject);
        this.object = object;
        this.metaClass = MetaClass.forClass(object.getClass(), metaObject.getReflectorFactory());
    }
    ```