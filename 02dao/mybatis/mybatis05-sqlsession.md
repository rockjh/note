### 关键类

1. SqlSessionFactory接口，用于生产sqlSession的工厂类，默认实现DefaultSqlSessionFactory

2. DefaultSqlSessionFactory，SqlSessionFactory的默认使用，只有一个属性Configuration

3. SqlSessionFactoryBuilder，用于构建DefaultSqlSessionFactory

4. SqlSessionManager，实现了SqlSessionFactory、SqlSession接口，引用了DefaultSqlSessionFactory，利用ThreadLocal来实现线程安全的sqlSession，目前源码中没有使用，暂不讨论

5. SqlSession，定义了查询数据库的一系列接口，默认实现DefaultSqlSession，mybatis对外暴露的就是sqlSession

6. DefaultSqlSession，默认查询数据库的一个实现，里面真正实现查询数据库的是executor，他是线程不安全的，因为executor的缓存原因

   > DefaultSqlSession线程不安全，不要设置为单例
   >
   > 一个SqlSession中只能有一个事务，一个事务又只能有一个Connection，JDBC的Connection对象本身不是线程安全的

7. SqlSessionTemplate，和mybatis-spring结合的一个线程安全的sqlSesssion

### SqlSessionTemplate保证线程安全

1. SqlSessionTemplate的创建
    ```java
    public SqlSessionTemplate(SqlSessionFactory sqlSessionFactory, ExecutorType executorType,
      PersistenceExceptionTranslator exceptionTranslator) {
        //实例化DefaultSqlSession的时候会用到
        this.sqlSessionFactory = sqlSessionFactory;
        //SIMPLE, REUSE, BATCH，executor的类型
        this.executorType = executorType;
        //
        this.exceptionTranslator = exceptionTranslator;
        //创建了一个接口sqlSession的动态代理，
        this.sqlSessionProxy = (SqlSession) newProxyInstance(
            SqlSessionFactory.class.getClassLoader(),
            new Class[] { SqlSession.class },
            new SqlSessionInterceptor());
    }
    ```
2. SqlSessionTemplate的执行，例如selectOne等都是使用的sqlSessionProxy方法，所以他的所有关于sql执行的都会进入代理的invoke方法，SqlSessionTemplate#SqlSessionInterceptor#invoke
    ```java
    //获取SqlSession(这个SqlSession才是真正使用的，它不是线程安全的)
    //这个方法可以根据Spring的事物上下文来获取事务范围内的sqlSession
    SqlSession sqlSession = getSqlSession(
        SqlSessionTemplate.this.sqlSessionFactory,
        SqlSessionTemplate.this.executorType,
        SqlSessionTemplate.this.exceptionTranslator);
    //调用从Spring的事物上下文获取事物范围内的sqlSession对象，其实就是DefaultSqlSession
    Object result = method.invoke(sqlSession, args);
    //然后判断一下当前的sqlSession是否被Spring托管 如果未被Spring托管则自动commit
    //结合spring的话，只要是insert、delete、update都不会进入条件
    if (!isSqlSessionTransactional(sqlSession, SqlSessionTemplate.this.sqlSessionFactory)) {
        // force commit even on non-dirty sessions because some databases require
        // a commit/rollback before calling close()
        sqlSession.commit(true);
    }
    ```
3. SqlSessionUtils#getSqlSession，当执行update、insert、delete时，关键代码如下
    ```java
    //根据sqlSessionFactory从当前线程对应的资源map中获取SqlSessionHolder，
    // 当sqlSessionFactory创建了sqlSession，
    //就会在事务管理器中添加一对映射：key为sqlSessionFactory，value为SqlSessionHolder，
    // 该类保存sqlSession及执行方式
    //这里就会直接进入spring的事务管理类
    SqlSessionHolder holder = (SqlSessionHolder) TransactionSynchronizationManager.getResource(sessionFactory);
    //如果存在可以直接通过getSqlSession获取到，然后就可以返回
    SqlSession session = sessionHolder(executorType, holder);
    if (session != null) {
      return session;
    }

    //下面还有新建sqlSession的代码
    sessionFactory.openSession(executorType);
    ```
4. DefaultSqlSessionFactory#openSession新建sqlSession
    ```java
    //获取环境变量
    final Environment environment = configuration.getEnvironment();
    //获取事务工厂
    final TransactionFactory transactionFactory = getTransactionFactoryFromEnvironment(environment);
    //创建事务
    tx = transactionFactory.newTransaction(environment.getDataSource(), level, autoCommit);
    //创建executor
    final Executor executor = configuration.newExecutor(tx, execType);
    //根据上面的参数创建sqlSession，这些参数在后面都会用上
    return new DefaultSqlSession(configuration, executor, autoCommit);
    ```
5. 所以SqlSessionTemplate是如何保证线程安全的呢，他会根据当前线程的事务判断是否新建一个sqlSession，如果之前和现在是同一个事务的话，就会返回之前的sqlSession

### 执行流程

1. 从spring-mybatis的结合结构说起，默认注入的sqlSession是SqlSessionTemplate
2. 执行到MapperMethod#execute，这里会进入到sqlSessiond的方法中，例如一个根据id查询会进入到
    ```java
    sqlSession.selectOne(command.getName(), param);
    ```
3. SqlSessionTemplate#selectOne，会直接跳转到SqlSessionTemplate#SqlSessionInterceptor#invoke方法
    ```java
    @Override
    public <T> T selectOne(String statement) {
        return this.sqlSessionProxy.<T> selectOne(statement);
    }
    ```
4. 直接跳转到DefaultSqlSession#selectOne开始执行sqlSessino的逻辑了
    ```java
    @Override
    public <T> T selectOne(String statement) {
        return this.<T>selectOne(statement, null);
    }
    ```
5. 最后不论什么参数，如果是更新sql，则会跳转到update方法，如果是select的sql则会跳转到executor.query
    ```java
    //都会执行，通过namespace+id获取到MappedStatement
    MappedStatement ms = configuration.getMappedStatement(statement);
    ```

