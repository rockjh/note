### Executor的子集

1. BaseExecutor，几乎都实现了该有的接口，余下的4个抽象接口，等待子类实现，根据不同的executor不同的规则有不同的实现，使用了模板模式
2. SimpleExecutor，默认的executor实现，使用statement对象访问数据库，每次访问都要创建新的statement对象
3. ReuseExecutor，使用预编译PrepareStatement对象访问数据库，访问时，会重用缓存中的statement对象
4. BatchExecutor，实现批量执行多条sql语句的能力
5. CachingExecutor，executor的一个缓存装饰器，针对二级缓存的一个设计，内部有一个真实的executor的引用

### StatementHandler

1. Statementhandler，完成mybatis最核心的工作，也就是executor的基础，包括创建statement对象、为sql语句绑定参数、执行增删改查sql语句、将结果映射集进行转化
2. BaseStatementHandler，所有子类的抽象父类，定义了初始化statement的操作顺序，由子类实现具体的实例化不同的statement，采用了模板模式
3. RoutingStatementHandler，statementhandler组件真正实例化的子类，使用静态代理模式，根据上下文决定创建哪个具体的实体类
4. SimpleStatementHandler，使用statement对象访问数据库，无需参数化
5. PreparedStatementHandler，使用预编PrepareStatement对象访问数据库，mybatis的默认实现，可以预防sql注入，也会进行缓存
6. CallableStatementHandler，调用存储过程

### Executor执行流程

1. Configuration#newExecutor在创建executor
    ```java
    Executor executor;
    //SIMPLE（简单的）, REUSE（可复用的）, BATCH（批量的）
    if (ExecutorType.BATCH == executorType) {
      executor = new BatchExecutor(this, transaction);
    } else if (ExecutorType.REUSE == executorType) {
      executor = new ReuseExecutor(this, transaction);
    } else {
      executor = new SimpleExecutor(this, transaction);
    }
    //这是一个装饰器模式，CacheingExecutor专门做缓存的
    if (cacheEnabled) {
      executor = new CachingExecutor(executor);
    }
    /加载插件的
    executor = (Executor) interceptorChain.pluginAll(executor);
    ```
    
2. 假如我们使用了缓存，那么在执行executor的方法的时候就会进入到CacheingExecutor，例如select，CacheingExecutor#query
    ```java
    //二级缓存
    Cache cache = ms.getCache();
    if (cache != null) {
        //如果有flushCache为true就清空
        flushCacheIfRequired(ms);
        if (ms.isUseCache() && resultHandler == null) {
            List<E> list = (List<E>) tcm.getObject(cache, key);
            if (list == null) {
                //没有缓存，就走executor真正的查询
                list = delegate.<E> query(ms, parameterObject, rowBounds, resultHandler, key, boundSql);
                tcm.putObject(cache, key, list);
            }
            return list;
        }
    }
    return delegate.<E> query(ms, parameterObject, rowBounds, resultHandler, key, boundSql);
    ```
    
3. BaseExecutor#query，实际的db查询关键代码
    ```java
    //首先从一级缓存localCache获取数据
    List<E> list = resultHandler == null ? (List<E>) localCache.getObject(key) : null;
    if (list != null) {
        handleLocallyCachedOutputParameters(ms, key, parameter, boundSql);
    } else {
        list = queryFromDatabase(ms, parameter, rowBounds, resultHandler, key, boundSql);
    }
    ```
    
4. 真实的从db查询数据BaseExecutor#queryFromDatabase
    ```java
    //查询数据，抽象类
    list = doQuery(ms, parameter, rowBounds, resultHandler, boundSql);
    localCache.putObject(key, list);
    ```
    
5. BaseExecutor#doQuery抽象类，具体由子类实现，例如SimpleExecutor，这里使用了模板模式
    ```java
    public <E> List<E> doQuery(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
        Statement stmt = null;
        try {
            Configuration configuration = ms.getConfiguration();
            //获取到RoutingStatementHandler
            StatementHandler handler = configuration.newStatementHandler(wrapper, ms, parameter, rowBounds, resultHandler, boundSql);
            //为statement设置参数，使用打印日志的connection代理
            stmt = prepareStatement(handler, ms.getStatementLog());
            //开始真正的执行sql，移交给PreparedStatementHandler
            return handler.<E>query(stmt, resultHandler);
        } finally {
            closeStatement(stmt);
        }
    }
    ```

6. RoutingStatementHandler构造参数如下

    ```java
    switch (ms.getStatementType()) {
        case STATEMENT:
            delegate = new SimpleStatementHandler(executor, ms, parameter, rowBounds, resultHandler, boundSql);
            break;
        case PREPARED:
            delegate = new PreparedStatementHandler(executor, ms, parameter, rowBounds, resultHandler, boundSql);
            break;
        case CALLABLE:
            delegate = new CallableStatementHandler(executor, ms, parameter, rowBounds, resultHandler, boundSql);
            break;
        default:
            throw new ExecutorException("Unknown statement type: " + ms.getStatementType());
    }
    ```

7. PreparedStatementHandler#query开始真正的执行sql

    ```java
    @Override
    public <E> List<E> query(Statement statement, ResultHandler resultHandler) throws SQLException {
        PreparedStatement ps = (PreparedStatement) statement;
        ps.execute();
        //获取处理结果集
        return resultSetHandler.<E> handleResultSets(ps);
    }
    ```

8. DefaultResultSetHandler#handleResultSets处理结果集

9. mybatis自带的分页，使用RowBounds来返回分页结果，DefaultResultSetHandler#skipRows，他是逻辑分页，也就是查询出来所有数据在进行分页，不可取，建议采用

     - 直接sql写limit分页
     - 引入pagehelper分页插件


### StatementHandler执行流程

1. 从前面第五步开始，SimpleExecutor#prepareStatement，初始化PreparedStatementHandler并调用parameterize初始化参数

   ```java
   private Statement prepareStatement(StatementHandler handler, Log statementLog) throws SQLException {
       Statement stmt;
     	//获取到连接，开启了日志sql就是获取的代理连接
       Connection connection = getConnection(statementLog);
     	//初始化PreparedStatementHandler,handler=RoutingStatementHandler
       stmt = handler.prepare(connection, transaction.getTimeout());
     	//设置参数,handler=RoutingStatementHandler
       handler.parameterize(stmt);
       return stmt;
   }
   ```

2. RoutingStatementHandler#parameterize设置参数

   ```java
   public void parameterize(Statement statement) throws SQLException {
     	//delegate = PreparedStatementHandler
       delegate.parameterize(statement);
   }
   ```

3. PreparedStatementHandler#parameterize设置参数

   ```java
   public void parameterize(Statement statement) throws SQLException {
     	//启动parameterHandler
       parameterHandler.setParameters((PreparedStatement) statement);
   }
   ```

4. DefaultParameterHandler#setParameters设置参数

   ```java
   //使用动态sql，TypeHandlerRegistry等进行参数替换，替换为sql可以识别的"?"这些
   ```

5. 回到上一个步骤的第五步，进入PreparedStatementHandler#query

   ```java
   public <E> List<E> query(Statement statement, ResultHandler resultHandler) throws SQLException {
       PreparedStatement ps = (PreparedStatement) statement;
     	//调用执行
       ps.execute();
     	//resultSetHandler进行结果集包装
       return resultSetHandler.<E> handleResultSets(ps);
   }
   ```

   