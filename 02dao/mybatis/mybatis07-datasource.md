### 数据库连接池

1. mybatis定义的DataSourceFactory接口，用于生产各种不同的数据源，使用工厂模式
    - UnpooledDataSourceFactory，不支持池化的数据源工厂
    - PooledDataSourceFactory，支持池化的数据源工厂
    - JndiDataSourceFactory，jndi的数据源工厂
    
2. UnpooledDataSourceFactory主要是数据源的一个封装，各种参数的一个处理，并且指定数据源UnpooledDataSource，他的获取连接跟jdbc获取的类似

3. PooledDataSourceFactory是UnpooledDataSourceFactory的子类，只是将数据源换成了PooledDataSource，PooledDataSource将UnpooledDataSource作为自己的一个属性，直接复用了之前封装好的DataSource一些属性和逻辑
    ```java
    public PooledDataSource() {
        dataSource = new UnpooledDataSource();
    }

    @Override
    public Connection getConnection() throws SQLException {
        return popConnection(dataSource.getUsername(), dataSource.getPassword()).getProxyConnection();
    }
    ```
    
4. 在获取连接这一块，使用了池化
    ```java
    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        //根据一些参数，获取连接池中的连接，如果没有空闲连接，并且没有超过最大连接数则创建，否则等待
        return popConnection(username, password).getProxyConnection();
    }
    ```
    
5. PooledConnection实现了InvocationHandler，被用来创建代理，里面包含realConnection，看invoke方法，使用代理的目的
    ```java
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        //将close方法替换为放入连接池
        if (CLOSE.hashCode() == methodName.hashCode() && CLOSE.equals(methodName)) {
            dataSource.pushConnection(this);
            return null;
        } else {
            try {
                if (!Object.class.equals(method.getDeclaringClass())) {
                    //检查连接
                    checkConnection();
                }
                return method.invoke(realConnection, args);
            } catch (Throwable t) {
                throw ExceptionUtil.unwrapThrowable(t);
            }
        }
    }
    ```
    
6. 他们之间有几个很巧妙的设计
    - PooledConnection，创建时，会将PooledDataSource传递进去当做参数、通过PooledDataSource中的UnpooledDataSource引用获取到真实的连接传递进去当做参数，达到了代码复用的目的
    - 新建PoolState是会传递PooledDataSource进去作为属性，目的是toString的时候可以将dataSource的属性也打印出来，同时PoolState是记录连接池的一个状态
    - PooledDataSource引用了PoolState，在连接进行变化时，设置连接状态的各种值，包括空闲连接，坏连接等；引用了PooledConnection，用于存储连接的创建等信息，用于代理，使用前后的一个有效性检查
    
7. 为什么要使用工厂模式，数据源的初始化复杂，要接入多个第三方数据源组件

