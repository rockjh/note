### 概述

mybatis的日志主要实现类似于slf4j，他直接引用的外部的日志框架，兼容了市面上主流的日志框架，当框架中有多种日志实现时，默认的加载顺序如下
```
Slf4j>Commons>Log4J2>Log4J>Jdk
```

### 初始化流程

1. LogFactory获取日志的工厂
2. LogFactory的静态方法块，尝试按照优先顺序获取日志的实现，一旦成功就将logConstructor设置为该日志类的构造方法，下次可以通过构造方法，获取实现，直接处理，获取失败会有忽略异常的处理，继续寻找下一个实现
    ```java
    //implClass表示各种日志的一个包装，继承自mybatis自定义的Log接口
    private static void setImplementation(Class<? extends Log> implClass) {
        try {
            //通过反射获取到构造方法
            Constructor<? extends Log> candidate = implClass.getConstructor(String.class);
          	//创建对日志的包装类的实例，例如Slf4jImpl
            Log log = candidate.newInstance(LogFactory.class.getName());
            if (log.isDebugEnabled()) {
                log.debug("Logging initialized using '" + implClass + "' adapter.");
            }
            logConstructor = candidate;
        } catch (Throwable t) {
            //上层会忽略这个异常
            throw new LogException("Error setting Log implementation.  Cause: " + t, t);
        }
    }
    ```
3. 举例使用Slf4j进行说明，Slf4jImpl是mybatis自定义的一个Log接口的实现类，获取到Slf4jImpl的构造方法
    ```java
    public Slf4jImpl(String clazz) {
        //通过slf4j获取到日志的一个实现
        Logger logger = LoggerFactory.getLogger(clazz);
        if (logger instanceof LocationAwareLogger) {
            try {
                // check for slf4j >= 1.6 method signature
                logger.getClass().getMethod("log", Marker.class, String.class, int.class, String.class, Object[].class, Throwable.class);
                //这里是作为一个对象适配器，里面有一个对象logger是对Slf4j的引用
                log = new Slf4jLocationAwareLoggerImpl((LocationAwareLogger) logger);
                return;
            } catch (SecurityException e) {
                // fail-back to Slf4jLoggerImpl
            } catch (NoSuchMethodException e) {
                // fail-back to Slf4jLoggerImpl
            }
        }
        //低版本使用这个实现 version < 1.6
        log = new Slf4jLoggerImpl(logger);
    }
    ```
4. Slf4jLocationAwareLoggerImpl实现mybatis自己定义的一个Log接口，引用了slf4j的一个log对象，使用了对象适配器模式，在使用的时候直接通过Slf4jLocationAwareLoggerImpl的日志方法，也就是父类mybatis自己定义的Log可以直接调用对应相匹配的slf4j的方法
    ```java
    //这个方法是mybatis自己定义的一个Log接口
    @Override
    public void error(String s, Throwable e) {
        //logger是slf4j的一个引用
        logger.log(MARKER, FQCN, LocationAwareLogger.ERROR_INT, s, null, e);
    }
    ```
5. LogFactory返回的也就是Slf4jImpl的实例，在打印的时候直接调用log.error()，回去调用Slf4jLocationAwareLoggerImpl它的log.error()，相当于就直接调用slf4j日志框架的日志
    ```java
    @Override
    public void error(String s, Throwable e) {
        log.error(s, e);
    }
    ```

### 打印sql

1. 前面经过一系列的包装，到了获取connection执行sql的时间
2. SimpleExecutor#prepareStatement准备statement
    ```java
    private Statement prepareStatement(StatementHandler handler, Log statementLog) throws SQLException {
        Statement stmt;
        //这里可能获取的是代理连接
        Connection connection = getConnection(statementLog);
        stmt = handler.prepare(connection, transaction.getTimeout());
        handler.parameterize(stmt);
        return stmt;
    }
    ```
3. BaseExecutor#getConnection
    ```java
    protected Connection getConnection(Log statementLog) throws SQLException {
        Connection connection = transaction.getConnection();
        //如果开启了debug日志，就可以直接生成一个代理对象，connection代理对象
        if (statementLog.isDebugEnabled()) {
            return ConnectionLogger.newInstance(connection, statementLog, queryStack);
        } else {
            return connection;
        }
    }
    ```
4. ConnectionLogger实现了InvocationHandler标准的jdk代理，看他的invoke方法，关键代码如下
    ```java
    //打印sql
    if (isDebugEnabled()) {
        debug(" Preparing: " + removeBreakingWhitespace((String) params[0]), true);
    }
    //执行方法，获得返回statement实例
    PreparedStatement stmt = (PreparedStatement) method.invoke(connection, params);
    //将statement实例进行封装，使用PreparedStatementLogger进行代理，然后返回
    stmt = PreparedStatementLogger.newInstance(stmt, statementLog, queryStack);
    ```
5. 类似的PreparedStatementLogger实现了InvocationHandler标准的jdk代理，invoke方法中的关键代码如下
    ```java
    //打印sql
    if (isDebugEnabled()) {
        debug("Parameters: " + getParameterValueString(), true);
    }
    //统计修改的行数时，打印出来
    if (updateCount != -1) {
        debug("   Updates: " + updateCount, false);
    }
    ```
6. 如果是查询，ResultSetLogger对最后的结果集进行处理，类似的实现了标准的jdk代理，invoke方法中的关键代码如下
    ```java
    //next获取下一个结果集的时候
    if ("next".equals(method.getName())) {
        //当存在下一个结果集的时候
        if (((Boolean) o)) {
            rows++;
            if (isTraceEnabled()) {
                ResultSetMetaData rsmd = rs.getMetaData();
                final int columnCount = rsmd.getColumnCount();
                if (first) {
                    first = false;
                    printColumnHeaders(rsmd, columnCount);
                }
                printColumnValues(columnCount);
            }
        } else {
            //当不存在下一个结果集的时候，就打印查找的总数
            debug("Total: " + rows, false);
        }
    }
    ```

### 配置打印sql

1. springboot中在application.yml中配置
    ```yml
    #前面的打印都会判断是否debug
    #固定包专门配置日志打印级别
    logging:
        level:
            com.rockjh.study.mapper : debug
    ```
2. 在logback中配置日志输出级别为debug，此种方式会打印出很多的其他日志，不方便调试

### 配置原理

1. 举例logback，其中Logger类中有一个Level属性，专门配置日志级别，还有一个属性effectiveLevelInt外部的存储日志级别
2. logback的日志级别是按照包命名的方式来处理日志级别的，比如我一个声明一个日志com.rockjh.study.service.UserService，其中com.rockjh.study.service前面每一个以.为分割，都是一个父级，是以层级来的，子集会继承父级的自定义日志级别
    - com
        - rockjh
            - study
                - service
3. 例如声明UserMapper.selectById的日志时，因为配置文件配置了该命名空间下为debug，所以这一个声明的时候就是一个debug级别
    ```yml
    #日志名
    com.rockjh.study.mapper.UserMapper.selectById
    ```
4. 创建日志关键代码如下
    ```java
    //循环，根据包名，从最父级com开始寻找，没有的话，依次创建
    while (true) {
        //以.进行分割，找从父级到子集
        int h = LoggerNameUtil.getSeparatorIndexOf(name, i);
        if (h == -1) {
            childName = name;
        } else {
            childName = name.substring(0, h);
        }
        //进行一个移动，方便找最后的
        i = h + 1;
        synchronized (logger) {
            childLogger = logger.getChildByName(childName);
            if (childLogger == null) {
                //没有找到就创建
                //示例中会在childName=com.rockjh.study.mapper.UserMapper.selectById时执行
                childLogger = logger.createChildByName(childName);
                loggerCache.put(childName, childLogger);
                incSize();
            }
        }
        logger = childLogger;
        if (h == -1) {
            return childLogger;
        }
    }
    ```
5. createChildByName关键代码如下
    ```java
    childLogger = new Logger(childName, this, this.loggerContext);
    childrenList.add(childLogger);
    //父级的级别属性赋值给子级
    childLogger.effectiveLevelInt = this.effectiveLevelInt;
    ```
6. application.yml配置文件获取到日志级别后，会直接实例化该命名空间的日志，示例中会直接实例化，命名分别为
    - com日志框架级别
    - com.rockjh日志框架级别
    - com.rockjh.study日志框架级别
    - com.rockjh.study.mapper配置application/yml的级别
7. 创建com.rockjh.study.mapper.UserMapper.selectById的时候就会继承父级的日志级别
8. log.isDebugEnabled()解析
    ```java
    public boolean isDebugEnabled(Marker marker) {
        final FilterReply decision = callTurboFilters(marker, Level.DEBUG);
        if (decision == FilterReply.NEUTRAL) {
            //进入这个方法
            return effectiveLevelInt <= Level.DEBUG_INT;
        } else if (decision == FilterReply.DENY) {
            return false;
        } else if (decision == FilterReply.ACCEPT) {
            return true;
        } else {
            throw new IllegalStateException("Unknown FilterReply value: " + decision);
        }
    }
    ```

### 日志如何定义的

```java
//MappedStatement#Builder方法，会初始化statementLog，根据前面存储的日志构造方法
logId = com.rockjh.study.mapper.UserMapper.selectById
mappedStatement.statementLog = LogFactory.getLog(logId)
```

### 设计模式

1. 采用适配模式分别去适配不同的日志实现。一般用来对接多个三方系统的时候使用，一般是两个系统不兼容的时候采用适配器模式
2. 使用了代理模式，采用代理来打印sql，一般是两个系统可以兼容但是调用起来不优雅