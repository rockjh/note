### 概述

插件是用来改变或者扩展mybatis的原有功能，mybatis的插件就是通过继承interceptor拦截器实现的，建议在完全理解插件后再进行插件开发，mybatis中能够使用插件进行拦截的接口和方法如下
    - Executor(update、query、flushStatement、commit、rollback、getTransaction、close、isClose)
        - StatementHandler(prepare、parmterize、batch、update、query)
        - ParameterHandler(getParameterObject、setParameters)
        - ResultSetHandler(handleResultSets、handleCursorResultSets、handleOutputParameters)

### 核心类

1. Interceptor，插件接口，自定义插件需要实现接口
2. Invocation，自定义插件中，关键复写的方法intercept的参数，用来封装target、method、arg关键参数
3. Plugin，插件的核心类，实现了InvocationHandler接口，可以用作代理类，根据已有插件，进行一些筛选条件吧，看插件符合条件，方便使用代理
4. InterceptorChain，存储已有的插件list，按顺序

### 自己写一个plugin

1. 需求，当查询操作时间超过这个阈值，记录里日志提供运维人员定位慢查询
2. 实现代码如下
    ```java
    @Component
    @Intercepts({
            @Signature(
                    type = StatementHandler.class,
                    method = "prepare",
                    args = {Connection.class, Integer.class}
                    )
        })
    public class SlowSqlInterceptor implements Interceptor {

        //慢查询阈值
        private long slowThreshold;

        /**
        * 方法拦截
        * @param invocation
        * @return
        * @throws Throwable
        */
        @Override
        public Object intercept(Invocation invocation) throws Throwable {
            long beginTime = System.currentTimeMillis();
            Object result = invocation.proceed();
            long endTime = System.currentTimeMillis();
            long runTime = endTime-beginTime;
            //根据执行时间是否是慢查询
            if(runTime>slowThreshold){
                //通过StatementHandler获取执行的sql
                StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
                BoundSql boundSql = statementHandler.getBoundSql();
                String sql = boundSql.getSql();

                System.out.println("sql语句："+sql);
                System.out.println("执行时间为:"+runTime+"毫秒，已经超过阈值");
                System.out.println("参数为："+ JSONObject.toJSONString(boundSql.getParameterObject()));
            }
            return result;
        }

        /**
        * 获取到拦截的对象，底层也是通过代理实现的，实际上是拿到一个目标代理对象
        * @param target
        * @return
        */
        @Override
        public Object plugin(Object target) {
            return Plugin.wrap(target,this);
        }

        /**
        * 获取设置的阈值等参数
        */
        @Override
        public void setProperties(Properties properties) {
            this.slowThreshold = Long.parseLong(properties.getProperty("mybatis.plug.param.slowThreshold"));
        }
    }
    ```
3. 注册为插件的方式
    - 直接在类上面添加@Component，注册为spring的bean
    - 通过mybatis的配置加入到拦截链中
        ```java
        @Bean
        public ConfigurationCustomizer configurationCustomizer() {
            return new ConfigurationCustomizer() {
                @Override
                public void customize(Configuration configuration) {
                    //插件拦截链采用了责任链模式，执行顺序和加入连接链的顺序有关
                    SlowSqlInterceptor slowSqlInterceptor = new SlowSqlInterceptor();
                    configuration.addInterceptor(slowSqlInterceptor);
                }
            };
        }
        ```
### 流程

1. 如上的一个plug拦截StatementHandler#prepare方法，首先代码执行到SimpleExecutor#doQuery，创建StatementHandler的实例
    ```java
    //获取Configuration
    Configuration configuration = ms.getConfiguration();
    //Configuration创建一个statementHandler
    StatementHandler handler = configuration.newStatementHandler(wrapper, ms, parameter, rowBounds, resultHandler, boundSql);
    //准备statement
    stmt = prepareStatement(handler, ms.getStatementLog());
    ```
2. Configuration#newStatementHandler，开始创建
    ```java
    //正常创建
    StatementHandler statementHandler = new RoutingStatementHandler(executor, mappedStatement, parameterObject, rowBounds, resultHandler, boundSql);
    //处理有插件增强的问题
    statementHandler = (StatementHandler) interceptorChain.pluginAll(statementHandler);
    ```
3. InterceptorChain#pluginAll
    ```java
    public Object pluginAll(Object target) {
        //获取声明的所有插件
        for (Interceptor interceptor : interceptors) {
            //会执行插件复写的plugin方法
            //例如上面的实例Plugin.wrap(target,this);
            target = interceptor.plugin(target);
        }
        return target;
    }
    ```
4. Plugin#wrap，是否符合插件，如果符合就是用plug类进行一个代理，Plugin实现了InvocationHandler接口
    ```java
    public static Object wrap(Object target, Interceptor interceptor) {
        Map<Class<?>, Set<Method>> signatureMap = getSignatureMap(interceptor);
        Class<?> type = target.getClass();
        //是否符合plugin定义时的目标对象
        Class<?>[] interfaces = getAllInterfaces(type, signatureMap);
        if (interfaces.length > 0) {
            //创建plug的代理类
            return Proxy.newProxyInstance(
                    type.getClassLoader(),
                    interfaces,
                    new Plugin(target, interceptor, signatureMap));
        }
        return target;
    }
    ```
5. Plugin#invoke，查看plugin代理的执行类
    ```java
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            Set<Method> methods = signatureMap.get(method.getDeclaringClass());
            //找到对应的执行方法，进入执行声明Plugin时复写的intercept方法
            if (methods != null && methods.contains(method)) {
                return interceptor.intercept(new Invocation(target, method, args));
            }
            //执行方法体
            return method.invoke(target, args);
        } catch (Exception e) {
            throw ExceptionUtil.unwrapThrowable(e);
        }
    }
    ```
### Pagehelper

1. maven引入相关配置
    ```xml
    <!-- mybatis的分页插件 -->
    <dependency>
        <groupId>com.github.pagehelper</groupId>
        <artifactId>pagehelper-spring-boot-starter</artifactId>
        <version>1.2.5</version>
    </dependency>
    ```
2. application.yml配置
    ```yml
    pagehelper:
        helperDialect: mysql
        reasonable: true
        supportMethodsArguments: true
        params: count=countSql
    ```
3. 在service中填写下面代码
    ```java
    //因为进行query拦截的时候，会判断是否需要进行分页，根据ThreadLocal设置的来进行判断
    //执行完之后，还会remove变量，因为容器一般都是使用线程池，所以要进行清除处理
    PageHelper.startPage(pageNum, pageSize);
    ```
4. 这样就可以完美使用分页插件了，他和普通插件一样，都是去修改sql，加上limit


