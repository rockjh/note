### 架构设计

​	![mybatis-架构](http://assets.processon.com/chart_image/5fcda2285653bb7156c1da13.png?_=1607313306906)

我们把Mybatis的功能架构分为三层：

- API接口层：提供给外部使用的接口API，开发人员通过这些本地API来操纵数据库。接口层一接收到调用请求就会调用数据处理层来完成具体的数据处理。

- 数据处理层：负责具体的SQL查找、SQL解析、SQL执行和执行结果映射处理等。它主要的目的是根据调用的请求完成一次数据库操作。

- 基础支撑层：负责最基础的功能支撑，包括连接管理、事务管理、配置加载和缓存处理，这些都是共用的东西，将他们抽取出来作为最基础的组件。为上层的数据处理层提供最基础的支撑

### MyBatis成员层次&职责

![](http://assets.processon.com/chart_image/5fcda8101e08536c596f6d29.png?_=1607324942265)

- SqlSession作为MyBatis工作的主要顶层API，表示和数据库交互的会话，完成必要数据库增删改查功能

- Executor MyBatis执行器，是MyBatis调度的核心，负责SQL语句的生成和查询缓存的维护

- StatementHandler封装了JDBC Statement操作，负责对JDBCstatement的操作，如设置参数、将Statement结果集转换成List集合。

- ParameterHandler负责对用户传递的参数转换成JDBC Statement所需要的参数

- ResultSetHandler负责将JDBC返回的ResultSet结果集对象转换成List类型的集合；

- TypeHandler负责java数据类型和jdbc数据类型之间的映射和转换

- MappedStatement维护了一条节点的封

- SqlSource负责根据用户传递的parameterObject，动态地生成SQL语句，将信息封装到BoundSql对象中，并返回

- BoundSql表示动态生成的SQL语句以及相应的参数信息

- Configuration MyBatis所有的配置信息都维持在Configuration对象之中

### 优点

1. 基于sql语句编程，灵活、动态sql、xml标签可重用
2. 对比jdbc消除了大量的重复代码
3. 很好的和各种数据库兼容（因为底层使用的是jdbc）
4. 跟spring很好的集成
5. 提供映射标签，支持对象与数据库的ORM字段关系映射；提供对象关系映射标签，支持对象关系组件维护


### 缺点

1. sql的编写工作量较大，特别是当关联表和字段多时
2. sql语句依赖数据库，导致数据库移植性差
3. 不支持级联删除，更新