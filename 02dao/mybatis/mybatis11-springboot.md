### 使用

1. 使用，引入maven包
    ```xml
    <dependency>
        <groupId>org.mybatis.spring.boot</groupId>
        <artifactId>mybatis-spring-boot-starter</artifactId>
        <version>1.3.2</version>
    </dependency>
    ```
2. application.yml的配置，存储在MybatisProperties类中
    ```yml
    mybatis:
        mapper-locations: classpath:mapping/*.xml
        type-aliases-package: com.rockjh.study.bean
    ```
3. 启动类中配置如下代码
    ```java
    @MapperScan(basePackages = "com.rockjh.study.mapper")
    ```

### 关键类

1. MapperFactoryBean，是作为一个mapper接口的FactoryBean
2. MybatisAutoConfiguration，mybatis和springboot结合的类入口，会根据条件注册各种所需要的类

### 源码解析

1. 进入@MapperScan类中，它import了MapperScannerRegistrar类，MapperScannerRegistrar#registerBeanDefinitions关键代码如下
    ```java
    //扫描刚刚配置的路径
    scanner.doScan(StringUtils.toStringArray(basePackages));
    ```
2. ClassPathMapperScanner#doScan，扫描并对扫描到的类进行后置处理
    ```java
    public Set<BeanDefinitionHolder> doScan(String... basePackages) {
        //父类的扫描包路径，映射为BeanDefinitionHolder
        Set<BeanDefinitionHolder> beanDefinitions = super.doScan(basePackages);

        if (beanDefinitions.isEmpty()) {
            logger.warn("No MyBatis mapper was found in '" + Arrays.toString(basePackages) + "' package. Please check your configuration.");
        } else {
            //后置处理扫描到的mapper接口
            processBeanDefinitions(beanDefinitions);
        }

        return beanDefinitions;
    }
    ```
3. ClassPathMapperScanner#processBeanDefinitions，扫描到的mapper接口的后置处理关键代码
    ```java
    //将class类型设置为MapperFactoryBean
    definition.setBeanClass(this.mapperFactoryBean.getClass());
    //设置注入的方式为按照类型注入
    definition.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
    ```
4. 上面介绍了包扫描，其实mybatis和springboot的总入口在这个类MybatisAutoConfiguration，它会去实例化SqlSessionFactoryBean，如果没有加上面的mapper扫描注解，会自动扫描包路径下@Mapper修饰的类，具体实现AutoConfiguredMapperScannerRegistrar
5. 实例化SqlSessionFactoryBean后，因为这个类实现了InitializingBean，会执行后置处理器afterPropertiesSet
    ```java
    public void afterPropertiesSet() throws Exception {
        this.sqlSessionFactory = buildSqlSessionFactory();
    }
    ```
6. SqlSessionFactoryBean#buildSqlSessionFactory构建sqlSeesionFactory，关键代码如下
    ```java
    //前面进行初始化各种的myabtis的所有配置
    ....
    //这里对mapper.xml进行翻译，解析
    XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(mapperLocation.getInputStream(),
              configuration, mapperLocation.toString(), configuration.getSqlFragments());
    xmlMapperBuilder.parse();
    ```

### mybatis是如何处理mapper方法级别的

1. 首先注入的mapper，是通过MapperFactoryBean来生成的，通过name找到对应的MapperFactoryBean，然后执行
    ```java
    public T getObject() throws Exception {
        //最后生成的一个MapperProxy的代理，所有的mapper都是这样类型
        //同一种mapper在不同的地方注入都是同一个bean
        return getSqlSession().getMapper(this.mapperInterface);
    }
    ```
2. 具体到执行sql的时候，因为是使用的SqlSessionTemplate，他使用了一个拦截器，前面说过，每次不同的事务，他都会去生成一个新的sqlSession，所以每次执行都是线程安全的