### JDBC问题

```java
public class FirstExample {

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/EMP";

    //  Database credentials
    static final String USER = "username";
    static final String PASS = "password";

    public static void main(String[] args) {
        Connection conn;
        Statement stmt;
        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT id, first, last, age FROM Employees";
            ResultSet rs = stmt.executeQuery(sql);

            //STEP 5: Extract data from result set
            while (rs.next()) {
                //Retrieve by column name
                int id = rs.getInt("id");
                int age = rs.getInt("age");
                String first = rs.getString("first");
                String last = rs.getString("last");

                //Display values
                System.out.print("ID: " + id);
                System.out.print(", Age: " + age);
                System.out.print(", First: " + first);
                System.out.println(", Last: " + last);
            }
            //STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException | ClassNotFoundException se) {
            //省略异常处理
        } finally {
            //省略连接关闭
        }
    }
}
```



1. sql语句在代码中硬编码，造成代码不易维护
2. 使用preparedStatement向占位符传参存在硬编码，因为sql语句的where条件不一定，可能多也可能少，修改sql还要修改java代码，不易维护
3. 对结果集解析存在硬编码（查询列表），sql变化导致解析代码变化，系统不易维护

### 问题解决思路

1. 数据库频繁创建、释放资源，采用连接池
2. sql语句及参数硬编码，采用配置文件
3. 利用反射解析封装返回结果集

### 自定义框架设计

#### 使用端

1. 提供核心配置文件
   - sqlMapConfig.xml，存放数据源信息，引入mapper.xml
   - mapper.xml，sql语句的配置文件信息

2. sqlMapConfig.xml

   ```xml
   <configuration>
       <!--数据库配置信息-->
       <dataSource>
           <property name="driverClass" value="com.mysql.jdbc.Driver"></property>
           <property name="jdbcUrl" value="jdbc:mysql:///zdy_mybatis"></property>
           <property name="username" value="root"></property>
           <property name="password" value="root"></property>
       </dataSource>
       <!--存放mapper.xml的全路径-->
       <mapper resource="UserMapper.xml"></mapper>
   </configuration>
   ```

3. mapper.xml

   ```xml
   <mapper namespace="com.lagou.dao.IUserDao">
   
       <!--sql的唯一标识：namespace.id来组成 ： statementId-->
       <select id="findAll" resultType="com.lagou.pojo.User" >
           select * from user
       </select>
   
       <select id="findByCondition" resultType="com.lagou.pojo.User" paramterType="com.lagou.pojo.User">
           select * from user where id = #{id} and username = #{username}
       </select>
   </mapper>
   ```

4. IUserDao

   ```java
   public interface IUserDao {
       //查询所有用户
       List<User> findAll() throws Exception;
       //根据条件进行用户查询
       User findByCondition(User user) throws Exception;
   }
   ```

5. IPersistenceTest

   ```java
   InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
   SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
   SqlSession sqlSession = sqlSessionFactory.openSession();
   
   //调用
   User user = new User();
   user.setId(1);
   user.setUsername("张三");
   IUserDao userDao = sqlSession.getMapper(IUserDao.class);
   
   List<User> all = userDao.findAll();
   ```

#### 框架端

1. 读取配置文件，将配置文件实例化为javaBean来存储

   - Configuration，存储数据库的基本信息，Map<唯一标识,MappedStatement>
   - MappedStatement，sql语句、statement类型、输入输出java类型
   
2. 解析配置文件，创建SqlSessionFactoryBuilder类，使用dom4j解析配置文件封装到Configuration和MappedStatement中
   ```java
   //解析sqlMapConfig.xml
   Document document = new SAXReader().read(inputStream);
   //<configuration>
   Element rootElement = document.getRootElement();
   List<Element> list = rootElement.selectNodes("//property");
   Properties properties = new Properties();
   for (Element element : list) {
      String name = element.attributeValue("name");
      String value = element.attributeValue("value");
      properties.setProperty(name,value);
   }
   
   ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
   comboPooledDataSource.setDriverClass(properties.getProperty("driverClass"));
   comboPooledDataSource.setJdbcUrl(properties.getProperty("jdbcUrl"));
   comboPooledDataSource.setUser(properties.getProperty("username"));
   comboPooledDataSource.setPassword(properties.getProperty("password"));
   
   configuration.setDataSource(comboPooledDataSource);
   
   //mapper.xml解析: 拿到路径--字节输入流---dom4j进行解析
   List<Element> mapperList = rootElement.selectNodes("//mapper");
   
   for (Element element : mapperList) {
      String mapperPath = element.attributeValue("resource");
      InputStream resourceAsSteam = Resources.getResourceAsSteam(mapperPath);
      XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(configuration);
      xmlMapperBuilder.parse(resourceAsSteam);
   
   }
   ```
   ```java
   //解析userMapper.xml
   Document document = new SAXReader().read(inputStream);
   Element rootElement = document.getRootElement();
   
   String namespace = rootElement.attributeValue("namespace");
   
   List<Element> list = rootElement.selectNodes("//select");
   for (Element element : list) {
      String id = element.attributeValue("id");
      String resultType = element.attributeValue("resultType");
      String paramterType = element.attributeValue("paramterType");
      String sqlText = element.getTextTrim();
      MappedStatement mappedStatement = new MappedStatement();
      mappedStatement.setId(id);
      mappedStatement.setResultType(resultType);
      mappedStatement.setParamterType(paramterType);
      mappedStatement.setSql(sqlText);
      String key = namespace+"."+id;
      configuration.getMappedStatementMap().put(key,mappedStatement);
   
   }
   ```
   
3. 创建SqlSessionFactory及其默认实现类DefaultSqlSessionFactory，用来创建SqlSession

   ```java
   public DefaultSqlSessionFactory(Configuration configuration) {
       this.configuration = configuration;
   }
   
   @Override
   public SqlSession openSession() {
       return new DefaultSqlSession(configuration);
   }
   ```

4. 创建SqlSeesioon及其默认实现类DefaultSqlSession

   ```java
   @Override
   public <E> List<E> selectList(String statementid, Object... params) throws Exception {
   
       //将要去完成对simpleExecutor里的query方法的调用
       SimpleExecutor simpleExecutor = new SimpleExecutor();
       MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementid);
       List<Object> list = simpleExecutor.query(configuration, mappedStatement, params);
   
       return (List<E>) list;
   }
   
   //根据条件查询单个，主要是调用selectList
   <T> T selectOne(String statementid,Object... params) throws Exception;
   
   @Override
   public <T> T getMapper(Class<?> mapperClass) {
       // 使用JDK动态代理来为Dao接口生成代理对象，并返回
   
       Object proxyInstance = Proxy.newProxyInstance(DefaultSqlSession.class.getClassLoader(), new Class[]{mapperClass}, new InvocationHandler() {
           @Override
           public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
               // 底层都还是去执行JDBC代码 //根据不同情况，来调用selctList或者selectOne
               // 准备参数 1：statmentid :sql语句的唯一标识：namespace.id= 接口全限定名.方法名
               // 方法名：findAll
               String methodName = method.getName();
               String className = method.getDeclaringClass().getName();
   
               String statementId = className+"."+methodName;
   
               // 准备参数2：params:args
               // 获取被调用方法的返回值类型
               Type genericReturnType = method.getGenericReturnType();
               // 判断是否进行了 泛型类型参数化
               if(genericReturnType instanceof ParameterizedType){
                   List<Object> objects = selectList(statementId, args);
                   return objects;
               }
   
               return selectOne(statementId,args);
   
           }
       });
   
       return (T) proxyInstance;
   }
   ```

   

5. 创建Executor接口及实现类SimpleExecutor，主要封装参数替换、生成动态sql、jdbc操作、封装返回结果

   ```java
   public <E> List<E> query(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception {
       // 1. 注册驱动，获取连接
       // 2. 获取sql语句 : select * from user where id = #{id} and username = #{username}
       //转换sql语句： select * from user where id = ? and username = ? ，转换的过程中，还需要对#{}里面的值进行解析存储
       // 3.获取预处理对象：preparedStatement
       // 4. 设置参数，获取到了参数的全路径
       // 5. jdbc执行sql
       // 6. 利用反射，封装返回结果集
   }
   ```

   