### 使用
1. 通过xml方式
    ```xml
    //bean.xml
    <bean id="person" class="com.rockjh.demo.Person">
        <property name="name" value="rockjh"/>
        <property name="age" value="19"/>
    </bean>

    ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("bean.xml");
    Person person = (Person) app.getBean("person");
    System.out.println(person);
    ```
2. 通过注解方式

    ```java
    //配置文件
    @Configuration
    public class MainConfig {
        @Bean
        public Person person(){
            return new Person("rockjh","19");
        }
    }

    AnnotationConfigApplicationContext app = new AnnotationConfigApplicationContext(MainConfig.class);
    Person person = (Person) app.getBean("person");
    System.out.println(person);
    ```
注解方式更加简单明了，后面几乎都将使用注解

### 常用注解

1. 包扫描，@ComponentScan等价于<context:component-scan base-package="com.rockjh"/>
	- basePackages基础扫描包
	- includeFilters,excludeFilters是用于包含和排除的筛选器
2. @Configuration、@Bean、@Scope

	- @Configuration相当于Beans标签，主要用于配置类
	- @Bean相当与Bean标签，所属类必须有@Configuration或@Component才有效
	- @Scope用于定义bean的生存范围
		- singleton全局有且仅有一个实例（默认）
		- prototype原型模式，每次获取Bean都会获取一个新的实例
		- request,session,global session不常用，使用他们的时候需要初始化web的时候做出配置

3. @Component、@Service、@Controller、@RestController、@Repository、@Conditional、@PropertySource、@Primary

	- @Component用来标识一个Bean，被修饰的class会被IOC加载为spring bean
	- @Service对应的是业务层的Bean
	- @Controller对应的是控制层的Bean
	- @RestController对应的是控制层的Bean，等同于@ResponseBody和@Controller的组合
	- @Repository对应的是数据访问层的Bean
	- @Conditional按照一定的条件进行判断，满足条件给容器注册bean，用于class、method上，一个注解可以同时有多个条件，条件类实现Condition接口
	- @PropertySource用于导入配置文件，使用classpath:/xxx、file:/分别指定类路径和文件系统，可以和@Configuration和@ConfigurationProperties联用，将属性注入到当前组件中，@ConfigurationProperties可以指定前缀
	- @Primary使用@Autowire自动装配时当出现多个Bean候选者时，被注解为@Primary的Bean将作为首选者

4. @Autowired、@Resource、@Qualifier,@Lookup,@Value,@Async

	- @Autowired按照类型注入Bean，默认required=true
	- @Qualifier可以和@Autowired联合使用，达到按照beanName注入的目的
	- @Resource可以按照name和type注入，如果name和type都没有指定，默认按照name注入
	- @Async异步执行，需要调用@EnableAsync启用，在一个类中，调用主方法是加了注解才可行
	- @Lookup当组件scope属性是singleton时，如果要想注入一个新的实例，可以在一个抽象方法中使用该注解，返回新的实例
	- @Value可以用来获取配置文件中的值@Value("${name.name}")，也可以用@Value("#{beanId.attr}")来获取另外一个Bean中的属性，@Value("#{${name}}")可以配合@PropertySource获取propertes文件中的数据，使用示例如下：
		```java
		//properties文件内容
		remote.path={\
			station_map_point_task:{'url':'/api/v1/scene/rect_monitor_obj','method':'post'}\
		}

		//类的注解配置
		@Component
		@PropertySource(value="classpath:remote_path_priperties")

		//通过@Value获取值
		@Value("#{${remote.path}}")
		private Map<String,Map<String,String>> pathMap;
		```
5. @Lazy
   
    - @Lazy主要针对单实例，容器启动的时候不加载，仅当第一次使用或者获取bean的时候初始化

### @Import

允许通过他引入@Configuration注解的类，引入ImportSelector接口和ImportBeanDefinitionRegistrar接口实现的类，定义如下：
```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Import {

    /**
     * {@link Configuration}, {@link ImportSelector}, {@link * * ImportBeanDefinitionRegistrar}
     * or regular component classes to import.
     */
    Class<?>[] value();

}
```
在springboot中对@Import的处理方式

1. 判断如果被Import的是ImportSelector接口的实现，会初始化被引用的这个类，然后调用它的selectImports方法的方法
    ```java
    //引入该类，可以将类Dog和类Cat实例化为Bean
    public class ImportSelectorClass implements ImportSelecto{

        @Override
        public String[] selectImports(AnnotationMetadata importingClassMetadata) {
            //importingClassMetadata.getAnnotationTypes()
            return new String[]{"com.rockjh.Dog","com.rockjh.Cat"};
        }
    }
    ```

2. 判断如果被Import的是importBeanDefinitionRegistrar接口的实现，会调用registerBeanDefinitions方法，直接进行注册
    ```java
    public class ImportBeanDefinitionRegistrarClass implements ImportBeanDefinitionRegistrar {
      @Override
    	public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata,
                                          BeanDefinitionRegistry registry) {
        //如果存在bean-dog的话，就注册cat类
        if (registry.containsBeanDefinition("com.rockjh.Dog")) {
            RootBeanDefinition beanDefinition = new RootBeanDefinition(Cat.class);
            registry.registerBeanDefinition("cat",beanDefinition);
        }
    }
    ```

3. 如果引用的是一个正常的Class，这样可以在BeanFactory中得到bean

### @AliasFor

1. 可以注解到自定义注解中的两个属性上，表示互为别名，这时候两个属性的值是同步的，指定其中一个，另外一个也是一样的值，如果两个都指定，指定的两个值一定要相同，否则会报错
	```java
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	@Documented
	@Inherited
	public @interface MyAnnotation {
		@AliasFor(attribute = "location")
		String value() default "";

		@AliasFor(attribute = "value")
		String location() default "";
	}
	```
2. 继承父注解的属性，可以拥有更加强大的功能
	```java
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	@Documented
	@Inherited
	@MyAnnotation
	public @interface SubMyAnnotation {

		//指定的和父类注解的location属性一致
		@AliasFor(value = "location", annotation = MyAnnotation.class)
		String subLocation() default "";

		//缺省指明继承的父注解的中的属性名称，则默认继承父注解中同名的属性名
		@AliasFor(annotation = MyAnnotation.class)
		String value() default "";
	}
	```
> @AliasFor注解是spring注解，只能通过AnnotationUtils工具类中的方法获取才有效