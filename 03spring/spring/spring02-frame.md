### spring体系结构

![spring架构](http://assets.processon.com/chart_image/5fd06d7a07912906da46bc11.png?_=1607495647447)

1. Spring核心容器(Core Container) 容器是Spring框架最核心的部分，它管理着Spring应用中bean的创建、配置和管理，也就是IOC的功能

2. 面向切面编程(AOP)/Aspects Spring对面向切面编程提供了丰富的支持

3. 数据访问与集成(Data Access/Integration)，Spring的JDBC和DAO模块封装了大量样板代码，Spring AOP为数据访问提供了事务管理服务，同时Spring还对ORM进行了集成，如Hibernate、MyBatis等

5. Web该模块提供了SpringMVC框架给Web应用，还提供了多种构建和其它应用交互的远程调用方案

6. Test为了使得开发者能够很方便的进行测试，Spring为使用Servlet、JNDI等编写单元测试提供了一系列的mock对象实现
7. 一般的jar功能
   - spring-core，spring核心，他是框架最基础的部分，提供ioc和依赖注入特性
   - spring-context，spring上下文容器，他是BeanFactory功能加强的一个子接口
   - spring-web，他提供Web应用开发的支持，比如模板引擎
   - spring-mvc，他针对Web应用中MVC思想的实现
   - spring-dao，提供对JDBC抽象层，简化了JDBC编码，同时，编码更具有健壮性
   - spring-orm，它支持用于流行的orm框架的整合，spring+mybatis、spring+jpa
   - spring-aop，面向切面编程，他提供了与aop联盟兼容的编程实现

