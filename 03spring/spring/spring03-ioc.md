### 定义

1. IOC容器（控制反转），我们不需要自己去new了，将对象交给IOC容器去实例化并且管理，我们需要使用哪个对象，向IOC容器要就行了，站在对象的角度就是对象的创建权力交给了IOC即控制反转
2. DI（依赖注入），站在容器的角度，容器会把对象依赖的其他对象注入
3. 引入IOC的好处，解耦、对象统一管理

### IOC容器体系

![IOC容器体系](http://assets.processon.com/chart_image/5fd089ca6376893e2c72fd81.png?_=1607502504525)

### 容器体系说明

1. BeanFactory是根容器，不能被实例化，它定义了所有IOC容器必须遵从的一套原则，具体的容器实现可以增加额外的功能
   - ClassPathXmlApplicationContext，包含了解析xml等一系列的内容
   - AnnotationConfigApplicationContext，包含了注解解析等一系列的内容
   - FileSystemXmlApplicationContext，包含了解析xml等一系列的内容

2. ApplicationContext是容器的高级接口，也叫上下文，比BeanFactory拥有更多的功能，比如说国际化支持和资源访问(xml，java配置类)等等
3. Spring的IOC继承体系非常聪明，需要使用哪个层次用哪个层次即可，充分体现了接口隔离原则

### 容器的核心方法

所有的容器核心都会进入AbstractApplicationContext#refresh

```java
synchronized (this.startupShutdownMonitor){
    //刷新前的预处理
    prepareRefresh();
  	//获取BeanFactory，默认实现是DefaultListableBeanFactory
		//加载BeanDefition，并注册到BeanDefitionRegistry
    ConfigurableListableBeanFactory beanFactory=obtainFreshBeanFactory();
    //BeanFactory的预准备工作(BeanFactory进行一些设置，比如context的类加载器等
    prepareBeanFactory(beanFactory);
    try{
        //子类添加对应的BeanFactoryPostProcess
        postProcessBeanFactory(beanFactory);
        //实例化并调用实现了BeanFactoryPostProcessor接口的Bean
        invokeBeanFactoryPostProcessors(beanFactory);
      	//上面就已经加载了所有的BeanPostProcess的BeanDefinition，包括注解、配置文件等方式
        //注册BeanPostProcessor(Bean的后置处理器)，在创建bean的前后等执行
        registerBeanPostProcessors(beanFactory);
        //初始化MessageSource组件(做国际化功能;消息绑定，消息解析)
        initMessageSource();
        //初始化事件派发器
        initApplicationEventMulticaster();
        //留给子容器(子类)，子类可以重写这个方法，在容器刷新的时候可以自定义逻辑
        //springboot的tomcat容器就是通过重写这个方法来加载的
        onRefresh();
        //将容器中所有的ApplicationListener注册进来
        registerListeners();
      	//初始化所有剩下的非懒加载的单例bean
      	//填充属性
      	//初始化方法调用(比如调用afterPropertiesSet方法、init-method方法)
      	//调用BeanPostProcessor(后置处理器)对实例bean进行后置处
        finishBeanFactoryInitialization(beanFactory);
      	//完成context的刷新。主要是调用LifecycleProcessor的onRefresh()方法，并且发布事件
        finishRefresh();
    }catch(BeansException ex){
        if(logger.isWarnEnabled()){
        logger.warn("Exception encountered during context initialization - "+
        "cancelling refresh attempt: "+ex);
        }
        destroyBeans();
        cancelRefresh(ex);
        throw ex;
    }finally{
        resetCommonCaches();
    }
}
```

1. obtainFreshBeanFactory，获取beanFactory

   - refreshBeanFactory，设置容器的刷新状态
   - getBeanFactory，获取通过构造函数创建的DefaultListableBeanFactory

2. prepareBeanFactory，beanFactory的预准备工作，设置一些属性

   - setBeanClassLoader，设置BeanFactory的类加载器，支持表达式解析器
   - addBeanPostProcessor，添加BeanPostProcessor【ApplicationContextAwareProcessor】
   - ignoreDependencyInterface，设置忽略的自动装配的接口EnvironmentAware、EmbeddedValueResolverAware等
   - registerResolvableDependency，注解可以解析的自动装配，我们可以直接在任务组件中自动注入BeanFactory、ApplicationContext、ApplicationEventPublisher等（其实就是容器本身）
   - addBeanPostProcessor，添加BeanPostProcessor【ApplicationListenerDetector】
   - registerSingleton，给BeanFactory中注册一些能用的组件SystemProperties、SystemEnvironment

3. postProcessBeanFactory，beanFactory准备工作完成后进行的后置处理工作

   - 子类通过重写这个方法来在BeanFactory创建并预准备完成

4. invokeBeanFactoryPostProcessors，执行BeanFactory的后置处理器

   - 有两个后置处理器BeanFactoryPostProcessor以及他的子接口BeanDefinitionRegistryPostProcessor
   - 可以自己分别实现两个接口进行一下beanFactory的后置操作
   - 先执行BeanDefinitionRegistryPostProcessor#postProcessBeanDefinitionRegistry方法
   - 再执行BeanDefinitionRegistryPostProcessor#postProcessBeanFactory
   - 最后执行BeanFactoryPostProcesso#postProcessBeanFactory

5. registerBeanPostProcessors，注册Bean的后置处理器

   ```java
   //基本上获取某一个类型的数据，都是这样获取的
   String[] postProcessorNames = beanFactory.getBeanNamesForType(BeanPostProcessor.class, true, false);
   //然后再去循环获取具体的对象，再判断类型这些，顺序初始化
   BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
   ```

   

   - 可以自己实现BeanPostProcessor，会在bean进行初始化的时候回调的
   - 具体常用的BeanPostProcessor作用在Bean的声明周期文章中，例如Aop

6. initMessageSource，国际化资源

   - 容器中是否注册对应的MessageSource类型的bean
   - 如果没有注册初始化一个空的，进行赋值，注册了也进行赋值

7. initApplicationEventMulticaster，事件派发器

   - 同理国际化资源，判断容器中是否注册了对应的bean
   - 如果没有注册初始化一个空的，进行赋值，注册了也进行赋值

8. onRefresh

   - 留给子容器(子类)，子类可以重写这个方法，在容器刷新的时候可以自定义逻辑
   - springboot的tomcat容器就是通过重写这个方法来加载的

9. registerListeners，将容器中所有的ApplicationListener注册进来

   - 从我们的客器中拿到所有的ApplicationListener
   - 将监听注册到事件派发器中的属性中

10. finishBeanFactoryInitialization，将容器中所有的单实例非懒加载的bean初始化

    - 详情见Bean生命周期中的Bean创建过程