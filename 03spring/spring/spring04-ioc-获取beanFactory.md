### 获取BeanFactory

![创建BeanFactory](http://assets.processon.com/chart_image/5fd0948d1e085306e0ea6d6b.png?_=1607505210349)

会返回DefaultListableBeanFactory实例

### BeanDefinition加载解析

```java
//接上面第4步，xml方式加载，注解类似，换成AnnotatedBeanDefinitionReader
AbstractRefreshableApplicationContext#loadBeanDefinitions
AbstractXmlApplicationContext#loadBeanDefinitions
AbstractBeanDefinitionReader#loadBeanDefinitions
XmlBeanDefinitionReader#loadBeanDefinitions
XmlBeanDefinitionReader#doLoadBeanDefinitions
XmlBeanDefinitionReader#registerBeanDefinitions

```

1. XmlBeanDefinitionReader#registerBeanDefinitions

   ```java
   BeanDefinitionDocumentReader documentReader = createBeanDefinitionDocumentReader();
   //获取已经有的BeanDefinition数量
   int countBefore = getRegistry().getBeanDefinitionCount();
   //重点关注createReaderContext、registerBeanDefinitions
   documentReader.registerBeanDefinitions(doc, createReaderContext(resource));
   return getRegistry().getBeanDefinitionCount() - countBefore;
   ```

2. XmlBeanDefinitionReader#createReaderContext

   ```java
   public XmlReaderContext createReaderContext(Resource resource) {
   		return new XmlReaderContext(resource, this.problemReporter, this.eventListener,
   				this.sourceExtractor, this, getNamespaceHandlerResolver());
   }
   
   public NamespaceHandlerResolver getNamespaceHandlerResolver() {
   		if (this.namespaceHandlerResolver == null) {
   			this.namespaceHandlerResolver = createDefaultNamespaceHandlerResolver();
   		}
   		return this.namespaceHandlerResolver;
   }
   
   protected NamespaceHandlerResolver createDefaultNamespaceHandlerResolver() {
   		ClassLoader cl = (getResourceLoader() != null ? getResourceLoader().getClassLoader() : getBeanClassLoader());
     	//创建默认的DefaultNamespaceHandlerResolver
   		return new DefaultNamespaceHandlerResolver(cl);
   }
   ```

3. DefaultBeanDefinitionDocumentReader#registerBeanDefinitions#doRegisterBeanDefinitions

   ```java
   //前面有一些校验，这里是关键代码
   parseBeanDefinitions(root, this.delegate);
   ```

4. DefaultBeanDefinitionDocumentReader#parseBeanDefinitions#parseDefaultElement

   ```java
   private void parseDefaultElement(Element ele, BeanDefinitionParserDelegate delegate) {
     	//import
   		if (delegate.nodeNameEquals(ele, IMPORT_ELEMENT)) {
   			importBeanDefinitionResource(ele);
   		}
   		else if (delegate.nodeNameEquals(ele, ALIAS_ELEMENT)) {
   			processAliasRegistration(ele);
   		}
     	//bean
   		else if (delegate.nodeNameEquals(ele, BEAN_ELEMENT)) {
   			processBeanDefinition(ele, delegate);
   		}
     	//beans
   		else if (delegate.nodeNameEquals(ele, NESTED_BEANS_ELEMENT)) {
   			// recurse
   			doRegisterBeanDefinitions(ele);
   		}
   }
   ```

   

5. DefaultBeanDefinitionDocumentReader#processBeanDefinition

   ```java
   //关键代码，注册
   BeanDefinitionReaderUtils.registerBeanDefinition(bdHolder, getReaderContext().getRegistry());
   ```

6. BeanDefinitionReaderUtils#registerBeanDefinition

   ```java
   //注册
   registry.registerBeanDefinition(beanName, definitionHolder.getBeanDefinition());
   ```

7. DefaultListableBeanFactory#registerBeanDefinition

   ```java
   //放进定义的Map
   this.beanDefinitionMap.put(beanName, beanDefinition);
   this.beanDefinitionNames.add(beanName);
   ```

8. 所谓的注册就是把封装的XML中定义的Bean信息封装为BeanDefinition对象之后放入一个Map中，BeanFactory是以Map的结构组织这些BeanDefinition的