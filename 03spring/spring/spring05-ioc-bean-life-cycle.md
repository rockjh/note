### 创建bean的几种方式

1. 使用@Bean和Configuration结合
2. 使用@Import结合ImportSelector的超类
3. 使用@Import结合ImportBeanDefinitionRegistrar的超类
4. 继承FactoryBean接口，重写getObject、getObjectType、isSingleton方法，返回实例
    ```java
    @Component
    public class MonkeyFactoryBean implements FactoryBean<Monkey> {

        @Override
        public Monkey getObject() throws Exception {
            return new Monkey();
        }

        @Override
        public Class<?> getObjectType() {
            return Monkey.class;
        }

        @Override
        public boolean isSingleton() {
            return true;
        }
    }
    ```
    ```java
    //获取applicationContext
    ApplicationContext app;
    //factoryBean本身的实例
    Object monkeyFactoryBean = app.getBean("&monkeyFactoryBean");
    //获取monkey实例
    Object Monkey = app.getBean("monkeyFactoryBean");
    ```
> 以上的几种形式必须在@ComponentScan的扫描包中

### Bean的创建具体过程

1. AbstractApplicationContext#refresh，刷新容器
2. AbstractApplicationContext#finishBeanFactoryInitialization，初始化单例非懒加载的bean
3. DefaultListableBeanFactory#preInstantiateSingletons，通过beanFactory获取bean
4. AbstractBeanFactory#getBean，获取bean
5. AbstractBeanFactory#doGetBean，获取bean
6. DefaultSingletonBeanRegistry#getSingleton创建单例
7. singletonFactory.getObject()，单例工厂获取对象
8. AbstractAutowireCapableBeanFactory#createBean，创建bean
9. AbstractAutowireCapableBeanFactory#doCreateBean，创建bean，使用反射创建beanInstance，再使用BeanWrapper对其进行封装
10. AbstractAutowireCapableBeanFactory#initializeBean，初始化bean，重中之重，bean的生命周期就在这里

### Bean的声明周期

1. 接上面的第10步，方法中的主要代码如下
    ```java
    //对bean进行BeanPostProcessor的前置处理
    wrappedBean = applyBeanPostProcessorsBeforeInitialization(wrappedBean, beanName);

    //对bean一些初始化操作
    invokeInitMethods(beanName, wrappedBean, mbd);

    //对bean进行BeanPostProcessor的后置处理
    wrappedBean = applyBeanPostProcessorsAfterInitialization(wrappedBean, beanName);
    ```
    
2. applyBeanPostProcessorsBeforeInitialization方法代码解析
    ```java
    //循环每一个BeanPostProcessor的实现，调用其方法对bean做其他处理
    for (BeanPostProcessor processor : getBeanPostProcessors()) {
        Object current = processor.postProcessBeforeInitialization(result, beanName);
        if (current == null) {
            return result;
        }
        result = current;
    }

    //postProcessBeforeInitialization举例ApplicationContextAwareProcessor的处理
    //它主要是用来设置环境等等操作的
    //首先判断bean是否实现了一些环境类，如果没有实现直接返回，如果实现了，执行invokeAwareInterfaces方法
    //ConfigurableApplicationContext applicationContext是下面这些实现接口的子类，向上转型，可以直接设置
    if (bean instanceof EnvironmentAware) {
        ((EnvironmentAware) bean).setEnvironment(this.applicationContext.getEnvironment());
    }
    if (bean instanceof EmbeddedValueResolverAware) {
        ((EmbeddedValueResolverAware) bean).setEmbeddedValueResolver(this.embeddedValueResolver);
    }
    if (bean instanceof ResourceLoaderAware) {
        ((ResourceLoaderAware) bean).setResourceLoader(this.applicationContext);
    }
    if (bean instanceof ApplicationEventPublisherAware) {
        ((ApplicationEventPublisherAware) bean).setApplicationEventPublisher(this.applicationContext);
    }
    if (bean instanceof MessageSourceAware) {
        ((MessageSourceAware) bean).setMessageSource(this.applicationContext);
    }
    if (bean instanceof ApplicationContextAware) {
        //所以只要我们实现了这个接口，复写这个方法就可以得到ApplicationContext
        ((ApplicationContextAware) bean).setApplicationContext(this.applicationContext);
    }
    ```
    
    ```java
    //postProcessBeforeInitialization举例InitDestroyAnnotationBeanPostProcessor的处理，他是专门支持JSR-250的一个处理器,@PostConstruct
    //直接到postProcessBeforeInitialization方法
    //找到注解修饰的方法，直接执行该方法
    LifecycleMetadata metadata = findLifecycleMetadata(bean.getClass());
    metadata.invokeInitMethods(bean, beanName);
    ```
    
    
    
3. invokeInitMethods方法代码解析

    ```java
    //如果bean实现了InitializingBean接口，就执行afterPropertiesSet方法进行bean的初始化
    boolean isInitializingBean = (bean instanceof InitializingBean);
    if (isInitializingBean && (mbd == null || !mbd.isExternallyManagedInitMethod("afterPropertiesSet"))) {
        if (logger.isTraceEnabled()) {
            logger.trace("Invoking afterPropertiesSet() on bean with name '" + beanName + "'");
        }
        if (System.getSecurityManager() != null) {
            try {
                AccessController.doPrivileged((PrivilegedExceptionAction<Object>) () -> {
                    ((InitializingBean) bean).afterPropertiesSet();
                    return null;
                }, getAccessControlContext());
            }
            catch (PrivilegedActionException pae) {
                throw pae.getException();
            }
        }
        else {
            ((InitializingBean) bean).afterPropertiesSet();
        }
    }
    
    //如果bean使用@Bean注入的，initMethod属性值设置了之后，就会执行下面的代码
    //前提，initMethod和afterPropertiesSet不能相同等
    if (mbd != null && bean.getClass() != NullBean.class) {
        String initMethodName = mbd.getInitMethodName();
        if (StringUtils.hasLength(initMethodName) &&
                !(isInitializingBean && "afterPropertiesSet".equals(initMethodName)) &&
                !mbd.isExternallyManagedInitMethod(initMethodName)) {
            invokeCustomInitMethod(beanName, bean, mbd);
        }
    }
    ```
    

4. 所以Bean的生命周期就很明确了，执行顺序依次是

    - @PostConstruct，对应的销毁@PreDestroy

    - 实现InitializingBean接口，对应的销毁实现DisposableBean接口

    - 使用@Bean注入initMethod，对应的销毁destroyMethod


### 其他

1. BeanPostProcessor，spring自己实现了很多后置处理器，包括AutowiredAnnotationBeanPostProcessor等，一般有bean的赋值，注入其他组件，生命周期注解功能等，可以自己实现
2. Aware系列的接口，都是在bean初始化时，可以做一些操作，比如获取beanName，获取applicationContext等
3. bean优先权的问题，一般通过@Component扫描得到的比@Bean的优先权要大

> 看源码小技巧，比如我看注解，@Autowire，源码中有@see AutowiredAnnotationBeanPostProcessor一般后面的类就是相关的处理类，注解就是注解处理类
