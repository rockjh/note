### 问题现象

类A依赖于类B，类B依赖于类A，形成一种环形状，在IOC初始化singleton模式时

- 先初始化A，反射实例化后（还不是一个完整的SpringBean），会去装配属性B

- 再初始化B，反射实例化后（还不是一个完整的SpringBean），会去装配属性A，但是这里A还没有初始化完全，这就看似形成了一个环形

  ```java
  //AbstractBeanFactory#doGetBean会对原型模式的循环依赖直接抛出异常
  if (isPrototypeCurrentlyInCreation(beanName)) {
  	throw new BeanCurrentlyInCreationException(beanName);
  }
  ```

  

  ```java
  @Component
  public class A {
      @Autowired
      private B b;
  }
  
  @Component
  public class B {
      @Autowired
      private A a;
  } 
  ```

### Spring三级缓存解决循环依赖

1. 回到创建Bean的流程AbstractAutowireCapableBeanFactory#doCreateBean，此时先创建类A

   ```java
   //...省略一些代码
   //反射创建对象实例
   instanceWrapper = createBeanInstance(beanName, mbd, args);
   //...省略一些代码
   //将利用反射实例化了的对象（不是完整的SpringBean）放进三级缓存
   addSingletonFactory(beanName, () -> getEarlyBeanReference(beanName, mbd, bean));
   //...省略一些代码
   //装配属性
   populateBean(beanName, mbd, instanceWrapper);
   ```

2. AbstractAutowireCapableBeanFactory#addSingletonFactory，加入到三级缓存

   ```java
   protected void addSingletonFactory(String beanName, ObjectFactory<?> singletonFactory) {
       Assert.notNull(singletonFactory, "Singleton factory must not be null");
     	//singletonObjects一级缓存，已经实例化好的完整的SpringBean
     	//earlySingletonObjects二级缓存，对于没有创建完全的bean
     	//需要被引用，应该是被处理过的bean，例如加入代理
     	//singletonFactories三级缓存，已经实例化好的不完整的SpringBean
       synchronized (this.singletonObjects) {
           if (!this.singletonObjects.containsKey(beanName)) {
               this.singletonFactories.put(beanName, singletonFactory);
               this.earlySingletonObjects.remove(beanName);
               this.registeredSingletons.add(beanName);
           }
       }
   }
   ```

   

3. AbstractAutowireCapableBeanFactory#getEarlyBeanReference，一个表达式，后面会用到，对三级缓存中的不完整的SpringBean进行代理BeanPostProcess（AbstractAutoProxyCreator）处理，然后放入二级缓存

   ```java
   protected Object getEarlyBeanReference(String beanName, RootBeanDefinition mbd, Object bean) {
       Object exposedObject = bean;
       if (!mbd.isSynthetic() && hasInstantiationAwareBeanPostProcessors()) {
           for (BeanPostProcessor bp : getBeanPostProcessors()) {
               if (bp instanceof SmartInstantiationAwareBeanPostProcessor) {
                   SmartInstantiationAwareBeanPostProcessor ibp = (SmartInstantiationAwareBeanPostProcessor) bp;
                   exposedObject = ibp.getEarlyBeanReference(exposedObject, beanName);
               }
           }
       }
       return exposedObject;
   }
   ```

4. AbstractAutowireCapableBeanFactory#populateBean，类A装配属性B会调用AbstractBeanFactory#doGetBean，继续创建类B，在类B走到装配属性时，又会调用到doGetBean

   ```java
   //doGetBean关键代码
   //尝试获取类A的实例
   Object sharedInstance = getSingleton(beanName)
   ```

5. DefaultSingletonBeanRegistry#getSingleton

   ```java
   protected Object getSingleton(String beanName, boolean allowEarlyReference) {
     	//从完整的SpringBean中获取不到类A
       Object singletonObject = this.singletonObjects.get(beanName);
       if (singletonObject == null && isSingletonCurrentlyInCreation(beanName)) {
           synchronized (this.singletonObjects) {
             	//从二级缓存中获取类A，获取不到
               singletonObject = this.earlySingletonObjects.get(beanName);
               if (singletonObject == null && allowEarlyReference) {
                 	//从三级缓存中获取类A，获取到了
                   ObjectFactory<?> singletonFactory = this.singletonFactories.get(beanName);
                   if (singletonFactory != null) {
                     	//调用前面的getEarlyBeanReference
                     	//将Bean做一些PostProcess处理，移除三级缓存，放进二级缓存
                       singletonObject = singletonFactory.getObject();
                       this.earlySingletonObjects.put(beanName, singletonObject);
                       this.singletonFactories.remove(beanName);
                   }
               }
           }
       }
       return singletonObject;
   }
   ```

6. 总结
   - 一级缓存放的是完整的SpringBean
   - 二级缓存存放的是不完整的SpringBean且已经被PostProcess处理过的，例如代理
   - 三级缓存存放的是不完整的SpringBean

### 循环依赖其他解决方案

循环依赖主要是在创建时的Bean依赖问题，可以从SpringBean创建完成之后，再去设置属性例如

	- @PostConstruct
	- 也可以通过注入ApplicationContext获取Bean后手动设值
	- 最好的是更改设计方案，尽量避免循环依赖