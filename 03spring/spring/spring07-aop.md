
### 使用

1. @Aspect配合@Component声明一个Aop类

2. 定义一个pointcut
    ```java

    @Pointcut("execution(public * com.rockjh.study.controller.UserController.*(..)))")
    public void ControllerPointcut() {

    }
    ```
    - execution方法执行时，触发
    - public作用域
    - *返回值类型
    - com.rockjh.study.controller.UserController切点所对应的方法所属的类
    - *(..)任意方法
    - execution(* *(..))表示匹配所有方法 
    - execution(public * com. savage.service.UserService.*(..)) 表示匹配com.savage.server.UserService中所有的公有方法
    - execution(* com.savage.server..*.*(..))表示匹配com.savage.server包及其子包下的所有方法
    
3. 语法
    - @Before前置通知, 在方法执行之前执行，可以添加参数JoinPoint
    - @After后置通知, 在方法执行之后执行，可以添加参数JoinPoint
    - @AfterRunning返回通知, 位于@After之后执行，在方法返回结果之后执行，可以设置参数返回值result，JoinPoint
    - @AfterThrowing异常通知, 在方法抛出异常之后执行，可以添加参数JoinPoint和抛出的异常
    - @Around环绕通知, 围绕着方法执行，需要在这里主动执行增强之前的代码，如果有返回值需要返回，否则将会吃掉返回值,可以添加参数JoinPoint
    
4. 实例
   
    ```java
    //被拦截的类
    @RestController
    @RequestMapping("/user")
    public class UserController {
    
        @Autowired
    private UserService userService;
    
        @RequestMapping("get_by_id/{id}")
        public User GetUser(@PathVariable int id) {
            return userService.selectById(id);
        }
    }
    ```
    ```java
    //第一个切面类
    @Aspect
    @Component
    public class AopComponent1 implements Ordered{
    
        @Pointcut("execution(public * com.rockjh.study.controller.UserController.*(..)))")
        private void pointcut() {
        
        }
    
        @Before(value = "pointcut()")
        public void doSomeBefore(JoinPoint joinPoint) {
            System.out.println("-----------before1---------");
       }
    
        @After("pointcut()")
        public void doSomeAfter(JoinPoint joinPoint) {
            System.out.println("----------after1------------");
        }
    }
    ```
    ```java
    @AfterReturning(value = "pointcut()", returning = "user")
    public void doSomeAfterReturning(JoinPoint joinPoint, Object user) {
        System.out.println("----------after returning1------------");
    }
      
    @AfterThrowing(value = "pointcut()", throwing = "e")
    public void doSomeAfterThrowing(JoinPoint joinPoint,Exception e) {
        System.out.println("----------after throwing1------------");
    }

    @Around("pointcut()")
    public Object doSomeAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("----------around before------------");
        Object proceed = null;
        proceedingJoinPoint.proceed();
        System.out.println("----------around after------------");
        return proceed;
    }

    @Override
    public int getOrder() {
        return -1;
    }
    ```
    ```java
    //第二个切面类
    @Aspect
    @Component
    public class AopComponent2 implements Ordered{
    
        @Pointcut("execution(public * com.rockjh.study.controller.UserController.*(..)))")
        private void pointcut() {
    
        }
    
        @Before(value = "pointcut()")
        public void doSomeBefore(JoinPoint joinPoint) {
            System.out.println("-----------before2---------");
        }
    
        @After("pointcut()")
        public void doSomeAfter(JoinPoint joinPoint) {
            System.out.println("----------after2------------");
        }

        @AfterReturning(value = "pointcut()", returning = "user")
        public void doSomeAfterReturning(JoinPoint joinPoint, Object user) {
            System.out.println("----------after returning2------------");
        }

        @AfterThrowing(value = "pointcut()", throwing = "e")
        public void doSomeAfterThrowing(JoinPoint joinPoint,Exception e) {
            System.out.println("----------after throwing2------------");
        }
    
        @Around("pointcut()")
        public Object doSomeAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
            System.out.println("----------around before------------");
            Object proceed = null;
            proceedingJoinPoint.proceed();
            System.out.println("----------around after------------");
            return proceed;
        }
    
        @Override
        public int getOrder() {
            return -2;
        }
    }
    ```
> 在around中不能捕获异常，否则被拦截的类出现异常不会抛出，造成生吞异常的情形
### 启动初始化

1. spring中引用相关包后，需要加入注解@EnableAspectJAutoProxy启用aop
2. 加入注解后，在注解中找到@Import的类，会装载一个bean定义
3. 在AopConfigUtils#registerOrEscalateApcAsRequired装载AOP的处理类
4. 装载名为org.springframework.aop.config.internalAutoProxyCreator，类型为AnnotationAwareAspectJAutoProxyCreator的bean，AnnotationAwareAspectJAutoProxyCreator的本质也是一个BeanPostProcessors
5. 上面只是创建了bean，没有实例化，上面是在refresh方法中的invokeBeanFactoryPostProcessors
6. 接下来执行refresh#registerBeanPostProcessors方法，初始化BeanPostProcessors
7. 进入进到PostProcessorRegistrationDelegate#registerBeanPostProcessors方法，里面有分类对bean进行初始化，该类分配到orderedPostProcessorNames数组
8. beanFactory.getBean(ppName, BeanPostProcessor.class)，这里又回到Bean创建过程
9. Aop的注解处理类被创建成功了，注意他属于一个BeanPostProcessors类

### 类拦截强化源码分析

1. 单例bean是在refresh#finishBeanFactoryInitialization方法中初始化的

2. 依次进入创建Bean的PostProcess的处理

    - AbstractAutowireCapableBeanFactory#doCreateBean

    - AbstractAutowireCapableBeanFactory#initializeBean

    - AbstractAutowireCapableBeanFactory#applyBeanPostProcessorsAfterInitialization

      ```java
      //遍历所有的BeanPostProcess
      for (BeanPostProcessor processor : getBeanPostProcessors()) {
      		//符合PostProcess的会对bean进行处理，不符合的直接返回原对象
          Object current = processor.postProcessAfterInitialization(result, beanName);
          if (current == null) {
              return result;
          }
          result = current;
      }
      ```

      

3. 找到前面注册的AnnotationAwareAspectJAutoProxyCreator进行AOP的处理AnnotationAwareAspectJAutoProxyCreator的子类AbstractAutoProxyCreator#postProcessAfterInitialization
    ```java
    public Object postProcessAfterInitialization(@Nullable Object bean, String beanName) {
        if (bean != null) {
          	//可能在前面二级缓存已经进行过动态代理了
            Object cacheKey = getCacheKey(bean.getClass(), beanName);
            if (this.earlyProxyReferences.remove(cacheKey) != bean) {
              	//判断根据条件决定是否进行动态代理
                return wrapIfNecessary(bean, beanName, cacheKey);
            }
        }
        return bean;
    }
    ```
    ```java
    protected Object wrapIfNecessary(Object bean, String beanName, Object cacheKey) {
        //...省略一些条件判断，直接返回不需要动态代理的bean
    
        //获取拦截链
        Object[] specificInterceptors = getAdvicesAndAdvisorsForBean(bean.getClass(), beanName, null);
        if (specificInterceptors != DO_NOT_PROXY) {
            this.advisedBeans.put(cacheKey, Boolean.TRUE);
          	//创建代理
            Object proxy = createProxy(
                    bean.getClass(), beanName, specificInterceptors, new SingletonTargetSource(bean));
            this.proxyTypes.put(cacheKey, proxy.getClass());
            return proxy;
        }
    
        this.advisedBeans.put(cacheKey, Boolean.FALSE);
        return bean;
    }
    ```

4. AbstractAdvisorAutoProxyCreator#getAdvicesAndAdvisorsForBean#findEligibleAdvisors，获得拦截链
    ```java
    protected List<Advisor> findEligibleAdvisors(Class<?> beanClass, String beanName) {
    	//获取所有的拦截链，aop的每一个方法一个拦截链共10个
      //添加Spring advisors的拦截链，如BeanFactoryTransactionAttributeSourceAdvisor（事务）
    	List<Advisor> candidateAdvisors = findCandidateAdvisors();
      //会对拦截链进行筛选，判断是否符合对应的切面，事务这里也会被解析
      //只要这个bean中有一个方法满足某一个切面，就会加入拦截链
    	List<Advisor> eligibleAdvisors = findAdvisorsThatCanApply(candidateAdvisors, beanClass, beanName);
    	extendAdvisors(eligibleAdvisors);
    	if (!eligibleAdvisors.isEmpty()) {
        //对拦截链进行排序
    		eligibleAdvisors = sortAdvisors(eligibleAdvisors);
    	}
    	return eligibleAdvisors;
    }
    ```

5. AnnotationAwareAspectJAutoProxyCreator#findCandidateAdvisors

    - AbstractAdvisorAutoProxyCreator#findCandidateAdvisors，添加Spring advisors的拦截链，例如事务拦截链Bean

    - BeanFactoryAspectJAdvisorsBuilder#buildAspectJAdvisors，获取AOP切面拦截链

      - 内部缓存切面类的beanName，是通过第一次访问时，遍历所有声明的bean，根据注解找到的

      - 直接获取到所有bean的拦截链

6. AbstractAdvisorAutoProxyCreator#sortAdvisors，拦截链进行排序，现在是反过来排序的，执行的时候再反过来执行顺序就正常了，这里的执行顺序看不懂往下看
    ```java
    //示例最终排序结果如下
    //所有的拦截链都是MethodInterceptor和Advice的子类
    //1. 异常时执行，也就是把所有的都用try-catch包裹
    aopComponent1:AspectJAfterThrowingAdvice
    //2. 在返回之前执行，先执行其他拦截链，再执行
    aopComponent1:AfterReturningAdviceInterceptor
    //3. 在执行完成之后，会放在finally中等待所有执行完之后再执行
    aopComponent1:AspectJAfterAdvice
    //4. 环绕执行，直接该执行方法（里面的顺序：before--拦截链--after）
    aopComponent1:AspectJAroundAdvice
    //5. 执行方法之前执行，先执行方法，再执行拦截链
    aopComponent1:MethodBeforeAdviceInterceptor
    aopComponent2:AspectJAfterThrowingAdvice
    aopComponent2:AfterReturningAdviceInterceptor
    aopComponent2:AspectJAfterAdvice
    aopComponent2:AspectJAroundAdvice
    aopComponent2:MethodBeforeAdviceInterceptor
      
    //解析之后的伪代码如下
    try {
        try {
            //around before
            //before
            //method.invoke方法执行，执行这里之前，会把所有的拦截都执行一遍
          	//例如aopComponent2的拦截，按照这个顺序类似的顺序再来一遍，相当于将外面的顺序复制
          	//注意，复制顺序不是按照around before--around before这样同样方法挨着的
            //around after
        }finally {
            //AspectJAfterAdvice
        }
        //AfterReturningAdviceInterceptor
    }catch (Exception e){
        //AspectJAfterThrowingAdvice
    }
    
    
    
    //不同的实现类具体实现不同
    //AspectJAfterThrowingAdvice
    public Object invoke(MethodInvocation mi) throws Throwable {
        try {
          	//继续回去执行拦截链
            return mi.proceed();
        }
        catch (Throwable ex) {
            if (shouldInvokeOnThrowing(ex)) {
              	//有异常时执行这个策略的方法
                invokeAdviceMethod(getJoinPointMatch(), null, ex);
            }
            throw ex;
        }
    }
    
    //AspectJAfterAdvice
    public Object invoke(MethodInvocation mi) throws Throwable {
        try {
          	//继续回去执行拦截链
            return mi.proceed();
        }
        finally {
          	//将这个策略的方法放在最后执行
            invokeAdviceMethod(getJoinPointMatch(), null, null);
        }
    }
    ```

7. AbstractAutoProxyCreator#createProxy，创建代理

    ```java
    //创建代理工厂
    ProxyFactory proxyFactory = new ProxyFactory();
    //将拦截链、代理对象等属性设置进代理工厂
    Advisor[] advisors = buildAdvisors(beanName, specificInterceptors);
    proxyFactory.addAdvisors(advisors);
    proxyFactory.setTargetSource(targetSource);
    customizeProxyFactory(proxyFactory);
    //创建代理对象
    return proxyFactory.getProxy(getProxyClassLoader());
    ```

8. ProxyFactory#getProxy

    ```java
    //获取代理
    public Object getProxy(@Nullable ClassLoader classLoader) {
      	//第一步，获取Aop代理cglib还是jdkDynamic，第二步获取具体代理
    		return createAopProxy().getProxy(classLoader);
    }
    ```

    

9. ProxyFactory的抽象父类ProxyCreatorSupport#createAopProxy

    ```java
    //默认创建AOP代理类的工厂
    private AopProxyFactory aopProxyFactory = new DefaultAopProxyFactory()
    //创建代理
    protected final synchronized AopProxy createAopProxy() {
        if (!this.active) {
            activate();
        }
        return getAopProxyFactory().createAopProxy(this);
    }
    //获取代理工厂
    public AopProxyFactory getAopProxyFactory() {
    		return this.aopProxyFactory;
    }
    ```

    

10. DefaultAopProxyFactory#createAopProxy

    ```java
    public AopProxy createAopProxy(AdvisedSupport config) throws AopConfigException {
        //选择cglib还是jdk代理，一般默认有接口就会使用jdk代理，没有接口使用cglib代理
        if (config.isOptimize() || config.isProxyTargetClass() || hasNoUserSuppliedProxyInterfaces(config)) {
            Class<?> targetClass = config.getTargetClass();
            if (targetClass.isInterface() || Proxy.isProxyClass(targetClass)) {
                return new JdkDynamicAopProxy(config);
            }
          	//ObjenesisCglibAopProxy继承自CglibAopProxy
            return new ObjenesisCglibAopProxy(config);
        }
        else {
            return new JdkDynamicAopProxy(config);
        }
    }
    ```

11. CglibAopProxy#getProxy，获取具体的代理

     ```java
     //对MethodInterceptor和Advice列表进行封装
     Callback[] callbacks = getCallbacks(rootClass);
     //CglibAopProxy#getCallbacks
     new DynamicAdvisedInterceptor(...)
     ```

     

12. CglibAopProxy#DynamicAdvisedInterceptor#intercept，执行代理类的方法时进入

     ```java
     //前面是对类进行创建代理时的判断条件是，只要类中有一个方法满足代理条件，就将切面加入到代理中
     //这个方法就是对所执行的方法进行判断，当前执行方法是否满足加入的切面
     List<Object> chain = this.advised.getInterceptorsAndDynamicInterceptionAdvice(method, targetClass);
     
     //执行拦截
     retVal = new CglibMethodInvocation(proxy, target, method, args, targetClass, chain, methodProxy).proceed();
     ```

13. ReflectiveMethodInvocation#proceed，这个方法中有序号，会遍历我们的拦截链，然后执行
     ```java
     //如果拦截链执行完了，就会执行具体的业务方法
     if (this.currentInterceptorIndex == this.interceptorsAndDynamicMethodMatchers.size() - 1) {
         return invokeJoinpoint();
     }
     //顺序获取拦截链中的拦截方法
     Object interceptorOrInterceptionAdvice = this.interceptorsAndDynamicMethodMatchers.get(++this.currentInterceptorIndex);
     //执行拦截链中的方法，不同的拦截链，有不同的实现
     ((MethodInterceptor) interceptorOrInterceptionAdvice).invoke(this);
     
     //invoke会执行拦截链中的方法，就像上面第六步所示，会循环执行调用
     ```

14. 如果有多个类，拦截同一个地方，在获取到拦截链的时候是会获取到所有符合条件的，然后再排序执行，拦截类是可以通过实现Ordered接口，来控制先后执行顺序的，比如上面的实例，返回为
     ```txt
     ----------around before2------------
     -----------before2---------
     ----------around before1------------
     -----------before1---------
     ----------around after1------------
     ----------after1------------
     ----------after returning1------------
     ----------around after2------------
     ----------after2------------
     ----------after returning2------------
     ```