### 启动配置

1. @EnableTransactionManagement启用注解事务管理，等同于xml配置方式的 <tx:annotation-driven />

2. 数据库使用mysql支持回滚的引擎，写如下service
    ```java
    @Service
    public class UserService {

        @Autowired
        private UserMapper userMapper;

        @Transactional
        public int insert(){
            User user = new User();
            user.setUsername(UUID.randomUUID().toString().replace("-","").substring(0,5));
            user.setPassword(UUID.randomUUID().toString().replace("-","").substring(0,5));
            user.setRealname(UUID.randomUUID().toString().replace("-","").substring(0,5));
            userMapper.insert(user);
            //异常，会造成事务回滚，注释掉这行代码，就可以成功插入数据
            int i = 1/0;
            return user.getId();
        }
    }
    ```
### 语法格式

1. @Transactional修饰public的class和method来开启事务，它里面有一些属性，可以定义事务的隔离级别，传播行为以及是否只读，回滚异常等，简单说一下隔离级别和传播行为
2. 事务隔离级别
	- TransactionDefinition.ISOLATION_DEFAULT：这是默认值，表示使用底层数据库的默认隔离级别。对大部分数据库而言，通常这值就是TransactionDefinition.ISOLATION_READ_COMMITTED。
	- TransactionDefinition.ISOLATION_READ_UNCOMMITTED：该隔离级别表示一个事务可以读取另一个事务修改但还没有提交的数据。该级别不能防止脏读，不可重复读和幻读，因此很少使用该隔离级别。比如PostgreSQL实际上并没有此级别。
	- TransactionDefinition.ISOLATION_READ_COMMITTED：该隔离级别表示一个事务只能读取另一个事务已经提交的数据。该级别可以防止脏读，这也是大多数情况下的推荐值。
	- TransactionDefinition.ISOLATION_REPEATABLE_READ：该隔离级别表示一个事务在整个过程中可以多次重复执行某个查询，并且每次返回的记录都相同。该级别可以防止脏读和不可重复读。
	- TransactionDefinition.ISOLATION_SERIALIZABLE：所有的事务依次逐个执行，这样事务之间就完全不可能产生干扰，也就是说，该级别可以防止脏读、不可重复读以及幻读。但是这将严重影响程序的性能。通常情况下也不会用到该级别
3. 事务传播行为
	- TransactionDefinition.PROPAGATION_REQUIRED：如果当前存在事务，则加入该事务；如果当前没有事务，则创建一个新的事务。这是默认值。
	- TransactionDefinition.PROPAGATION_REQUIRES_NEW：创建一个新的事务，如果当前存在事务，则把当前事务挂起。
	- TransactionDefinition.PROPAGATION_SUPPORTS：如果当前存在事务，则加入该事务；如果当前没有事务，则以非事务的方式继续运行。
	- TransactionDefinition.PROPAGATION_NOT_SUPPORTED：以非事务方式运行，如果当前存在事务，则把当前事务挂起。
	- TransactionDefinition.PROPAGATION_NEVER：以非事务方式运行，如果当前存在事务，则抛出异常。
	- TransactionDefinition.PROPAGATION_MANDATORY：如果当前存在事务，则加入该事务；如果当前没有事务，则抛出异常。
	- TransactionDefinition.PROPAGATION_NESTED：如果当前存在事务，则创建一个事务作为当前事务的嵌套事务来运行；如果当前没有事务，则该取值等价于TransactionDefinition.PROPAGATION_REQUIRED
4. 使用示例
    ```java
	//事务传播行为为TransactionDefinition.PROPAGATION_REQUIRED
	//事务隔离级别为TransactionDefinition.ISOLATION_DEFAULT
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT)
	```

### 源码分析

1. 首先进入到@EnableTransactionManagement，里面import了TransactionManagementConfigurationSelector类

2. 进入到TransactionManagementConfigurationSelector类中，他将注册两个bean实体AutoProxyRegistrar、ProxyTransactionManagementConfiguration

    - AutoProxyRegistrar#AopConfigUtils#，代理类，和AOP使用同一个beanName，如果事务和AOP都使用了，会只注册AOP的bean实例
      - InfrastructureAdvisorAutoProxyCreator，事务的bean class
      - AnnotationAwareAspectJAutoProxyCreator，AOP的bean class
    - ProxyTransactionManagementConfiguration，事务管理配置类

3. ProxyTransactionManagementConfiguration配置类，会注册三个事务相关的bean
    - BeanFactoryTransactionAttributeSourceAdvisor，事务的advisor，类似AOP的切面类，将下面两个bean当做属性进行传递

      - AnnotationTransactionAttributeSource，解析事务的注解信息

      - TransactionInterceptor，事务拦截器，拦截链中被执行的方法

4. AnnotationTransactionAttributeSource#SpringTransactionAnnotationParser#parseTransactionAnnotation方法，对我们的事务注解属性进行翻译

5. TransactionInterceptor#invoke，执行事务拦截链时的方法
    ```java
    //事务拦截执行
    return invokeWithinTransaction(invocation.getMethod(), targetClass, invocation::proceed)
    
    //TransactionAspectSupport#invokeWithinTransaction
    //创建一个事务，mybatis+spring的事务整合就在这里实现的
    TransactionInfo txInfo = createTransactionIfNecessary(ptm, txAttr, joinpointIdentification);
    Object retVal;
    try {
        retVal = invocation.proceedWithInvocation();
    }catch (Throwable ex) {
        //回滚事务
        completeTransactionAfterThrowing(txInfo, ex);
        throw ex;
    }finally {
      	//清理事务
        cleanupTransactionInfo(txInfo);
    }
    //提交事务
    commitTransactionAfterReturning(txInfo);
    return retVal;
    ```

6. TransactionAspectSupport#createTransactionIfNecessary，为当前ThreadLocal添加事务信息，mybatis+spring会用到

   ```java
   //Spring事务开启流程
   //AbstractPlatformTransactionManager#getTransaction
   //AbstractPlatformTransactionManager#startTransaction
   //DataSourceTransactionManager#doBegin
   //TransactionSynchronizationManager#bindResource
   //以各种参数为key，在ThreadLocal中添加事务的一些信息
   Object actualKey = TransactionSynchronizationUtils.unwrapResourceIfNecessary(key);
   Assert.notNull(value, "Value must not be null");
   Map<Object, Object> map = resources.get();
   // set ThreadLocal Map if none found
   if (map == null) {
     map = new HashMap<>();
     resources.set(map);
   }
   Object oldValue = map.put(actualKey, value);
   
   //mybatis中获取事务
   //SqlSessionTemplate#SqlSessionInterceptor#invoke
   //判断当前线程是否存在事务
   isSqlSessionTransactional(sqlSession, SqlSessionTemplate.this.sqlSessionFactory)
   //SqlSessionUtils#isSqlSessionTransactional
   //通过前面设置的resource去尝试获取
   SqlSessionHolder holder = (SqlSessionHolder) TransactionSynchronizationManager.getResource(sessionFactory);
   //TransactionSynchronizationManager#getResource#doGetResource
   //获取到了就返回，说明当前线程已经开启了事务
   Object actualKey = TransactionSynchronizationUtils.unwrapResourceIfNecessary(key);
   Map<Object, Object> map = resources.get();
   if (map == null) {
     return null;
   }
   Object value = map.get(actualKey);
   return value;
   ```

### 一个事务的初始化到执行完成流程

1. @EnableTransactionManagement开启事务，注册AOP的bean或者事务bean，注册ProxyTransactionManagementConfiguration事务管理器bean
2. 实例化业务bean时，比如userService，走到前面aop说过的AbstractAutoProxyCreator#postProcessAfterInitialization
3. 获得拦截链，AbstractAdvisorAutoProxyCreator#findEligibleAdvisors获得拦截链
    ```java
    protected List<Advisor> findEligibleAdvisors(Class<?> beanClass, String beanName) {
    	//获取所有的拦截链，aop的每一个方法一个拦截链共10个
      //添加Spring advisors的拦截链，如BeanFactoryTransactionAttributeSourceAdvisor（事务）
    	List<Advisor> candidateAdvisors = findCandidateAdvisors();
      //会对拦截链进行筛选，判断是否符合对应的切面，事务这里也会被解析
      //只要这个bean中有一个方法满足某一个切面，就会加入拦截链
    	List<Advisor> eligibleAdvisors = findAdvisorsThatCanApply(candidateAdvisors, beanClass, beanName);
    	extendAdvisors(eligibleAdvisors);
    	if (!eligibleAdvisors.isEmpty()) {
        //对拦截链进行排序
    		eligibleAdvisors = sortAdvisors(eligibleAdvisors);
    	}
    	return eligibleAdvisors;
    }
    ```
4. 获取拦截链进入AnnotationAwareAspectJAutoProxyCreator#findCandidateAdvisors方法
    ```java
    protected List<Advisor> findCandidateAdvisors() {
        //有了事务，会返回一个transactionAdvisor前面说过的一个
        List<Advisor> advisors = super.findCandidateAdvisors();
        if (this.aspectJAdvisorsBuilder != null) {
            //会加入其他AOP的advisors
            advisors.addAll(this.aspectJAdvisorsBuilder.buildAspectJAdvisors());
        }
        return advisors;
    }
    ```
5. 获取到所有拦截链之后，就创建代理对象
6. 执行insert方法，因为创建的是aop代理，进入CglibAopProxy#DynamicAdvisedInterceptor#intercept执行，类似aop，分别执行拦截链，直到执行到拦截链transactionInterceptor
7. TransactionInterceptor#invoke，跳转到TransactionAspectSupport#invokeWithinTransaction类执行事务，遇到异常回滚，无异常提交事务