### 架构

1. 三层架构分别代表
   - Web（表现层），负责接收客户端请求、向客户端响应结果，表现层包括展示层和控制层（表现层的设计一般使用MVC模型），它依赖于业务层
   - Service（业务层），负责业务逻辑处理，和需求息息相关
   - Dao（持久层），和数据库交互，对数据库表的增删改查

2. MVC设计模式（仅仅是针对的表现层）
   - Model（模型），包含业务模型和数据模型，数据模型用于封装数据、业务模型用于处理数据
   - View（视图），通常指jsp、html等，用于展示数据
   - Controller（控制器），是应用程序中处理用户交互的部分（接收请求）

3. SpingMVC，它通过一套注解，让一个简单的 Java 类成为处理请求的控制器，本质可以认为是对servlet的封装

### SpringMVC监听器、过滤器、拦截器

1. Filter（过滤器），对Request请求起到过滤的作用，如果配置为/*可以对所 有的资源访问(servlet、js/css静态资源等)进行过滤处理（Restful下没有价值）

2. Listener（监听器），实现了javax.servlet.ServletContextListener 接口的服务器端组件，它随 Web应用的启动而启动，只初始化一次，然后会一直运行监视，随Web应用的停止而销毁

3. Interceptor（拦截器），只会拦截访问的控制器方法（Handler）

   ```java
   public interface HandlerInterceptor {
   
       // 执行业务业务逻辑拦截一次
       boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler);
   
       // 执行业务逻辑后但未跳转页面之前拦截一次
       void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView);
   
       // 在跳转页面之后拦截一次
       void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex);
   }
   ```

   ```java
   //使用教程
   @Component
   public class MyHandlerInterceptor implements HandlerInterceptor {
       @Override
       public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
           return true;
       }
   
       @Override
       public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
           System.out.println("postHandle");
       }
   
       @Override
       public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
           System.out.println("afterCompletion");
       }
   }
   
   //MVC中的所有配置，都可以通过这个类来进行详细配置，重写不同的方法
   @Configuration
   public class WebConfig implements WebMvcConfigurer {
   
       @Autowired
       private MyHandlerInterceptor myHandlerInterceptor;
   
       @Override
       public void addInterceptors(InterceptorRegistry registry) {
           // 这里需要注意顺序, 优先级越小，越先执行
           // InterceptorA.preHandle->InterceptorB.preHandle->InterceptorB.postHandle->InterceptorA.postHandle
           registry.addInterceptor(myHandlerInterceptor)
                   .addPathPatterns("/user/**")
                   .excludePathPatterns("/user/findByPage");
   
       }
   }
   ```
   

### SpringMVC核心流程DispatcherServlet#doDispatch

![mvc-流程图](http://assets.processon.com/chart_image/5fd70e86e0b34d06f4f44559.png?_=1607930744839)

```java
protected void doDispatch(HttpServletRequest request, HttpServletResponse response) {
    HttpServletRequest processedRequest = request;
    HandlerExecutionChain mappedHandler = null;
    boolean multipartRequestParsed = false;

    WebAsyncManager asyncManager = WebAsyncUtils.getAsyncManager(request);

    try {
        ModelAndView mv = null;
        Exception dispatchException = null;

        try {
            // 1 检查是否是文件上传的请求
            processedRequest = checkMultipart(request);
            multipartRequestParsed = (processedRequest != request);

            // 2 取得处理当前请求的Controller，这里也称为Handler，即处理器
            // 这里并不是直接返回Controller，而是返回HandlerExecutionChain请求处理链对象
            // HandlerExecutionChain该对象封装了Handler和Interceptor
            mappedHandler = getHandler(processedRequest);
            if (mappedHandler == null) {
                // 如果 handler 为空，则返回404
                noHandlerFound(processedRequest, response);
                return;
            }

            // 3 获取处理请求的处理器适配器HandlerAdapter
            HandlerAdapter ha = getHandlerAdapter(mappedHandler.getHandler());
            // 处理 last-modified 请求头
            String method = request.getMethod();
            boolean isGet = "GET".equals(method);
            if (isGet || "HEAD".equals(method)) {
                long lastModified = ha.getLastModified(request, mappedHandler.getHandler());
                if (new ServletWebRequest(request, response).checkNotModified(lastModified) && isGet) {
                    return;
                }
            }

            if (!mappedHandler.applyPreHandle(processedRequest, response)) {
                return;
            }

            // 4 实际处理器处理请求，返回结果视图对象
            mv = ha.handle(processedRequest, response, mappedHandler.getHandler());

            if (asyncManager.isConcurrentHandlingStarted()) {
                return;
            }
            // 结果视图对象的处理
            applyDefaultViewName(processedRequest, mv);
            mappedHandler.applyPostHandle(processedRequest, response, mv);
        }
        catch (Exception ex) {
            dispatchException = ex;
        }
        catch (Throwable err) {
            // As of 4.3, we're processing Errors thrown from handler methods as well,
            // making them available for @ExceptionHandler methods and other scenarios.
            dispatchException = new NestedServletException("Handler dispatch failed", err);
        }
        // 5 跳转⻚面，渲染视图
        processDispatchResult(processedRequest, response, mappedHandler, mv, dispatchException);
    }
    catch (Exception ex) {
        // 最终会调用HandlerInterceptor的afterCompletion 方法
        triggerAfterCompletion(processedRequest, response, mappedHandler, ex);
    }
    catch (Throwable err) {
        // 最终会调用HandlerInterceptor的afterCompletion 方法
        triggerAfterCompletion(processedRequest, response, mappedHandler,
                new NestedServletException("Handler processing failed", err));
    }
    finally {
        if (asyncManager.isConcurrentHandlingStarted()) {
            // Instead of postHandle and afterCompletion
            if (mappedHandler != null) {
                mappedHandler.applyAfterConcurrentHandlingStarted(processedRequest, response);
            }
        }
        else {
            // Clean up any resources used by a multipart request.
            if (multipartRequestParsed) {
                cleanupMultipart(processedRequest);
            }
        }
    }
}
```

