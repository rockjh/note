### 简介

1. HandlerMapping（处理器映射器），不同方式注册的Handler存储在不同的HandlerMapping中

2. HandlerAdapter（处理器适配器），不同方式注册的Handler使用不同的HandlerAdapter

3. HandlerExceptionResolver（异常解析器），全局异常处理

4.  ViewResolver（视图解析器），用于将String类型的视图名和Locale解析为View类型的视图，做的事情如下
   - ViewResolver 找到渲染所用的模板
   - 到视图的类型，如JSP（JSP的视图解析器InternalResourceViewResolver），并填入参数

5. RequestToViewNameTranslator（默认视图名转换器组件）

6. LocaleResolver（国际化解析器）

7. ThemeResolver（主题解析器）

8. MultipartResolver（文件上传解析器）

9. FlashMapManager（用于重定向时的参数传递）

   ```java
   /**
    * 转发:A找B借钱400，B没有钱但是悄悄的找到C借了400块钱给A
    * url不会变参数也不会丢失，一个请求
    *
    * 重定向:A找B借钱400，B说我没有钱，你找别人借去，那么A又带着400块的借钱需求找到
    * url会变,参数会丢失需要重新携带参数，两个请求，通过组件FlashMapManager暂存参数
    * @param name
    * @param redirectAttributes
    * @return
    */
   @RequestMapping("/handleRedirect")
   public String handleRedirect(String name, RedirectAttributes redirectAttributes) {
       // 拼接参数安全性、参数⻓度都有局限
       //return "redirect:handle01?name=" + name;
   
       // addFlashAttribute方法设置了一个flash类型属性
       // 该属性会被暂存到session中，在跳转到⻚面之后该属性销毁
       redirectAttributes.addFlashAttribute("name",name);
       return "redirect:handle01";
   }
   ```

   

### DispatcherServlet结构

![DispatcherServlet类结构图](http://assets.processon.com/chart_image/5fd72fc963768906e6dcbe5d.png?_=1607938164243)

DispatcherServlet就是对tomcat的类进行一个继承，然后在FrameworkServlet处对IOC容器进行一个封装，MVC将IOC封装为他的一个父容器，MVC自己作为IOC的子容器，这样MVC和IOC就整合在了一起

```java
//DispatcherServlet关键字段
//九大组件缓存局部变量
private MultipartResolver multipartResolver;
private LocaleResolver localeResolver;
private ThemeResolver themeResolver;
private List<HandlerMapping> handlerMappings;
private List<HandlerAdapter> handlerAdapters;
private List<HandlerExceptionResolver> handlerExceptionResolvers;
private RequestToViewNameTranslator viewNameTranslator;
private FlashMapManager flashMapManager;
private List<ViewResolver> viewResolvers;

//引用的IOC的容器变量
private WebApplicationContext webApplicationContext;
```



### 初始化

1. DispatcherServlet本质上是一个Servlet，Web容器装载Servlet第一步都是执行init方法，且是在项目启动后，第一次访问时装载Servlet的

   ```java
   // IOC在前面加载的时候，会将DispatcherServlet的完整类名设置到ServletClass中去
   // 到了具体装载Servlet时，会反射实例化DispatcherServlet，然后调用初始化方法init
   javax.servlet.GenericServlet#init //空实现
   ```

2. HttpServletBean#init，调用抽象父类init()

   ```java
   public final void init() throws ServletException {
       // 1.读取init parameters 等参数，其中就包括设置contextConfigLocation 
       PropertyValues pvs = new ServletConfigPropertyValues(getServletConfig(), this.requiredProperties);
       //...省略代码
       //2.初始化servlet中使用的bean
       initServletBean();
   }
   ```

3. FrameworkServlet#initServletBean调用具体实现类，初始化bean

   ```java
   protected final void initServletBean() throws ServletException {
       //关键代码
       //初始化webApplicationContext
       this.webApplicationContext = initWebApplicationContext();
   }
   
   //FrameworkServlet#initWebApplicationContext
   protected WebApplicationContext initWebApplicationContext() {
       //1.获取父容器rootWebApplicationContext
       WebApplicationContext rootContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
       WebApplicationContext wac = null;
       if (this.webApplicationContext != null) {
           // 直接使用父容器
           wac = this.webApplicationContext;
           //...省略代码
       }
       if (!this.refreshEventReceived) {
           //当前上下文不是带refresh的容器，走这一个分支，调用onRefresh方法进行初始化
           synchronized (this.onRefreshMonitor) {
               onRefresh(wac);
           }
       }
       return wac;
   }
   ```

4. DispatcherServlet#onRefresh，初始化容器的八大组件

   ```java
   protected void onRefresh(ApplicationContext context) {
       initStrategies(context);
   }
   
   protected void initStrategies(ApplicationContext context) {
       // 文件上传解析器
       initMultipartResolver(context);
       // 国际化解析器
       initLocaleResolver(context);
       // 主题解析器
       initThemeResolver(context);
       // 处理器映射器，重点
       initHandlerMappings(context);
       // 处理器适配器，重点
       initHandlerAdapters(context);
       // 异常解析器，重点
       initHandlerExceptionResolvers(context);
       // 默认视图名转换器组件
       initRequestToViewNameTranslator(context);
       // 视图解析器
       initViewResolvers(context);
       // 用于重定向时的参数传递
       initFlashMapManager(context);
   }
   ```

5. DispatcherServlet#initHandlerMappings，初始化处理映射器

   ```java
   // 初始化的handlerMapping是几种不同的封装handler的方式
   // 具体的handler可以根据request在里面找到
   private void initHandlerMappings(ApplicationContext context) {
       this.handlerMappings = null;
       if (this.detectAllHandlerMappings) {
           // 按照HandlerMapping.class类型去IOC容器中找到所有的HandlerMapping
           Map<String, HandlerMapping> matchingBeans =
                   BeanFactoryUtils.beansOfTypeIncludingAncestors(context, HandlerMapping.class, true, false);
           if (!matchingBeans.isEmpty()) {
               this.handlerMappings = new ArrayList<>(matchingBeans.values());
               AnnotationAwareOrderComparator.sort(this.handlerMappings);
           }
       } else {
           try {
               // 否则在IOC中按照固定名称id（handlerMapping）去找
               HandlerMapping hm = context.getBean(HANDLER_MAPPING_BEAN_NAME, HandlerMapping.class);
               this.handlerMappings = Collections.singletonList(hm);
           }
           catch (NoSuchBeanDefinitionException ex) {
               // Ignore, we'll add a default HandlerMapping later.
           }
       }
       if (this.handlerMappings == null) {
           // 按照默认方式实例化生成handlerMapping
         	// DispatcherServlet.properties中加载默认的bean
           this.handlerMappings = getDefaultStrategies(context, HandlerMapping.class);
       }
   }
   ```

6. 其他组件注册方式都类似，只有多部件解析器的初始化必须按照**id**注册对象(**multipartResolver**)

### DispatcherServlet#doDispatch重点步骤讲解

1. DispatcherServlet#getHandler，获取封装的HandlerExecutionChain（HandlerMethod+HandlerInterceptor）

   ```java
   protected HandlerExecutionChain getHandler(HttpServletRequest request) throws Exception {
       if (this.handlerMappings != null) {
         	//循环遍历，根据request查找HandlerExecutionChain
         	//一般情况下我们都采用的@Controller+@RequestMapping的方式注册
         	//因此注册的实现类为RequestMappingHandlerMapping
           for (HandlerMapping mapping : this.handlerMappings) {
               HandlerExecutionChain handler = mapping.getHandler(request);
               if (handler != null) {
                   return handler;
               }
           }
       }
       return null;
   }
   
   //AbstractHandlerMapping#getHandler
   //RequestMappingInfoHandlerMapping#getHandlerInternal
   //AbstractHandlerMethodMapping#getHandlerInternal
   protected HandlerMethod getHandlerInternal(HttpServletRequest request) {
       //获取请求路径
       String lookupPath = getUrlPathHelper().getLookupPathForRequest(request);
       request.setAttribute(LOOKUP_PATH, lookupPath);
       //mappingRegistry是一个对象，封装注册的requestMapping映射请求路径和相对应的handlerMethod对象
       this.mappingRegistry.acquireReadLock();
       try {
           HandlerMethod handlerMethod = lookupHandlerMethod(lookupPath, request);
           return (handlerMethod != null ? handlerMethod.createWithResolvedBean() : null);
       }
       finally {
           this.mappingRegistry.releaseReadLock();
       }
   }
   
   //AbstractHandlerMapping#getHandler
   //AbstractHandlerMapping#getHandlerExecutionChain
   //获取handlerMethod对应的HandlerInterceptor，并封装，然后一起返回
   ```

2. DispatcherServlet#getHandlerAdapter，获取处理请求的处理器适配器HandlerAdapter

   ```java
   protected HandlerAdapter getHandlerAdapter(Object handler) throws ServletException {
       if (this.handlerAdapters != null) {
           //遍历容器中的HandlerAdapter，根据不同方式注册的HandlerMethod寻找支持的HandlerAdapter
           //一般情况下我们都采用的@Controller+@RequestMapping的方式注册
           //因此注册的实现类为RequestMappingHandlerAdapter
           for (HandlerAdapter adapter : this.handlerAdapters) {
               if (adapter.supports(handler)) {
                   return adapter;
               }
           }
       }
   }
   
   //AbstractHandlerMethodAdapter#supports
   public final boolean supports(Object handler) {
     	//专门处理HandlerMethod类型的适配
       return (handler instanceof HandlerMethod && supportsInternal((HandlerMethod) handler));
   }
   ```

   

3. AbstractHandlerMethodAdapter#handle，进入到适配器进行处理请求

   ```java
   public final ModelAndView handle(HttpServletRequest request, HttpServletResponse response, Object handler) {
   
       return handleInternal(request, response, (HandlerMethod) handler);
   }
   
   //RequestMappingHandlerAdapter#handleInternal
   //RequestMappingHandlerAdapter#invokeHandlerMethod
   //根据HandlerMethod封装invocableMethod，里面包括method，参数、controller等信息
   ServletInvocableHandlerMethod invocableMethod = createInvocableHandlerMethod(handlerMethod);
   //利用封装的invocableMethod反射执行controller中的方法
   invocableMethod.invokeAndHandle(webRequest, mavContainer);
   //ServletInvocableHandlerMethod#invokeAndHandle
   Object returnValue = invokeForRequest(webRequest, mavContainer, providedArgs);
   //InvocableHandlerMethod#invokeForRequest
   //InvocableHandlerMethod#doInvoke，执行
   //获取方法，获取controller，invoke反射执行
   getBridgedMethod().invoke(getBean(), args)
   ```

4. ServletInvocableHandlerMethod#invokeAndHandle，返回值消息转换

   ```java
   //执行获取返回值
   Object returnValue = invokeForRequest(webRequest, mavContainer, providedArgs);
   //对返回值进行处理
   this.returnValueHandlers.handleReturnValue(
   					returnValue, getReturnValueType(returnValue), mavContainer, webRequest);
   
   //HandlerMethodReturnValueHandlerComposite#handleReturnValue
   //获取具体的返回值处理器
   HandlerMethodReturnValueHandler handler = selectHandler(returnValue, returnType);
   //处理器处理返回值
   handler.handleReturnValue(returnValue, returnType, mavContainer, webRequest);
   //RequestResponseBodyMethodProcessor#handleReturnValue
   //处理方法
   writeWithMessageConverters(returnValue, returnType, inputMessage, outputMessage);
   //RequestResponseBodyMethodProcessor#writeWithMessageConverters
   //省略一部分初始化代码，比如获取mediaType
   for (HttpMessageConverter<?> converter : this.messageConverters) {
   		//遍历所有的messageConverters，查看支持的mediaType
   }
   ```

   

