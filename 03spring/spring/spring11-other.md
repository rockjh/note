### BeanFactory和FactoryBean

1. BeanFactory是Ioc容器的根类，他负责实例化、定位、配置应用程序中的对象以及建立这些对象的依赖。DefaultListableBeanFactory、XmlBeanFactory、ApplicationContext 等都是他的子类
2. FactoryBean是对bean实例化的一个简化操作，通常我们要实例化一个bean会设置各种各样的属性，以前在xml中配置很繁琐，如果实现FactoryBean的话，就可以直接在java代码中进行实现，下面看几个例子
    ```java
    @Component
    public class ExampleFactoryBean implements FactoryBean<Monkey> {

        @Override
        public Monkey getObject(){
            //里面可以做一些操作
            return new Monkey();
        }

        @Override
        public Class<?> getObjectType() {
            return Monkey.class;
        }

        @Override
        public boolean isSingleton() {
            return true;
        }
    }
    ```
    - 通过applicationContext.getBean("&exampleFactoryBean")获取的是FactoryBean本身
    - applicationContext.getBean("exampleFactoryBean")获取的是Monkey这个bean

### Ordered加载顺序

1. 在refresh#invokeBeanFactoryPostProcessors方法中
2. 跳转到PostProcessorRegistrationDelegate#invokeBeanFactoryPostProcessors
3. 在方法中有对多个Processor进行排序sortPostProcessors
4. 最终会执行如下代码OrderComparator#doCompare
    ```java
    private int doCompare(@Nullable Object o1, @Nullable Object o2, @Nullable OrderSourceProvider sourceProvider) {
        boolean p1 = (o1 instanceof PriorityOrdered);
        boolean p2 = (o2 instanceof PriorityOrdered);
        if (p1 && !p2) {
            return -1;
        }
        else if (p2 && !p1) {
            return 1;
        }

        int i1 = getOrder(o1, sourceProvider);
        int i2 = getOrder(o2, sourceProvider);
        return Integer.compare(i1, i2);
    }
    ```

### Event-Driven

#### 事件驱动模型介绍

1. spring对于事件机制的支持对应消息驱动模型中的监听器、事件源、事件等概念
2. ApplicationListener，事件监听器，接口代码如下
    ```java
    public interface ApplicationListener<E extends ApplicationEvent> extends EventListener {
    void onApplicationEvent(E event);
    }
```
    - 通过泛型参数过滤实现对于特定的事件类型的监听
    - 需要对于特定类型的事件监听，只需要在Bean中实现这个接口即可
    - 也可以通过@EventListener实现一个事件监听器
3. ApplicationEventPublisher，事件源，接口代码如下
    ```java
    public interface ApplicationEventPublisher {

        default void publishEvent(ApplicationEvent event) {
            publishEvent((Object) event);
        }

        void publishEvent(Object event);

    }
    ```
    - 从接口继承图可以看到，ApplicationContext是ApplicationEventPublisher的子类，也就是说所有的容器都实现了该事件源
    - 可以通过ApplicationContext来发布事件
    - 获取ApplicationContext可以通过实现Aware接口、直接注入（前面系列文件有讲解）
4. ApplicationEventMulticaster，事件广播器
    - 将ApplicationContext发布的Event广播给所有符合监听事件的监听器
    - 维护监听器的添加、删除、通知监听器
    - 其子类实现了对于事件监听器类型的缓存、分类、事件同步、异步处理、异常处理等
    - 默认实现为SimpleApplicationEventMulticaster
#### spring的事件处理流程
1. spring的事件处理流程
    - AbstractApplicationContext#refresh刷新容器
    - initApplicationEventMulticaster，初始化加载事件广播器
    - registerListeners，将ApplicationListener的实现注册到事件广播器中，并没有实例化，用到的时候才会实例化
    - finishRefresh完成容器的注册，进入方法将会发布完成事件
        ```java
        publishEvent(new ContextRefreshedEvent(this))
        ```
2. 进入事件发布，执行如下代码
    ```java
    //获取事件广播器中缓存的事件，按照事件的分类进行发布
    getApplicationEventMulticaster().multicastEvent(applicationEvent, eventType)
    ```
3. 事件广播器的默认实现类SimpleApplicationEventMulticaster#multicastEvent
    ```java
    ResolvableType type = (eventType != null ? eventType : resolveDefaultEventType(event));
    //是否注入了异步线程池，没有则同步执行
    Executor executor = getTaskExecutor();
    //获取对应组的一个监听器，根据事件的Class来获取的，里面还有一些参数
    //一个事件类型可以对应有多个事件监听器
    for (ApplicationListener<?> listener : getApplicationListeners(event, type)) {
        if (executor != null) {
            executor.execute(() -> invokeListener(listener, event));
        }
        else {
            invokeListener(listener, event);
        }
    }
    ```
4. SimpleApplicationEventMulticaster#invokeListener
    ```java
    //是否有错误处理器
    ErrorHandler errorHandler = getErrorHandler();
    if (errorHandler != null) {
        try {
            doInvokeListener(listener, event);
        }
        catch (Throwable err) {
            errorHandler.handleError(err);
        }
    }
    else {
        doInvokeListener(listener, event);
    }
    ```
5. SimpleApplicationEventMulticaster#doInvokeListener
    ```java
    //直接执行监听器复写的方法
    listener.onApplicationEvent(event);
    ```

### 加载数据源

1. 一般是Hikari数据源，会通过反射执行HikariConfig.setDriverClassName()
    ```java
    //关键代码，反射实例化对应的驱动
    //driverClass="com.mysql.jdbc.Driver"
    driverClass.getConstructor().newInstance();
    ```
2. 会触发类加载com.mysql.jdbc.Driver，执行静态语句块
    ```java
    java.sql.DriverManager.registerDriver(new Driver());
    ```
3. 会触发java.sql.DriverManager类加载，具体的类加载流程图如下
    ![spi-jdbc流程图](http://assets.processon.com/chart_image/5f8e942fe0b34d0711118f4b.png?_=1603184030993)
4. 类加载完毕直接执行registerDriver方法即可