
### springboot解决的问题

1. 起步依赖，告诉springboot需要什么功能，他就能引入需要的库
2. 自动配置，针对很多spring应用程序常见的应用功能，springboot能自动提供相关配置

### 起步依赖分析

1. springboot通过maven，定义一个springboot的版本号，对所使用的其他spring组件版本进行了规定

   ```xml
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-web</artifactId>
   </dependency>
   ```

   

2. 可以通过mvn dependency:tree查看依赖树

   ```xml
   - org.springframework.boot:spring-boot-starter-web:jar:2.1.2.RELEASE:compile
      +- org.springframework.boot:spring-boot-starter:jar:2.1.2.RELEASE:compile
      |  +- org.springframework.boot:spring-boot:jar:2.1.2.RELEASE:compile
      |  +- org.springframework.boot:spring-boot-autoconfigure:jar:2.1.2.RELEASE:compile
      |  +- org.springframework.boot:spring-boot-starter-logging:jar:2.1.2.RELEASE:compile
      |  |  +- ch.qos.logback:logback-classic:jar:1.2.3:compile
      |  |  |  +- ch.qos.logback:logback-core:jar:1.2.3:compile
      |  |  |  \- org.slf4j:slf4j-api:jar:1.7.25:compile
      |  |  +- org.apache.logging.log4j:log4j-to-slf4j:jar:2.11.1:compile
      |  |  |  \- org.apache.logging.log4j:log4j-api:jar:2.11.1:compile
      |  |  \- org.slf4j:jul-to-slf4j:jar:1.7.25:compile
      |  +- javax.annotation:javax.annotation-api:jar:1.3.2:compile
      |  +- org.springframework:spring-core:jar:5.1.4.RELEASE:compile
      |  |  \- org.springframework:spring-jcl:jar:5.1.4.RELEASE:compile
      |  \- org.yaml:snakeyaml:jar:1.23:runtime
      +- org.springframework.boot:spring-boot-starter-json:jar:2.1.2.RELEASE:compile
      |  +- com.fasterxml.jackson.core:jackson-databind:jar:2.9.8:compile
      |  |  +- com.fasterxml.jackson.core:jackson-annotations:jar:2.9.0:compile
      |  |  \- com.fasterxml.jackson.core:jackson-core:jar:2.9.8:compile
      |  +- com.fasterxml.jackson.datatype:jackson-datatype-jdk8:jar:2.9.8:compile
      |  +- com.fasterxml.jackson.datatype:jackson-datatype-jsr310:jar:2.9.8:compile
      |  \- com.fasterxml.jackson.module:jackson-module-parameter-names:jar:2.9.8:compile
      +- org.springframework.boot:spring-boot-starter-tomcat:jar:2.1.2.RELEASE:compile
      |  +- org.apache.tomcat.embed:tomcat-embed-core:jar:9.0.14:compile
      |  +- org.apache.tomcat.embed:tomcat-embed-el:jar:9.0.14:compile
      |  \- org.apache.tomcat.embed:tomcat-embed-websocket:jar:9.0.14:compile
      +- org.hibernate.validator:hibernate-validator:jar:6.0.14.Final:compile
      |  +- javax.validation:validation-api:jar:2.0.1.Final:compile
      |  +- org.jboss.logging:jboss-logging:jar:3.3.2.Final:compile
      |  \- com.fasterxml:classmate:jar:1.4.0:compile
      +- org.springframework:spring-web:jar:5.1.4.RELEASE:compile
      |  \- org.springframework:spring-beans:jar:5.1.4.RELEASE:compile
      \- org.springframework:spring-webmvc:jar:5.1.4.RELEASE:compile
         +- org.springframework:spring-aop:jar:5.1.4.RELEASE:compile
         +- org.springframework:spring-context:jar:5.1.4.RELEASE:compile
         \- org.springframework:spring-expression:jar:5.1.4.RELEASE:compile
   ```

   

3. 排除起步依赖中的某一项直接加入exclusions标签或者直接进行覆盖

   ```xml
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-web</artifactId>
       <exclusions>
           <exclusion>
               <groupId>com.fasterxml.jackson.core</groupId>
           </exclusion>
       </exclusions>
   </dependency>
   
   <!--和上面作用大致相同-->
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-web</artifactId>
   </dependency>
   <dependency>
       <groupId>com.fasterxml.jackson.core</groupId>
       <artifactId>jackson-databind</artifactId>
       <version>2.4.3</version>
   </dependency>
   ```

### 读取配置

1. springboot获取配置属性的不同方式，优先级从高到低排序如下

   - 命令行参数

     ```sh
     //配置spring.main.show-banner=false
     java -jar readinglist-0.0.1-SNAPSHOT.jar --spring.main.show-banner=false
     ```

   - java:comp/env里的JNDI属性

   - JVM系统属性

   - 操作系统环境变量

     ```sh
     export spring_main_show_banner=false
     ```

   - 通过application.yml或者application.properties

     ```yml
     #properties配置文件
     spring.main.show-banner=false
     
     #yml配置文件
     spring:
         main:
         show-banner: false
     ```

2. application.properties和application.yml文件能放在以下四个位置，优先级从高到低如下

   - 外置，在相对于应用程序运行目录的/config子目录里

   - 外置，在应用程序运行的目录里

   - 内置，在config包内

   - 内置，在Classpath根目录

### 日志集成

1. springboot的spring-boot-starter默认引入了spring-boot-starter-logging包
2. spring-boot-starter-logging默认引入了slf4j+logback，如果要使用其他日志先排除logback包，调整日志配置如下
    ```yml
    #全局级别
    logging:
    	level:
    		root: INFO
    		
    #局部级别
    logging:
    	level:
    		cn.enjoy.controller: DEBUG
    		
    #其他日志文件等配置建议直接在logback.xml中配置
    ```

### 多数据库事务XA（参考）

1. pom引入
    ```xml
    <dependency>
        <groupId>org.mybatis.spring.boot</groupId>
        <artifactId>mybatis-spring-boot-starter</artifactId>
        <version>1.3.0</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-jta-atomikos</artifactId>
    </dependency>
    ```
2. application.yml配置
    ```yml
    spring:
        datasource:
            spring:
                driverClassName: com.mysql.jdbc.Driver
                jdbcUrl: jdbc:mysql://127.0.0.1:3306/spring?serverTimezone=GMT%2B8
                username: root
                password: root1234%

            spring2:
                driverClassName: com.mysql.jdbc.Driver
                jdbcUrl: jdbc:mysql://127.0.0.1:3306/spring2?serverTimezone=GMT%2B8
                username: root
                password: root1234%
    ```
3. DBConfig
    ```java
    //DBConfig1
    @Data
    @Component
    @ConfigurationProperties(prefix = "spring.datasource.spring")
    public class DBConfig1 {
        private String driverClassName;
        private String jdbcUrl;
        private String username;
        private String password;
    }

    //DBConfig2
    @Data
    @Component
    @ConfigurationProperties(prefix = "spring.datasource.spring2")
    public class DBConfig2 {
        private String driverClassName;
        private String jdbcUrl;
        private String username;
        private String password;
    }
    ```
4. DataSourceConfig
    ```java
    //DataSource1Config
    @Configuration
    @MapperScan(basePackages = "cn.enjoy.dao.users", sqlSessionFactoryRef = "test1SqlSessionFactory")
    public class DataSource1Config {

        @Bean(name = "test1SqlSessionFactory")
        @Primary
        public SqlSessionFactory testSqlSessionFactory(@Qualifier("test1DataSource") DataSource dataSource)
                throws Exception {
            SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
            bean.setDataSource(dataSource);
            bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapping/users/*.xml"));
            return bean.getObject();
        }

        @Bean(name = "test1DataSource")
        @Primary
        public DataSource testDataSource(DBConfig1 config1) {
            MysqlXADataSource mysqlXADataSource=new MysqlXADataSource();
            mysqlXADataSource.setUrl(config1.getJdbcUrl());
            mysqlXADataSource.setPassword(config1.getPassword());
            mysqlXADataSource.setUser(config1.getUsername());

            AtomikosDataSourceBean atomikosDataSourceBean=new AtomikosDataSourceBean();
            atomikosDataSourceBean.setXaDataSource(mysqlXADataSource);
            atomikosDataSourceBean.setUniqueResourceName("test1Datasource");
            return atomikosDataSourceBean;

        }

        @Bean(name = "test1SqlSessionTemplate")
        @Primary
        public SqlSessionTemplate testSqlSessionTemplate(
                @Qualifier("test1SqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
            return new SqlSessionTemplate(sqlSessionFactory);
        }

    }

    //DataSource2Config
    @Configuration
    @MapperScan(basePackages = "cn.enjoy.dao.orders", sqlSessionFactoryRef = "test2SqlSessionFactory")
    public class DataSource2Config {

        @Bean(name = "test2SqlSessionFactory")
        public SqlSessionFactory testSqlSessionFactory(@Qualifier("test2DataSource") DataSource dataSource)
                throws Exception {
            SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
            bean.setDataSource(dataSource);
            bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapping/orders/*.xml"));
            return bean.getObject();
        }

        @Bean(name = "test2DataSource")
        public DataSource testDataSource(DBConfig2 config2) {
            MysqlXADataSource mysqlXADataSource=new MysqlXADataSource();
            mysqlXADataSource.setUrl(config2.getJdbcUrl());
            mysqlXADataSource.setPassword(config2.getPassword());
            mysqlXADataSource.setUser(config2.getUsername());

            AtomikosDataSourceBean atomikosDataSourceBean=new AtomikosDataSourceBean();
            atomikosDataSourceBean.setXaDataSource(mysqlXADataSource);
            atomikosDataSourceBean.setUniqueResourceName("test2Datasource");
            return atomikosDataSourceBean;

        }

        @Bean(name = "test2SqlSessionTemplate")
        public SqlSessionTemplate testSqlSessionTemplate(
                @Qualifier("test2SqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
            return new SqlSessionTemplate(sqlSessionFactory);
        }

    }
    ```

5. service事务示例
    ```java
    //出现异常，两个数据库的数据都会回滚
    @Transactional
    public void addOrder(Orders orders, Users users) {
        usersMapper.insertSelective(users);
        int i = 10/0;
        ordersMapper.insertSelective(orders);
    }
    ```