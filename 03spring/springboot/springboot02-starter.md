### Starter加载机制

1. 启动代码，针对注解进行解析

   ```java
   @SpringBootApplication
   public class StudyApplication {
   
       public static void main(String[] args) {
           SpringApplication.run(StudyApplication.class, args);
       }
   }
   ```

2. @SpringBootApplication

   ```java
   //声明是一个springboot的配置类，是@Configuration子类型
   @SpringBootConfiguration
   //启动自动配置功能，核心配置
   @EnableAutoConfiguration
   //包扫描器，在@SpringBootApplication中配置的扫描属性最终会设置为@ComponentScan的属性
   @ComponentScan(excludeFilters = { @Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class),
   		@Filter(type = FilterType.CUSTOM, classes = AutoConfigurationExcludeFilter.class) })
   public @interface SpringBootApplication {
     	//手动设置包扫描路径，最终是设置到了@ComponentScan中去
   		@AliasFor(annotation = ComponentScan.class, attribute = "basePackageClasses")
   		Class<?>[] scanBasePackageClasses() default {};
     	@AliasFor(annotation = ComponentScan.class, attribute = "basePackages")
   		String[] scanBasePackages() default {};
   		//省略部分属性
   }
   ```

3. @EnableAutoConfiguration

   ```java
   //自动配置扫描启动类的包及其子包路径的所有spring组件
   @AutoConfigurationPackage
   //自动配置类扫描导入
   @Import(AutoConfigurationImportSelector.class)
   public @interface EnableAutoConfiguration {
   		//省略属性
   }
   ```

4. @AutoConfigurationPackage

   ```java
   //引入AutoConfigurationPackages.Registrar进行注册bean
   @Import(AutoConfigurationPackages.Registrar.class)
   public @interface AutoConfigurationPackage {
   		//省略属性
   }
   
   //AutoConfigurationPackages.Registrar
   static class Registrar implements ImportBeanDefinitionRegistrar, DeterminableImports {
   		
     	//spring的ImportBeanDefinitionRegistrar子类ioc会执行这个方法
       @Override
       public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {
         	//注册进ioc
         	//new PackageImports(metadata).getPackageNames().toArray(new String[0])返回的是启动类的包名
         	//默认扫描路径就是启动类的包及其子包路径
           register(registry, new PackageImports(metadata).getPackageNames().toArray(new String[0]));
       }
   
   }
   ```

5. AutoConfigurationImportSelector

   ```java
   public class AutoConfigurationImportSelector
           implements DeferredImportSelector, BeanClassLoaderAware, ResourceLoaderAware,
           BeanFactoryAware, EnvironmentAware, Ordered {
       // 这个方法告诉springboot都需要导入那些组件
       @Override
       public String[] selectImports(AnnotationMetadata annotationMetadata) {
           //判断 enableautoconfiguration注解有没有开启，默认开启（是否进行自动装配）
           if (!isEnabled(annotationMetadata)) {
               return NO_IMPORTS;
           }
           //1. 加载配置文件META-INF/spring-autoconfigure-metadata.properties，从中获取所有支持自动配置类的条件
           //作用：SpringBoot使用一个Annotation的处理器来收集一些自动装配的条件，那么这些条件可以在META-INF/spring-autoconfigure-metadata.properties进行配置。
           // SpringBoot会将收集好的@Configuration进行一次过滤进而剔除不满足条件的配置类
           // 自动配置的类全名.条件=值
           AutoConfigurationMetadata autoConfigurationMetadata = AutoConfigurationMetadataLoader.loadMetadata(this.beanClassLoader);
           AutoConfigurationEntry autoConfigurationEntry = getAutoConfigurationEntry(autoConfigurationMetadata, annotationMetadata);
           return StringUtils.toStringArray(autoConfigurationEntry.getConfigurations());
       }
   }
   ```

6. AutoConfigurationMetadataLoader#loadMetadata

   ```java
   static AutoConfigurationMetadata loadMetadata(ClassLoader classLoader, String path) {
   	try {
   		//1.读取spring-boot-autoconfigure.jar包中spring-autoconfigure-metadata.properties的信息生成urls枚举对象
   		// 获得 PATH 对应的 URL 们
   		Enumeration<URL> urls = (classLoader != null) ? classLoader.getResources(path) : ClassLoader.getSystemResources(path);
   		// 遍历 URL 数组，读取到 properties 中
   		Properties properties = new Properties();
   
   		//2.解析urls枚举对象中的信息封装成properties对象并加载
   		while (urls.hasMoreElements()) {
   			properties.putAll(PropertiesLoaderUtils.loadProperties(new UrlResource(urls.nextElement())));
   		}
   		// 将 properties 转换成 PropertiesAutoConfigurationMetadata 对象
   
   		//根据封装好的properties对象生成AutoConfigurationMetadata对象返回
   		return loadMetadata(properties);
   	} catch (IOException ex) {
   		throw new IllegalArgumentException("Unable to load @ConditionalOnClass location [" + path + "]", ex);
   	}
   }
   ```

7. AutoConfigurationImportSelector#getAutoConfigurationEntry，获得AutoConfigurationEntry对象

   ```java
   protected AutoConfigurationEntry getAutoConfigurationEntry(AutoConfigurationMetadata autoConfigurationMetadata, AnnotationMetadata annotationMetadata) {
   	// 1. 判断是否开启注解。如未开启，返回空串
   	if (!isEnabled(annotationMetadata)) {
   		return EMPTY_ENTRY;
   	}
   	// 2. 获得注解的属性
   	AnnotationAttributes attributes = getAttributes(annotationMetadata);
   
   	// 3. getCandidateConfigurations()用来获取默认支持的自动配置类名列表
   	// springboot在启动的时候，使用内部工具类SpringFactoriesLoader，查找classpath上所有jar包中的META-INF/spring.factories，
   	// 找出其中key为org.springframework.boot.autoconfigure.EnableAutoConfiguration的属性定义的工厂类名称，
   	// 将这些值作为自动配置类导入到容器中，自动配置类就生效了
   	List<String> configurations = getCandidateConfigurations(annotationMetadata, attributes);
   
   
   	// 3.1 //去除重复的配置类，若我们自己写的starter 可能存在重复的
   	configurations = removeDuplicates(configurations);
   	// 4. 如果项目中某些自动配置类，我们不希望其自动配置，我们可以通过EnableAutoConfiguration的exclude或excludeName属性进行配置，
   	// 或者也可以在配置文件里通过配置项“spring.autoconfigure.exclude”进行配置。
   	//找到不希望自动配置的配置类（根据EnableAutoConfiguration注解的一个exclusions属性）
   	Set<String> exclusions = getExclusions(annotationMetadata, attributes);
   	// 4.1 校验排除类（exclusions指定的类必须是自动配置类，否则抛出异常）
   	checkExcludedClasses(configurations, exclusions);
   	// 4.2 从 configurations 中，移除所有不希望自动配置的配置类
   	configurations.removeAll(exclusions);
   
   	// 5. 对所有候选的自动配置类进行筛选，根据项目pom.xml文件中加入的依赖文件筛选出最终符合当前项目运行环境对应的自动配置类
   
   	//@ConditionalOnClass ： 某个class位于类路径上，才会实例化这个Bean。
   	//@ConditionalOnMissingClass ： classpath中不存在该类时起效
   	//@ConditionalOnBean ： DI容器中存在该类型Bean时起效
   	//@ConditionalOnMissingBean ： DI容器中不存在该类型Bean时起效
   	//@ConditionalOnSingleCandidate ： DI容器中该类型Bean只有一个或@Primary的只有一个时起效
   	//@ConditionalOnExpression ： SpEL表达式结果为true时
   	//@ConditionalOnProperty ： 参数设置或者值一致时起效
   	//@ConditionalOnResource ： 指定的文件存在时起效
   	//@ConditionalOnJndi ： 指定的JNDI存在时起效
   	//@ConditionalOnJava ： 指定的Java版本存在时起效
   	//@ConditionalOnWebApplication ： Web应用环境下起效
   	//@ConditionalOnNotWebApplication ： 非Web应用环境下起效
   
   	//总结一下判断是否要加载某个类的两种方式：
   	//根据spring-autoconfigure-metadata.properties进行判断。
   	//要判断@Conditional是否满足
   	// 如@ConditionalOnClass({ SqlSessionFactory.class, SqlSessionFactoryBean.class })表示需要在类路径中存在SqlSessionFactory.class、SqlSessionFactoryBean.class这两个类才能完成自动注册。
   	configurations = filter(configurations, autoConfigurationMetadata);
   
   
   	// 6. 将自动配置导入事件通知监听器
   	//当AutoConfigurationImportSelector过滤完成后会自动加载类路径下Jar包中META-INF/spring.factories文件中 AutoConfigurationImportListener的实现类，
   	// 并触发fireAutoConfigurationImportEvents事件。
   	fireAutoConfigurationImportEvents(configurations, exclusions);
   	// 7. 创建 AutoConfigurationEntry 对象
   	return new AutoConfigurationEntry(configurations, exclusions);
   }
   ```

8. AutoConfigurationImportSelector#getCandidateConfigurations，获取META-INF/spring.factories中的配置类

   ```java
   // 让SpringFactoryLoader去加载一些组件的名字
   //getSpringFactoriesLoaderFactoryClass() = EnableAutoConfiguration.class自动配置类
   List<String> configurations = SpringFactoriesLoader.loadFactoryNames(getSpringFactoriesLoaderFactoryClass(), getBeanClassLoader());
   
   //SpringFactoriesLoader#loadSpringFactories
   //获取类加载器
   //FACTORIES_RESOURCE_LOCATION = META-INF/spring.factories
   /**
    * spring.factories类似，key为org.springframework.boot.autoconfigure.EnableAutoConfiguration
    * org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
    * org.springframework.boot.autoconfigure.admin.SpringApplicationAdminJmxAutoConfiguration,\
    * org.springframework.boot.autoconfigure.aop.AopAutoConfiguration,\
    * org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration,\
    * org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration,\
    * org.springframework.boot.autoconfigure.cache.CacheAutoConfiguration,\
    * org.springframework.boot.autoconfigure.cassandra.CassandraAutoConfiguration,\
    */
   Enumeration<URL> urls = (classLoader != null ?
           classLoader.getResources(FACTORIES_RESOURCE_LOCATION) :
           ClassLoader.getSystemResources(FACTORIES_RESOURCE_LOCATION));
   result = new LinkedMultiValueMap<>();
           while (urls.hasMoreElements()) {
       URL url = urls.nextElement();
       UrlResource resource = new UrlResource(url);
       Properties properties = PropertiesLoaderUtils.loadProperties(resource);
       for (Map.Entry<?, ?> entry : properties.entrySet()) {
           String factoryTypeName = ((String) entry.getKey()).trim();
           for (String factoryImplementationName : StringUtils.commaDelimitedListToStringArray((String) entry.getValue())) {
               result.add(factoryTypeName, factoryImplementationName.trim());
           }
       }
   }
   ```

   

### 自定义配置

@ConditionalOnMissingBean，当缺少某个bean的时候，就采用默认配置

```java
//RedisTemplate的默认配置
//如果代码中定义了RedisTemplate的配置，这个默认配置就会失效
@Bean
@ConditionalOnMissingBean(name = "redisTemplate")
public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory)
        throws UnknownHostException {
    RedisTemplate<Object, Object> template = new RedisTemplate<>();
    template.setConnectionFactory(redisConnectionFactory);
    return template;
}
```



### 自定义redis-starter

1. pom引入redis对应的客户端包

   ```xml
   <dependency>
       <groupId>redis.clients</groupId>
       <artifactId>jedis</artifactId>
       <version>3.0.1</version>
   </dependency>
   ```

2. 读取redis配置文件的类RedisProperties

   ```java
   @Date
   @ConfigurationProperties(prefix = "redis")
   public class RedisProperties {
   
       private String host;
       private int port;
   }
   ```

3. 加载配置，实例化redis客户端

   ```java
   @Configuration //开启配置
   @ConditionalOnClass(Jedis.class)
   @EnableConfigurationProperties(RedisProperties.class) //开启使用映射实体对象
   @ConditionalOnProperty//存在对应配置信息时初始化该配置类
   (
   prefix = "redis",//存在配置前缀redis
   value = "enabled",//开启
   matchIfMissing = true//缺失检查
   )
   public class RedisAutoConfiguration {
       @Bean
       @ConditionalOnMissingBean
       public Jedis jedis(RedisProperties redisProperties){
           return new Jedis(redisProperties.getHost(), redisProperties.getPort());
       }
   }
   ```

4. 在resource目录新建META-INF/spring.factories

   ```factories
   org.springframework.boot.autoconfigure.EnableAutoConfiguration=cn.enjoy.redis.RedisAutoConfiguration
   ```

5. 客户端使用时只需要maven引入这个jar包+Jedis的包就可以了