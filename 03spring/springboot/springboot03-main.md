### SpringApplication实例化

1. 构造方法，设置的属性后面都会用到

   ```java
   public SpringApplication(ResourceLoader resourceLoader, Class<?>... primarySources) {
       //项目启动类 SpringbootDemoApplication.class设置为属性存储起来
       this.primarySources = new LinkedHashSet<>(Arrays.asList(primarySources));
   
       //设置应用类型是SERVLET应用（Spring 5之前的传统MVC应用）还是REACTIVE应用（Spring 5开始出现的WebFlux交互式应用）
       this.webApplicationType = WebApplicationType.deduceFromClasspath();
   
       //设置初始化器(Initializer),最后会调用这些初始化器
       //所谓的初始化器就是org.springframework.context.ApplicationContextInitializer的实现类,在Spring上下文被刷新之前进行初始化的操作
       setInitializers((Collection) getSpringFactoriesInstances(ApplicationContextInitializer.class));
   
       // 设置监听器(Listener)
       setListeners((Collection) getSpringFactoriesInstances(ApplicationListener.class));
   
       // 初始化 mainApplicationClass 属性:用于推断并设置项目main()方法启动的主程序启动类
       this.mainApplicationClass = deduceMainApplicationClass();
   }
   ```

2. SpringApplication#getSpringFactoriesInstances，设置初始化器和监听器的方法

   ```java
   private <T> Collection<T> getSpringFactoriesInstances(Class<T> type,
                                                         Class<?>[] parameterTypes, Object... args) {
       ClassLoader classLoader = getClassLoader();
       // Use names and ensure unique to protect against duplicates
       // 加载指定类型对应的，在 `META-INF/spring.factories` 里的类名的数组
       //前面starter机制也是使用的这个
       Set<String> names = new LinkedHashSet<>(SpringFactoriesLoader.loadFactoryNames(type, classLoader));
       //org.springframework.boot.autoconfigure.SharedMetadataReaderFactoryContextInitializer,\
       //org.springframework.boot.autoconfigure.logging.ConditionEvaluationReportLoggingListener
       // 根据names来进行实例化
       List<T> instances = createSpringFactoriesInstances(type, parameterTypes, classLoader, args, names);
       // 对实例进行排序
       AnnotationAwareOrderComparator.sort(instances);
       return instances;
   }
   ```

### SpringApplication#run方法

```java
public ConfigurableApplicationContext run(String... args) {
    // 创建 StopWatch 对象，并启动。StopWatch 主要用于简单统计 run 启动过程的时长。
    StopWatch stopWatch = new StopWatch();
    stopWatch.start();
    // 初始化应用上下文和异常报告集合
    ConfigurableApplicationContext context = null;
    Collection<SpringBootExceptionReporter> exceptionReporters = new ArrayList<>();
    // 配置 headless 属性
    configureHeadlessProperty();
    //（1）获取并启动监听器
  	// 从META-INF/spring.factories中读取Key为SpringApplicationRunListener的Values
    SpringApplicationRunListeners listeners = getRunListeners(args);
    listeners.starting();
    try {
        // 创建  ApplicationArguments 对象 初始化默认应用参数类
        // args是启动Spring应用的命令行参数，该参数可以在Spring应用中被访问。如：--server.port=9000
        ApplicationArguments applicationArguments = new DefaultApplicationArguments(args);

        //（2）项目运行环境Environment的预配置
        // 创建并配置当前SpringBoot应用将要使用的Environment
        // 并遍历调用所有的SpringApplicationRunListener的environmentPrepared()方法
        ConfigurableEnvironment environment = prepareEnvironment(listeners, applicationArguments);

        configureIgnoreBeanInfo(environment);
        // 准备Banner打印器 - 就是启动Spring Boot的时候打印在console上的ASCII艺术字体
        Banner printedBanner = printBanner(environment);

        // （3）创建Spring容器
      	// 会根据构造函数中的webApplicationType来选择创建哪种容器
        context = createApplicationContext();
        // 获得异常报告器 SpringBootExceptionReporter 数组
        // 从META-INF/spring.factories中读取Key为SpringBootExceptionReporter的Values
        exceptionReporters = getSpringFactoriesInstances(
                SpringBootExceptionReporter.class,
                new Class[] { ConfigurableApplicationContext.class }, context);
      
        // （4）Spring容器前置处理
        //这一步主要是在容器刷新之前的准备动作。包含一个非常关键的操作：将启动类注入容器，为后续开启自动化配置奠定基础。
        prepareContext(context, environment, listeners, applicationArguments,
                printedBanner);

        // （5）：刷新容器
      	// 会调用AbstractApplicationContext#refresh
      	// 开启（刷新）Spring 容器,通过refresh方法对整个IoC容器的初始化（包括Bean资源的定位、解析、注册等等）
        refreshContext(context);

        // （6）：Spring容器后置处理
        // 扩展接口，设计模式中的模板方法，默认为空实现。
        // 如果有自定义需求，可以重写该方法。比如打印一些启动结束log，或者一些其它后置处理
        afterRefresh(context, applicationArguments);
        // 停止 StopWatch 统计时长
        stopWatch.stop();
        // 打印 Spring Boot 启动的时长日志
        if (this.logStartupInfo) {
            new StartupInfoLogger(this.mainApplicationClass).logStarted(getApplicationLog(), stopWatch);
        }
        // （7）发出结束执行的事件通知
        listeners.started(context);

        // （8）：执行Runners
        //用于调用项目中自定义的执行器XxxRunner类，使得在项目启动完成后立即执行一些特定程序
        //Runner 运行器用于在服务启动时进行一些业务初始化操作，这些操作只在服务启动后执行一次。
        //Spring Boot提供了ApplicationRunner和CommandLineRunner两种服务接口
      	//使用context直接获取这两中类型的bean接口，然遍历执行
        callRunners(context, applicationArguments);
    } catch (Throwable ex) {
        // 如果发生异常，则进行处理，并抛出 IllegalStateException 异常
        handleRunFailure(context, ex, exceptionReporters, listeners);
        throw new IllegalStateException(ex);
    }

    //   (9)发布应用上下文就绪事件
    //表示在前面一切初始化启动都没有问题的情况下，使用运行监听器SpringApplicationRunListener持续运行配置好的应用上下文ApplicationContext，
    // 这样整个Spring Boot项目就正式启动完成了。
    try {
        listeners.running(context);
    } catch (Throwable ex) {
        // 如果发生异常，则进行处理，并抛出 IllegalStateException 异常
        handleRunFailure(context, ex, exceptionReporters, null);
        throw new IllegalStateException(ex);
    }
    //返回容器
    return context;
}
```

### 内嵌Tomcat

1. spring.factories中会加载ServletWebServerFactoryAutoConfiguration类

2. ServletWebServerFactoryAutoConfiguration，web容器的自动装配类

   ```java
   @EnableConfigurationProperties(ServerProperties.class)
   @Import({ ServletWebServerFactoryAutoConfiguration.BeanPostProcessorsRegistrar.class,
           //spring-boot-starter-web包含了tomcat的依赖，tomcat容器满足条件
           ServletWebServerFactoryConfiguration.EmbeddedTomcat.class,
           ServletWebServerFactoryConfiguration.EmbeddedJetty.class,
           ServletWebServerFactoryConfiguration.EmbeddedUndertow.class })
   public class ServletWebServerFactoryAutoConfiguration {
       //...省略
   }
   ```

3. ServerProperties，就是我们所配置的端口主机号，根地址等

   ```java
   @ConfigurationProperties(prefix = "server", ignoreUnknownFields = true)
   public class ServerProperties {
       private Integer port;
       private InetAddress address;
   }
   ```

   

4. EmbeddedTomcat，生成TomcatServletWebServerFactory

   ```java
   @Configuration(proxyBeanMethods = false)
   @ConditionalOnClass({ Servlet.class, Tomcat.class, UpgradeProtocol.class })
   @ConditionalOnMissingBean(value = ServletWebServerFactory.class, search = SearchStrategy.CURRENT)
   static class EmbeddedTomcat {
   
       @Bean
       TomcatServletWebServerFactory tomcatServletWebServerFactory(
               ObjectProvider<TomcatConnectorCustomizer> connectorCustomizers,
               ObjectProvider<TomcatContextCustomizer> contextCustomizers,
               ObjectProvider<TomcatProtocolHandlerCustomizer<?>> protocolHandlerCustomizers) {
           TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
           factory.getTomcatConnectorCustomizers()
                   .addAll(connectorCustomizers.orderedStream().collect(Collectors.toList()));
           factory.getTomcatContextCustomizers()
                   .addAll(contextCustomizers.orderedStream().collect(Collectors.toList()));
           factory.getTomcatProtocolHandlerCustomizers()
                   .addAll(protocolHandlerCustomizers.orderedStream().collect(Collectors.toList()));
           return factory;
       }
   
   }
   ```

5. TomcatServletWebServerFactory#getWebServer，是生成Tomcat的工厂

   ```java
   Tomcat tomcat = new Tomcat();
   //省略tomcat的一些参数初始化
   return getTomcatWebServer(tomcat);
   
   //TomcatServletWebServerFactory#getTomcatWebServer，实例化TomcatWebServer
   return new TomcatWebServer(tomcat, getPort() >= 0, getShutdown());
   ```

6. TomcatWebServer，构造方法会调用initialize方法

   ```java
   //TomcatWebServer#initialize
   //省略其他代码
   //初始化tomcat
   this.tomcat.start();
   ```

7. 也就是说谁调用TomcatServletWebServerFactory#getWebServer，那么就是那里实例化的tomcat的

8. 回到前面的SpringApplication#run

   ```java
   //context = createApplicationContext ，context = AnnotationConfigServletWebServerApplicationContext
   //refreshContext(context)，会去调用AbstractApplicationContext#refresh
   //AbstractApplicationContext#refresh，调用AnnotationConfigServletWebServerApplicationContext#onRefresh
   protected void onRefresh() {
       super.onRefresh();
       try {
         	//会去获取ServletWebServerFactory，然后调用getWebServer
           createWebServer();
       }
       catch (Throwable ex) {
           throw new ApplicationContextException("Unable to start web server", ex);
       }
   }
   ```
