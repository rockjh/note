eureka-client
1. 读取client的配置信息
2. 获取server配置信息
3. 从server拉取注册表信息缓存到本地
4. 服务注册
5. 初始化发送心跳、注册表缓存刷新
6. 定时发送心跳、更新注册表缓存
7. 结束时，向服务发起注销

eureka-server
1. 接收注册
2. 服务主动下线
3. 服务剔除（定时任务）
4. 服务续约更新

ribbon
1. 使用springboot的启用注解，@Import方式引用
    - IClientConfig，client的配置类，具体指的DefaultClientConfigImpl
    - IRule，负载均衡的策略类,默认轮询RoundRobinRule
    - IPing，服务可用性检查，默认DummyPing
    - ServerList，服务列表获取，ConfigurationBasedServerList
    - ServerListFilter，服务列表过滤，ZonePreferenceServerListFilter

2. 常见的负载均衡策略
    - BestAvailableRule，选择最小请求数的服务器
    - RoundRobinRule，轮询
    - ClientConfigEnabledRoundRobinRule，使用RoundRobinRule选择服务器
    - RetryRule，根据选的轮询的方式重试
    - WeightedResponseTimeRule，根据响应时间去计算一个权重weight，weight越低，被选择的可能性就越低
    - ZoneAvoidanceRule，根据server的zone区域和可用性来轮询选择

feign
1. feign依赖ribbon，也就是feign作为eureka client的，可以直接配置ribbon的负载均衡策略
    ```
    Feign = RestTempate + HttpHeader + Ribbon + Eureka
    ```
1. 写一个接口，声明和服务提供方做得一致，后面会被扫描到
2. spirng将这个接口注册进入IOC容器，以FactoryBean的形式注入
3. FactoryBean的getObject获取对象
4. 通过子容器，获取到一个Client，最后通过动态代理的形式
5. 最终执行时，使用的客户端是LoadBalancerFeignClient，LoadBalancerFeignClient封装了负载均衡lbClientFactory

hystrix
1. hystrix是针对调用服务异常的容错处理（快速失败），防止服务级联失败造成服务雪崩
    - 服务提供方存活，但调用接口报错（熔断）
    - 服务提供方系统崩溃，例如网络不通（降级）
2. 当服务提供方抛出异常的时候，会有一个快速失败的方法
    ```
    @HystrixCommand(commandProperties = {
        @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"),
        @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "10000"),
        @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "60")
    },fallbackMethod = "getFallback")
    ```
3. 当服务提供方网络异常时，客户端会降级处理
    ```
    @FeignClient(name = "MICROCLOUD-PROVIDER-PRODUCT",configuration = FeignClientConfig.class,fallbackFactory = IProductClientServiceFallbackFactory.class)
    ```
4. hystrix是通过aop来实现的，切入点是注解

zuul
1. zuul、ribbon、euraka相结合，可以实现智能路由和负载均衡的功能，zuul能够将请求流量按某种策略分发到集群状态的多个服务实例
2. 网关将所有api聚合，统一对外暴露，外界系统不需要知道微服务中各个服务调用的复杂性。也保护了内部微服务单元的api接口，防止其被外界直接调用，导致服务的敏感信息对外暴露
3. 网关服务可以用作用户身份认证和权限认证，防止非法请求操作api接口，对服务器起保护作用
4. 网关可以实现监控，实时日志输出，对请求进行记录
5. 网关可以用来实现流量监控，在高流量的情况下，对服务进行降级。就是当服务器压力剧增的情况下根据当前的业务情况对一些服务有策略的进行降级，以此释放服务器资源，保证核心任务的正常运行，一般服务降级的方式有：服务接口拒绝服务、页面拒绝服务、延迟持久化
6. api接口从内部分离出来，方便做测试

服务消费流程

1. zuul类似于spring mvc中的DispatcServlet来对请求进行控制，他的核心是一系列过滤器，可以在http请求的发起和相应返回期间执行一系列过滤器，主要包括以下4中过滤器
    - PRE过滤器，他是在请求路由到具体服务之前执行，适合做安全认证，身份认证，参数校验
    - ROUTING过滤器，它用于将请求路由到具体的服务实例，默认情况下使用http client进行网络请求
    - POST过滤器，他是在请求已被路由到服务后执行的，一般情况下用于收集统计信息、指标、以及响应传输到客户端
    - ERROR过滤器，在其他过滤器发生错误时执行
2. zuul采取了动态读取、编译和运行这些过滤器，过滤器之间不能直接通信，通过，RequestContext对象来共享数据，每一个请求都会创建一个RequestContext对象。zuul过滤器具有以下特性：
    - Type类型，如果PRE、ROUTING、POST、ERROR
    - Execution Order执行顺序，order值越小，越先执行
    - Criteria标准，Filter执行所需的条件
    - Action行动，如果符合执行条件，则执行逻辑代码

zuul集群

1. 复制多个zuul，修改不同的端口，feign配置负载均衡策略访问zuul集群
2. 多个zuul连接注册中心集群B注册，feign连接注册中心集群B注册
3. 当提供的服务是集群时，zuul会自动进行负载均衡，因为zuul默认集成了ribbon，可以直接配置负载均衡策略
4. feign访问zuul，zuul通过服务名访问注册中心注册的服务提供者

config

1. 分布式配置中心，可以从本地仓库读取配置文件，也可以从远程git仓库读取配置文件。本地仓库是指将所有的配置文件统一写在config server工程目录下，config server暴露http api接口，config client通过http api接口来查询读取配置文件
2. springboot中
    - application.yml，对应的是用户级的资源配置项
    - bootstrap.yml，对应的是系统级的资源配置，其优先级更高

eureka和zk各自的优势

1. zk主要是保持CP，一致性和分区容错性（不保证高可用），在选举的时候会拒绝服务，如果因为网络分区问题，超过半数的机器宕机后，整个集群剩余的机器处于崩溃恢复阶段，不可访问，实际上提供服务的节点是正常的
2. eureka主要保持AP，高可用和分区容错性（不保证一致性，注册中心集群之间的信息可能不对等），集群是对等的，地位相同，可能不能保证一致性，但是当出现网络问题的时候eureka会通过心跳去剔除服务，但是有一个前提就是如果很多服务都会被剔除，会启用保护机制，即时有些服务eureka心跳连接不上也会被保留，因为这些服务可能是正常的，eureka是因为网络分区问题导致的不能心跳这些正常的服务提供者