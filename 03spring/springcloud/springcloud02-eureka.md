### 配置使用

#### server

1. 搭建高可用集群的注册中心服务端
2. 引入pom配置
    ```
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
    </dependency>
    ```
3. application.yml
    ```
    #第一台
    server:
        port: 7001
    eureka:
        server:
            ##设置清理的间隔时间，而后这个时间使用的是毫秒单位（默认是60秒）
            eviction-interval-timer-in-ms: 1000
            #设置为false表示关闭保护模式
            enable-self-preservation: true
        client:
            #要不要去注册中心获取其他服务的地址
            #也就是在注册中心点击链接的时候，是不是显示的ip地址
            fetch-registry: false
            #不作为一个客户端去注册
            register-with-eureka: false
            service-url:
                #这个参数导致eureka相互注册，形成集群注册中心
                defaultZone: http://eureka1:7001/eureka,http://eureka2:7002/eureka,http://@eureka3:7003/eureka
        instance: # eureak实例定义
            hostname: eureka1  #定义Eureka实例所在的主机名称
    
    #第二台和第三台都需要修改
    #访问端口port，7002、7003
    #hostname修改为自己的主机名eureka1、eureka2、eureka3
    ```
4. 启动类
    ```
    @SpringBootApplication
    @EnableEurekaServer
    public class EurekaApp {
        public static void main(String[] args) {
            SpringApplication.run(EurekaApp.class,args);
        }
    }
    ```

#### client

1. 搭建高可用的客户端调用
2. 引入pom配置
    ```
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    ```
3. application.yml
    ```
    spring:
        application:
            #集群的关键，集群的客户端这个值必须相同
            name: microcloud-provider-product
    eureka:
        client: # 客户端进行Eureka注册的配置
            register-with-eureka: true
            service-url:
                defaultZone: http://admin:enjoy@eureka1:7001/eureka,http://admin:enjoy@eureka2:7002/eureka,http://admin:enjoy@eureka3:7003/eureka
        instance:
            instance-id: microcloud-provider-product
            prefer-ip-address: true
            lease-renewal-interval-in-seconds: 2 # 设置心跳的时间间隔（默认是30秒）
            lease-expiration-duration-in-seconds: 5  # 如果现在超过了5秒的间隔（默认是90秒）

    ```
4. 启动类
    ```
    @SpringBootApplication
    @EnableEurekaClient
    public class ConsumerApp {
        public static void main(String[] args) {
            SpringApplication.run(ConsumerApp.class,args);
        }
    }

    ```
### 源码解析-client

1. client阶段总结
    - 应用启动阶段
        - 读取与eureka server交互的配置信息，封装成 EurekaClientConfig
        - 读取自身服务实例配置信息，封装成EurekalnstanceConfig
        - 从eureka server拉取注册表信息并缓存到本地
        - 服务注册
        - 初始化发送心跳、缓存刷新（拉取注册表信息更新本地缓存）和按需注册（监控服务实例信息变化,决定是否重新发起注册,更新注册表中的服务实例元数据）定时任务
    - 应用执行阶段
        - 定时发送心跳到eureka server中维持在注册表的租约
        - 定时从eureka server中拉取注册表信息，更新本地注册表缓存
        - 监控应用自身信息变化，若发生变化，需要重新发起服务注册
    - 应用销毁阶段
        - 从eureka server注销自身服务实例
2. 根据springboot的加载，找到eureka相关的spring.factories相关配置类，一些相关自动配置的类
    - EurekaClientAutoConfiguration，EurekeClient自动配置类，负责Eureka关键Beans的配置和初始化，如AppplicationInfoManager和EurekaClientConfig等
    - RibbonEurekaAutoConfiguration，Ribbon负载均衡相关配置
    - EurekaDiscoveryClientConfiguration，配置自动注册和应用的健康检查器
3. EurekaClientAutoConfiguration中的各项配置bean的作用
    - EurekaClientConfig，为其提供了一个默认配置类的EurekaClientConfigBean，可以在配置文件中通过前缀eureka.client属性名进行属性覆盖
    - ApplicationInfoManager，作为应用信息管理器，管理服务实例的信息类InstanceInfo和服务实例的配置信息类EurekaInstanceConfig
    - InstanceInfo，一个服务的元数据，包含了服务的信息，在eureka server中代表了一个服务实例
    - EurekaInstanceConfig，封装eureka client的实例信息，可以通过eureka.instance前缀进行配置，EurekalnstanceConfigBean提供了默认的配置类
    - org.springframework.cloud.client.discovery.DiscoveryClient，定义用来服务发现的客户端接口，他的实现类EurekaDiscoveryClient，成员变量
        ```
        //netflix的config
        EurekaInstanceConfig
        //netflix的EurekaClient，他的实现类DiscoveryClient
        EurekaClient
        ```
4. EurekaClientAutoConfiguration初始化流程
    ```
    //注册cloud包下面的CloudEurekaClient
    //是netflix包下的DiscoveryClient的父类
    @Bean(destroyMethod = "shutdown")
    @ConditionalOnMissingBean(value = EurekaClient.class, search = SearchStrategy.CURRENT)
    public EurekaClient eurekaClient(ApplicationInfoManager manager, EurekaClientConfig config) {
        return new CloudEurekaClient(manager, config, this.optionalArgs,
                this.context);
    }

    //注册springcloud中的DiscoveryClient
    //需要一个参数netflix包下的EurekaClient
    //直接取CloudEurekaClient这个类型的bean
    @Bean
	public DiscoveryClient discoveryClient(EurekaInstanceConfig config, EurekaClient client) {
		return new EurekaDiscoveryClient(config, client);
	}
    ```
5. 看似使用的springcloud的discoveryClient，实际上通过一层属性的包装使用的是netflix的DiscoveryClient，配置这些同理，springcloud提供了配置入口和默认配置，实际还是使用的etflix组件
6. netflix的DiscoveryClient构造方法代码片段
    ```
    //传入BackupRegistry（NotImplementedRegistryImpl）备份注册中心
    this.backupRegistryProvider = backupRegistryProvider;

    //从eureka server拉起注册表信息eureka.client.fetch-register
    if (config.shouldFetchRegistry()) {
        this.registryStalenessMonitor = new ThresholdLevelsMetric(this, METRIC_REGISTRY_PREFIX + "lastUpdateSec_", new long[]{15L, 30L, 60L, 120L, 240L, 480L});
    } else {
        this.registryStalenessMonitor = ThresholdLevelsMetric.NO_OP_METRIC;
    }

    // 当前的客户端是否应该注册到erueka中 eureka.client.register-with-eureka  
    if (config.shouldRegisterWithEureka()) {
        this.heartbeatStalenessMonitor = new ThresholdLevelsMetric(this, METRIC_REGISTRATION_PREFIX + "lastHeartbeatSec_", new long[]{15L, 30L, 60L, 120L, 240L, 480L});
    } else {
        this.heartbeatStalenessMonitor = ThresholdLevelsMetric.NO_OP_METRIC;
    }

    //既不需要注册，也不需要拉去数据，直接返回,初始结束
    if (!config.shouldRegisterWithEureka() && !config.shouldFetchRegistry()) {
        ..........
         // no need to setup up an network tasks and we are done
        return;
    }

    //线程池大小为2，一个用户发送心跳，另外个缓存刷新
    scheduler = Executors.newScheduledThreadPool(2,
        new ThreadFactoryBuilder()
            .setNameFormat("DiscoveryClient-%d")
            .setDaemon(true)
            .build());

    //初始化client与server交互的jersey客户端
    eurekaTransport = new EurekaTransport();

    //拉取注册表的信息
    if (clientConfig.shouldFetchRegistry() && !fetchRegistry(false)) {
        fetchRegistryFromBackup();
    }

    //将服务实例进行注册
    if (clientConfig.shouldRegisterWithEureka() && clientConfig.shouldEnforceRegistrationAtInit()) {
        if (!register() ) {
            throw new IllegalStateException("Registration error at startup. Invalid server response.");
        }
    }

    //初始心跳定时任务，缓存刷新
    initScheduledTasks();
    ```
7. 拉取注册表的信息fetchRegistry(boolean)
    ```
    //全量拉取
    getAndStoreFullRegistry()
    //增量拉取
    //如果增量拉去失败了就直接全量拉去
    getAndUpdateDelta()
    ```
8. 服务注册register()
    ```
    //把自身的实例发送给服务端
    httpResponse = eurekaTransport.registrationClient.register(instanceInfo);
    ```
9. 定时任务initScheduledTasks()
    ```
    if (clientConfig.shouldFetchRegistry()) {
        //拉取服务默认30秒
        //eureka.client.register-fetch-interval-seconds
        int registryFetchIntervalSeconds = clientConfig.getRegistryFetchIntervalSeconds();
        int expBackOffBound = clientConfig.getCacheRefreshExecutorExponentialBackOffBound();
        scheduler.schedule(
            new TimedSupervisorTask(
                    "cacheRefresh",
                    scheduler,
                    cacheRefreshExecutor,
                    registryFetchIntervalSeconds,
                    TimeUnit.SECONDS,
                    expBackOffBound,
                    new CacheRefreshThread()
            ),
            registryFetchIntervalSeconds, TimeUnit.SECONDS);
    }

    if (clientConfig.shouldRegisterWithEureka()) {
        //心跳服务，默认30秒
        int renewalIntervalInSecs = instanceInfo.getLeaseInfo().getRenewalIntervalInSecs();
        int expBackOffBound = clientConfig.getHeartbeatExecutorExponentialBackOffBound();
        scheduler.schedule(
            new TimedSupervisorTask(
                    "heartbeat",
                    scheduler,
                    heartbeatExecutor,
                    renewalIntervalInSecs,
                    TimeUnit.SECONDS,
                    expBackOffBound,
                    new HeartbeatThread()
            ),
            renewalIntervalInSecs, TimeUnit.SECONDS);
        ............
    }
    ```
10. 服务下线shutdown()
    ```
    @PreDestroy
    public synchronized void shutdown() {
        if (isShutdown.compareAndSet(false, true)) {
            
            if (statusChangeListener != null && applicationInfoManager != null) {
                //注销状态监听器
                applicationInfoManager.unregisterStatusChangeListener(statusChangeListener.getId());
            }
            //取消定时任务
            cancelScheduledTasks();
            .....
            //关闭与server连接的客户端
            if (eurekaTransport != null) {
                eurekaTransport.shutdown();
            }

            //关闭相关监控
            heartbeatStalenessMonitor.shutdown();
            registryStalenessMonitor.shutdown();
        }
    }
    ```

### 源码解析-server

1. 据springboot的加载，找到eureka-相关的spring.factories相关配置类，EurekaServerAutoConfiguration
    ```
    //关键bean，所有功能都和这个类有关系
    @Bean
	public PeerAwareInstanceRegistry peerAwareInstanceRegistry(ServerCodecs serverCodecs) {
		this.eurekaClient.getApplications(); // force initialization
		return new InstanceRegistry(this.eurekaServerConfig, 
            this.eurekaClientConfig,
            serverCodecs,
            this.eurekaClient,
            this.instanceRegistryProperties.getExpectedNumberOfRenewsPerMin(),
            this.instanceRegistryProperties.getDefaultOpenForTrafficCount());
	}

    //cloud包下的类InstanceRegistry继承netflix包下的类PeerAwareInstanceRegistryImpl
    //类PeerAwareInstanceRegistryImpl继承类AbstractInstanceRegistry
    //类AbstractInstanceRegistry实现接口InstanceRegistry
    //接口InstanceRegistry继承接口LeaseManager、LookupService
    ```
2. LeaseManager接口定义如下
    ```
    //注册
    void register(T var1, int var2, boolean var3);
    //下线
    boolean cancel(String var1, String var2, boolean var3);
    //跟新
    boolean renew(String var1, String var2, boolean var3);
    //服务剔除
    void evict();
    ```
3. 服务注册AbstractInstanceRegistry--->register
    ```
    //加锁
    read.lock();
    //registry是注册中心实际上就是一个map
    //gMap<appName,Lease>是一个存储每个实例处理心跳的实例
    Map<String, Lease<InstanceInfo>> gMap = registry.get(registrant.getAppName());

    //key为appName（可以作为集群的那个id），如果存在，返回存在的值，否则添加，返回null
    gMap = (Map)this.registry.putIfAbsent(registrant.getAppName(), gNewMap);

    //根据instanceId（每个实例唯一那个）获取实例的处理心跳实例
    Lease<InstanceInfo> existingLease = (Lease)((Map)gMap).get(registrant.getId());

    //如果存储处理心跳的实例，就使用最新的处理心跳的实例，如果不存在就新增，然后放进gMap
    ((Map)gMap).put(registrant.getId(),lease);

    //释放锁
    this.read.unlock();
    ```
4. 接收心跳服务AbstractInstanceRegistry--->renew
    ```
    //client默认每隔30s发送心跳服务

    //获取服务集群所有的心跳服务处理实例
    Map<String, Lease<InstanceInfo>> gMap = registry.get(appName);

    //通过id获取具体实例的心跳处理实例
    Lease<InstanceInfo> leaseToRenew = gMap.get(id);

    //服务实例最终的状态
    InstanceStatus overriddenInstanceStatus = this.getOverriddenInstanceStatus(
        instanceInfo, leaseToRenew, isReplication);

    //如果状态为UNKNOWN，取消续约  
    if(overriddenInstanceStatus == InstanceStatus.UNKNOWN) {
        EurekaMonitors.RENEW_NOT_FOUND.increment(isReplication);
        return false;
    }

    //更新处理心跳实例的有效时间
    leaseToRenew.renew();
    ```
5. 服务剔除AbstractInstanceRegistry--->evict
    ```
    //client服务注册后，由于网络等问题没有完成心跳续约，也没有下线
    //就需要进行服务剔除，定时任务处理

    //如果自我保护状态，不允许剔除服务
    if(!this.isLeaseExpirationEnabled()) {
        return;
    }

    //遍历注册表registry，获取所有过期的租约
    //经过一些算法，配合配置文件中的一些配置，得出应该剔除哪些服务实例
    //循环逐个剔除
    this.internalCancel(appName, id, false);
    ```
6. 服务下线AbstractInstanceRegistry--->cancel--->internalCancel
    ```
    //读锁，防止被其他线程进行修改
    this.read.lock();

    //根据appName获取服务实例集群
    Map<String, Lease<InstanceInfo>> gMap = (Map)this.registry.get(appName);

    //移除服务实例租约
    if(gMap != null) {
        leaseToCancel = (Lease)gMap.remove(id);
    }

    //租约不存在，返回false
    if(leaseToCancel == null) {
        return false;
    }

    //设置租约的下线时间
    leaseToCancel.cancel();

    //设置缓存过期
    this.invalidateCache(appName, vip, svip);

    //释放锁
    this.read.unlock();
    ```
7. 集群同步
    - server在启动过程中从其他的peer节点中拉取注册表信息，并讲这些服务实例注册到本地注册表中
        ```
        //PeerAwareInstanceRegistryImpl--->syncUp

        //根据配置休停下
        Thread.sleep(serverConfig.getRegistrySyncRetryWaitMs());

        //获取所有的服务实例
        Applications apps = eurekaClient.getApplications();
        for (Application app : apps.getRegisteredApplications()) {
            for (InstanceInfo instance : app.getInstances()) {
                //判断是否可以注册
                if (isRegisterable(instance)) {
                    //注册到自身的注册表中
                    register(instance, instance.getLeaseInfo().getDurationInSecs(), true);
                    count++;
                }
            }
        }
        ```
    - server每次对本地注册表进行操作时，同时会将操作同步到他的peer节点中，达到数据一致
        ```
        //PeerAwareInstanceRegistryImpl--->replicateToPeers

        //下面三个都会调用者这个方法
        //PeerAwareInstanceRegistryImpl--->cancel
        //PeerAwareInstanceRegistryImpl--->register
        //PeerAwareInstanceRegistryImpl--->renew

        //向peer集群中的每一个peer进行同步
        for (final PeerEurekaNode node : peerEurekaNodes.getPeerEurekaNodes()) {
            if (peerEurekaNodes.isThisMyUrl(node.getServiceUrl())) {
                continue;
            }
            //根据action调用不同的同步请求
            replicateInstanceActionsToPeers(action, appName, id, info, newStatus, node);
        }
        ```