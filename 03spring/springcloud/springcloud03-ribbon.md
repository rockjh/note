### 配置使用

1. ribbon一般和eureka client联合使用，也就是说ribbon具备了eureka client相关的特性，ribbon是向注册中心中获取的服务
2. 引入pom配置
    ```
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    ```
3. application.yml
    ```
    server:
        port: 80

    eureka:
        client:
            register-with-eureka: false
            service-url:
                defaultZone: http://eureka1:7001/eureka,http://eureka2:7002/eureka,http://@eureka3:7003/eureka
    ```
4. 启动类
    ```
    @SpringBootApplication
    @EnableEurekaClient
    public class ConsumerApp {
        public static void main(String[] args) {
            SpringApplication.run(ConsumerApp.class,args);
        }
    }
    ```
5. restTemplate配置类
    ```
    //RestTemplate配置
    @Configuration
    public class RestConfig {
        @Bean
        //实现负载均衡的关键
        //当有多个服务提供者AppName=MICROCLOUD-PROVIDER-PRODUCT时
        //默认实现轮询的负载均衡
        @LoadBalanced
        public RestTemplate restTemplate() {
            return  new RestTemplate();
        }

        @Bean
        public HttpHeaders getHeaders() { 
            //要进行一个Http头信息配置
            HttpHeaders headers = new HttpHeaders(); // 定义一个HTTP的头信息
            String auth = "root:enjoy"; // 认证的原始信息
            byte[] encodedAuth = Base64.getEncoder()
                    .encode(auth.getBytes(Charset.forName("US-ASCII"))); // 进行一个加密的处理
            String authHeader = "Basic " + new String(encodedAuth);
            headers.set("Authorization", authHeader);
            return headers;
        }
    }
    ```
6. 远程调用服务
    ```
    //远程调用服务
    //MICROCLOUD-PROVIDER-PRODUCT是服务注册时的AppName
    public static final String PRODUCT_GET_URL = "http://MICROCLOUD-PROVIDER-PRODUCT/prodcut/get/";

    //注入restTemplate
    @Resource
    private RestTemplate restTemplate;
    
    //注入httpHeads
    @Resource
    private HttpHeaders httpHeaders;

    //开始远程调用
    restTemplate.exchange(PRODUCT_GET_URL + id,HttpMethod.GET,new HttpEntity<Object>(httpHeaders), Product.class).getBody();
    ```
7. 全局自定义路由
    ```
    @Bean
    public IRule ribbonRule() {
        //随机的访问策略
        return new com.netflix.loadbalancer.RandomRule()
    }
    ```
8. 常见的负载均衡策略
    - BestAvailableRule，选择最小请求数的服务器
    - RoundRobinRule，轮询
    - ClientConfigEnabledRoundRobinRule，使用RoundRobinRule选择服务器
    - RetryRule，根据选的轮询的方式重试
    - WeightedResponseTimeRule，根据响应时间去计算一个权重weight，weight越低，被选择的可能性就越低
    - ZoneAvoidanceRule，根据server的zone区域和可用性来轮询选择


### 源码解析

1. 根据springboot的加载，找到ribbon相关的spring.factories相关配置类RibbonAutoConfiguration
    ```
    @RibbonClients
    public class RibbonAutoConfiguration{
        .....
    }

    @Import(RibbonClientConfigurationRegistrar.class)
    public @interface RibbonClients {
        .....
    }
    ```
2. 注册自定义局部的负载均衡策略RibbonClientConfigurationRegistrar--->registerBeanDefinitions
    ```
    //会处理@RibbonClient和@RibbonClients注解，自定义负载均衡策略
    
    //通过这种形式注册进ioc中
    registerClientConfiguration(registry, name,
        attrs.get("defaultConfiguration"));

    //registerClientConfiguration注册进ioc的类型
    //是RibbonClientSpecification.class类型
    ```
3. RibbonAutoConfiguration加载自定义负载均衡策略
    ```
    //加载RibbonClientSpecification类的实例
    @Autowired(required = false)
	private List<RibbonClientSpecification> configurations = new ArrayList<>();

    //实例化一个SpringClientFactory存储这些策略
    @Bean
	public SpringClientFactory springClientFactory() {
		SpringClientFactory factory = new SpringClientFactory();
		factory.setConfigurations(this.configurations);
		return factory;
	}
    ```
4. ribbon核心类LoadBalancerClient实现了LoadBalancerClient
    ```
    @Bean
	@ConditionalOnMissingBean(LoadBalancerClient.class)
	public LoadBalancerClient loadBalancerClient() {
		return new RibbonLoadBalancerClient(springClientFactory());
	}
    ```
5. LoadBalancerClient及其父接口ServiceInstanceChooser的接口定义
    ```
    //LoadBalancerClient

    //从servericeId所代表的服务列表中选择一个服务器来发送网络请求
    <T> T execute(String serviceId, LoadBalancerRequest<T> request);
    <T> T execute(String serviceId, ServiceInstance serviceInstance, LoadBalancerRequest<T> request);

    //构建网络请求URI
    URI reconstructURI(ServiceInstance instance, URI original);

    //ServiceInstanceChooser

    //根据serviceId从服务器列表中选择一个ServiceInstance
    ServiceInstance choose(String serviceId);
    ```
6. RibbonLoadBalancerClient--->execute具体的实现
    ```
    //是从SpringClientFactory中获取的
    //每次发送请求都回获取一个ILoadBalancer
    //会涉及负载均衡（IRULS）、服务器列表集群（ServerList）、和检验服务是否存活（IPing）等细节实现
    ILoadBalancer loadBalancer = getLoadBalancer(serviceId);

    //直接调用ILoadBalancer的chooseServer
    //从已知的服务器列表中选择一个服务器
    Server server = getServer(loadBalancer);

    if (server == null) {
        throw new IllegalStateException("No instances available for " + serviceId);
    }
    RibbonServer ribbonServer = new RibbonServer(serviceId, server, isSecure(server,
            serviceId), serverIntrospector(serviceId).getMetadata(server));
    return execute(serviceId, ribbonServer, request);
    ```
7. 默认的负载均衡策略ZoneAwareLoadBalancer实现了ILoadBalancer接口，构造方法如下
    ```
    public ZoneAwareLoadBalancer(IClientConfig clientConfig,
        IRule rule,
        IPing ping,
        ServerList<T> serverList,
        ServerListFilter<T> filter,
        ServerListUpdater serverListUpdater) {
            
        super(clientConfig,rule, ping, serverList, filter, serverListUpdater);
    }
    ```
    - IClientConfig，client的配置类，具体指的DefaultClientConfigImpl
    - IRule，负载均衡的策略类,默认轮询RoundRobinRule
    - IPing，服务可用性检查，默认DummyPing
    - ServerList，服务列表获取，ConfigurationBasedServerList
    - ServerListFilter，服务列表过滤，ZonePreferenceServerListFilter
8. 重要方法ZoneAwareLoadBalancer--->chooseServer
    ```
    //如果就一个zone，直接返回
    if (!ENABLED.get() || getLoadBalancerStats().getAvailableZones().size() <= 1) {
        return super.chooseServer(key);
    }
    //下面处理有多个zone的逻辑，主要是跨机房调用
    //最终也是调用super.chooseServer(key);
    ```
9. 单个BaseLoadBalancer--->chooseServer
    ```
    rule.choose(key);
    ```