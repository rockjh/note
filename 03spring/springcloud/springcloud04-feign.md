### 配置使用

1. feign依赖ribbon，也就是feign作为eureka client的，可以直接配置ribbon的负载均衡策略
    ```
    Feign = RestTempate + HttpHeader + Ribbon + Eureka
    ```
2. 引入pom配置
    ```
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-openfeign</artifactId>
    </dependency>
    ```
3. application.yml
    ```
    server:
        port: 80

    #打印feign日志，默认的是NONE
    logging:
        level:
            cn.enjoy.service: DEBUG

    eureka:
        client:
            register-with-eureka: false
            service-url:
                defaultZone: http://admin:enjoy@eureka1:7001/eureka,http://admin:enjoy@eureka2:7002/eureka,http://admin:enjoy@eureka3:7003/eureka

    feign:
        hystrix:
            enabled: true
        compression:
            request:
                enabled: true # 默认不开启
                mime-types: # 可以被压缩的类型
                - text/xml
                - application/xml
                - application/json
                min-request-size: 2048 # 超过2048的字节进行压缩
    ```
4. 启动类
    ```
    @SpringBootApplication
    @EnableEurekaClient
    //扫描的接口配置类
    @EnableFeignClients("cn.enjoy.service")
    public class ConsumerFeignApp {
        public static void main(String[] args) {
            SpringApplication.run(ConsumerFeignApp.class,args);
        }
    }
    ```
5. feign配置类
    ```
    @Configuration
    public class FeignClientConfig {
        //添加安全认证
        @Bean
        public BasicAuthRequestInterceptor getBasicAuthRequestInterceptor() {
            return new BasicAuthRequestInterceptor("admin", "enjoy");
        }

        //开启日志输出
        @Bean
        public Logger.Level getFeignLoggerLevel() {
            return feign.Logger.Level.FULL ;
        }

    }


    ```
6. feign接口配置类
    ```
    //MICROCLOUD-PROVIDER-PRODUCT和服务提供者配置的一致
    @FeignClient(name = "MICROCLOUD-PROVIDER-PRODUCT",configuration = FeignClientConfig.class)
    public interface IProductClientService {

        //请求路径参数需要和服务提供者一致
        @RequestMapping("/prodcut/get/{id}")
        public Product getProduct(@PathVariable("id")long id);

        @RequestMapping("/prodcut/list")
        public  List<Product> listProduct() ;

        @RequestMapping("/prodcut/add")
        public boolean addPorduct(Product product) ;

    }
    ```
7. 直接远程调用
    ```
    //注入代理的类
    @Resource
    private IProductClientService iProductClientService;

    //远程调用
    iProductClientService.getProduct(id);
    ```

### 源码解析

1. 启动类中@EnableFeignClients
    ```
    //EnableFeignClients
    @Import(FeignClientsRegistrar.class)
    public @interface EnableFeignClients {
        .....
    }

    //FeignClientsRegistrar，spring ioc加载，执行覆盖的方法

    //通过注解的属性来构建Feign的Configuration，没有配置使用默认配置
    registerDefaultConfiguration(metadata, registry);
    //扫描package，注册被@FeignClient修饰的接口类Bean的信息
	registerFeignClients(metadata, registry);
    ```
2. FeignClientsRegistrar--->registerDefaultConfiguration注册配置
    ```
    //解析配置
    
    //将配置作为一个bean注册进ioc容器中
    registerClientConfiguration(registry, name,
		defaultAttrs.get("defaultConfiguration"));
    ```
3. FeignClientsRegistrar--->registerClientConfiguration
    ```
    //注册FeignClientSpecification.class类型到ioc容器中
    private void registerClientConfiguration(
        BeanDefinitionRegistry registry, 
        Object name,
		Object configuration) {

		BeanDefinitionBuilder builder = BeanDefinitionBuilder
				.genericBeanDefinition(FeignClientSpecification.class);
		builder.addConstructorArgValue(name);
		builder.addConstructorArgValue(configuration);
		registry.registerBeanDefinition(
				name + "." + FeignClientSpecification.class.getSimpleName(),
				builder.getBeanDefinition());
	}
    ```
4. 根据springboot的加载，找到feign相关的spring.factories相关配置类FeignAutoConfiguration
    ```
    //将刚刚注入到ioc的配置对象注入进这个属性
    @Autowired(required = false)
	private List<FeignClientSpecification> configurations = new ArrayList<>();

    //设置FeignContext的config属性
    @Bean
	public FeignContext feignContext() {
		FeignContext context = new FeignContext();
		context.setConfigurations(this.configurations);
		return context;
	}
    ```
5. FeignContext是NamedContextFactory的子类，NamedContextFactory会创建容器，作为ioc的子容器
    - 子容器，子容器可以get父容器的bean，父容器不能get子容器的bean
    - 父子容器之间的配置相互隔离
    - spring可以管理service、mapper，springmvc管理controller，controller就需要调用service、mapper，但是service找不到controller，保证了资源的局限性，因为springmvc是spring的子容器
    ```
    public abstract class NamedContextFactory<C extends NamedContextFactory.Specification>
		implements DisposableBean, ApplicationContextAware {

        //设置父容器
        public void setApplicationContext(ApplicationContext parent) throws BeansException {
            this.parent = parent;
        }

        //创建子容器
        protected AnnotationConfigApplicationContext createContext(String name) {
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

            //获取name所对应的configuration,如果有就注册到子context中

            //注册default的Configuration,也就是FeignClientsRegistrar类中
            //registerDefaultConfiguration方法中注册的Configuration

            //设置Environment的propertySources属性源

            context.refresh();
		    return context;
        }

        //实现了DisposableBean接口，所以当实例消亡的时候会调用
        public void destroy() {
            Collection<AnnotationConfigApplicationContext> values = this.contexts.values();
            for (AnnotationConfigApplicationContext context : values) {
                context.close();
            }
            this.contexts.clear();
        }
    }
    ```
6. FeignClientsRegistrar--->registerFeignClients扫描指定包下的类文件，注册
@FeignClient修饰的接口信息
    ```
    public void registerFeignClients(AnnotationMetadata metadata,BeanDefinitionRegistry registry) {

        //扫描获取被@FeignClient修饰的接口

        //解析@FeignClient元信息

        ////对单独的某个FeignClient的configuration进行配置，会被注册到子容器中
        registerClientConfiguration(registry, name,attributes.get("configuration"));

        //注册
		registerFeignClient(registry, annotationMetadata, attributes);
    }
    ```
7. FeignClientsRegistrar--->registerFeignClient，注册被@FeignClient修饰的接口，注册进ioc容器
    ```
    private void registerFeignClient(
        BeanDefinitionRegistry registry,
		AnnotationMetadata annotationMetadata,
        Map<String, Object> attributes) {

        //定义bean类型
        BeanDefinitionBuilder definition = BeanDefinitionBuilder
            .genericBeanDefinition(FeignClientFactoryBean.class);

        //属性赋值

        //注册
        AbstractBeanDefinition beanDefinition = definition.getBeanDefinition();
        BeanDefinitionHolder holder = new BeanDefinitionHolder(
            beanDefinition,
            className,
            new String[] { alias });
        BeanDefinitionReaderUtils.registerBeanDefinition(holder, registry);
    }
    ```
8. FeignClientFactoryBean是FactoryBean工厂类，通过getObject来获取实例
    ```
    public Object getObject(){
        //获取子容器
        FeignContext context = applicationContext.getBean(FeignContext.class);
		Feign.Builder builder = feign(context);

        //调用FeignContext的getInstance方法获取Client对象
		Client client = getOptional(context, Client.class);

        //里面的实现逻辑
        //解析mvc的注解，fallback相关的类，进行封装
        //使用动态代理，代理接口的方法，为每个method接口生成对应的handler
        //代理的内容就是使用RequestTemplat包装，发送url请求
        loadBalance(builder, context, new HardCodedTarget<>(this.type,this.name, url))
    }
    ```
9. 最终执行时，使用的客户端是LoadBalancerFeignClient，LoadBalancerFeignClient封装了负载均衡lbClientFactory