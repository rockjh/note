### 配置使用-hystrix

1. hystrix是针对调用服务异常的容错处理（快速失败），防止服务级联失败造成服务雪崩
    - 服务提供方存活，但调用接口报错（熔断）
    - 服务提供方系统崩溃，例如网络不通（降级）
2. 服务提供方存活，但调用接口报错，抛出异常，需要在服务提供方进行处理
3. 引入pom配置
    ```
    <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
    </dependency>
    ```
4. application.yml
    ```
    spring:
        application:
            name: microcloud-provider-product
    
    eureka:
        client:
            register-with-eureka: true
            service-url:
                defaultZone: http://admin:enjoy@eureka1:7001/eureka,http://admin:enjoy@eureka2:7002/eureka,http://admin:enjoy@eureka3:7003/eureka
    instance:
        instance-id: microcloud-provider-product
        prefer-ip-address: true
        lease-renewal-interval-in-seconds: 2 # 设置心跳的时间间隔（默认是30秒）
        lease-expiration-duration-in-seconds: 5  # 如果现在超过了5秒的间隔（默认是90秒）
    ```
5. 启动类
    ```
    @SpringBootApplication
    @MapperScan("cn.enjoy.mapper")
    @EnableEurekaClient
    @EnableCircuitBreaker
    public class ProductHystrixApp {
        public static void main(String[] args) {
            SpringApplication.run(ProductHystrixApp.class,args);
        }
    }
    ```
6. controller提供接口的类，当抛出异常时，会直接调用fallbackMethod后面的方法
    ```
    @RequestMapping(value="/get/{id}")
    @HystrixCommand(commandProperties = {
        @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"),
        @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "10000"),
        @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "60")
    },fallbackMethod = "getFallback")
    public Object get(@PathVariable("id") long id) {
        Product product = this.iProductService.get(id);
        if(product == null) {
            //执行getFallback方法
            throw new RuntimeException("该产品已下架！") ;
        }
        return  product;
    }

    //参数和原方法一致
    public Object  getFallback(@PathVariable("id") long id){
        Product product = new Product();
        product.setProductName("HystrixName");
        product.setProductDesc("HystrixDesc");
        product.setProductId(0L);
        return product;
    }
    ```
7. 服务提供方系统崩溃，例如网络不通，需要在服务消费方进行修改配置
8. 引入pom配置
    ```
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>

    <!--包含了hystrix的包-->
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-openfeign</artifactId>
    </dependency>
    ```
9. application.yml
    ```
    feign:
        hystrix:
            enabled: true # 开启hystrix
    eureka:
        client:
            register-with-eureka: false
                service-url:
                    defaultZone: http://admin:enjoy@eureka1:7001/eureka,http://admin:enjoy@eureka2:7002/eureka,http://admin:enjoy@eureka3:7003/eureka
    ```
10. 启动类
    ```
    @SpringBootApplication
    @EnableEurekaClient
    @RibbonClient(name = "MICROCLOUD-PROVIDER-PRODUCT", configuration = RibbonConfig.class)
    @EnableFeignClients("cn.enjoy.service")
    public class ConsumerFeignApp {
        public static void main(String[] args) {
            SpringApplication.run(ConsumerFeignApp.class,args);
        }
    }
    ```
11. 其他类
    ```
    //定义的消费端接口
    @FeignClient(name = "MICROCLOUD-PROVIDER-PRODUCT",configuration = FeignClientConfig.class,fallbackFactory = IProductClientServiceFallbackFactory.class)
    public interface IProductClientService {
        @RequestMapping("/prodcut/get/{id}")
        public Product getProduct(@PathVariable("id")long id);

        @RequestMapping("/prodcut/list")
        public  List<Product> listProduct() ;

        @RequestMapping("/prodcut/add")
        public boolean addPorduct(Product product) ;

    }

    //IProductClientServiceFallbackFactory回滚配置类
    //当访问生产者出现网络不通此类情况，直接执行这个配置类
    @Component
    public class IProductClientServiceFallbackFactory implements FallbackFactory<IProductClientService> {
        @Override
        public IProductClientService create(Throwable throwable) {
            return  new IProductClientService() {
                @Override
                public Product getProduct(long id) {
                    Product product = new Product();
                    product.setProductId(999999L);
                    product.setProductName("feign-hystrixName");
                    product.setProductDesc("feign-hystrixDesc");
                    return  product;
                }

                @Override
                public List<Product> listProduct() {
                    return null;
                }

                @Override
                public boolean addPorduct(Product product) {
                    return false;
                }
            };
        }
    }
    ```

### 配置使用-hystrixDashboard

1. 在hystrix里面提供一个Dashboard（仪表盘）的功能，他是一种监控的功能，可以利用它来进行整体服务的监控
2. 引入pom配置
    ```
     <!--健康检查模块-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
    ```
3. application.yml
    ```
    server:
        port: 9001
    ```
4. 启动类
    ```
    @SpringBootApplication
    @EnableHystrixDashboard
    public class HystrixDashboardApp {
        public static void main(String[] args) {
            SpringApplication.run(HystrixDashboardApp.class,args);
        }
    }
    ```
5. 在上一个生产端的application.yml配置文件中加入，并且加入健康检查模块，他的访问端口是8080
    ```
    management:
        endpoints:
            web:
                exposure:
                    include: '*'

    ```
6. 使用
    ```
    //访问地址
    http://localhost:9001/hystrix

    //在输入框输入下面地址，查看上一个生产者访问监控
    http://admin:enjoy@localhost:8080/actuator/hystrix.stream
    ```

### 配置使用-turbine

1. HystrixDashboard只能监控一个微服务，我们需要对更多的微服务在一个页面上同时监控，需要使用到Turbine
2. 引入pom配置
    ```
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-turbine</artifactId>
    </dependency>
    ```
3. application.yml，通过配置可以看到，实际上是通过eureka进行监控
    ```
    server:
        port: 9101 
    eureka:
        client:
            register-with-eureka: false
            service-url:
                defaultZone: http://admin:enjoy@eureka1:7001/eureka,http://admin:enjoy@eureka2:7002/eureka,http://admin:enjoy@eureka3:7003/eureka

    turbine:
        # 服务名
        app-config: MICROCLOUD-PROVIDER-PRODUCT,MICROCLOUD-PROVIDER-USERS
        cluster-name-expression: new String("default")
    ```
4. 启动类
    ```
    @SpringBootApplication
    @EnableTurbine
    public class TurbineApp {
        public static void main(String[] args) {
            SpringApplication.run(TurbineApp.class,args);
        }
    }
    ```
5. 使用
    ```
    //启动Dashboard
    http://localhost:9001/hystrix

    //在输入框中输入
    http://localhost:9101/turbin.stream
    ```

### 执行原理

1. 当服务的某个api接口调用失败次数在一定时间内，小于设定的阈值时，熔断器处于关闭状态
2. 当服务的某个api接口调用失败次数大于设定的阈值，会被判定接口出现了故障，打开熔断器
3. 执行打开熔断器的api接口，会执行快速失败逻辑（fallback）
4. 处于打开状态的熔断器，一段时间后会处于半打开状态，并将一定数量的请求执行正常逻辑，若执行正常逻辑的请求失败了，则熔断器保持打开，反之则关闭熔断器，这就是熔断器的自我修复能力

### 源码讲解

1. 根据springboot的加载，找到eureka-相关的spring.factories相关配置类，HystrixCircuitBreakerConfiguration
    ```
    @Configuration
    public class HystrixCircuitBreakerConfiguration {

        @Bean
        public HystrixCommandAspect hystrixCommandAspect() {
            return new HystrixCommandAspect();
        }
    }
    ```
2. HystrixCommandAspect是一个切面类，典型的aop使用
    ```
    @Aspect
    public class HystrixCommandAspect {

        //以注解作为切面，使用了该注解的，就可以作为切面
        @Pointcut("@annotation(com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand)")
        public void hystrixCommandAnnotationPointcut(){}

        //切面之后所做的事情
        @Around("hystrixCommandAnnotationPointcut() || hystrixCollapserAnnotationPointcut()")
        public Object methodsAnnotatedWithHystrixCommand(final ProceedingJoinPoint joinPoint){
            //根据切点获取Method（被@HystrixCommand标记方法）
            Method method = getMethodFromTarget(joinPoint);
            
            .....
            //准备各种材料后，创建HystrixInvokable
            //HystrixInvokable只是一个空接口，没有任何方法，只是用来标记具备可执行的能力
            HystrixInvokable invokable = HystrixCommandFactory.getInstance().create(metaHolder);
            ExecutionType executionType = metaHolder.isCollapserAnnotationPresent() ?
                    metaHolder.getCollapserExecutionType() : metaHolder.getExecutionType();

            .....

            //利用工具CommandExecutor来执行
            result = CommandExecutor.execute(invokable, executionType, metaHolder);
            return result;
        }
    }
    ```
3. 大致就是利用aop，反射类实现上面的具体实现逻辑