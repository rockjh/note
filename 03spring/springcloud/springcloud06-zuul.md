### 简介

zuul作为路由网关组件，在微服务架构中有着非常重要的作用，主要体现在以下6个方面
1. zuul、ribbon、euraka相结合，可以实现智能路由和负载均衡的功能，zuul能够将请求流量按某种策略分发到集群状态的多个服务实例
2. 网关将所有api聚合，统一对外暴露，外界系统不需要知道微服务中各个服务调用的复杂性。也保护了内部微服务单元的api接口，防止其被外界直接调用，导致服务的敏感信息对外暴露
3. 网关服务可以用作用户身份认证和权限认证，防止非法请求操作api接口，对服务器起保护作用
4. 网关可以实现监控，实时日志输出，对请求进行记录
5. 网关可以用来实现流量监控，在高流量的情况下，对服务进行降级。就是当服务器压力剧增的情况下根据当前的业务情况对一些服务有策略的进行降级，以此释放服务器资源，保证核心任务的正常运行，一般服务降级的方式有：服务接口拒绝服务、页面拒绝服务、延迟持久化
6. api接口从内部分离出来，方便做测试

服务消费流程

1. zuul类似于spring mvc中的DispatcServlet来对请求进行控制，他的核心是一系列过滤器，可以在http请求的发起和相应返回期间执行一系列过滤器，主要包括以下4中过滤器
    - PRE过滤器，他是在请求路由到具体服务之前执行，适合做安全认证，身份认证，参数校验
    - ROUTING过滤器，它用于将请求路由到具体的服务实例，默认情况下使用http client进行网络请求
    - POST过滤器，他是在请求已被路由到服务后执行的，一般情况下用于收集统计信息、指标、以及响应传输到客户端
    - ERROR过滤器，在其他过滤器发生错误时执行
2. zuul采取了动态读取、编译和运行这些过滤器，过滤器之间不能直接通信，通过，RequestContext对象来共享数据，每一个请求都会创建一个RequestContext对象。zuul过滤器具有以下特性：
    - Type类型，如果PRE、ROUTING、POST、ERROR
    - Execution Order执行顺序，order值越小，越先执行
    - Criteria标准，Filter执行所需的条件
    - Action行动，如果符合执行条件，则执行逻辑代码

### 配置使用

1. 引入pom配置
    ```
    <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-zuul</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    ```
2. application.yml
    ```
    server:
        port: 9501

    eureka:
        client: # 客户端进行Eureka注册的配置
            register-with-eureka: false
            service-url:
                defaultZone: http://admin:enjoy@eureka1:7001/eureka,http://admin:enjoy@eureka2:7002/eureka,http://admin:enjoy@eureka3:7003/eureka

    spring:
        application:
            name: microcloud-zuul-gateway

    zuul:
        routes:
            #使用别名访问这个服务
            #http://localhost:9501/users-proxy/users/get/1
            user:
                path: /users-proxy/**
                serviceId: microcloud-provider-users

            #可以配置多个
            product:
                path: /product-proxy/**
                serviceId: microcloud-provider-product

        ignored-services:
            #不配置这个可以使用访问
            #http://localhost:9501/microcloud-provider-users/users/get/1
            microcloud-provider-users #屏蔽掉使用服务名访问，“*”号会屏蔽所有的服务名访问
        #加上统一前缀，所有的访问地址都会加上，例如
        #http://localhost:9501/v1/users-proxy/users/get/1
        prefix: /v1
            


    ```
3. 启动类
    ```
    @SpringBootApplication
    @EnableZuulProxy
    public class ZuulApp {
        public static void main(String[] args) {
            SpringApplication.run(ZuulApp.class,args);
        }
    }
    ```
4. 添加一个过滤器，访问请求前，添加认证
    ```
    @Component
    public class AuthorizedRequestFilter extends ZuulFilter {
        @Override
        public String filterType() {
            return FilterConstants.PRE_TYPE;
        }

        @Override
        public int filterOrder() {
            return 0;
        }

        @Override
        public boolean shouldFilter() {
            return true;
        }

        @Override
        public Object run() throws ZuulException {
            RequestContext currentContext = RequestContext.getCurrentContext() ; // 获取当前请求的上下文
            String auth = "admin:enjoy"; // 认证的原始信息
            byte[] encodedAuth = Base64.getEncoder()
                    .encode(auth.getBytes(Charset.forName("US-ASCII"))); // 进行一个加密的处理
            String authHeader = "Basic " + new String(encodedAuth);
            currentContext.addZuulRequestHeader("Authorization", authHeader);
            return null;
        }
    }
    ```
5. 开启一个熔断处理，当生产者宕机时，进行快速失败
    ```
    @Component
    public class ProviderFallback implements FallbackProvider  {
        @Override
        public String getRoute() {
            //可以是一个服务名
            return "*";
        }

        @Override
        public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
            return new ClientHttpResponse() {

                @Override
                public HttpHeaders getHeaders() {
                    HttpHeaders headers = new HttpHeaders() ;
                    headers.set("Content-Type", "text/html; charset=UTF-8");
                    return headers;
                }

                @Override
                public InputStream getBody() throws IOException {
                    // 响应体
                    return new ByteArrayInputStream("产品微服务不可用，请稍后再试。".getBytes());
                }

                @Override
                public HttpStatus getStatusCode() throws IOException {
                    return HttpStatus.BAD_REQUEST;
                }

                @Override
                public int getRawStatusCode() throws IOException {
                    return HttpStatus.BAD_REQUEST.value();
                }

                @Override
                public String getStatusText() throws IOException {
                    return HttpStatus.BAD_REQUEST.getReasonPhrase();
                }

                @Override
                public void close() {

                }
            };
        }
    }
    ```
6. feign访问zuul
    ```
    //接口访问层
    @FeignClient(name = "MICROCLOUD-ZUUL-GATEWAY",configuration = FeignClientConfig.class,fallbackFactory = IZUUlClientServiceallbackFactory.class)
    public interface IZUUlClientService {

        @RequestMapping("/enjoy-api/product-proxy/prodcut/get/{id}")
        public Product getProduct(@PathVariable("id")long id);

        @RequestMapping("/enjoy-api/product-proxy/prodcut/list")
        public List<Product> listProduct() ;

        @RequestMapping("/enjoy-api/product-proxy/prodcut/add")
        public boolean addPorduct(Product product) ;

        @RequestMapping("/enjoy-api/users-proxy/users/get/{name}")
        public Users getUsers(@PathVariable("name")String name);
    }

    //异常处理类
    @Component
    public class IZUUlClientServiceallbackFactory implements FallbackFactory<IZUUlClientService> {
        @Override
        public IZUUlClientService create(Throwable throwable) {
            return  new IZUUlClientService() {
                @Override
                public Product getProduct(long id) {
                    Product product = new Product();
                    product.setProductId(999999L);
                    product.setProductName("feign-zuulName");
                    product.setProductDesc("feign-zuulDesc");
                    return  product;
                }

                @Override
                public List<Product> listProduct() {
                    return null;
                }

                @Override
                public boolean addPorduct(Product product) {
                    return false;
                }

                @Override
                public Users getUsers(String name) {
                    Users user = new Users();
                    user.setSex("F");
                    user.setAge(17);
                    user.setName("zuul-fllback："+name);
                    return user;
                }
            };
        }
    }
    ```
7. zuul集群
    - 复制多个zuul，修改不同的端口，feign配置负载均衡策略访问zuul集群
    - 多个zuul连接注册中心集群B注册，feign连接注册中心集群B注册
    - 当提供的服务是集群时，zuul会自动进行负载均衡，因为zuul默认集成了ribbon，可以直接配置负载均衡策略
    - feign访问zuul，zuul通过服务名访问注册中心注册的服务提供者

### 使用方式

1. 对不同的渠道使用不同的zuul来进行路由，移动端，web端，其他客户端各使用一个路由，
2. 通过nginx和zuul相互结合来做负载均衡，暴露在最外面的是nginx主从双热备进行keepalive，nginx经过某种路由策略，将请求的路由转发到zuul集群上，再由zuul最终将请求分发到具体的服务

### 源码解析

1. 启动类注解@EnableZuulProxy，开启zuul功能
    ```
    //启用熔断功能
    @EnableCircuitBreaker
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Import(ZuulProxyMarkerConfiguration.class)
    public @interface EnableZuulProxy {
    }

    //ZuulProxyMarkerConfiguration配置类生效
    @Configuration
    public class ZuulProxyMarkerConfiguration {

        //注册一个标识类，后面springboot会将这个作为加载的条件
        @Bean
        public Marker zuulProxyMarkerBean() {
            return new Marker();
        }

        class Marker {
        }
    }
    ```
2. 根据springboot的加载，找到eureka-相关的spring.factories相关配置类
    - ZuulServerAutoConfiguration
    - ZuulProxyAutoConfiguration是ZuulServerAutoConfiguration的子类
    ```
    /注册一个zuulServlet，核心类ZuulServlet
    @Bean
	@ConditionalOnMissingBean(name = "zuulServlet")
	@ConditionalOnProperty(name = "zuul.use-filter", havingValue = "false", matchIfMissing = true)
	public ServletRegistrationBean zuulServlet() {
		ServletRegistrationBean<ZuulServlet> servlet = new ServletRegistrationBean<>(
				new ZuulServlet(), this.zuulProperties.getServletPattern());
		servlet.addInitParameter("buffer-requests", "false");
		return servlet;
	}
    ```
3. ZuulServlet--->service，编排这些ZuulFilter的执行顺序
    ```
    public void service(ServletRequest servletRequest, ServletResponse servletResponse){

        //执行逻辑，路由类型zuulType
        try {
            //具体进入之后会进行排序
            preRoute();
        } catch (ZuulException e) {
            error(e);
            postRoute();
            return;
        }
        try {
            route();
        } catch (ZuulException e) {
            error(e);
            postRoute();
            return;
        }
        try {
            postRoute();
        } catch (ZuulException e) {
            error(e);
            return;
        }
    }
    ```
4. 进入到具体的过滤器，回到这里选择执行的过滤器FilterProcessor--->runFilters(string type)
    ```
    public Object runFilters(String sType) throws Throwable {
        
        //找出这个类型的过滤器
        List<ZuulFilter> list = FilterLoader.getInstance().getFiltersByType(sType);
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                ZuulFilter zuulFilter = list.get(i);
                //具体执行过滤器，最终会执行run方法
                Object result = processZuulFilter(zuulFilter);
                if (result != null && result instanceof Boolean) {
                    bResult |= ((Boolean) result);
                }
            }
        }
        return bResult;
    }
    ```
