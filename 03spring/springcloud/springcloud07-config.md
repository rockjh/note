### 简介

1. 分布式配置中心，可以从本地仓库读取配置文件，也可以从远程git仓库读取配置文件。本地仓库是指将所有的配置文件统一写在config server工程目录下，config server暴露http api接口，config client通过http api接口来查询读取配置文件
2. springboot中
    - application.yml，对应的是用户级的资源配置项
    - bootstrap.yml，对应的是系统级的资源配置，其优先级更高


### 配置使用-config-server，声明config配置

1. 引入pom配置
    ```
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-config-server</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    ```
2. application.yml
    ```
    server:
        port: 7101

    spring:
        application:
            name: microcloud-config
        cloud:
            config:
                server:
                    git:
                        uri: https://github.com/enjoyeud/microconfig.git


    eureka:
        client:
            service-url:
                defaultZone: http://admin:enjoy@localhost:7301/eureka
        instance:
            prefer-ip-address: true # 在地址栏上使用IP地址进行显示
            instance-id: microcloud-config1
    ```
3. 启动类
    ```
    @SpringBootApplication
    @EnableConfigServer
    public class ConfigApp {
        public static void main(String[] args) {
            SpringApplication.run(ConfigApp.class,args);
        }
    }
    ```

### 配置使用-config-client，使用config

1. 引入pom配置
    ```
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-config</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    </dependency>
    ```
2. bootstrap.yml，其他配置都在git上面
    ```
    spring:
        cloud:
            config:
                name: microcloud-config-client # 定义要读取的资源文件的名称
                profile: dev # 定义profile的名称
                label: master # 定义配置文件所在的分支
                username: admin # 连接的用户名
                password: enjoy # 连接的密码
                discovery:
                    enabled: true   # 通过配置中心加载配置文件
                    service-id: MICROCLOUD-CONFIG   # 在eureka之中注册的服务ID
    
    application:
        name: microcloud-config-client

    eureka:
        client:
            service-url:
                defaultZone: http://admin:enjoy@localhost:7301/eureka
    ```
3. 启动类
    ```
    @SpringBootApplication
    @EnableEurekaClient
    public class ConfigClientApp {
        public static void main(String[] args) {
            SpringApplication.run(ConfigClientApp.class,args);
        }
    }
    ```

### 配置使用-config-eureka，config服务集群需要

1. 引入pom配置
    ```
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
    </dependency>
    ```
2. application.yml
    ```
    server:
        port: 7301
    eureka:
        server:
            eviction-interval-timer-in-ms: 1000   #设置清理的间隔时间，而后这个时间使用的是毫秒单位（默认是60秒）
            enable-self-preservation: false #设置为false表示关闭保护模式
        client:
            fetch-registry: false
            register-with-eureka: false
            service-url:
                defaultZone: http://admin:enjoy@localhost:7301/eureka
        instance: # eureak实例定义
            hostname: localhost # 定义 Eureka 实例所在的主机名称
    spring:
        application:
            name: microcloud-ha-config-eureka
    ```
3. 启动类
    ```
    @SpringBootApplication
    @EnableEurekaServer
    public class HaConfigEurekaApp {
        public static void main(String[] args) {
            SpringApplication.run(HaConfigEurekaApp.class,args);
        }
    }
    ```

### 使用bus刷新配置

1. spring cloud bus是用轻量的消息代理将分布式的节点连接起来，可以用于广播配置文件的更改或者服务的监控管理。可选的消息代理组件包括，rabbitMQ，kafka，AMQP等，一般使用rabbitMQ

2. 如果有几十个微服务，每个微服务又有多个实例，当更改配置时，配置中心会向某个服务实例发送配置更新请求，该服务实例再向消息总线发送配置更新，消息总线会向每个实例推送配置更新的消息，每个实例收到消息后，再去配置中心拉去配置
    ```
    @SpringBootApplication
    @RestController
    @EnableEurekaClient
    //加上该注解，可以在不重启的情况下，重新加载配置
    @RefreshScope
    public class ConfigClientApplication {
    
        public static void main(String[] args) {
            SpringApplication.run(ConfigClientApplication.class, args);
        }
        //从配置中心拉去的值
        @Value("${foo}")
        String foo;
        @GetMapping(value = "/foo")
        public String hi(){
            return foo;
        }
    }
    ```