### 简介

一个微服务系统往往有很多个服务单元，由于服务单元数量众多，业务复杂性较高，如果出现了错误和异常，很难去定位。所以在微服务架构中，必须实现分布式链路追踪，去跟进一个请求到底有哪些服务参与，参与的顺序又是怎样的，从而达到每个请求步骤都清晰可见，除了问题可以快速定位

sleuth采用了google开源项目dapper的专业术语
- span基本工作单元，发送一个远程调度任务就会产生一个span，span包含摘要、时间戳时间、span的id、进程id。span由一个64位id唯一标识
- trace由一系列span组成，一个请求的所有span组成了一个trace，trace由一个64位id唯一标识
- Annotation用于记录一个事件，一些核心注解用于定义一个请求的开始和结束，这些请求的注解如下：
    - cs-Client Sent客户端发送一个请求，这个注解描述了span的开始
    - sr-Server Received服务端获得请求并准备开始处理他，如果将sr-cs可以得到网络传输的时间戳
    - ss-Server Send服务发送响应，该注解表明请求处理完成，用ss-sr得到服务响应时间戳
    - cr-Client Received客户端接收响应，此时span结束，cr-cs得到整个请求消耗的时间戳

### 链路存放

可以在链路数据中添加自定义数据，链路信息可以存放内存、消息组件、mysql、elasticSearch

elasticSearch可以和kibana结合，将数据链路展示在kibana