### Nosql

1. 定义，Not Only Sql（不仅仅是sql），本质也是一种数据库的技术，他不会遵循sql标准、ACID属性，表结构等
2. 为什么需要nosql
    - 满足对数据库的高并发读写
    - 对海量数据的高效存储和访问
    - 对数据库高扩展性和高可用性
    - 灵活的数据结构，满足数据结构不固定的场景
3. 特点
    - key-valu存储，常用于缓存、高并发数据访问，例如redis
    - 列式数据库，常用于分布式文件系统，例如Hbase
    - 文档型数据库，常用于web应用、并发能力强、表结构可变，例如mongodb
    - 图结构数据库，常用于社交网络、推荐系统、关注构建图谱，例如infoGrid Neo4J

4. 缺点
    - 一般不支持事务
    - 实现复杂SQL查询比较复杂
    - 运维人员数据维护门槛较高
    - 目前不是主流的数据库技术

### 什么是MongoDB

1. 定义，是一个数据库 ,高性能、无模式、文档性，目前nosql中最热门的数据库，开源产品，基于c++开发。是nosql数据库中功能最丰富，最像关系数据库的
    - 文档，是mongodb中最小数据集单元，是由多个键--值对有序组合在一起的数据单元，类似行
    - 集合，就是存储一组文档，集合内的文档格式可以不同，类似表
    - 库，多个集合组成数据库，每个数据库都是完全独立的，有自己的用户，权限信息，独立的存储文件集
    - 实例，系统上运行库的进程及节点集合（分布式），一个实例可以有多个库
2. 特性
    - 面向集合文档的存储：适合存储Bson（json的扩展）形式的数据；
    - 格式自由，数据格式不固定，生产环境下修改结构都可以不影响程序运行；
    - 强大的查询语句，面向对象的查询语言，基本覆盖sql语言所有能力；
    - 完整的索引支持，支持查询计划；
    - 支持复制和自动故障转移；
    - 支持二进制数据及大型对象（文件）的高效存储；
    - 使用分片集群提升系统扩展性；
    - 使用内存映射存储引擎，把磁盘的IO操作转换成为内存的操作
3. 数据类型
    - null，{"key":null}，null表示空值或者不存在该字段
    - 未定义，{"key":undefined}，值没有定义，null和undefined是不同的
    - 布尔，{"key","true"}{"key","false"}，布尔类型表示真或者假
    - 32位整数，{"key":8}，存储32位整数，但再shell界面显示会被自动转成64位浮点数
    - 64位整数，{"key":{"floatApprox":8}}，存储64位整数，floatApprox意思是使用64位浮点数近似表示一个64位整数
    - 64位浮点数，{"key":8.21}，存储64位整数，shell客户端显示的数字都是这种类型
    - 字符串，{"key":"value"}{"key":"8"}，UTF-8格式
    - 对象ID，{"key":ObjectId()}，12字节的唯一ID
    - 日期，{"key":new Date()}
    - 代码，{"key":function(){}}
    - 二进制数据，主要存储文件
    - 数组，{"key":[16,15,17]}，集合或者列表
    - 内嵌文档，{"user":{"name":"lison"}}，子对象
    - Decimal128，{"price":NumberDecimal("2.099")}，3.4版本新增的数据类型，无精度问题



4. 应用场景，应用不需要事务及复杂的join支持（mongodb是支持事务和join的，但是性能不友好），比如游戏、物流、电商、内容管理、社交、物联网、视频直播
    - 游戏，存储游戏用户信息，用户的装备、积分等直接以内嵌文档的形式存储，方便查询、更新
    - 物流，存储订单信息，订单状态在运送过程中会不断更新，以内嵌数组的形式来存储，一次查询就能将订单所有的变更读取出来。
    - 社交，存储用户信息，以及用户发表的朋友圈信息，通过地理位置索引实现附近的人、地点等功能
    - 物联网，存储接入的智能设备信息，以及设备汇报的日志信息，并对这些信息进行多维度的分析
    - 视频直播，存储用户信息、礼物信息等
5. 不建议的应用场景
    - 高度事务性系统，例如银行、财务等系统
    - 传统的商业智能应用，特定问题的数据分析，多数据实体关联，涉及到复杂的、高度优化的查询方式
    - 使用sql方便的时候；数据结构相对固定，使用sql进行查询统计更加便利的时候

### 安装

1. 下载mongodb、解压
    ```
    https://www.mongodb.com/try
    ```
2. 解压tar -xvzf，并在安装目录创建data目录，以及logs目录和logs/mongodb.log文件

3. 在bin目录创建配置文件mongodb.conf
    ```
    #数据存储路径
    storage:
        dbPath: "/Volumes/file/mongodb-4.0.20/data"
    systemLog:
        #设置了为日志文件就必须要设置文件路径
        destination: file
        path: "/Volumes/file/mongodb-4.0.20/logs/mongodb.log"
    net:
        port: 27017
    ```
4. 编写启动的shell脚本
    ```
    #nohup表示终端关闭，程序也会运行
    #&表示启动后，可以ctrl+c该终端继续做自己的事情
    #指定配置文件启动
    nohup ./mongod -f mongodb.conf &
    ```
5. 使用start-mongodb.sh启动mongodb实例
6. 使用命令行客户端连接
    ```
    mongo localhost:27017
    ```
7. 插入数据
    ```
    db.users.drop();
    var user1 = {
            "username" : "lison",
            "country" : "china",
            "address" : {
                    "aCode" : "411000",
                    "add" : "长沙"
            },
            "favorites" : {
                    "movies" : ["杀破狼2","战狼","雷神1"],
                    "cites" : ["长沙","深圳","上海"]
            },
            "age" : 18,
        "salary":NumberDecimal("18889.09"),
        "lenght" :1.79
            
    };
    db.users.insert(user1);
    ```

### Nosql示例

1. 查询喜欢的城市包含东莞和东京的user
    ```
    db.users.find({ "favorites.cites" : { "$all" : [ "东莞" , "东京"]}})
    ```
2. 查询国籍为英国或者美国，名字中包含s的user
    ```
    db.users.find({ 
        "$and" : [{ "username" : { "$regex" : ".*s.*"}} , 
        { "$or" : [ { "country" : "English"} , { "country" : "USA"}]}]
    })
    ```
3. 把lison的年龄修改为6岁
    ```
    db.users.updateMany({ "username" : "lison"},{ "$set" : { "age" : 6}},true)
    ```
4. 喜欢的城市包含东莞的人，给他喜欢的电影加入"小电影2""小电影3"
    ```
    db.users.updateMany({"favorites.cites" : "东莞"}, 
        { "$addToSet" : { "favorites.movies" : { "$each" : [ "小电影2 " , "小电影3"]}}},
    true)
    ```
5. 删除名字为lison的user
    ```
    db.users.deleteMany({ "username" : "lison"} )
    ```
6. 删除年龄大于8小于25的user
    ```
    db.users.deleteMany({"$and" : [ {"age" : {"$gt": 8}} , {"age" : {"$lt" : 25}}]})
    ```

### 集成springboot

1. pom文件
    ```
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-mongodb</artifactId>
    </dependency>
    ```
2. application.yml配置
    ```
    spring:
        data:
            mongodb:
                database: test
                port: 27017
                host: 127.0.0.1
    #spring.data.mongodb.uri: mongodb://127.0.0.1:27017/test
    ```
3. 实体类
    ```
    //user
    //必须和库中的字段对应
    @Data
    @Document(collection="users")
    public class User {
        private ObjectId id;
        //必须和库中的字段对应
        @Field
        private String username;
        private String country;
        private Address address;
        private Favorites favorites;
        private int age;
        private BigDecimal salary;
        private float lenght;
    }

    //Favorites
    @Data
    public class Favorites {
        private List<String> movies;
        private List<String> cites;
    }

    //adress
    @Data
    public class Address {
        private String aCode;
        private String add;
    }
    ```
4. 操作类
    ```
    @Autowired
    private MongoOperations template;

    //插入数据
    public void insert(){
        User user = new User();
        user.setUsername("cang");
        user.setCountry("USA");
        user.setAge(20);
        user.setLenght(1.77f);
        user.setSalary(new BigDecimal("6265.22"));

        //添加“address”子文档
        Address address1 = new Address();
        address1.setaCode("411222");
        address1.setAdd("sdfsdf");
        user.setAddress(address1);

        //添加“favorites”子文档，其中两个属性是数组
        Favorites favorites1 = new Favorites();
        favorites1.setCites(Arrays.asList("东莞","东京"));
        favorites1.setMovies(Arrays.asList("西游记","一路向西"));
        user.setFavorites(favorites1);
    ```


        User user1 = new User();
        user1.setUsername("chen");
        user1.setCountry("China");
        user1.setAge(30);
        user1.setLenght(1.77f);
        user1.setSalary(new BigDecimal("6885.22"));
        Address address2 = new Address();
        address2.setaCode("411000");
        address2.setAdd("我的地址2");
        user1.setAddress(address2);
        Favorites favorites2 = new Favorites();
        favorites2.setCites(Arrays.asList("珠海","东京"));
        favorites2.setMovies(Arrays.asList("东游记","一路向东"));
        user1.setFavorites(favorites2);
    
        template.insertAll(Arrays.asList(user,user1));
    }
    
    //删除
    public void delete(){
    	//delete from users where username = ‘lison’
    	Query query = query(where("username").is("lison"));
    	WriteResult remove = template.remove(query, User.class);
    	System.out.println("--------------------->"+remove.getN());
    }
    
    //更新
    public void update(){
    	//update  users  set age=6 where username = 'lison' 
    	Query query = query(where("username").is("lison"));
    	Update update = update("age", 6);
    	WriteResult updateFirst = template.updateMulti(query, update, User.class);
    	System.out.println(updateFirst.getN());
    }
    
    //查询
    public void find(){
    	//select * from users  where favorites.cites has "东莞"、"东京"
    	Criteria all = where("favorites.cites").all(Arrays.asList("东莞","东京"));
    	List<User> userList = template.find(query(all), User.class);
    }
    ```