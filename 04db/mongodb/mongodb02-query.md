### 语法

1. 基本语法
    - query，可选，使用查询操作符指定查询条件
    - projection，可选，使用投影操作符指定返回的键，不设置默认显示全部字段（0表示字段排除，非0表示字段选择并排除其他字段，设置的字段要么全部为0，要么全部为1）
    - 需要以易读的方式来读取数据，可以使用 pretty() 方法
    ```
    db.collection.find(query, projection)

    //查询条件username=lison，age=18的记录，显示除了username和age之外的所有字段
    db.users.find({"$and":[{"username":"lison"},{"age":18}]},{"username":0,"age":0})
    ```
2. 查询选择器
    - 范围运算符，$eq（等于）、$lt（小于）、$gt（大于）、$lte（小于等于）、$gte（大于等于）、$in（判断元素是否在指定的集合范围里）、$all（判断数组中是否包含某几个元素,无关顺序）、$nin（判断元素是否不在指定的集合范围里）
    - 布尔运算符，$ne（不等于，不匹配参数条件）、$not（不匹配结果）、$or（有一个条件成立则匹配）、$nor（所有条件都不匹配）、$and（所有条件都必须匹配）、$exists（判断元素是否存在）
    - 其他，.（子文档匹配）、$regex（正则表达式匹配，不会走索引）

### 简单查询

1. in选择器示例
    ```
    //查询姓名为lison、mark和james这个范围的人
    db.users.find({"username":{"$in":["lison", "mark", "james"]}}).pretty()
    ```
2. exists选择器示例
    ```
    //判断文档有没有关心的字段
    db.users.find({"lenght":{"$exists":true}}).pretty()
    ```
3. not选择器示例
    ```
    //查询高度小于1.77或者没有身高的人
    //not语句会把不包含查询语句字段的文档 也检索出来
    db.users.find({"lenght":{"$not":{"$gte":1.77}}}).pretty()
    ```
4. 映射、查询唯一值
    ```
    //只显示username字段
    db.users.find({},{'username':1})
    //不显示username，其他字段都显示
    db.users.find({},{'username':0})

    //查询指定字段的唯一值，将所有记录的该字段以数据的形式显示
    db.users.distinct("username")
    ```
5. 排序、分页（都会扫描前面的数据，所以尽量使用查询条件来进行筛选而不是分页，例如按时间查询，可以将上次最后一条的时间作为新的查询）
    ```
    //1升序，-1降序
    db.users.find().sort({"username":1},{"age":1})

    //skip(n)：跳过n条数据
    //limit(n)：限制n条数据
    //显示2-4条数据
    db.users.find().sort({"username":1}).limit(2).skip(2)
    ```

### 字符串数组选择查询

1. 数组单元素查询
    ```
    //查询数组中包含"蜘蛛侠"
    db.users.find({"favorites.movies":"蜘蛛侠"})
    ```
2. 数组精确查找
    ```
    //查询数组等于[ "杀破狼2", "战狼", "雷神1" ]的文档，严格按照数量、顺序
    db.users.find(
        {"favorites.movies":[ "杀破狼2", "战狼", "雷神1" ]},
    {"favorites.movies":1})
    ```
3. 数组多元素查询
    ```
    //查询数组包含["雷神1", "战狼" ]的文档，跟顺序无关，跟数量有关
    db.users.find(
        {"favorites.movies":{"$all":[ "雷神1", "战狼" ]}},
    {"favorites.movies":1})

    //查询数组包含["雷神1", "战狼" ]中任意一个的文档，跟顺序无关，跟数量无关
    db.users.find(
        {"favorites.movies":{"$in":[ "雷神1", "战狼" ]}},
    {"favorites.movies":1})
    ```
4. 索引查询
    ```
    //查询数组中第一个为"妇联4"的文档
    db.users.find(
        {"favorites.movies.0":"妇联4"},
    {"favorites.movies":1})
    ```
5. 返回数组子集（<font color='red'>slice数组是从0开始的</font>）
    ```
    //$slice可以取两个元素数组,分别表示跳过和限制的条数；
    db.users.find({},
        {"favorites.movies":{"$slice":[1,2]},"favorites":1}
    )
    ```

### 对象数组选择查询

1. 单元素查询
    ```
    db.users.find({"comments":{
        "author" : "lison6",
        "content" : "lison评论6","commentTime" : ISODate("2017-06-06T00:00:00Z")
    }})
    ```
2. 查找lison1或者lison12评论过的user （$in查找符） 
    ```
    db.users.find({"comments.author":{"$in":["lison1","lison12"]}})
    ```
3. 查找lison1和lison12都评论过的user
    ```
    db.users.find({"comments.author":{"$all":["lison12","lison1"]}})
    ```
4. 查找lison5评语为包含"苍老师"关键字的user（$elemMatch查找符）
    ```
    db.users.find({
        "comments":{"$elemMatch":{"author":"lison5","content":{ "$regex":".*苍老师.*"}}}
    })
    ```
    > $elemMatch，数组查询操作用于查询数组值中至少有一个能完全匹配所有的查询条件的文档

    > $and，同一个文档有两个匹配，就算不在同一个数组也可以，和上面的区别就在于是否是同一个数组

### 关联查询

1. mongodb不建议使用关联查询，一般使用内联（直接加一个键，里面放对象），因为mongodb关联查询速度不快
2. 单个bson文档最大不能超过16M
3.  关联查询，使用引用（DBRef），在主表里存储一个id值，指向另一个表中的id值
    - $ref，引用文档所在的集合的名称
    - $id，所在集合的_id字段值
    - $db，可选，集合所在的数据库实例
    ```
    { "$ref" : <value>, "$id" : <value>, "$db" : <value> }
    ```
4. 示例
    ```
    //DBRef只是关联信息的数据载体，本身并不会去关联数据
    var lison = db.users.findOne({"username":"lison"});
    var dbref = lison.comments;
    db[dbref.$ref].findOne({"_id":dbref.$id})
    ```

### 聚合

1. 定义，聚合相当于统计，它允许我们通过转化合并由多个文档的数据来生成新的在单个文档里不存在的文档信息，利用aggregate关键字做聚合，聚合框架就是定义的一连串管道，数据源从第一个管道流入，经过第一个管道处理后的数据，作为第二个管道的数据源，管道依次这样处理，每个管道逗号分隔
2. 管道常见的命令
    - $project，投影，指定输出文档中的字段
    - $match，用于过滤数据只输出符合条件的文档，$match使用MongoDB的标准查询操作
    - $limit，用来限制MongoDB聚合管道返回的文档数
    - $skip，在聚合管道中跳过指定数量的文档，并返回余下的文档
    - $unwind，将文档中的某一个数组类型字段拆分成多条，每条包含数组中的一个值
    - $group，将集合中的文档分组，分组的数据可以执行如下表达式
        - $sum，计算总和
        - $avg，计算平均值
        - $min，根据分组，获取集合中所有文档对应值得最小值
        - $max，根据分组，获取集合中所有文档对应值得最大值
    - $sort，将输入文档排序后输出
3. 示例
    ```
    //查询2015年4月3号之前，每个用户每个月消费的总金额，并按用户名进行排序
    db.orders.aggregate([
        {"$match":{ "orderTime" : { "$lt" : new Date("2015-04-03T16:00:00.000Z")}}}, 
        {"$group":{"_id":{"useCode":"$useCode","month":{"$month":"$orderTime"}},"total":{"$sum":"$price"}}}, 
        {"$sort":{"_id":1}}    
    ])

    //查询2015年4月3号之前，每个审核员分别审批的订单总金额，按审核员名称进行排序
    db.orders.aggregate([
        {"$match":{ "orderTime" : { "$lt" : new Date("2015-04-03T16:00:00.000Z")}}}, 
        {"$unwind":"$Auditors"},
        {"$group":{"_id":{"Auditors":"$Auditors"},"total":{"$sum":"$price"}}},
        {"$sort":{"_id":1}}
    ])
    ```
### 连接池配置

1. 定义，mongodb客户端自带连接池
2. 关键参数含义
    - minConnectionsPerHost，最小连接数，connections-per-host
    - connectionsPerHost，最大连接数，默认100
    - threadsAllowedToBlockForConnectionMultiplier，此参数跟connectionsPerHost的乘机为一个线程变为可用的最大阻塞数（在队列中阻塞等待执行），超过此乘机数之后的所有线程将及时获取一个异常，默认5
    - maxWaitTime，一个线程等待链接可用的最大等待毫秒数，0表示不等待，默认值1000 * 60 * 2
    - connectTimeout，连接超时时间，默认值1000*10

### springboot使用的类
查询和更新的API类
1. 查询器，org.springframework.data.mongodb.core.query.Query
2. 查询条件，org.springframework.data.mongodb.core.query.Criteria

