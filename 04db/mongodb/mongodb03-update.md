
### insert

1. insertOne，插入单个文档
2. insertMany，插入多个文档
3. 如果数据库和集合不存在，insert操作将自动创建
4. 对于插入的数据，mongoDB自动生成 ObjectId 作为_id 字段（物理主键）

### delete

1. deleteOne(query)，删除单个文档
2. deleteMany(query)，删除多个文档
3. 删除操作是不会删除索引的，就算你把数据全部删除

### update

1. 语法格式
    - query，update的查询条件，类似sql update查询内where后面的
    - update，update的对象和一些更新的操作符（如$,$inc...）等，也可以理解为sql update查询内set后面的
        - 分为操作符更新和替换更新
        - 操作符更新性能更好，只更新写出来的字段
        - 替换更新会将其他所有字段都给覆盖掉
    - upsert，可选，这个参数的意思是，如果不存在update的记录，是否插入,true为插入，默认是false
    - multi，可选，mongodb默认是false，只更新找到的第一条记录，如果这个参数为true,就把按条件查出来多条记录全部更新
    - writeConcern，可选，写策略配置
    ```
    db.collection.update( 
        <query>, 
        <update>, 
        {upsert: <boolean>, multi: <boolean>, writeConcern:<document>
    )
    ```
2. update操作符
    - 操作符，$inc（指定值加n）、$set（更新指定字段）、$unset（将指定字段删除）、$rename（更新字段名称）
    - 数据操作符，$（定位到某一个元素）、$push（添加值到数组中）、$addToSet（添加值到数组中，有重复则不处理）、$pop（删除数组第一个或者最后一个）、$pull（从数组中删除匹配查询条件的值）、$pullAll（从数组中删除多个值）
    - 数组运算修饰符，$each（与$push和$addToSet等一起使用来操作多个值）、$slice（与$push和$each一起使用来操作用来缩小更新后数组的大小）、$sort（与$push、$each和$slice一起使用来对数组进行排序）

3. 普通示例
    ```
    //查找username=cang的，设置age=18，如果不存在就插入一条
    db.users.update(
        {"username":"cang"},
        {"$set":{"age":18}},
        {"upsert":true}
    )

    //删除字段示例
    db.users.updateMany(
        {"username":"lison"},
        {"$unset":{"country":"","age":""}}
    )

    //更新字段名称示例
    db.users.updateMany(
        {"username":"lison"},
        {"$rename":{"lenght":"height", "username":"name"}}
    )

    //$each作用示例
    db.users.updateMany(
        { "username" : "james"},
        { "$addToSet" : { "favorites.movies" : [ "小电影2 " , "小电影3"]}}
    )
    db.users.updateMany(
        { "username" : "james"},
        { "$addToSet" : { "favorites.movies" : { "$each" : [ "小电影2 " , "小电影3"]}}}
    )

    //删除字符串数组中元素示例
    db.users.updateMany(
        { "username" : "james"},
        { "$pull" : { "favorites.movies" : [ "小电影2 " , "小电影3"]}}
    )
    db.users.updateMany(
        {"username" : "james"},
        {"$pullAll" : { "favorites.movies" : [ "小电影2 " , "小电影3"]}}
    )
    ```
4. 向对象数组中插入元素
    ```
    //给james老师增加一条评论（$push,默认放在数组最后）
    db.users.updateOne(
        {"username":"james"},
        {"$push":{"comments":{"author":"lison23","content":"ydddyyytttt","commentTime":ISODate("2019-01-06T00:00:00")}}}
    )

    //给james老师批量新增两条评论（$push,$each）
    db.users.updateOne(
        {"username":"james"},     
        {"$push":{"comments":{"$each":[
            {"author":"lison22","content":"yyyytttt","commentTime":ISODate("2019-02-06T00:00:00")},
            {"author":"lison23","content":"ydddyyytttt","commentTime":ISODate("2019-03-06T00:00:00")}
        ]}}}
    )

    //给james老师批量新增两条评论并对数组进行排序（$push,$each,$sort）
    db.users.updateOne(
        {"username":"james"},
        {"$push": {"comments":{"$each":[
            {"author":"lison22","content":"yyyytttt","commentTime":ISODate("2019-04-06T00:00:00")},
            {"author":"lison23","content":"ydddyyytttt","commentTime":ISODate("2019-05-06T00:00:00")}
        ], 
        $sort: {"commentTime":-1}}}}
    )
    ```
5. 删除对象数组中元素示
    ```
    //删除lison22对james的所有评论 （批量删除）
    db.users.update(
        {"username":"james"},
        {"$pull":{"comments":{"author":"lison22"}}}
    )

    //删除lison5对lison评语为"lison是苍老师的小迷弟"的评论
    db.users.update(
        {"username":"lison"},
        {"$pull":{"comments":{"author":"lison5","content":"lison是苍老师的小迷弟"}}}
    )

    //更新对象数组中元素，$符号示例
    db.users.updateMany(
        {"username":"james","comments.author":"lison1"},
        {“$set”:{“comments.$.content":"xxoo","comments.$.author":"lison10" }}
    )
    ```
6. findAndModify命令，在同一往返过程中原子更新文档并返回它
    ```
    //常规的update的方法不能返回更新后的数据
    db.fam.update({"name":"morris1"},{"$inc":{"age":1}})

    //使用findandModify方法在修改数据同时返回更新前的数据或更新后的数据
    db.fam.findAndModify(
        {query:{name:'morris1'}, 
        update:{$inc:{age:1}}, 
        'new':true}
    )
    ```
> mongodb的更新都是原子的，mongodb所有的写操作都是有锁的。mongoDB 2.2之前锁级别为实例级别，mongoDB 2.2到3.2之前的版本锁级别为数据库级别，mongoDB 3.2以后，WiredTiger的锁级别是文档级别

### 其他常用命令

1. show dbs，显示数据库列表
2. show collections，显示集合列表
3. db，显示当前数据库
4. db.stats()，显示数据库信息
5. db.serverStatus()，查看服务器状态
6. db.dropDatabase()，删除数据库
7. db.help()，db.collection.help()，内置帮助，显示各种方法的说明
8. db.collection.find().size()，获取查询集合的数量；
9. db.collection.drop()，删除集合

### springboot使用的类

1. 更新器，org.springframework.data.mongodb.core.query.Update