### Mongodb关机

1. 不要用kill -9关掉mongodb的进程，很可能造成mongodb的数据丢失
2. 第一种关机方式，依次执行命令
    ```
    use admin
    db.shutdownServer()
    ```
3. 第二种关机方式执行命令
    ```
    mongod --shutdown -f mongodb.conf
    ```

### 数据管理

1. 数据备份mongodump
    - -h，指定ip和端口
    - -d，备份的数据库名称
    - -o，指定备份的路径
    ```
    ./mongodump -h localhost:27022 -d lison -o /usr/local/mongodb/mongodb-linux-x86_64-3.4.18/backup
    ```
2. 数据恢复mongorestore
    - --drop 已存在lison库则删除原数据库，去掉--drop则是合并
    ```
    ./mongorestore -h localhost:27022  -d lison /usr/local/mongodb/mongodb-linux-x86_64-3.4.18/backup/lison --drop
    ```
3. 数据导出mongoexport（针对集合）
    - -c，指定导出的集合
    - -f，要导出的字段
    - --type：导出的文件格式类型[csv,json]
    ```
    ./mongoexport -h localhost:27022 -d lison -c users -f id,username,age,salary --type=csv -o /usr/local/mongodb/mongodb-linux-x86_64-3.4.18/backup/users.csv
    ```
4. 数据导入mongoimport（针对集合）
       - --upsert，表示更新现有数据，如果不适用—upsert,则导入时已经存在的文档会报id重复，数据不再插入，也可以使用—drop删除原有数据
    ```
    ./mongoexport -h localhost:27022 -d lison -c users /usr/local/mongodb/mongodb-linux-x86_64-3.4.18/backup/users.csv --upsert
    ```

### 权限，基于角色控制

1. 数据库一般角色（Database User Roles），每个数据库都包含的一般角色
    - read，提供读取所有非系统集合和部分系统集合的数据的能力，系统集合包括：system.indexes，system.js和system.namespaces集合
    - readWrite，提供read角色的所有权限以及修改所有非系统集合和system.js集合上的数据的能力
2. 数据库管理角色（Database Administration Roles），每个数据库都包含的数据库管理角色
    - dbAdmin，提供执行管理任务的能力，如与模式相关的任务，索引，收集统计信息。 此角色不授予用户和角色管理的权限
    - userAdmin，提供在当前数据库上创建和修改角色和用户的能力
    - dbOwner，提供对数据库执行任何管理操作的能力。 此角色结合了readWrite，dbAdmin和userAdmin角色授予的权限
3. 集群管理角色（Cluster Administration Roles），在admin数据库创建，用于管理整个数据库集群系统而不是特定数据库的角色。 这些角色包括但不限于副本集和分片群集管理功能
    - clusterManager，在集群上提供管理和监视操作。 具有此角色的用户可以分别访问在分片和复制中使用的config和local数据库
    - clusterMonitor，为监控工具（如MongoDB Cloud Manager和Ops Manager监控代理）提供只读访问权限
    - hostManager，提供监视和管理服务器的能力
    - clusterAdmin，提供权限最高的群集管理访问，此角色结合了由clusterManager，clusterMonitor和hostManager角色授予的权限，此外，该角色还提供了dropDatabase操作
4. 备份和恢复角色（Backup and Restoration Roles），在admin数据库创建，用于专门的备份和恢复的角色
    - backup，提供备份数据所需的权限。 此角色提供足够的权限来使用MongoDB Cloud Manager备份代理，Ops Manager备份代理或使用mongodump。
    - restore，提供使用mongorestore恢复数据所需的权限
5. 全数据库角色（All-Database Roles），在admin数据库创建，适用于除mongod实例中的local和config之外的所有数据库：
    - readAnyDatabase，提供与读取相同的只读权限，除了适用于群集中除本地和配置数据库以外的所有权限。 该角色还提供了整个集群上的listDatabases操作
    - readWriteAnyDatabase，提供与readWrite相同的读取和写入权限，除了它适用于群集中除本地和配置数据库以外的所有数据。 该角色还提供了整个集群上的listDatabases操作
    - userAdminAnyDatabase，提供与userAdmin相同的用户管理操作访问权限，除了适用于群集中除本地数据库和配置数据库外的所有数据
    - dbAdminAnyDatabase，提供与dbAdmin相同的数据库管理操作访问权限，除了它适用于除集群中的本地数据库和配置数据库以外的所有数据库管理操作。 该角色还提供了整个集群上的listDatabases操作
6. 超级角色（Superuser Roles），所有资源的完整权限
    - root，提供对readWriteAnyDatabase，dbAdminAnyDatabase，userAdminAnyDatabase，clusterAdmin，还原和备份相结合的操作和所有资源的访问

### 客户端授权
1. 服务器启动需要加上auth参数连接服务器才需要验证
    ```
    ./mongod -f mongodb.conf --auth
    ```
2. 切换到数据库上，才能给当前数据库创建用户
    ```
    db.createUser(
        {'user':'boss', 'pwd':'boss', 'roles':[ 
            {'role':'userAdminAnyDatabase', 'db':'admin'}
        ]}
    )
    db.createUser(
        {'user':'lison','pwd':'lison','roles':[
            {'role':'readWrite','db':lison'}
        ]}
    )
    ```
### Mongodb权限初始化过程

1. 服务器启动加上auth参数
2. 数据库增加安全模式后，初始化一个“userAdminAnyDatabase”非常重要
   通过客户端连接，使用admin数据库， 执行如下脚本
    ```
    db.createUser(
        {'user':'boss', 'pwd':'boss', 'roles':[
            {'role':'userAdminAnyDatabase', 'db':'admin'}
        ]}
    )
    ```
3. 使用刚创建成功的用户登录
    ```
    db.auth("boss","boss")
    ```
4. 切换到lison数据库（use lison），创建读写权限用户
    ```
    db.createUser(
        {'user':'lison','pwd':'lison','roles':[
            {'role':'readWrite','db':'lison'}
        ]}
    )
    ```
5. 使用读写权限用户lison登录，db.auth("lison","lison")，登录后测试
6. 如果第一次创建用户，权限没有到位，只能通过非安全模式重新登录数据库，清空admin数据库中的数据信息，再重新执行一遍
