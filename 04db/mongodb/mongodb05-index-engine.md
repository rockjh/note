### 存储引擎

1. MMAPV1，3.2版本之前默认的存储引擎，其采用linux操作系统内存映射技术,但一直饱受诟病
2. WiredTiger，3.4以上版本默认的存储引擎，优势
    - 读写操作性能更好,可以更好的发挥多核处理器
    - 使用文档级别锁，并发以及吞吐量大大提高
    - 使用索引前缀压缩，节省内存空间的损耗（mongodb运行时，会将索引放入内存）
    - 提供压缩算法,节省约60%以上的硬盘空间

### 写策略

1. Journaling日志，类似于关系数据库中的事务日志，能够使MongoDB数据库由于意外故障后快速恢复
    - 没有开启时，写流程如下
        - 应用请求写数据
        - mongodb处理层，接收请求并调用引擎接口提交
        - mongodb引擎层，将处理放入缓存，每60秒或容量达到2G提交一次硬盘处理
        - 满足条件，写磁盘文件
        - 意外宕机大概率丢失数据
    - 开启时（2.4以上版本默认），写流程如下
        - 应用请求写数据
        - mongodb处理层，接收请求并调用引擎接口提交
        - mongodb引擎层，将处理放入缓存，每60秒或容量达到2G提交一次硬盘处理
        - mongodb引擎层，同时写入Journaling日志，Journaling日志每100毫秒刷新日志更新到Journal磁盘文件
        - 满足条件，写磁盘文件
        - 意外宕机，最多丢失200ms的数据，重启时，会从Journaling日志读取到内存中
        - 推荐生产环境开启Journaling日志，他只会对写性能有一定的影响，不会影响读
2. 语法

    - w，数据写入到number个节点才向用客户端确认
        - 0，对客户端的写入不需要发送任何确认，适用于性能要求高，但不关注正确性的场景
        - 1，默认的writeConcern，数据写入到Primary就向客户端发送确认
        - majority，数据写入到副本集大多数成员后向客户端发送确认，适用于对数据安全性要求比较高的场景，该选项会降低写入性能
        - 当时集群时，如果设置数据需要成功写入number个节点才算成功，如果有节点故障，可能会一直不能满足条件，可以设置wtimeout超时，来判断失败
    - j，写入操作的journal持久化后才向客户端确认，默认false
    - wtimeout，写入超时时间，仅w的值大于1时有效
    ```
    { w: <value>, j: <boolean>, wtimeout: <number> }
    ```
3. 作用范围
    - 配置MongoClient，所有写请求生效
    - 配置MongoDatabase，所有有关这个数据库的写请求生效
    - 配置MongoCollection，所有有关这个集合的写请求生效
    - 上面都是利用WriteConcern来设置写策略

### 索引

1. 语法
    - key为要创建的索引字段，升序值为1，降序值为-1，哈希索引为hashed（哈希索引是等值查询、均匀分布的、不能范围查询，适合分片集合）
    - options为索引的属性
        - background，boolean，是否后台构建索引，生产环境建议加上此属性，会为其他读写操作让路
        - unique，boolean，是否为唯一索引
        - name，string，索引名
        - sparse，boolean，是否为稀疏索引,索引仅引用具有指定字段的文档

    ```
    db.collection.createIndex(keys, options)
    ```
2. 索引实战
    ```
    //单键唯一索引
    db.users.createIndex({username :1},{unique:true})

    //单键唯一稀疏索引
    db.users.createIndex(
        {username :1},{unique:true,sparse:true}
    )

    //复合唯一稀疏索引
    db.users. createIndex(
        {username:1,age:-1},{unique:true,sparse:true}
    )
    
    //创建哈希索引并后台运行
    db.users. createIndex(
        {username :'hashed'},{background:true}
    )

    //根据索引名字删除某一个指定索引
    db.users.dropIndex("username_1")
    
    //删除某集合上所有索引
    db.users.dropIndexs()
    
    //重建某集合上所有索引
    db.users.reIndex()
    
    //查询集合上所有索引
    db.users.getIndexes()
    ```
3. 索引建议
    - 索引有用，但是会全部加载进内存，写数据时会变慢（需要处理索引）
    - 复合索引的顺序很重要，尽量不要在数据量大的生产环境建索引（耗时）
    - 索引用来查询小范围数据，写比读多和每次都返回大部分文档数据不适合索引

### 查询优化

1. 开启慢查询
    - n的值
        - 0，默认值表示不记录
        - 1，记录慢速操作，如果值为1，m必须赋值
        - 2，记录所有的读写操作
    - m的值，当n=1时，单位为ms，用于定义慢速查询时间的阈值
    ```
    db.setProfilingLevel(n,{m})
    ```
2. 慢查询保存在system.profile集合中，默认分配了128kb的空间
    - 大小或者数量固定
    - 不能做update和delete操作
    - 容量满了以后，按照时间顺序，新文档会覆盖旧文档
3. 分析慢查询，explain
    - explain的入参可选值为
        - queryPlanner，默认值，仅仅展示执行计划信息
        - executionStats，表示展示执行计划信息同时展示被选中的执行计划的执行情况信息（推荐）
        - allPlansExecution，展示执行计划信息同时展示被选中的执行计划的执行情况信息，还展示备选的执行计划的执行情况信息
    ```
    db.orders.find({'price':{'$lt':2000}}).explain('executionStats')
    ```
4. 解读explain结果
    - queryPlanner，执行计划描述
    - winningPlan，被选中的执行计划
	- stage，COLLSCAN 没有走索引，IXSCAN使用了索引
 	- rejectedPlans，候选的执行计划
    - executionStats，执行情况描述
	- nReturned，返回的文档个数
    - executionTimeMillis，执行时间ms
    - totalKeysExamined，检查的索引键值个数
    - totalDocsExamined，检查的文档个数
5. 优化目标
    - 根据需求建立索引
    - 每个查询都要使用索引以提高查询效率，winningPlan.stage必须为IXSCAN
    - 追求totalDocsExamined = nReturned
