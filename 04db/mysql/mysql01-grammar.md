## count

### count语义

    count()是一个聚合函数，对于返回的结果集，一行一行的判断，如果count()的参数不为null，累计值加1，否则不加，最后返回累加值

### count的几种用法

1. count(*)，优化器已经优化过，执行行累加
2. count(1)，innoDB引擎遍历整张表，但不取值，server层对于返回的每一行，放一个数字1进去，判断不可能为空，执行行累加
3. count(主键)，innoDB会遍历整张表，把每一行的id都取出来，返回给server层，server判断不可能为空，按行累加
4. count(字段)，如果这个字段定义为not null，一行行从记录里面读取这个字段，判断不能为null，按行累加；如果这个字段定义允许为null，那么执行的时候，还需要把值取出来判断是否为null，不能null累加

### 执行效率排序

    count(字段)<count(主键 id)<count(1)≈count(*)

## order by

1. 建表如下
    ```
    CREATE TABLE `t` (
    `id` int(11) NOT NULL,
    `city` varchar(16) NOT NULL,
    `name` varchar(16) NOT NULL,
    `age` int(11) NOT NULL,
    `addr` varchar(128) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `city` (`city`)
    ) ENGINE=InnoDB;
    ```
2. 查询sql如下
    ```
    select city,name,age from t where city='杭州' order by name limit 1000  ;
    ```
3. 排序执行流程
    - mysql会给每个线程分配一块内存你用于排序，称为sort_buffer。初始化sort_buffer，确定放入city、name、age三个字段
    - 从索引city中找到满足条件主键的id
    - 根据主键id取出name、city、age，存入sort_buffer
    - 从索引city中取出下一个主键id，一直重复直到不满足条件
    - 对sore_buffer中的数据按照name进行快排
    - 按照排序结果返回前1000行数据
    > 到了这里完了称为全字段排序，还有一种不是全字段排序，第一步只取出所有满足条件的id和name结果集，排好序之后，再进行一次回表，查询出age和city
4. 走索引，无需排序，添加索引和执行流程如下
    ```
    alter table t add index city_user(city, name);
    ```
    - 从索引 (city,name) 找到第一个满足 city='杭州’条件的主键 id
    - 到主键 id 索引取出整行，取 name、city、age 三个字段的值，作为结果集的一部分直接返回
    - 从索引 (city,name) 取下一个记录主键 id
    - 重复步骤直到查到第 1000 条记录，或者是不满足 city='杭州’条件时循环结束
    > 因为索引已经排好序了，可以直接取，如果sql的条件是city='杭州' or city='成都'，放在一条执行的话也会走排序，可以分为两条sql执行，然后业务逻辑使用归并排序处理
