### 简介

数据库的索引就是为了提高数据查询效率，就像书的目录一样

### 索引常见的数据模型

- 哈希表，是一种以键值对存储数据的结构，当有重复的哈希键时，会以链表的形式存储。哈希表的数据结构只适用于等值查询，区间查询是一场噩梦
- 有序数组，有序数组在等值查询和范围查询的场景都是比较优秀的，但是如果是中间插入一个数据，后续的必须要顺位移动。有序数组适合静态存储引擎，比如2017年的城市人口
- 平衡二叉树，每个节点的左节点小于父节点，右节点大于父节点，查询和更新的时间复杂度都是O(log(N))，虽然他的效率很好，但是一般都不使用二叉树，主要是访问磁盘次数太多了
- 多叉树（B+）,对比二叉树，树不高访问磁盘次数少，每一个节点有多个子节点。目前mysql使用的B+树模型

### 索引特性

- 主键索引的叶子节点存储的是整行数据，非主键索引的叶子节点存储的是主键的值
- 一般来说使用主键索引作为查询条件可以直接获取到整行数据，只需要扫描一颗索引数；使用非主键索引作为查询条件，首先是查询到具体的索引，然后再回表根据主键值查询行数据
- 最左前缀原则，一般我们建立联合索引后（name,age）是先按照name排序，再按照age排序的，所以如果查询条件只有name，那么他是可以走这个索引的
- 索引下推，如果同时按name和age查询，是先查询name，然后在当前索引中再查询age同时满足条件的主键取出来，进行回表查询
- 覆盖索引，组合索引（name，age）如果是查询张%的age，可以不用进行回表，直接在索引中有age的值，直接返回

### change buffer

1. redo log先记录下需要操作的项
2. 一条更新sql，对于没有唯一性的约束，如果数据页在内存中，直接在内存更新；如果不在内存，在change buffer记录更新逻辑
3. 对于在内存中更新的数据，会在后台操作写入磁盘；对于在change buffer中记录的逻辑，会在后台操作记录到系统表空间（ibdata1，硬盘）
4. 对于在change buffer中记录的逻辑，如果有查询数据页的内容，会读取磁盘内容到内存，进行处理数据，然后返回正确的数据并将数据更新的磁盘中；可以是一段时间后自动更新到磁盘；mysql服务正常关闭
5. change buffer适用于写多少读场景，change buffer主要节省的是随机度读磁盘的IO消耗

### 唯一索引和普通索引

- 唯一索引比普通索引就是多了一个唯一性，唯一索引每次的增加，修改，都会去检测该索引所有数据的值，判断是否重复
- 唯一索引不会使用change buffer，因为他需要检测所有的值
- 在查询时，唯一索引查询到第一个满足等于条件后，就会停止查询；普通索引会一直查询完所有的值。一般来说索引有排序的，也不会查询所有的值
> 综合来说普通索引在性能上好很多，对于内存会有更多的命中

### 字符串索引

1. 创建字符串索引的方式
    - 创建全索引，占用空间更大
        ```
        alter table SUser add index index1(email);
        ```
    - 创建局部索引，占用空间相对较小，如果前几个索引是重复的，就需要额外增加记录扫描，如下前6个字符
        ```
        alter table SUser add index index2(email(6));
        ```
    - 倒序存储，例如身份证号，最后几位才是随机的，存储和查询的时候都是用reverse倒序处理
    - hash字段处理，例如身份证号，新建一个字段存储身份证号hash值，每次查询时如下
        ```
        select field_list from t where id_card_crc=crc32('input_id_card_string') and id_card='input_id_card_string'
        ```
2. 几种创建索引方式的不同
    - 全索引占用空间大，可以使用覆盖索引（就是如果只需要显示主索引不需要回表操作）
    - 局部索引节省空间，如果前面局部索引重复，需要额外增加记录扫描
    - 倒序存储和hash字段处理都只能进行等值查询，不支持区间处理，因为没有排序
### 索引选择

1. 一个表有很多索引，我们书写sql的时候没有主动指定使用什么索引，具体使用哪个索引是由优化器来决定的。但有些时候索引的选择可能会有误差，索引的选择主要由以下几个要点处理
    - 行数，采取的是采样统计，数据并不精确，如果差距太大可以使用analyze table t校验一下行数
    - 排序，因为索引是拍好序的，所以也会综合考虑进去
    - 临时表，
2. 创建表结构如下
    ```
    CREATE TABLE `t` (
    `id` int(11) NOT NULL,
    `a` int(11) DEFAULT NULL,
    `b` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `a` (`a`),
    KEY `b` (`b`)
    ) ENGINE=InnoDB；
    ```
    如下sql会使用什么索引
    ```
    #使用索引b，行扫描50000多
    select * from t where (a between 1 and 1000) and (b between 50000 and 100000) order by b limit 1;

    #使用索引a，行扫描1000多，因为修改了排序
    select * from t where (a between 1 and 1000) and (b between 50000 and 100000) order by b,a limit 1;
    ```
3. 可以声明强制使用某个索引，但是sql不便于移植，如下
    ```
    #已经建立了索引create_time
    #已经建立了组合索引 union_index包括字段 create_time subtype type company_id
    #走的create_time索引
    select * from table  where create_time and create_time>=时间戳 and create_time<=时间戳
    and subtype='xx' and type='xx' and company_id =x order by create_time limited 90,30 ;
    ```
    强制force index声明使用索引unioin_index
    ```
    select * from table force index(b)  where create_time and create_time>=时间戳 and create_time<=时间戳
    and subtype='xx' and type='xx' and company_id =x order by create_time limited 90,30 ;
    ```
4. 索引选择异常，如何纠正？
    - 使用force index强行选择一个索引，不过这种方式可移植性不好
    - 修改sql，引导使用我们期望的索引，比如加一些order by
    - 新建一个更合适的索引，或者删除之前的索引，比如3中删除掉create_time索引也行
5. 对索引字段做函数操作，可能会破坏索引的有序性，因此优化器就放弃走树搜索功能如下语句
    ```
    #查询7月份的计数，很慢，全索引扫描，可以优化where条件为查询每年的7=8月
    select count(*) from tradelog where month(t_modified)=7;

    #tradeid是字符串类型，mysql会优先自己将字符串转为number，也用了全索引
    select * from tradelog where tradeid=110717;
    ```
### 索引维护

- 索引树在增加和删除数据时的数据移动可能导致页分裂和页合并，页分裂和页合并时会耗费很多性能
- 当为自增主键时，添加都是追加操作，不会产生页分裂
- 普通索引因为会存储主键索引的值，所以主键索引越小占用的空间越小
- 当需求只有一个索引，该索引是唯一索引，那么使用业务字段作为主键是没有影响的
- 有些时候删除了数据，但是索引没有删除，需要重建索引。重建普通索引可以使用两句sql删除之后重建，对于主键索引不适用，因为无论是删除还是新建主键都会将整个表重建，推荐使用alter table T engine=InnoDB重建主键索引

### 建索引的条件

1. 某一列相对来说唯一
2. 经常用来查询显示的列
3. 经常用来关联的列where条件中用到的列，以及join on用到的列
> 索引太多的话，会影响db的更新，一般最多5个

### 有时候mysql的执行效率低

1. 有些时候sql会执行得特别慢，可能是mysql在将数据持久化到数据中，下面这些情况会导致数据持久化
    - redo log数据满了，需要记录到磁盘中，然后擦除一些
    - 内存不足，需要将内存中的数据持久化到磁盘，然后释放一些内存
    - 数据库空闲时，慢慢开始持久化
    - mysql正常关闭时，会将内存、redo log、change buffer中的都持久化到磁盘中
2. innoDB持久化磁盘策略参数
    - innodb_io_capacity参数是告诉innoDB你的磁盘能力，比如明明是SSD结果设置的机械硬盘的参数，可以通过fio这个工具来测试，设置如下
        ```
        fio -filename=$filename -direct=1 -iodepth 1 -thread -rw=randrw -ioengine=psync -bs=16k -size=500M -numjobs=10 -runtime=10 -group_reporting -name=mytest 
        ```
    - 等
4. redo log设置过小的后果？
    - redo log很快就写满了，需要将redo log的持久化到磁盘
    - redo log中记录了change buffer值，change buffer也要持久化到磁盘
    - 由于redo log还记录了undo的变化,undo log buffer也要持久化进undo log
    - 当innodb_flush_log_at_trx_commit设置为非1,还要把内存里的redo log持久化到磁盘上

### 实例

表定义如下

```
CREATE TABLE `geek` (
  `a` int(11) NOT NULL,
  `b` int(11) NOT NULL,
  `c` int(11) NOT NULL,
  `d` int(11) NOT NULL,
  PRIMARY KEY (`a`,`b`),
  KEY `c` (`c`),
  KEY `ca` (`c`,`a`),
  KEY `cb` (`c`,`b`)
) ENGINE=InnoDB;
```
使用的sql如下

```
select * from geek where c=N order by a limit 1;
select * from geek where c=N order by b limit 1;
```
问题：索引的建立是否合理？

答：一共四颗索引树
- （a,b）相当于order by a,b
- （c）相当于order by c,a,b
- （c,a）相当于order by c,a,b
- （c,b）相当于order by c,b,a

因此索引（c,a）可以删除
