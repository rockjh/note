### 慢查询

1. 定义，记录查询、更新慢的sql信息

2. 常用配置
    - slow_query_log启动停止记录慢查询日志（默认关闭）
    - slow_query_log_file指定慢查询日志的存储路径和文件（默认mysql安装目录data）
    - long_query_time指定记录慢查询日志SQL执行时间的阈值（单位秒，默认10）
    - log_queries_not_using_indexes是否记录未使用索引的sql
    - log_output日志存放的地方，【TABLE】、【FILE】、【TABLE,FILE】（推荐使用FILE，默认FILE）
3. 记录符合条件的sql
    - 查询语句
    - 数据修改语句
    - 已经回滚的语句
4. 设置慢查询参数，开始记录慢查询
    ```
    //设置查看慢查询开关
    set global slow_query_log = 1
    show variables like 'long_query_time';
    
    //查看慢查询文件存储路径
    show variables like 'slow_query_log_file'

    //类似的前面进行查看设置
    ```
5. 上面使用的是mysql记录慢查询，分析慢查询可以使用mysql自带的工具mysqldumpslow，也可以使用pt_query_digest工具（更好）

### 执行计划

1. 定义，是使用explain关键字可以模拟优化器执行sql查询语句，从而知道是如何处理sql语句的，分析sql或者表结构的性能瓶颈
2. 作用，知道以下信息
    - 表的读取顺序
    - 数据读取操作的操作类型
    - 哪些索引可以使用
    - 哪些索引被实际引用
    - 表之间的引用
    - 每张表有多少行被优化器查询
3. 使用
    ```
    //一般来说sql中使用到了几个表，结果会有几行数据
    explain sql语句
    ```
4. 执行结果解析
    - id，多行数据有相同和不同的情况
        - id相同可以认为是一组，从上往下顺序执行
        - id不同，就属于不同的组，值越大，优先级越高，越先执行（一般子查询会出现这种情况）
    - select_type，主要用于区分普通查询、联合查询、子查询等复杂查询
        - SIMPLE，简单的select查询，查询中不包括子查询或者union
        - PRIMARY，查询中若包含任何复杂的子部分，最外层查询则被标记为此种
        - SUBQUERY，在select或where列表中包含了子查询
        - DERIVED，在from列表中包含的子查询被标注为derived（衍生）
        - UNION，若第二个select出现在union之后，则被标注为union，若union包含在from字句的子查询中，外层select将被标注为derived
        - UNION RESULT，从union表获取结果的select
    - table，数据出自哪个表
    - type，显示的是访问类型，是比较重要的一个指标
        - system，表只有一行记录（等于系统表），这是const类型的特列，平时不会出现
        - const，表示通过索引一次就找到了，const用于比较primary key 或者unique索引
        - eq_ref，唯一性索引扫描，对于每个索引键，表中只有一条记录与之匹配。常见于主键或唯一索引扫描
        - ref，非唯一性索引扫描，返回匹配某个单独值的所有行
        - range，只检索给定范围的行，使用一个索引来选择行，key列显示使用了哪个索引，一般就是在你的where语句中出现between、< 、>、in等的查询
        - index，遍历索引，属于全表扫描
        - all，从硬盘中读取，遍历整个表

        ```
        //结果从好到坏
        system>const>eq_ref>range>index>ALL

        //表t_user是const，因为是通过主键等值查找
        //临时表d1是system，因为他只有一条记录，是第二次加载的
        explain
        select * from (select * from t_user where id = 1) d1

        //表t1是eq_ref类型的，因为是通过主键id进行关联的
        explain
        select * from t1,t2 where t1.id=t2.id

        //表t1是ref索引，列coll是普通索引
        explain
        select * from t1 where coll = 'text'

        //表t_user是range类型的，因为使用了>符号
        explain
        select * from t_user where id>10

        //表t_user是index类型的，因为查询了所有的所有的索引id，走的是索引扫描，扫描索引是为了取出数据
        explain
        select id from t_user

        //表t_user是走的all类型，因为需要取数据，走的全表扫描
        explain
        select * from t_user
        ```
    - possible_keys，显示可能应用在这张表中的索引，可能某一个查询条件的列被建立了多个索引
    - key，实际使用的索引，如果为NULL，则没有使用索引。（可能原因包括没有建立索引或索引失效）
    - key_len，显示的值为索引字段的最大可能长度（表设计时的字段大小），复合索引具有最左前缀原则，如果复合索引能够全部用上key_len是复合索引字段的索引长度和key_len = 字段定义的长度+1(允许为null)，在字符情况下字段定义的长度如下
        - char和varchar跟字符编码也有密切的联系，不同的字符编码占用的存储空间不同
            - latin1占用一个字节
            - gbk占用两个字节
            - utf-8占用三个字节
        - char字段定义的长度=长度*占用的字节数
        - varchar字段定义的长度=长度*占用的字节数+2（可扩展的特性）
    - ref，显示索引的哪一列被使用了，如果可能是一个常数
        - const，表示查询条件使用的是一个列
        - 数据库.表.字段，表示具体使用了那一个列的索引
    - rows，根据表统计信息及索引选用情况，大致估算出找到所需的记录所需要读取的行数，也就是说，用的越少越好
    - extra，包含不适合在其他列中显示，但十分重要的额外信息
        - Using filesort，使用了文件排序，而没有使用索引的排序，因为order by的字段没有在索引中，需要优化
        - Using temporary，使用了临时表保存中间结果，mysql在对查询结果排序时使用了临时表，常见于order by和group by，需要优化
        - Using index，使用了覆盖索引，也就是查询显示的字段全部都建立了索引的字段
            ```
            //username是建立了索引的，这里是覆盖索引
            select username from t_user
            ```
        - Using where，我们使用了where语句  
        - Using join buffer，表名使用了连接缓存，join on
        - Impossible WHERE，是指不会执行的sql，优化器得知查询不到值
            ```
            explain
            select * from t_user where 1=2
            ```

### sql优化

1. 尽量全职匹配，一个复合索引有三列，尽量三列都用上
2. 最佳左前缀原则，创建复合索引时，最前面的列必须要用到才能使用索引
3. 不要在索引列上做任何操作，计算、函数、自动或手动的数据类型转换，会导致索引失效
4. 在复合索引中，如果要使用范围查询，最好将范围查询的列，放在复合索引的最后（name、pos、age），age使用范围查询，否则会导致范围查询后面的索引失效
5. 覆盖索引（查询的列都在索引列中）要尽量使用，减少select * 的使用
6. 不等于要慎用，不等于会导致索引失效，使用全表扫描
    ```
    //使用了不等于，全表扫描
    select * from t_user where name!='jh'
    //使用了覆盖索引，复合索引（name,pos,age）
    select name,pos,age form t_user where name!='jh'
    ```
7. null或者not null对索引有影响
    - name字段定义为允许为null，有索引（name,pos,age）
        ```
        //使用索引
        select * from t_user where name is null
        //全表扫描，可以修改为覆盖索引，只查询索引列字段
        select * from t_user where name is not null
        ```
    - name字段定义为不允许为null，有索引（name,pos,age）
        ```
        //全表扫描，解决方法使用覆盖索引，复合索引（name,pos,age）
        select * from t_user where name is not null
        ```
8. like查询要当心
    ```
    //会使用复合索引（name,pos,age）
    select * from t_user where name like 'j%'
    //全表扫描，解决方法使用覆盖索引，复合索引（name,pos,age）
    select * from t_user where name like '%j%'
    ```
9. 字符类型查询参数不加引号，会导致索引失效

10. 针对limit的优化，limit越大，后面的数据读取越慢，可以根据业务条件在where处处理

### 批量导入大量数据

1. 普通的导入insert需要注意的点
    - 提交前关闭自动提交
    - 尽量使用批量insert语句
    - 可以使用MyISAM存储引擎
2. LOAD DATA INFLILE比普通导入快20倍
    - 导出表文件
        ```
        select * into OUTFILE '/Users/rockjh/t_user.txt' from t_user
        ```
    - 导入标文件
        ```
        laod data INFILE '/Users/rockjh/t_user.txt' into table t_user
        ```