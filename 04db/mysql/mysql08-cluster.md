### 为什么要关注Mysql架构

1. 提高可用性，当一个数据库实例出现问题，可以快速的切换到其他数据库实例
2. 提供性能，对数据库的访问可以分散到不同的数据库实例上，也可以进行读写分离

### 集群方案

1. MySQL Cluster，由mysql本身提供（不适合线上系统）
    - 优点，可用性高、性能好，每份数据至少可在不同主机存一份拷贝，且冗余数据拷贝实时同步
    - 缺点，维护非常复杂、存在部分bug
2. DRBD磁盘网络镜像方案，通过网络来镜像整个设备(磁盘).它允许用户在远程机器上建立一个本地块设备的实时镜像,与心跳链接结合使用（成本高昂）
    - 优点，数据可在底层快设备级别跨物理主机镜像，且可根据性能和可靠性要求配置不同级别的同步，IO操作保持顺序，可满足数据库对数据一致性的苛刻要求
    - 缺点，必须分布式文件系统、维护成本最高
3. MySQL Replication，将主数据库的操作通过二进制日志传到从库上，在从库上对这些日志重新执行(也叫重做)，使得从库和主库的数据保持同步（推荐使用）
    - 主从数据库并不是完全同步，有一定的时间差，实时数据还是通过主数据库查询
    - 优点
        - 如果主库出现问题,可以快速切换到从库提供服务
        - 可以在从库上执行查询操作, 降低主库的访问压力
        - 某些数据库维护工作，比如备份，可以在从库上执行，以避免备份期间影响主库的服务
    - 原理
        - 主库在事务提交时会把数据变更作为事件events记录在二进制日志文件binlog中，主库上的sync_binlog参数控制binlog日志刷新到磁盘
        - 主库推送二进制日志文件binlog中的事件到从库的中继日志relay log，之后从库根据中继日志relay log重做数据变更操作，通过逻辑复制以此来达到主库和从库的数据一致
        - mysql的复制是主库主动推送日志到从库去的，是推模式
    - 流程
        - 通过3个线程来完成主从库间的数据复制
        - 其中binlog dump线程跑在主库上，I/0线程和sql线程跑在从库上当在从库上启动复制时，首先创建I/0程连接主库
        - 主库随后创建binlog dump线程读取数据库事件并发送给I/0线程，I0线程获取到事件数据后更新到从库的中继日志relaylog中去
        - 从库上的sql线程读取中继日志relaylog中更新的数据库事件并应用
        ```
        //主库中査看BinlogDump线程
        SHOW PROCESSLIST
        //从库上可以看到i/o线程和sql线程
        SHOW PROCESSLIST
        ```
    - 日志文件解析
        - binlog会把sql的所有修改操作写入数据，包括insert、delete、update和修改表结构，relaylog和binlog的数据格式一模一样
        - relaylog，sql线程会自动删除已经处理过的relaylog，从库上默认还会创建两个日志文件master.info和relay_log.info，分别用来保存复制的进度
        - 查看从库复制状态
            ```
            //可以携带更多的参数
            show slave status
            ```
    - 复制技术，二进制文件binlog的三种格式
        - statement，基于sql语句级别，每条修改数据的sql都会保存到binlog
        - row，基于行级别，记录每一行数据的变化，也就是sql执行后的处理（占用空间大，从库使用起来快）
        - mixed，混合statement和row模式,默认情况下采用statement模式记录,某些情况下会切换到row模式
        ```
        //查看当前复制方式
        show variables like '%binlog%format%';
        //更改复制方式
        //也可以只更改session
        set global binlog_format = 'ROW'; 
        set global binlog_format = 'STATEMENT';

        ```

### 复制架构（核心都是主从复制）

1. 一主多从，一实现读写分离, 把大量对实时性要求不是特别高的读请求通过负载均衡分布到多个从库上，主库宕机后会把一个从库切换为主库继续服务
2. 多级复制，主库会为每个从库都创建Binlog Dump线程来发送事件，主库压力大，可以将某些从库作为一些从库的主库，会导致后续从库的数据延时性大大增加
3. 双主复制，master和master2互为主从，client客户端的写请求都访问主库 master,而读请求可以选择访问主库master或master2
4. 双主多级复制架构，双主复制+一主多从

### 主从复制搭建过程

1. 异步复制，主库执行完commit后，在主库写入binlog日志后即可成功返回客户端，无需等binlog日志传送给从库（主库宕机从库可能丢数据）

    - 主从库安装了相同版本的数据库
    - 主库设置一个复制使用的账户，并授予REPLICATION SLAVE权限
        ```
        GRANT REPLICATION SLAVE ON *.* To 'rep1'@'192.168.56.103' IDENTIFIED BY '1234test';
        ```
    - 主库配置my.cnf，然后启动主库
        ```
        #需要先创建log目录
        log-bin=/home/mysql/log/mysql-bin.log
        #全局唯一，每个数据库的id都不同
        server-id= 1
        ```
    - 从库配置my.cnf
        ```
        server-id=2
        ```
    - 忽略复slave线程启动从库
        ```
        ./bin/mysqld_safe  --defaults-file=/home/mysql/mysql3307/my.cnf --skip-slave-start
        ```
    - 对从数据库服务器做相应设置，指定复制使用的用户、主数据库服务器的IP、端口以及开始执行复制的日志文件和位置等
        ```
        CHANGE MASTER TO MASTER_HOST='192.168.56.103',
        MASTER_PORT=3306,
        MASTER_USER='rep1',
        MASTER_PASSWORD='1234test',
        MASTER_LOG_FILE='mysql-bin.000001',
        MASTER_LOG_POS=428;
        ```
    - 在从库上启动slave线程
        ```
        start slave
        ```
2. 半同步复制，等待其中一个从库也接收到binlog事务并成功写入relaylog之后，才返回commit操作成功给客户端

    - 先安装好异步复制
    - 判断mysql服务器是否支持动态增加插件
        ```
        select @@have_dynamic_loading
        ```
    - 确认支持动态增加插件后，检查mysql的安装目录下是否存在插件
    - 在主库上安装插件semisync_master.so
        ```
        install plugin rpl_semi_sync_master SONAME 'semisync_master.so'
        ```
    - 从库上则安装插件semisync_slave.so
        ```
        install plugin rpl_semi_sync_slave SONAME 'semisync_slave.so'
        ```
    - 安装完成后，从plugin表中能够看到刚才安装的插件
        ```
        select * from mysql.plugin
        ```
    - 需要分别在主库和从库上配置参数打开半同步semi-sync，默认半同步设置是不打开的，主库上配置全局参数
        ```
        #主库
        set global rpl_semi_sync_master_enabled=1
        set global rpl_semi_sync_master timeout 30000
        #从库
        set global rpl_semi_sync_slave_enabled=1
        ```

### 读写分离实战

1. 使用框架，springboot+mybatis
2. 思想
    - 利用spring提供的路由数据源、aop
    - AbstractRoutingDataSource内部维护了一组目标数据源，并且做了路由key与目标数据源之间的映射
    - spring获取数据源的流程
        - getConnection方法获取连接
        - getConnection中调用determineTargetDataSource
        - determineTargetDataSource调用determineCurrentLookupKey
        - determineCurrentLookupKey进行覆写
3. yml配置数据源
    ```
    spring:
        datasource:
            master:
                jdbc-url: jdbc:mysql://localhost:3306/User?serverTimezone=GMT%2b8&autoReconnect=true&useUnicode=true&characterEncoding=utf-8&autocommit=false
                username: root
                password: 123456
                driver-class-name: com.mysql.cj.jdbc.Driver
            slave1:
                jdbc-url: jdbc:mysql://localhost:3306/Order?serverTimezone=GMT%2b8&autoReconnect=true&useUnicode=true&characterEncoding=utf-8&autocommit=false
                username: root
                password: 123456
                driver-class-name: com.mysql.cj.jdbc.Driver
    ```
4. 自定义，AbstractRoutingDataSource实现类
    ```
    public class MyRoutingDataSource extends AbstractRoutingDataSource {
        //在运行时, 根据某种规则
        //比如key值
        //比如当前线程的id
        来动态切换到真正的DataSource上
        @Override
        protected Object determineCurrentLookupKey() {
            return DBContextHolder.get();
        }
    }
    ```
5. DB上下文管理DBContextHolder
    ```
    public class DBContextHolder {
        //保存系统中存在的数据源的标识符
        //然后通过该标识符定位到实际的数据源实体
        private static final ThreadLocal<DBTypeEnum> contextHolder
                = new ThreadLocal<DBTypeEnum>();

        public static void set(DBTypeEnum dbTypeEnum){
            contextHolder.set(dbTypeEnum);
        }

        public static DBTypeEnum get(){
            return contextHolder.get();
        }

        public static void master(){
            set(DBTypeEnum.MASTER);
            System.out.println("切换到主库-----------------------");
        }

        public static void slave(){
            set(DBTypeEnum.SLAVE);//轮询
            System.out.println("切换到从库-----------------------");
        }

    }
    //数据库枚举
    public enum DBTypeEnum {
        MASTER,SLAVE;//SLAVE2.....
    }
    ```
6. 数据源配置
    ```
    @Configuration
    public class DataSourceConfig {
        //创建主库数据源，创建默认的数据源
        @Bean
        @ConfigurationProperties("spring.datasource.master")
        public DataSource masterDataSource() {
            return DataSourceBuilder.create().build();
        }

        //创建从库数据源，创建默认的数据源
        @Bean
        @ConfigurationProperties("spring.datasource.slave1")
        public DataSource slave1DataSource() {
            return DataSourceBuilder.create().build();
        }

        //自定义路由数据源
        //内部持有了主库和从库的数据源，
        //通过某种机制让应用程序在进行数据读写时，按业务情况走主库或者从库
        @Bean
        public DataSource myRoutingDataSource(
                @Qualifier("masterDataSource")DataSource masterDataSource,
                @Qualifier("slave1DataSource")DataSource slaveDataSource){
            Map<Object, Object> targetDataSources = new HashMap<>();
            targetDataSources.put(DBTypeEnum.MASTER,masterDataSource);
            targetDataSources.put(DBTypeEnum.SLAVE,slaveDataSource);
            MyRoutingDataSource myRoutingDataSource = new MyRoutingDataSource();
            myRoutingDataSource.setTargetDataSources(targetDataSources);
            /*当执行的方法没有被Aop拦截时，缺省使用的数据源*/
            myRoutingDataSource.setDefaultTargetDataSource(masterDataSource);
            return myRoutingDataSource;
        }
    }
    ```
7. 注入数据源到mybatis
    ```
    @EnableTransactionManagement
    @Configuration
    @MapperScan(
            basePackages = "cn.enjoyedu.rwseparation.dao",
            annotationClass = Repository.class)
    public class MybatisConfig {

        @Resource(name = "myRoutingDataSource")
        private DataSource myRoutingDataSource;

        @Bean
        public SqlSessionFactory sqlSessionFactory() throws Exception {
            SqlSessionFactoryBean sqlSessionFactoryBean
                    = new SqlSessionFactoryBean();
            sqlSessionFactoryBean.setDataSource(myRoutingDataSource);
            sqlSessionFactoryBean.setMapperLocations(
                    new PathMatchingResourcePatternResolver()
                            .getResources("classpath:mapper/*.xml"));
            return sqlSessionFactoryBean.getObject();
        }

        @Bean
        public PlatformTransactionManager platformTransactionManager() {
            return new DataSourceTransactionManager(myRoutingDataSource);
        }
    }
    ```
8. aop拦截方法，哪些使用从库，哪些使用主库
    ```
    @Aspect
    @Component
    public class DataSourceAop {
        /*从库的切点,没有标注Master注解，并且方法名为select和get开头的方法，走从库*/
        @Pointcut("!@annotation(cn.enjoyedu.rwseparation.annotation.Master) " +
                "&& (execution(* cn.enjoyedu.rwseparation.service..*.select*(..)) " +
                "|| execution(* cn.enjoyedu.rwseparation.service..*.get*(..))" +
                "|| execution(* cn.enjoyedu.rwseparation.service..*.find*(..))" +
                "|| execution(* cn.enjoyedu.rwseparation.service..*.query*(..)))")
        public void slavePointcut() {

        }

        /*主库的切点,或者标注了Master注解或者方法名为insert、update等开头的方法，走主库*/
        @Pointcut("@annotation(cn.enjoyedu.rwseparation.annotation.Master) " +
                "|| execution(* cn.enjoyedu.rwseparation.service..*.insert*(..)) " +
                "|| execution(* cn.enjoyedu.rwseparation.service..*.add*(..)) " +
                "|| execution(* cn.enjoyedu.rwseparation.service..*.update*(..)) " +
                "|| execution(* cn.enjoyedu.rwseparation.service..*.edit*(..)) " +
                "|| execution(* cn.enjoyedu.rwseparation.service..*.delete*(..)) " +
                "|| execution(* cn.enjoyedu.rwseparation.service..*.remove*(..))")
        public void masterPointcut() {
        }

        @Before("slavePointcut()")
        public void slave() {
            DBContextHolder.slave();
        }

        @Before("masterPointcut()")
        public void master() {
            DBContextHolder.master();
        }
    }

    //一个声明的master注解，强制使用主库
    public @interface Master {

    }
    ```

### Keepalived和HAProxy

1. Keepalived
    - 定义，它工作在IP、TCP、应用层，用以检测、剔除、增加相对应的服务器
    - 工作流程
        - IP层定期发送ping命令，没有响应则剔除
        - TCP层检测应用的端口是否启动，没有则剔除
        - 应用层对多个url进行get访问，通过统计算法和预测的不符合就剔除
        - 当剔除的服务器恢复正常后，自动增加到服务器集群
        - Keepalived有一台主服务器和多台备份服务器，他们是相同的服务配置，使用虚拟ip对外提供服务，当主服务器宕机后，虚拟ip自动漂浮到备份服务器继续提供服务
    - 优点，可以实现动态横向扩展

2. HAProxy
    - 定义，类似nginx通过后端进行负载均衡
    - HAProxy处理了负载均衡的，引来了HAProxy自己的单点问题
3. Keepalived+Keepalived可以解决HAProxy的单点问题，然后HAProxy再对mysql进行负载均衡处理