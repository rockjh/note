### 概述

1. 什么样的数据适合缓存
    - 访问频率高
    - 读多写少
    - 一致性要求低
2. 缓存追求的目标
    - 命中率，访问多少次，有多少次使用的缓存，有多少次查询了DB
    - 绝对过期，例如10分钟有效期
    - 滑动过期，例如30分钟没有访问session就过期
3. 常规代码实现逻辑
    - 开始--->从缓存中获取数据
    - 是否命中缓存，命中缓存--->返回获取数据，未命中缓存--->DB读取数据
    - 将DB获取的数据写入缓存，返回


### springboot-cache

1. 引入jar包，spring-cache
    ```
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-cache</artifactId>
    </dependency>
    ```
2. 配置spring-cache
    ```
    @Configuration
    @EnableCaching
    public class CacheConfig extends CachingConfigurerSupport {

        //key的生成，springcache的内容，跟具体实现缓存器无关
        @Bean
        public KeyGenerator keyGenerator() {
            return new KeyGenerator() {
                //执行对象、执行方法、方法参数
                @Override
                public Object generate(Object target, Method method, Object... params) {

                    StringBuilder sb = new StringBuilder();
                    sb.append(target.getClass().getSimpleName());
                    sb.append(method.getName());
                    for (Object obj : params) {
                        sb.append(obj.toString());
                    }
                    return sb.toString();
                }
            };
        }

        //开启一个CacheManager
        //有SimpleCacheManager,基于内存Map存放的，不会有过期时间
        //有RedisCacheManager，基于redis的缓存
        @Bean
        public CacheManager cacheManager(RedisConnectionFactory connectionFactory) {
            return RedisCacheManager
                    .builder(connectionFactory)
                    .cacheDefaults(
                            RedisCacheConfiguration.defaultCacheConfig()
                            .entryTtl(Duration.ofSeconds(20))) //缓存时间绝对过期时间20s
                    .transactionAware()
                    .build();
        }

        //声明一个RedisTemplate
        @Bean
        public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
            RedisTemplate<String, Object> template = new RedisTemplate<>();
            template.setConnectionFactory(factory);

            Jackson2JsonRedisSerializer serializer = new Jackson2JsonRedisSerializer(Object.class);
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
            mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
            serializer.setObjectMapper(mapper);
            template.setValueSerializer(serializer);
            //使用StringRedisSerializer来序列化和反序列化redis的key值
            template.setKeySerializer(new StringRedisSerializer());
            template.afterPropertiesSet();
            return template;
        }
    }
    ```
3. 基本的标签使用方法
    - @Cacheable，在存放缓存的方法上，表明将返回值放进缓存，redis的实现下，不存在xxx缓存名则创建，本地内存实现下，不存在则失效
    - @CachePut，数据增改时，在方法上用于更新缓存
    - @CacheEvict，数据删除时，在方法上用于删除缓存
    - @Caching，组合前面的三个注解
    - @CacheConfig，在类上使用，全局声明该类下的cacheNames、keyGenerator、cacheManager、cacheResolver
    ```
    //myCache是一个缓存器，就是根缓存名
    //#userName是一个SpEl表达式，userName是方法的一个参数
    //#username.length()>2是SpEl表达式
    //存储是redis中的map结构，这里的key可以是前面的配置自动生成
    @Cacheable(value = "myCache",key = "#userName",condition = "#username.length()>2")
    ```
4. SpEl
    - methodName，当前被调用的方法名，#root.methodName
    - method，当前被调用的方法，#root.method.name
    - target，当前被调用目标实例，#root.target
    - targetClass，当前被调用的类，#root.targetClass
    - args，当前被调用的方法参数，#root.args[0]
    - caches，当前方法调用使用的缓存列表，#root.caches[0].name
    - ArgumentName，当前被调用的方法的参数，#xxx.id（xxx代表参数，表明参数的id）
    - result，方法执行后的返回值，#result

### 缓存过期一致性

1. 数据实时同步更新
    - 强一致性，更新数据库同时更新缓存
    - 缺点，代码耦合、运行期耦合
    - 适用一致实时性要求较高的场景，银行业务、证券交易
2. 数据准实时更新
    - 准一致性，更新数据库后，异步更新缓存
    - 缺点，较短延迟需要补偿机制
    - 不适合写操作频繁并且数据一致实时性要求严格的场景
3. 缓存失效机制
    - 弱一致性，缓存有效期到了之后，再去重新取数据
    - 缺点，有一定延迟，不保证强一致性，存在缓存雪崩问题
    - 适合读多写少的场景，能接受一定数据延时
4. 任务调度更新
    - 最终一致性，采用任务调度框架，按照一定频率更新
    - 不保证一致性，依赖定时任务，容易堆积垃圾数据
    - 适合复杂统计类数据缓存更新，对数据一致实时性要求低的场景；如：统计类报表数据，BI分析等

