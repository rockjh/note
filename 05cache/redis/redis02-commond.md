### 基本命令
1. redis-cli连接命令
    ```
    #127.0.0.1:6379，如果没有密码省略-a
    redis-cli -h 127.0.0.1 -p 6379 -a "mypass"
    ```
2. redis-cli乱码
    ```
    #可以避免中文乱码
    redis-cli --raw
    ```
### key 命令
-  del key删除key
-  exists key检查给定key是否存在
-  expire key seconds为给定key设置过期时间，秒计算
-  expireat key timestamp为给定key设置过期时间点
-  keys pattern查找所有符合给定模式的key，锁整个redis表，有数百万的数据就要考虑性能的问题
-  persist key移除key的过期时间，key将永久保存
-  ttl key以秒为单位，返回给定key的剩余时间
-  rename key newkey 修改key的名称
-  type key返回key所存储的值的类型

### string
- mget key1 key2获取所有给定key的值
- mset key1 value1 key2 value2设置多个key和value
- setnx key value当key不存在时设置key的值
- msetnx key1 value1 key2 value2当key1、key2不存在时，设置值
- append key value将value追加到key的值后面
- strlen key返回key所存储的字符串长度

### hash
- hdel key field1 field2删除一个或者多个字段
- hexistis key field查看哈希表中是否存在field
- hgetall key获取在哈希表中指定key的所有field和value
- hkeys key获取哈希表中的所有field
- hlen key获取哈希表中的字段
- hmget key field1 field2获取哈希表中field1、field2的值
- hsetnx key field value当哈希表中不存在field字段时，设置哈希表中的字段field值为value
- havls key获取哈希表中所有值

### list
- blpop key1 key2 timeout移除并获取列表的第一个元素，如果列表没有元素会阻塞直到超时
- brpop key1 key2 timeout移除并获取列表的最后一个元素，如果列表没有元素会阻塞直到超时
- lindex key index通过索引获取列表中的元素
- llen key获取元素的长度
- lpop key移除并获取第一个元素
- rpop key移除并获取最后一个元素
- rpush key value1 value2在队列尾部添加值
- lpush key value1 value2在队列头部插入值

### set
- scard key获取集合的成员数量
- sdiff key1 key2返回给定所有集合的差集
- sinter key1 key2返回给定所有集合的交集
- sunion key1 key2返回所有给定集合的并集
- sismember key member判断member是否在集合中
- smembers key返回集合的所有成员
- spop移除并返回集合中的一个随机元素
- srem key member1 member2移除集合中的一个或多个元素

### zset(sore set)
- zcard key获取有序集合的成员数量
- zcount key min max计算在集合中，指定区间分数的成员数
- zscore key member返回集合中成员的分数
- zrevrank key member返回有序集合中指定成员排名，有序集合成员按分数值递减排序

### 事务

- multi表示一个事务块的开始
- exec执行所有事务块内的命令
- discard取消事务，放弃执行事务块内的所有命令
- Redis中，单条命令是原子性执行的，但事务不保证原子性，且没有回滚。事务中任意命令执行失败，其余的命令仍会被执行
- unwatch取消watch命令对所有key的监视
- watch监视一个或者多个key，如果在事务执行之前这些key被其他命令所改动，那么事务将会被打断

