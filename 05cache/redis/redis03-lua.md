### 简介

lua是一个小巧的脚本语言，由标准C编写而成，代码简洁优美，几乎在所有操作系统和平台上都可以编译，运行。
一个完整的lua解释器不过200k，在目前所有脚本引擎中，lua的速度是最快的

lua语言里的操作是原子性的

### 数据类型

- nil空类型，只包含一个值nil，所有没有赋值的变量或表的字段都是nil
- boolean布尔型，包含true和false
- number整数和浮点数都是使用数字类型存储
- string字符串类型可以存储字符串、转义字符，可以使用单引号或者双引号表示
- table表类型是lua语言中唯一的数据结构，既可以当数组又可以当字典，十分灵活
- function函数在lua中是一等值，可以存储在变量中，作为函数的参数或返回结果

### 变量

变量名必须是非数字开头，只能包含字母、数字、下划线、区分大小写、不能和lua保留关键字相同，保留关键字如下

```
and break do else elseif
end false for function if 
in local nil not or
repeat return then true until while
```
其他规则如下

1. lua的变量分为全局变量和局部变量，全局变量无需声明可以直接使用，默认为nil，和js类似
2. 在redis脚本中不能使用全局变量，只允许使用局部变量，采用local关键字进行声明
3. 局部变量的作用域为从声明开始到所在层的语句块末尾

### 注释

- 单行注释以“--”开始，一般习惯在"--"后跟上一个空格
- 多行注释以"--[["开始，"]]"结束

### 赋值

lua支持多重赋值，比如
- local a,b = 1,2  -- a=1,b=2
- local c,d = 1,2,2 --c=1,d=2,值3被舍弃
- local e,f = 1 -- e=1,f=nil

> 再执行多重赋值时，lua会先计算所有表达式的值

### 操作符

1. 数字操作符，+、-、*、/、%、-、^（加、减、乘、除、取模、取负、幂运算）
2. 比较运算符，==、~=、<、>、<=、>=（类型和值都相等、不等、小于、大于、小于等于、大于等于）
3. 逻辑操作符，not、and、or（只要操作数不是nil或者false，逻辑操作符就认为操作符为真，否则是假，特别需要注意的是即使是0或者空字符串也被当做是真，lua逻辑操作符支持短路）
4. 连接操作符，..（用来连接两个字符串，会把数字类型转为字符串类型）
5. 取长度操作符，#（例如print(#'hello')打印5）

运算符的优先级

```
^
not # - (一元)
* / % 
+ - 
.. 
< > <= >= ~= == 
and 
or
```

### 语法

#### if

```
if 条件表达式 then
    语句块
elseif 条件表达式 then
    语句块
else 语句块
end

if redis.call('exists','key')==1 then 
   exists = true 
else 
    exists = false
end
```
> 在Lua中只有nil和false才是假，其余值都是真，包括空字符串和0，都是认为是真的。这是一点容易出错的地方

#### 循环语句

while

```
while 条件表达式 do
    语句块
end
```

repeat

```
repeat 语句块 until 条件表达式
```

for

```
for 变量 = 初值 ，终值，步长 do
    语句块
end

for i=1 ,100 do
    sum = sum+1 
end
```

#### 表类型

1. 定义
    ```
    -- 赋值对象表
    local a = {} -- 将变量a赋值为一个空表
    a['field'] = 'value'
    print(a.field)

    local people = {name = 'jiangh',age = 19}

    -- 赋值为数组
    local a = {}
    a[1] = 'value'

    local people = {'jiangh','liuj'}
    ```
2. 遍历

    ```
    -- 通用形式的for语句遍历数组
    -- ipairs会遍历所有值不为nil的索引
    for index,value in ipairs(a) do 
        print(index)
        print(value)
    end

    -- 使用数字形式的for语句遍历数组
    -- #a作用是获取表a的长度
    for i =1 ,#a do 
        print(i)
        print(a[i])
    end

    -- 迭代器pairs用来遍历非数组的表值
    -- pairs会从索引1开始递增遍历到最后一个值不为nil的整数索引
    people = {name = 'zhouy',age = 29}
    for index, value in pairs(people) do 
        print(index)
        print(value)
    end
    ```

#### 函数

1. 定义

    ```
    function(参数列表)
        函数体
    end

    local square = function(num)
        return num*num
    end
    ```
### 其他库

1. 获取字符串长度string.len('value')
2. 转换大小写string.lower('value')，string.upper('value')
3. 获取子字符串string.sub('value',start,end)索引从1开始，-1代表最后一个元素，end参数如果省略，默认-1
4. 将数组转为字符串table.concat(tab表，分隔符，起始位置1位为第一个，结束位置)，例如
    ```
    -- 'b-c'
    table.concat({'a','b','c','d'},'-',2,3)
    ```
5. 向数组中插入元素table.insert(table,[post,] value)，向指定索引位置pos插入元素value，并将后面的元素顺序后移，默认pos的值是数组长度加1，即在数组尾部插入,注意索引从1开始的
6. 从数组中弹出一个元素，table.remove(table [,pos])，从指定索引位置删除元素，并将后面的元素前移，返回删除元素的值
7. cjson.encode，序列化为字符串
8. cjson.decode，将序列化后的字符串还原为表
9. cmsgpack.unpack将序列化后的字符串还原为表

### 执行

redis支持两种方法运行脚本，一种是直接输入一些lua语言的程序代码，另一种是将lua语言编写成文件

1. 直接使用eval执行lua程序代码

    命令格式为
    ```
    eval lua-script key-num [key1 key2 key3 ....] [value1 value2 value3 ....]

    eval "redis.call('set',KEYS[1],ARGV[1])" 1 lua-key lua-value
    ```
    - eval代表执行Lua语言的命令
    - lua-script代表lua语言脚本
    - key-num表示参数中有多少个key，需要注意的是key是从1开始的，如果没有key的参数就写0
    - [key1 key2...]是作为参数传递给lua语言，也可以不填，但是需要和key-num的个数对应起来，针对KEYS[1]，KEYS[2]...
    - [value1 value2...]这些参数传递给lua语言，他们是可填可不填的，针对ARGV[1]，ARGV[2]...

2. 执行lua文件，后缀名为.lua

    test.lua
    ```
    redis.call('set', KEYS[1], ARGV[1])
    redis.call('set', KEYS[2], ARGV[2])
    local n1 = tonumber(redis.call('get', KEYS[1]))
    local n2 = tonumber(redis.call('get', KEYS[2]))
    if n1 > n2 then
        return 1
    end
    if n1 == n2 then 
        return 0
    end
    if n1 < n2 then
        return 2
    end
    ```
    执行命令
    ```
    -- 注意逗号的左右两边的都有一个空格
    redis-cli --eval test.lua key1 key2 , 2 4
    ```
    在java中无法这样这样执行，可以考虑使用evalsha命令，evalsha可以缓存脚本，并返回32位的sha1标识，这样可以提高传输性能，减少很多的网络传输。直接执行lua代码也可以使用这种方式

    使用evalsha，我们可以将sha1标识字符串保存起来，就可以通过标识反复执行该脚本，只需要传递sha1标识和参数即可

### 对lua的管理

1. 清空脚本，script flush
2. 判断脚本是否存储script exists
3. 杀掉正在执行的脚本script kill

### 限流

可以使用redis进行记录，为每个访问的ip记录访问次数，限定每秒只能访问两次，直接写一个lua脚本执行就行，设置一个hash，存储当前ip是否在这一秒内访问了多少次，超过次数限制访问，该键1秒后过期清除。一般在过滤器中添加该功能