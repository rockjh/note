### 持久化

#### 存储路径

- redis.conf中配置
    ```
    #默认当前路径
    dir ./
    ```
- redis-cli登录后，可以通过config get dir查看

#### rdb（默认）

1. rdb做镜像全量持久化，它分为save和bgsave两种方式
2. save是阻塞式的rdb持久化，当执行这个命令的时候，redis的主进程把内存里的数据库状态写入到rdb文件中，备份完成之前redis不处理任何请求命令
3. bgsave属于非阻塞式的rdb持久化，他会创建一个子进程专门去把内存中的数据库状态写入到rdb文件中，同时主进程可以处理来自客户端的请求。子进程基本都是复制的主进程，相当于两个相同大小的redis进程在系统中运行，会造成内存使用率大大增加
4. 在执行redis-cli shutdown关闭redis服务时，如果没有开启AOF持久化，自动执行bgsave
5. 优点，压缩后的二进制文件，适用于备份、全量复制，用于灾难恢复，加载rdb恢复数据远快于aof方式
6. 缺点无法做到实时持久化，每次都要创建子进程，频繁操作成本过高，保存后的二进制文件，存在老版本不兼容新版本rdb文件的问题
7. 关闭rdb的方法（默认开启），在redis.conf中
    ```
    #这一段注释掉
    save 900 1 #表示900 秒内如果至少有 1 个 key 的值变化，则保存
    save 300 10 #表示300 秒内如果至少有 10 个 key 的值变化，则保存
    save 60 10000 #表示60 秒内如果至少有 10000 个 key 的值变化，则保存

    #这一段放开
    save ""
    ```
> bgsave满足条件后，会在后台自动运行，也可以直接敲命令客户端运行
#### aof

1. redis开启aof后，服务端每执行一个操作就会把该命令追加到一个单独的aof缓冲区的末尾，存储的是resp协议内容，在redis.conf中开启
    ```
    #默认关闭
    appendonly yes 
    ```
2. 然后把aof缓冲区的内容写入到一个内存空间，redis.conf中可配置在适当的时机同步aof文件
    ```
    # appendfsync always #总是同步
    appendfsync everysec #默认每秒
    # appendfsync no #由操作系统决定
3. 持久化流程
    - 所有的写入命令(set hset)会append追加到aof_buf缓冲区中
    - AOF缓冲区向硬盘做sync同步
    - 随着AOF文件越来越大，需定期对AOF文件rewrite重写，达到压缩
    - 当redis服务重启，可load加载AOF文件进行恢复
#### 恢复数据

1. 当aof和rdb同时存在时，优先加载aof
2. 若关闭了aof，加载rdb
3. 加载aof/rdb成功，redis重启成功
4. aof/rdb存在错误，启动失败打印错误信息

### 管道技术

1. redis是一种基于客户端-服务端模型以及请求/响应协议的TCP服务，这意味着通常情况下一个请求会遵循以下步骤
    - 客户端向服务端发送一个查询请求，并监听socket返回，通常以阻塞模式，等待服务端响应
    - 服务端处理命令，并将结果返回给客户端
2. redis管道可以将多个命令打包，一次性发送给服务端，当命令之间不存在依赖关系时，相比于一条命令一次请求的普通操作方式，管道的操作对使用者几乎透明
3. 例如如下使用java操作管道
    ```
    Jedis jedis = new Jedis("127.0.0.1", 6379);  
    Pipeline pipeline = jedis.pipelined();  
    for (int i = 0; i < 1000; i++) {  
        pipeline.incr("test2");  
    }  
    pipeline.sync();  
    jedis.disconnect(); 
    ```
4. 通过pipeline方式当有大量操作的时候，我们可以节省很多原来浪费在网络延迟的时间，需要注意的是用pipeline方式打包命令发送，redis必须在处理完所有命令前，先缓存起所有命令的处理结果，打包的命令越多，消耗的内存也越多并且增加了客户端的等待时间，可能造成网络阻塞，因此打包的命令要适度
5. pipeline不是原子性的，它主要是节省网络开销

### redis内部实现

redis内部是通过serverSocket监听端口，下面仿写一个redis-server

```
//打印客户端传送过来的数据
public static void main(String[] args) {
    try {
        ServerSocket serverSocket = new ServerSocket(6379);
        Socket socket = serverSocket.accept();
        byte[] messageByte = new byte[1024];
        socket.getInputStream().read(messageByte);
        System.out.println(new String(messageByte));
    } catch (IOException e) {
        e.printStackTrace();
    }
}
```

创建一个jedis的客户端连接刚刚的客户端
```
public static void main(String[] args) {
    Jedis jedis = new Jedis("127.0.0.1",6379);
    jedis.set("jiangh","boy");
    jedis.close();
}
```

服务端会打印设置的值，这个就是jedis发送给服务端的协议，resp协议
```
*3      //表示有3组数据
$3      //表示下面数据有3个字符
SET
$6
jiangh
$3
boy
```

### jedis

1. redis通过监听一个tcp端口或者unix socket的方式来接收来自客户端的连接当一个连接建立之后，redis内部会进行以下一些操作
    
    - 客户端的socket会被设置为非阻塞模式，因为redis在网络事件处理上采用的是非阻塞多路复用模型
    - 为这个socket设置TCP_NODELAY属性，禁用nagle算法
    - 创建一个可读的文件事件，事件用于监听这个客户端socket的数据发送