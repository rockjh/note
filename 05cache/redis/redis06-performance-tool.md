### 慢查询

与mysql一样，当执行时间超过阀值，会将发生时间、耗时、命令记录

#### 慢查询阈值

1. 在redis.conf中找到slowlog-log-slower-than 10000设置为10毫秒，执行时间超过10毫秒的将会被列为慢查询，记录到慢查询列表中，方便后续优化
    > slowlog-log-slower-than值为0记录所有命令 -1命令都不记录
2. 在redis.conf中slow-max-len是存放慢查询列表的长度，一般生产环境设置为1000以上，当慢查询过多超过了存放列表，会按照时间丢弃最开头的一个慢查询
    > 可以定期的对慢查询列表导出进行永久存储
3. redis.conf中的属性都可以被动态的修改，比如
    ```
    config set slowlog-log-slower-than 10000
    config set slow-max-len 1000
    #将配置持久化保存到redis.conf
    config rewrite
    ```

#### 慢查询命令

1. slowlog get，获取队列中慢查询信息
2. slowlog len，获取慢查询列表当前的长度
3. slowlog reset，慢查询清理

### redis-cli

1. redis-cli -r 3 -a 12345678 ping，返回pong表示地址正常能通，-r表示ping的次数
2. redis-cli -r 100 -i 1 info |grep used_memory_human，每秒输出内存使用量,输100次
3. redis-cli -h 127.0.0.1 -p 6379 -a 12345678，连接命令

### redis-server

1. redis-server ./redis.conf &，指定配置文件启动，&符号表示即使当前端口关闭，程序也会在后台运行
2. redis-server --test-memory 1024，检测操作系统能否提供1G内存给redis, 常用于测试，想快速占满机器内存做极端条件的测试，可使用这个指令

### redis-benchmark

1. redis的基本测试命令格式和参数含义如下

    ```
    redis-benchmark [option] [option value]
    ```   
    - -h指定服务器主机名
    - -p指定服务器端口
    - -s指定服务器的socket
    - -c指定并发连接数
    - -n指定请求数
    - -P通过管道传输<numreq>请求
    - -q强制退出redis，仅显示query/sec值
    - --csv以csv格式输出
    - -l生成循环永久进行测试
    - -t仅运行测试以逗号分隔的命令列表

2. redis-benchmark -h 192.168.42.111 -p 6379 -c 100 -n 100000，100个并发连接，100000个请求，检测host为localhost 端口为6379的redis服务器性能
3. redis-benchmark -h 192.168.42.111 -p 6379 -q -d 100，测试存取大小为100字节的数据包的性能
4. redis-benchmark -t set,lpush -n 100000 -q，只测试set,lpush操作的性能
5. redis-benchmark -n 100000 -q script load "redis.call('set','foo','bar')"，只测试某些数值存取的性能