## 主从复制

### 搭建环境

1. 使用ubuntu下载redis进行
    ```
    wget http://download.redis.io/releases/redis-5.0.5.tar.gz
    ```
2. 解压
    ```
    tar xzf redis-5.0.5.tar.gz
    ```
3. 进入redis目录生成
    ```
    sudo make失败则 使用 sudo make MALLOC=libc后再sudo make
    ```
4. 进入redis的src目录进行测试
    ```
    sudo make test #这段运行时间会较长
    ```
5. 在src目录将redis的命令安装到/usr/bin/目录
    ```
    sudo make install
    ```
6. 配置
    ```
    #注释掉这一行，让其他机器可以连接该服务端
    bind 127.0.0.1 

    #添加密码为123456
    requirepass 123456

    ###复制三个redis.conf文件
    #redis6379.conf #主数据库c
    #redis6380.conf #从数据库
    #redis6381.conf #从数据库

    #配置两个从数据库
    slaveof 10.211.55.8 6379 #声明属于哪个的从数据库
    masterauth 123456 #主数据库的认证信息
    slave-read-only yes #从数据库只读，预防数据不一致
    ```
7. 启动
    ```
    #先启动主数据库
    #如果县启动从数据库，从数据库先去同步数据失败，会导致启动失败
    redis-server redis6379.conf

    #再启动从数据库
    redis-server redis6380.conf
    redis-server redis6381.conf
    ```
8. 测试
    ```
    #连接主数据库
    redis-cli -h 10.211.55.8 -p 6379 -a 123456
    set name jianghang #设置值

    #登录从数据库，get name可以获取到数据
    ```
### 其他命令配置

1. redis.conf中配置复制存在网络延迟
    - 关闭时，无论大小都会及时发布到从节点，占带宽，适用于主从网络好的场景
    - 开启时，主节点合并所有数据成TCP包节省带宽，默认为40毫秒发一次，取决于内核，主从的同步延迟40毫秒，适用于网络环境复杂或带宽紧张，如跨机房
    ```
    #默认关闭
    repl-disable-tcp-nodelay no
    ```
2. 查看节点状态
    ```
    info replication
    ```
3. 主从有长连接心跳，主节点默认每10S向从节点发ping命令，repl-ping-slave-period控制发送频率
4. 数据同步使用psync完成，分为全量复制和部分复制，全量一般用于第一建立，部分是在网络出现问题时，从节点再次连接主节点，主节点补发缺少数据
### 复制原理

1. 从节点保存主节点信息
2. 主从建立socket连接
3. 发送ping命令
4. 权限验证
5. 同步数据集
6. 命令持续复制

> 主从复制不适用于生产环境，因为master出现问题后，需要人工的去重新配置，重启，做不到自动处理问题，主从复制主要是为后面的哨兵，集群做铺垫

## 哨兵机制

实现多个服务，一个服务挂了，可以自动切换到另外一个服务上，不影响客户体验，也就是高可用

### 搭建环境

1. 启动主从复制中的三个redis服务器
2. 再配置启动3个sentinel（哨兵）
    ```
    #redis目录中sentinel.conf添加一下配置
    #sentinel monitor <master-name> <ip> <redis-port> <quorum>
    sentinel monitor mymaster 192.168.42.111 6379 2  #监听主节点6379
    sentinel auth-pass mymaster 12345678 #连接主节点时的密码
    #通过设置不同master-name可以一个哨兵监控多个主节点

    #复制三个文件分别为，分别修改端口
    #sentinel26379.conf
    #sentinel26380.conf
    #sentinel26381.conf

    #启动哨兵
    redis-sentinel sentinel26379.conf
    ```
### 其他命令

1. 执行故障转移时， 最多可以有多少个从节点同时对新的主节点进行数据同步
    ```
    sentinel config-epoch mymaster 2
    ```
2. 故障转移超时时间
    
    - 如果转移超时失败，下次转移时时间为之前的2倍
    - 从节点变主节点时，从节点执行slaveof no one命令一直失败的话，当时间超过180S时，则故障转移失败
    - 从节点复制新主节点时间超过180S转移失败

    ```
    #180s
    sentinel failover-timeout mymaster 180000
    ```
3. sentinel节点定期向主节点ping命令
    ```
    sentinel down-after-milliseconds mymaster 300000
    ```

### 工作流程

1. 哨兵对redis服务的监控方式
    - 每隔10秒发送一次info
    - 每隔2秒发送一次publish/subscribe
    - 每隔1秒发送一个ping
2. 当一个其中一个哨兵每隔1秒发一次ping超过down-after-milliseconds无回复，该哨兵就主观的认为这个redis服务器下线了，这时候不会发生故障转移
3. 当有quorum（配置）个哨兵都认为某个redis服务器下线了，就叫做客观下线，发生故障转移
4. 处理故障转移，哨兵A发现的主观下线，哨兵A称为领导者，处理故障转移
    - 哨兵A根据之前的监控找出最适合做主节点的从节点，执行slaveof no one解除从节点身份，变为新的master
    - 其他从节点变成新master的从节点
    - 若原主节点恢复，也变成新主节点的从节点
    - 哨兵A通知应用程序新的主节点地址

### 部署建议

1. sentinel节点应部署在多台物理机（线上环境）
2. 至少三个且奇数个sentinel节点
3. 3个sentinel可同时监控一个主节点或多个主节点，当监听N个主节点较多时，如果sentinel出现异常，会对多个主节点有影响，同时还会造成sentinel节点产生过多的网络连接，一般线上建议还是， 3个sentinel监听一个主节点

### 实战

springboot中，直接引用redis模块，配置哨兵的地址在redisTemplate的Bean中，可以实现这些功能

## 分布式集群

哨兵能够实现高可用，但实际上也只是一个单点存储，需要考虑存储空间不足，扩容问题，就需要分布式存储，就会用到分布式集群

RedisCluster是分布式解决方案，当遇到单机内存，并发等瓶颈时可考虑

### 搭建环境

1. 至少配置3主3从的集群redis服务器
    ```
    #集群配置redis.conf
    cluster-enabled yes #开启集群模式
    cluster-node-timeout 15000 #节点超时时间
    cluster-config-file /opt/redis/clustercon/nodes-6379.conf #集群内部配置文件，相互之间的通信
    
    #复制6个配置文件，分别设置不同的端口，从数据库配置主数据库密码
    6379 #主
    6389 #从

    6380 #主
    6390 #从

    6381 #主
    6391 #从
    ```
2. redis5.0以后（所有redis数据库没有数据，持久化路径没有对应的备份文件，cluster-config-file配置的路径下，没有集群配置文件），在启动了上述所有的redis服务器后
    ```
    #-a是需要认证的信息
    #--cluster-replicas后面跟的是每一个主数据库跟的从数据库个数
    #配置是先跟主数据库，然后再顺序跟从数据库
    redis-cli --cluster create 10.211.55.8:6379 10.211.55.8:6380 10.211.55.8:6381 10.211.55.8:6389 10.211.55.8:6390 10.211.55.8:6391 --cluster-replicas 1 -a 123456
    ```
3. 连接
    ```
    #-c是启动集群模式，设置值的时候可以根据槽数，自动跳转对应redis服务器
    redis-cli -c -h 10.211.55.8 -p 6379 -a 123456
    ```
4. 测试，杀死6380端口的redis，稍等一段时间可以自动将6390转为主数据库
5. 加一个主从配置redis服务器
    ```
    配置文件同上
    6382 #主
    6392 #从
    
    #加入主数据库
    cluster meet 10.211.55.8 6382 #执行，将节点添加到集群
    redis-cli --cluster reshard 10.211.55.8:6382 #重新分槽
    #迁移多少个槽，根据已有的服务器n决定16384/n
    #选择接收方的nodeId，通过cluster nodes找到刚刚添加的节点的nodeId
    #选择迁出方，一般都是从所有节点部分迁出，输入all
    #确认信息，输入yes

    #加入对应的从数据库
    #输入命令，将当前从数据库添加到主数据库
    redis-cli --cluster add-node 10.211.55.8:6392 10.211.55.8:6381 --cluster-slave --cluster-master-id 50fd86522dbcb02d497b15328253e8f62c9fa6a1 -a 123456
    ```
6. 移除节点
    ```
    #先下线从节点
    redis-cli --cluster del-node 10.211.55.8:6392 44e7e807fd87e323c37df2977fa790c10d1b6a0d -a 123456

    #在下线主节点，因为主节点涉及到槽，需要先做数据迁移，然后在下线
    #将节点中的几个槽，平均分配给剩余的三个主节点
    redis-cli --cluster reshard 10.211.55.8:6379 #和添加节点类似
    redis-cli --cluster del-node 10.211.55.8:6382 50fd86522dbcb02d497b15328253e8f62c9fa6a1 #删除节点
    ```

### 其他命令

- cluster nodes查看集群的各个节点状态
- cluster keyslot key，查看key对应的slot
- cluster slots，查看slot和节点的对应关系

### 工作流程

1. 分布式数据库是把整个数据按分区规则映射到多个节点，即把数据划分到多个节点上，每个节点负责处理整体数据的一个子集
2. 分区规则一般有顺序分区和哈希分区，redis采用哈希分区
3. redisCluster采用了哈希分区的虚拟槽分区方式
4. 所有的键根据哈希函数(CRC16[key]&16383)映射到0－16383槽内，共16384个槽位，每个节点维护部分槽及槽所映射的键值数据
5. 各个节点之间采用Gossip协议进行通信，Gossip协议就是指节点彼此之间不断通信交换信息
    - meet消息，用于通知新节点加入，消息发送者通知接收者加入到当前集群，meet消息通信完后，接收节点会加入到集群中，并进行周期性ping pong交换
    - ping消息，集群内交换最频繁的消息，集群内每个节点每秒向其它节点发ping消息，用于检测节点是在在线和状态信息，ping消息发送封装自身节点和其他节点的状态数据
    - pong消息，当接收到ping meet消息时，作为响应消息返回给发送方，用来确认正常通信，pong消息也封闭了自身状态数据
    - fail消息，当节点判定集群内的另一节点下线时，会向集群内广播一个fail消息
6. redisCluster的路由
    - 发送键命令
    - 计算槽对应的节点
    - 如果是指向自身，执行命令
    - 如果不是执行自身，回复move
    - 重定向发送键命令

### 故障

1. 接收ping消息
2. 消息解析，包含其他pfail节点，主节点发送消息
3. 维护故障列表，尝试客观下线，是否大于槽节点总数的一半
4. 大于就更新为客观下线并且向集群广播下线节点的fail消息，否则该条pfail消息就忽略
5. 如果是主节点故障下线，需要在从节点中选择一个替换他，保证高可用
6. 资格检查、准备选举时间、发起选举、选举投票、替换主节点

### 缺陷

1. 键的操作支持有限，比如mget、mset等
2. 键的事务支持有限，在不同的节点无法使用事务
3. 键是数据分区的最小粒度，不能将一个很大的键值对映射到不同的节点
4. 不支持多数据库，只有0，select 0
5. 复制结构只支持单层结构，不支持树形结构

### 实战

springboot中，直接引用redis模块，配置集群模式的节点地址，然后实例化JedisCluster，通过JedisCluster来设置redis的键值对