### 定义

1. RPC（Remote Procedure Call）远程过程调用，调用远程计算机上的服务就像调用本地服务一样
2. 主要用于分布式、微服务、系统拆分后的场景

### RMI

1. 定义，是RPC的java版本，允许运行在一个java虚拟机的对象调用运行在另一个java虚拟机上的对象方法
2. 实现流程
    - 创建一个远程接口，并继承java.rmi.Remote接口
    - 实现远程接口，并且继承UnicastRemoteObject
    - 创建服务器程序，同时使用createRegistty方法注册远程调用对象
    - 创建客户端程序，通过Naming类的lookup方法来远程调用接口中的方法
3. 远程接口
    ```
    public interface IOrder extends Remote {

        void pay(String orderId);
    }
    ```
4. 远程接口实现类
    ```
    public class OrderImpl extends UnicastRemoteObject implements IOrder {


        protected OrderImpl() throws RemoteException {
            super();
        }

        @Override
        public String pay(String orderId) {
            return "支付成功，订单号："+orderId;
        }
    }
    ```
5. 服务器程序
    ```
    public static void main(String[] args) throws Exception{
        IOrder order = new OrderImpl();
        LocateRegistry.createRegistry(6666);
        Naming.bind("rmi://localhost:6666/order",order);
        System.out.println("服务器已经启动了");
    }
    ```
6. 客户端程序
    ```
    public static void main(String[] args) throws Exception {
        IOrder order = (IOrder) Naming.lookup("rmi://localhost:6666/order");
        System.out.println(order.pay("order-id-1"));
    }
    ```
7. RMI的缺点
    - 业务代码中耦合了框架代码，如必须实现什么接口、类
    - 在代码中指定具体绑定的开放端口和访问路径

### 手写RPC

1. rpc实现步骤
    - 服务端定义接口和服务实现类，并且注册服务
    - 服务端在注册了服务之后，会发起一个socket监听，当有连接到来时，根据传入的类、方法、参数直接执行方法，获得执行结果，然后返回
    - 客户端定义相同的服务接口，
    - 在客户端需要调用对应的服务接口时，对这个服务接口进行动态代理，获得动态代理的实现来作为这个服务接口的实例，然后调用对应的方法
    - 调用对应的方法后，会执行动态代理的invoke，invoke会通过socket远程调用提供服务，获得返回值
2. 服务端接口定义和服务实现类，注册服务
    ```
    Map<String,Class> serviceRegistry = new HashMap<>();
    //将class名和具体的class放入缓存
    serviceRegistry.put(className,clazz);
    ```
3. 在一个线程池中执行，监听一个socket地址，等待客户端远程调用
    ```
    ServerSocket serverSocket = new ServerSocket();
    serverSocket.bind(new InetSocketAddress(6666));
    Socket client = serverSocket.accept();
    ObjectInputStream inputStream = new ObjectInputStream(client.getInputStream());
    ObjectOutputStream outputStream;
    //接口名
    String serviceName = inputStream.readUTF();
    //方法名
    String methodName = inputStream.readUTF();
    //参数类型
    Class<?>[] paramTypes = (Class<?>[]) inputStream.readObject();
    //实际参数
    Object[] arguments = (Object[]) inputStream.readObject();
    //获取到实现类
    Class serviceClass = serviceRegistry.get(serviceName);
    //获取到方法
    Method method = serviceClass.getMethod(methodName, paramTypes);
    //使用反射机制调用具体的方法，获取到结果值
    Object result = method.invoke(serviceClass.newInstance(), arguments);
    outputStream = new ObjectOutputStream(client.getOutputStream());
    //将结果返回给客户端
    outputStream.writeObject(result);
    //关闭
    outputStream.close();
    inputStream.close();
    client.close();
    ```
4. 客户端实例化一个接口的动态代理
5. 动态代理的增强invoke方法
    ```
    Socket socket = new Socket();
    socket.connect(new InetSocketAddress(6666));
    ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
    //按照顺序写入类名，方法名，参数类型，参数名
    outputStream.writeUTF(serviceInterface.getName);
    outputStream.writeUTF(method.getName);
    outputStream.writeObject(method.getParameterTypes());
    outputStream.writeObject(args);
    //刷新缓存，立马发送数据
    outputStream.flush();

    ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
    //返回真实方法的执行结果
    return inputStream.readObject();
    ```
6. 手写rpc缺点，通讯效率低、采用的是BIO形式、序列化速度慢、服务的管理不到位