### 锁的本质

1. 锁解决的问题，资源数据会不一致
    > 线程安全类，无状态的资源（只有逻辑）、有状态的资源（他的状态变化是原子的，例如AtomicLong）
2. 锁要达成的目标，让资源使用起来，像原子性一样
3. 锁达成目标的手段，让使用者访问资源时，只能排队，一个一个地去访问资源
4. 同一个jvm可以通过工具协调资源像原子性一样
    - sychronized，java语言天生支持
    - lock，jdk有接口标准

### 分布式锁

1. 定义，在分布式环境中，只能利用各个系统都认可的三方服务来做见证人，以协调资源
    - db，mysql（性能差，容易出现死锁）
    - file，文件访问权（性能差，容易出现死锁）
    - redis，基于redis的setnx命令实现，并通过lua脚本保证解锁时对缓存操作序列的原子性
    - zk，基于zk的节点特性以及watch机制实现（推荐）
2. 要求
    - 目标，通过公证人来发出信号，来协调分布式的访问者，排队访问资源
    - 条件，任何一个能够提供（是/否）信号量的事物，都可以做公证人
    - 陷阱，发出锁信号量的动作必须是原子性的
3. 分布式锁的实现
    - mysql，利用一条sql语句执行成功还是失败，利用了数据的唯一索引，效率低、不能预防宕机
    - redis，利用设置的key是否存在，设置过期时间预防持有锁的线程宕机，删除是判定是否是自己加的锁
        ```
        //加锁，并且设置过期时间
        jedis.set(String key, String value, String ex, int time,String nx)

        //使用lua语言保证执行的原子性
        //错误解锁方式
        public void unlockWrong() {
            //获取redis的原始连接
            Jedis jedis = (Jedis) factory.getConnection().getNativeConnection();
            String uuid = jedis.get(KEY);//现在锁还是自己的

            //uuid与我的相等，证明这是我当初加上的锁
            if (null != uuid && uuid.equals(local.get())){//现在锁还是自己的
                //删锁
                jedis.del(KEY);
            }
        }
        ```
    - zk，详见zk