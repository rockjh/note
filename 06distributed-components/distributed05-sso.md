### 单机登录

1. oauth2与单点登陆的区别
    - oauth2，不同的企业之间的登陆，应用之间的信任度较低
    - 单点登陆，是同一企业的产品系列间的登陆，相互信任度较高
2. session-cookie
    - session-cookie出现的原因，http连接是无状态的连接，服务器无法识别哪些请求是哪些浏览器发送的
    - 为了标识请求的来源是哪个浏览器，引入了两种方式
        - 直接在url里加一个标识参数（对前端开发有侵入性），如: token
        - http请求时，自动携带浏览器的cookie和session（对前端开发无知觉）
        - cookie可以在一个请求中设置cookie，页面的请求会有set-ccokie字样，cookie存储在浏览器中，依赖域名
            - 顶级域名下cookie，会被二级以下的域名请求，自动携带
            - 二级域名的cookie，不能携带被其它域名下的请求携带
        - session，前端默认传递sessionid，后端通过request.getsession获得session，后端可以在session中设置属性值，存储在服务端
3. 浏览器标识在网络上的传输，是明文的，不安全的
    - 改用https来进行加密传输
    - https会在传输到网络之前对数据进行加密
    - 每个https都会有证书，在浏览器客户端加密，服务端解密，使用的对称加密算法

### session共享的sso

1. 应用环境，多个应用共用同一个顶级域名，sessionid被种在顶级域名的cookie里
2. 当请求到达服务器时，服务器直接解读cookie中的sessionid，然后通过sessionid到redis中查找到对应会话session对象
3. 后台判断请求是否已登陆，主要校验session对象中，是否存在登陆用户信息
4. 如果未登录，跳转到登录页面
5. 登录请求到达后端，校验密码成功后，需要给页面种cookie，然后将session登录信息放进redis
6. getSession为了request.getsession时，自动能拿到redis中共享的session，我们需要重写request的getsession方法（使用HttpServletRequestWrapper包装原request）

7. 拦截器请求代码如下
    ```
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        //包装request对象
        MyRequestWrapper myRequestWrapper = new MyRequestWrapper(request,redisTemplate);

        System.out.println("代码1");
        //如果未登陆，则拒绝请求，转向登陆页面
        String requestUrl = request.getServletPath();
        if (!"/toLogin".equals(requestUrl)//不是登陆页面
                && !requestUrl.startsWith("/login")//不是去登陆
                && !myRequestWrapper.isLogin()) {//不是登陆状态

                HttpServletResponse response = (HttpServletResponse)servletResponse;
                response.sendRedirect("http://login.dev.com:8090/toLogin?url="+request.getRequestURL().toString());

                return ;
        }

        try {
            filterChain.doFilter(myRequestWrapper,servletResponse);
        } finally {
            System.out.println("代码2");
            //提交session到redis
            myRequestWrapper.commitSession();
        }
    }
    ```
### cas的sso

1. 对于完全不同域名的系统，cookie是无法跨域名共享的
2. cas方案，直接启用一个专业的用来登陆的域名（比如：cas.com）来供所有的系统登陆。
3. 当业务系统（如b.com）被打开时，借助cas系统来登陆，过程如下
    - b.com打开时，发现自己未登陆，于是跳转到cas.com去登陆，跳转过去会带一个当前访问b.com的url参数
    - cas.com登陆页面被打开，用户输入帐户/密码登陆成功 
    - cas.com登陆成功，种cookie到cas.com域名下，把sessionid放入后台redis<ticket，sesssionid>，页面跳回b.com
    - b.com重新被打开，发现仍然是未登陆，但是有了一个ticket值
    - b.com用ticket值，到redis里查到sessionid，并做session同步，种cookie给自己，页面原地重跳
    - b.com打开自己页面，此时有了cookie，后台校验登陆状态，成功
4. cas.com的登陆页面被打开时，如果此时cas.com本来就是登陆状态的，则自动返回生成ticket给业务系统
5. 为提高安全性，ticket应该使用过即作废
6. 单点注销，sso认证中心一直监听全局会话的状态，一旦全局会话销毁，监听器将通知所有注册系统执行注销操作
