### 单机任务调度

1. 使用quartz，来调用定时任务，直接使用@Scheduled配置corn表达式
2. quartz实现思想
    - 调度线程是单线程（守护线程）不停轮循trigger触发器
    - 若触发器到达临界值，则根据触发器对应出它的job，去线程池里取一个线程来执行job
    - 则线程池全忙，则阻塞等待线程（线程池知识）
    > 实现思想，java底层，轮询线程守护的使用非常普遍，监听（网络端口、tomcat网关监听、mq队列）是一个线程在不停的轮询
3. corn表达式
    - 秒，0-59，特殊字符,-*/
    - 分，0-59，特殊字符,-*/
    - 时，0-23，特殊字符,-*/
    - 日，0-31（需要考虑月的天数），特殊字符,-*/?LWC
        - L表示最后一天
    - 月，1-12或者JAN-DEC，特殊字符,-*/
    - 星期，1-7或者SUN-SAT（1=SUN），特殊字符,-*/?LC#
        - L表示周六，6L表示该月的最后一个周五
    - 年（可选，留空），1970-2099，特殊字符,-*/
    - 公用特殊字符解释
        - ,表达多个的意思
        - -表达一个范围
        - *表示对应时间域的任意时间
        - /表示一个等步长序列，如在分钟字段中使用0/15，表示0,15,30和45秒
        - ?无意义的值

### 分布式任务调度

1. 面临的问题，对集群机器中配置的任务，只能运行一个
2. 解决方式
    - 当集群中所有机器都触发时，只有第一个得到锁，可以运行，后面的机器发现任务已运行，则自动取消运行
    - 触发器被布置多份，zk选举出一个leader有效，其它暂置为无效（暂时未实现）
3. 业界流行的解决方案框架
    - 利用mysql的实现方案
        - 实现思路，利用数据库自身提供的锁机制实现，要求数据库支持行级锁
        - 优点，实现简单，稳定可靠
        - 缺点，性能差，无法适应高并发场景，容易出现死锁的情况，无法优雅的实现阻塞式锁
    - 利用redis的实现方案
        - 实现思路，使用Setnx和lua脚本机制实现，保证对缓存操作序列的原子性
        - 优点，性能好
        - 缺点，实现相对较复杂，有出现死锁的可能性，无法优雅的实现阻塞式锁
    - 利用zookeeper的实现方案
        - 实现思路，基于zk的节点特性以及watch机制实现
        - 优点，性能好，稳定可靠性高，能较好的实现阻塞式锁
        - 缺点，实现相对复杂
    - Quartz
        - 实现思路，多节点部署，通过竞争数据库锁来保证只有一个节点执行任务
        - 优点，实现简单，支持高可用
        - 缺点，不支持分片技术，同一次任务不支持并发执行
    - Elastic-Job
        - 实现思路，当当提供的开源分布式调度工具，封装Quartz，使用Zookeeper协调任务
        - 优点，通过zookeeper来动态扩容，分片和弹性扩容性能好，业务量大的时候也能非常好的调度
        - 缺点，使用和配置复杂，任务控制不灵活
    - xxl-job
        - 实现思路，大众点评员工徐雪的分布式任务调度平台，调度中心通过DB锁保证一致性
        - 优点，原理简单、实现简洁，对任务控制更灵活
        - 缺点，执行器的扩展，会增大DB压力

### Elastic-Job

1. 引入pom
    ```
    <dependency>
        <groupId>com.github.yinjihuan</groupId>
        <artifactId>elastic-job-spring-boot-starter</artifactId>
        <version>1.0.2</version>
    </dependency>
    ```
2. application.yml指定zk和出发规则、分片信息等
    ```
    # zk注册中心
    elastic.job.zk.serverLists=127.0.0.1:2181
    elastic.job.zk.namespace=enjoy_job

    elastic.job.EnjoyJob.cron=0/10 * * * * ?
    # 分片为2来跑
    elastic.job.EnjoyJob.shardingTotalCount=2
    elastic.job.EnjoyJob.shardingItemParameters=0=beijing,1=shanghai
    ```
3. 定时任务实例
    ```
    //跟配置里的elastic.job.EnjoyJob对应
    @ElasticJobConf(name = "EnjoyJob")
    public class EnjoySimpleJob implements SimpleJob {
        @Autowired
        private EnjoyBusiness enjoyBusiness;

        public void execute(ShardingContext context) {
            System.out.println("当前分片："+context.getShardingParameter());
            int total = context.getShardingTotalCount();
            int shardingItem = context.getShardingItem();

            enjoyBusiness.process(shardingItem,total,"");
        }

    }

    //分片执行的service
    @Service
    public class EnjoyBusiness  {

        /**
        * index指示，现在跑的是第几片
        * total指示，总共有几片
        */
        public void process(int index,int total,String param) {
            if (total == 1){
                System.out.println("当前执行全业务处理,参数："+param);
                return;
            }
            //分片任务
            if (index == 1 ){
                // select * from xxx limit 0,500
            } else {
                // select * from xxx limit 500,1000
            }
            System.out.println("当前执行第 "+(index+1)+"/"+total+"片的数据,参数："+param);
        }

    }
    ```

### xxl-job

1. 调度器（控制台）配置，github下载代码，打jar包作为一个项目运行，然后登陆控制台，可以进行灵活的配置
    ```
    https://github.com/xuxueli/xxl-job
    ```
2. 下面就是执行器的配置，引入pom
    ```
    <dependency>
        <groupId>com.xuxueli</groupId>
        <artifactId>xxl-job-core</artifactId>
        <version>2.0.1</version>
    </dependency>
    ```
3. application.yml配置控制台地址、执行器名称、通信端口等
    ```
    ### 调度器的地址----- 发消息
    xxl.job.admin.addresses=http://172.17.0.3:8080/admin
    ### 当前执行器的标识名称，同一个名字的执行器构成集群
    xxl.job.executor.appname=xxl-enjoy
    # 执行器与调度器通信的ip / port
    xxl.job.executor.ip=
    xxl.job.executor.port=9995
    ### xxl-job, access token
    xxl.job.accessToken=
    ### xxl-job log path
    xxl.job.executor.logpath=/logs/xxl/job
    ### xxl-job log retention days
    xxl.job.executor.logretentiondays=-1
    ```
4. 在控制台添加执行器，AppName为项目中配置的xxl-enjoy
5. 执行器中编写调度任务
    ```
    //value值对应的是调度中心新建任务的JobHandler
    @JobHandler(value="enjoySimple")
    @Component
    public class EnjoySimple extends IJobHandler {

        @Autowired
        private EnjoyBusiness enjoyBusiness;

        @Override
        public ReturnT<String> execute(String param) throws Exception {
            enjoyBusiness.process(1,1,param);
            return SUCCESS;
        }

    }
    ```
6. 可以在后台控制台直接开始调度/停止任务，也可以自定义调度临时任务（用到了热部署）