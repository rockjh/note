### 需求

1. rpc调用需要定制，额外的工作量
2. 分布式中，动辄几十上百，相互之间的调用错综复杂，相互依赖严重
3. 对集群性的服务，需要负载策略
4. 对集群性的服务，能动态扩展节点

### 特点

1. 分布式、高性能、透明化的rpc服务框架
2. 提供服务注册、自动发现等高效服务治理方案
3. 高性能nio通讯及多协议集成、服务动态寻址及路由、软负载均衡与容错、依赖分析与降级

### 结构

1. 包括几个部分
    - provider，服务提供者
    - consumer，服务消费者
    - registry，注册中心
    - monitor，服务的后台监控，有多少服务在消费某个服务，切入切出功能
2. 运行流程
    - container负责启动、加载、运行provider
    - provider启动时，向registry注册自己的服务
    - consumer启动时，向registry订阅自己需要的服务，registry提供privoder列表给consumer，实时推送变动情况
    - consumer根据provider列表，按负载算法选一台provider调用
    - monitor统计rpc调用次数，monitor可以直接在控制页面将某个provider去除列表，也可以加入列表

### API形式

1. pom引用
    ```
    <dependency>
        <groupId>com.101tec</groupId>
        <artifactId>zkclient</artifactId>
        <version>0.11</version>
        <exclusions>
            <exclusion>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-log4j12</artifactId>
            </exclusion>
        </exclusions>
    </dependency>

    <dependency>
        <groupId>com.alibaba.spring.boot</groupId>
        <artifactId>dubbo-spring-boot-starter</artifactId>
        <version>2.0.0</version>
    </dependency>
    ```
2. 服务接口
    ```
    public interface HelloService {

        String hi();
    }
    ```
3. 服务实现
    ```
    public class HelloServiceImpl implements HelloService {

        public String hi() {
            return "hi";
        }
    }
    ```
4. 服务提供者
    ```
    public class ApiProvider {

        public static void main(String[] args) throws IOException {
            //当前应用配置
            ApplicationConfig applicationConfig = new ApplicationConfig();
            applicationConfig.setName("api-type-provider");

            //连接注册中心配置
            RegistryConfig registryConfig = new RegistryConfig();
            registryConfig.setProtocol("zookeeper");
            registryConfig.setAddress("127.0.0.1:2181");

            //服务提供者协议配置
            ProtocolConfig protocolConfig = new ProtocolConfig();
            protocolConfig.setName("dubbo");
            protocolConfig.setPort(21180);
            protocolConfig.setThreads(100);

            //注意，serviceConfig为重对象，内部封装了与注册中心的连接，以及开启服务端口
            //服务提供者暴露服务配置
            //此实例很重，封装了与注册中心的连接，请自行缓存，否则可能造成内存和连接泄漏
            ServiceConfig<HelloService> serviceConfig = new ServiceConfig<>();
            serviceConfig.setApplication(applicationConfig);
            //多个注册中心可以使用setRegistries
            serviceConfig.setRegistry(registryConfig);
            //多个协议可以使用setProtocols
            serviceConfig.setProtocol(protocolConfig);
            serviceConfig.setInterface(HelloService.class);
            serviceConfig.setRef(new HelloServiceImpl());

            //暴露及注册服务
            serviceConfig.export();

            System.in.read();
        }
    }
    ```
5. 服务消费者
    ```
    public class ApiConsumer {

        public static void main(String[] args){
            //当前应用配置
            ApplicationConfig applicationConfig = new ApplicationConfig();
            applicationConfig.setName("api-type-consumer");

            //连接注册中心配置
            RegistryConfig registryConfig = new RegistryConfig();
            registryConfig.setProtocol("zookeeper");
            registryConfig.setAddress("127.0.0.1:2181");

            //注意，ReferenceConfig为重对象，内部封装了与注册中心的连接，以及服务提供方的连接
            //引用远程服务
            ReferenceConfig<HelloService> referenceConfig = new ReferenceConfig<>();
            referenceConfig.setApplication(applicationConfig);
            //多个注册中心可以使用setRegistries
            referenceConfig.setRegistry(registryConfig);
            referenceConfig.setInterface(HelloService.class);
            //代理对象较重，内部封装了所有的通讯细节，请缓存使用
            HelloService helloService = referenceConfig.get();
            String hi = helloService.hi();
            System.out.println(hi);

            referenceConfig.destroy();
        }
    }
    ```

### springboot-alibaba形式

1. 服务提供方pom
    ```
    <dependency>
        <groupId>com.101tec</groupId>
        <artifactId>zkclient</artifactId>
        <version>0.11</version>
        <exclusions>
            <exclusion>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-log4j12</artifactId>
            </exclusion>
        </exclusions>
    </dependency>

    <dependency>
        <groupId>com.alibaba.spring.boot</groupId>
        <artifactId>dubbo-spring-boot-starter</artifactId>
        <version>2.0.0</version>
    </dependency>
    ```
2. 服务消费方额外增加一个依赖
    ```
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
        <version>1.5.10.RELEASE</version>
    </dependency>
    ```
3. 服务消费方和提供方的接口声明
    ```
    public interface HelloService {

        String hi();
    }
    ```
4. 服务提供方其余代码和配置
    ```
    //服务的实现
    @Component
    @com.alibaba.dubbo.config.annotation.Service(interfaceClass = HelloService.class)
    public class HelloServiceImpl implements HelloService {

        public String hi() {
            return "hi";
        }
    }

    //启动类
    @EnableDubboConfiguration
    @SpringBootApplication
    public class ProviderApplication {

        public static void main(String[] args) {
            SpringApplication.run(ProviderApplication.class);
        }

    }

    //application.yml
    server:
        port: 8888
    spring:
        dubbo:
            application:
                name: springboot-dubbo-privider
                id: springboot-dubbo-privider
            protocol:
                name: dubbo
                port: 20880
            registry:
                address: zookeeper://127.0.0.1:2181
            server: true
    ```
5. 服务消费方其余代码和配置
    ```
    //远程调用
    @RestController
    public class HelloController {

        @Reference
        private HelloService helloService;

        @RequestMapping("hello")
        public String hello(){
            return helloService.hi();
        }
    }

    //启动类
    @SpringBootApplication
    @EnableDubboConfiguration
    public class ConsumerApplication {

        public static void main(String[] args) {
            SpringApplication.run(ConsumerApplication.class);
        }

    }

    //application.yml
    server:
        port: 7777
    spring:
        dubbo:
            application:
                name: springboot-dubbo-consumer
                id: springboot-dubbo-consumer
            protocol:
                name: dubbo
                port: 20880
            registry:
                address: zookeeper://127.0.0.1:2181
    ```

### springboot-apache形式

1. 服务方提供的pom
    ```
    <dependencies>

        <!-- Dubbo Spring Boot Starter -->
        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo-spring-boot-starter</artifactId>
            <version>2.7.7</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>2.3.0.RELEASE</version>
            <optional>true</optional>
        </dependency>

        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-recipes</artifactId>
            <version>2.13.0</version>
        </dependency>
    </dependencies>
    ```
2. 服务方提供的代码和配置
    ```
    //application.yml
    server:
        port: 8881
    dubbo:
        application:
            name: springboot-dubbo-privider
            id: springboot-dubbo-privider
        protocol:
            name: dubbo
            port: 20881
        registry:
            address: zookeeper://127.0.0.1:2181
        server: true

    //启动类
    @EnableDubbo
    @SpringBootApplication
    public class ProviderApplication {

        public static void main(String[] args) {
            SpringApplication.run(ProviderApplication.class);
        }

    }

    //服务实现类
    @DubboService//里面可以写很多配置，包括和xml中类似的所有配置
    public class HelloServiceImpl implements HelloService {

        public String hi() {
            return "hi-2";
        }
    }
    ```

### dubbo-admin监控器启动

1. [下载地址](https://github.com/apache/dubbo-admin)
2. 配置文件存放的路径
    ```
    dubbo-admin-server/src/main/resources/application.properties
    ```
3. build项目
    ```
    mvn clean package
    ```
4. 运行
    ```
    cd dubbo-admin-distribution/target
    java -jar dubbo-admin-0.1.jar
    ```
5. 查看地址信息
    ```
    http://localhost:8080
    root/root
    ```