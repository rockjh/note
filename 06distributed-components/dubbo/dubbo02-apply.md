### 配置用法（采用apache-springboot形式）

1. 启动时检查，默认true服务不可用时，会抛出异常，建议开发阶段设置为false，正式使用阶段设置为true
    ```
    //针对某个服务消费的配置
    @DubboReference(check = false)

    //application.yml
    dubbo:
        consumer:
            check: false
    ```
2. 集群容错配置
    - failover（默认），当出现失败时，重试其他服务器，retries=”2“ 来设置重试次数（不含第一次），适合幂等性操作（无论操作几次不影响结果），如读操作
    - failfast，快速失败，只发起一次调用，失败立即报错，适合非幂等性操作，如写操作
    - failsafe，出现异常，直接忽略，适合无关紧要的旁支操作，如日志
    - failback，后台记录失败请求，定时重发，后续专业处理
    - forking，并行调用多个服务器，只有有一个成功就返回，forks="2"来设置最大并行数
3. 负载均衡配置
    - random，按权重分配，根据weight值（服务方配置）来随机
    - roundrobin，轮询
    - leastactive，最少活跃数（正在处理的数）、收到的请求少
4. 缓存
    - lru，基于最近最少使用原则，会删除多余的缓存
    - threadlocal，当前线程缓存
5. 泛化调用，generic=“true”，没有接口api时，强转为GenericService类型，然后直接调用
    ```
    //获取到一个泛化的类
    GenericService genericService = (GenericService)ctx.getBean(id);
    //使用反射生成调用对象，方法名、方法参数类型列表、方法实际参数
    Object reslut = genericService.$invoke("hello",new Object[]{},new Object[]{});
    ```
6. 回声测试，测试reference的服务是否准备就绪
    ```
    //能获取到bean就证明这个服务可用
    EchoService echoService = (EchoService)ctx.getBean(id);
    echoService.$echo("ok")
    ```
7. 服务分组group、多版本version、异步调用async="true"（不推荐）、事件通知onreturn

> application.yml全局配置，会覆盖上面的配置

### 与springboot的整合（基于apache-springboot形式）

1. 进入@EnableDubbo--->@DubboComponentScan---DubboComponentScanRegistr
2. 因为DubboComponentScanRegistrar是通过@Import引入的，并且类实现了ImportBeanDefinitionRegistrar类，所以，他会在spring容器加载时被执行
3. DubboComponentScanRegistrar--->registerBeanDefinitions
    ```
    //获取包扫描路径
    Set<String> packagesToScan = getPackagesToScan(importingClassMetadata);
    //注册ServiceAnnotationBeanPostProcessor进入到spirng容器，并设置包扫描路径为实例化参数
    registerServiceAnnotationBeanPostProcessor(packagesToScan, registry);
    //注册dubbo所需要common-bean
    registerCommonBeans(registry);
    ```
4. ServiceAnnotationBeanPostProcessor实际上是一个BeanFactoryPostProcessor，会在容器初始化时被加载
5. AbstractApplicationContext--->refresh()--->invokeBeanFactoryPostProcessors(beanFactory)实例化容器中所有BeanFactoryPostProcessor
6. ServiceAnnotationBeanPostProcessor已经被实例化放进了容器中，然后还是在第5步，执行postProcessBeanDefinitionRegistry方法
    ```
    //注册包扫描中的类
    registerServiceBeans(resolvedPackagesToScan, registry);
    ```
7. ServiceAnnotationBeanPostProcessor--->registerServiceBeans
    ```
    //扫描得到类定义，注册进容器
    Set<BeanDefinitionHolder> beanDefinitionHolders =
        findServiceBeanDefinitionHolders(scanner, packageToScan, registry, beanNameGenerator);
    ```

8. @EnableDubbo--->@EnableDubboConfig--->DubboConfigConfigurationRegistrar
9. DubboConfigConfigurationRegistrar实现了ImportBeanDefinitionRegistrar方法，且被@Import引用
    ```
    //DubboConfigConfiguration.Single.class是dubbo重要的配置类
    //会将各种配置类如ApplicationConfig等注册进容器
    //RegistryConfig、ProtocolConfig、ApplicationConfig
    registerBeans(registry, DubboConfigConfiguration.Single.class);
    ```

### 关键类
1. RegistryConfig、ProtocolConfig、ApplicationConfig都会注册进入容器
2. 服务提供方是ServiceBean、ServiceConfig
3. 服务消费方式ReferenceBean、ReferenceConfig
4. 最终会通过各种不同的协议来进行发布如dubbo的DubboProtocol--->export


### SPI

1. 定义，spi机制是同一个接口，多种实现，从多个实现中选择一种去使用，典型的策略模式
2. 示例Protocol，按照道理来说，每一个服务，他各自的协议不同，不应该使用类变量，但是这里使用了类变量
    ```
    private static final Protocol PROTOCOL = ExtensionLoader.getExtensionLoader(Protocol.class).getAdaptiveExtension();
    ```
3. ExtensionLoader--->getExtensionLoader()
    ```
    //从缓存中获取类，为null就新建
    ExtensionLoader<T> loader = (ExtensionLoader<T>) EXTENSION_LOADERS.get(type);
    if (loader == null) {
        EXTENSION_LOADERS.putIfAbsent(type, new ExtensionLoader<T>(type));
        loader = (ExtensionLoader<T>) EXTENSION_LOADERS.get(type);
    }
    ```
4. new ExtensionLoader<T>(type)，新建然后返回，执行getAdaptiveExtension获取适配类，返回的是一个实例对象
    ```
    //缓存中没有就新建，前面赋值了一个type，类变量中放入了自己
    instance = createAdaptiveExtension();
    ```
5. 创建一个对象
    ```
    //getAdaptiveExtensionClass自动生成类，来加载返回class对象
    injectExtension((T) getAdaptiveExtensionClass().newInstance());
    ```
6. createAdaptiveExtensionClass
    ```
    //生成了类的字符串
    String code = new AdaptiveClassCodeGenerator(type, cachedDefaultName).generate();
    //找到类加载器
    ClassLoader classLoader = findClassLoader();
    //编译
    org.apache.dubbo.common.compiler.Compiler compiler = ExtensionLoader.getExtensionLoader(org.apache.dubbo.common.compiler.Compiler.class).getAdaptiveExtension();
    //返回class对象
    return compiler.compile(code, classLoader);
    ```
7. 完成，生成类，大致如下
    ```
    public class Protocol$Adaptive implements org.apache.dubbo.rpc.Protocol {
        public void destroy() {
            throw new UnsupportedOperationException("The method public abstract void org.apache.dubbo.rpc.Protocol.destroy() of interface org.apache.dubbo.rpc.Protocol is not adaptive method!");
        }

        public int getDefaultPort() {
            throw new UnsupportedOperationException("The method public abstract int org.apache.dubbo.rpc.Protocol.getDefaultPort() of interface org.apache.dubbo.rpc.Protocol is not adaptive method!");
        }

        public org.apache.dubbo.rpc.Exporter export(org.apache.dubbo.rpc.Invoker arg0) throws org.apache.dubbo.rpc.RpcException {
            if (arg0 == null) throw new IllegalArgumentException("org.apache.dubbo.rpc.Invoker argument == null");
            if (arg0.getUrl() == null)
                throw new IllegalArgumentException("org.apache.dubbo.rpc.Invoker argument getUrl() == null");
            org.apache.dubbo.common.URL url = arg0.getUrl();
            //通过url获取协议的名称
            String extName = (url.getProtocol() == null ? "dubbo" : url.getProtocol());
            if (extName == null)
                throw new IllegalStateException("Failed to get extension (org.apache.dubbo.rpc.Protocol) name from url (" + url.toString() + ") use keys([protocol])");
            //获取到Protocol的ExtensionLoader
            //然后再getExtension具体的协议名称，获得具体协议的实例
            org.apache.dubbo.rpc.Protocol extension = (org.apache.dubbo.rpc.Protocol) ExtensionLoader.getExtensionLoader(org.apache.dubbo.rpc.Protocol.class).getExtension(extName);
            //调用具体实例的export方法，完成该方法的调用
            return extension.export(arg0);
        }

        public org.apache.dubbo.rpc.Invoker refer(java.lang.Class arg0, org.apache.dubbo.common.URL arg1) throws org.apache.dubbo.rpc.RpcException {
            if (arg1 == null) throw new IllegalArgumentException("url == null");
            org.apache.dubbo.common.URL url = arg1;
            String extName = (url.getProtocol() == null ? "dubbo" : url.getProtocol());
            if (extName == null)
                throw new IllegalStateException("Failed to get extension (org.apache.dubbo.rpc.Protocol) name from url (" + url.toString() + ") use keys([protocol])");
            org.apache.dubbo.rpc.Protocol extension = (org.apache.dubbo.rpc.Protocol) ExtensionLoader.getExtensionLoader(org.apache.dubbo.rpc.Protocol.class).getExtension(extName);
            return extension.refer(arg0, arg1);
        }

        public java.util.List getServers() {
            throw new UnsupportedOperationException("The method public default java.util.List org.apache.dubbo.rpc.Protocol.getServers() of interface org.apache.dubbo.rpc.Protocol is not adaptive method!");
        }
    }
    ```
8. 大致思想如下
    - 每个配置类都使用类变量，通过类似上面的形式获取到一个动态生成的class适配器的实例
    - 这个实例会实现带有@Adaptive注解的方法，其他方法抛出异常
    - 通过url获取具体实例的名称，例如dubbo
    - 实现的方法中会调用getExtension方法去获取对应的实例
    ```
    //ExtensionLoader类常用的缓存
    //非static变量，都是针对的某一SPI类型，例如Protocol
    //SPI接口和对应的ExtensionLoader类
    ConcurrentMap<Class<?>, ExtensionLoader<?>> EXTENSION_LOADERS
    //具体类SPI实现类和他的实例，例如DubboProtocol和他的实例
    ConcurrentMap<Class<?>, Object> EXTENSION_INSTANCES
    //某类SPI的具体实现类和名称，例如protocol的SPI中，缓存DubboProtocol.class、dubbo(类中定义的名称)
    ConcurrentMap<Class<?>, String> cachedNames
    //和上面类似，key和value进行了对调
    Holder<Map<String, Class<?>>> cachedClasses
    //具体SPI类型中的，name和实例的对应
    ConcurrentMap<String, Holder<Object>> cachedInstances
    ```

### 自定义负载均衡策略

1. 创建自定义负载类，继承AbstractLoadBalance，重写doSelect方法，这个方法就是定义算法规则的地方
2. 添加dubbo负载扩展点，在src/main/resources目录下创建META-INFO/dubbo目录，在目录下创建org.apache.dubbo.rpc.cluster.LoadBalance文件，里面配置对应的负载算法类，如下
    ```
    gray=com.dalaoyang.balance.GrayLoadBalance
    ```
3. 配置文件中使用
    ```
    @DubboReference(loadbalance = "gray")
    ```