### 概述

1. 什么是消息中间件？消息中间件属于分布式系统中的一个子系统，关注与数据的发送和接收，利用高效可靠的异步消息传递机制对分布式系统中其余各个子系统进行集成
2. 为什么要用消息中间件？消息中间件主要是用来解决分布式系统之间的消息传递，同时为分布式系统中的其他子系统提供了伸缩性和扩展性
    - 低耦合，程序或者模块之间使用中间件间接异步通信，无需依赖
    - 缓冲能力，将高峰期大量的请求存储下来，慢慢交给后台进行处理，例如秒杀
    - 伸缩性，通过不断向集群中加入服务器来缓解不断上升的用户并发访问
    - 扩展性，增加业务功能时，可以不改动或者少改动现有代码，例如增加用户注册成功后发送邮件，只需要邮件模块增加用户注册成功的监听即可，不需要对用户模块修改
3. 和RPC的区别、联系
    - RPC，是强依赖、同步执行的、RPC调用就像本地调用一样、对象级和函数级的通信、强依赖指某个业务必须要调用RPC服务才算完整执行例如，秒杀情况下用户下单用到的订单模块和商品库存模块就属于强依赖
    - 中间件，不依赖、异步执行、系统级和模块级的通信，只需要发布到中间件，需要通知消息的去中间件订阅就行
    - 联系，都是分布式下面的通信方式
4. 使用场景
    - 异步处理，多线程处理会消耗更多的系统资源（线程占用系统资源）
    - 应用解耦，普通商品订单模式下，用户下单和减库存发货是两个步骤，下单成功后允许减库存发货失败，这两个不应该是耦合
    - 流量削峰，秒杀活动导致流量暴增，用户的请求可以先进入消息队列，如果消息队列长度超过最大数量，直接抛弃用户请求或转到错误页面，业务根据队列中的请求做后续处理
    - 日志处理，比如Kafka的应用，解决大量日志传输的问题
    - 消息通讯，A、B订阅同一个队列，进行点对点通讯，A、B、C订阅同一个主题，可以进行聊天室通讯

### 常见的消息中间件

1. ActiveMQ
    - 单台性能，近万级
    - 社区活跃高
    - 优点，成熟、文档完善、多个协议支持、开发语言java、主流
    - 缺点，性能较弱，缺乏大规模吞吐的场景应用
2. RabbitMQ
    - 单台性能，万级
    - 社区活跃高
    - 优点，性能好、管理界面丰富、多个协议支持、主流
    - 缺点，使用erlang语言开发，内部机制很难掌握，集群不支持动态扩展
3. RocketMQ
    - 单台性能，近十万级
    - 社区活跃中
    - 优点，阿里开发、模型简单、接口易用、性能好，版本更新快
    - 缺点，文档少、支持的语言协议少、不是主流
4. kafka
    - 单台性能，百万级
    - 社区活跃高
    - 优点，天生分布式、性能最好、经常用于大数据领域
    - 缺点，运维难度大、有数据混乱的情况、对zk强依赖、多副本机制对带宽有要求
5. 技术选型
    - 主要是基于解耦和异步来用的，可以考虑ActiveMQ
    - 中小型公司，技术实力较为一般，技术挑战不是特别高，用ActiveMQ、RabbitMQ
    - 大型公司，基础架构研发实力较强，用RocketMQ是很好的选择
    - 大数据领域的实时计算、日志采集等场景，用Kafka是业内标准的