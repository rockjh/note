### JMS和ActiveMQ

1. 定义，JMS（Java Messaging Service）是Java平台上有关面向消息中间件的技术规范，是一套api，ActiveMQ而是这个规范的一个具体实现
2. JMS对象模型
    - 连接工厂，连接工厂负责创建一个JMS连接。
    - JMS连接（Connection），JMS客户端和服务器端之间的一个活动的连接
    - JMS会话（Session），JMS客户与JMS服务器之间的会话状态
    - JMS目的（Broker），客户用来指定它生产的消息的目标和它消费的消息的来源的对象
    - 生产者（Message Producer）和消费者（Message Consumer），对象由Session对象创建，用于发送和接收消息
3. JMS消息组成
    - 消息头，每个消息头字段都有相应的getter和setter方法。
    - 消息属性，如果需要除消息头字段以外的值，那么可以使用消息属性。
    - 消息体，JMS定义的消息类型如下，ActiveMQ也有对应的实现
        - TextMessage
        - MapMessage
        - BytesMessage
        - StreamMessage
        - ObjectMessage

4. JMS消息模型
    - Point-to-Point（p2p点对点），ActiveMQ中对应的queue
        - 每一个队列都应该被消费且只能被一个消费者消费，如果有多个该队列的消费者，采用轮询的模式
        - 队列中的数据进行了持久化，如果没有消费者，会一直被保存，被消费之后，删除该项持久化
        - 如果希望发送的每个消息都应该被成功处理的话，使用这个P2P模式
    - Topic（主题），ActiveMQ中对应的topic
        - 消息生产者将消息发布到topic中，有多个消费者时，每个消费者都能收到消息
        - 当一个生产者发布消息后，如果没有消费者，这条消息不会被做任何处理
        - 如果你希望发送的消息可以不被做任何处理、或者被一个消息者处理、或者可以被多个消费者处理的话，那么可以采用topic模型

### 安装、部署和运行

1. 下载解压后，进入bin目录运行以下命令启动
    ```
    ./activemq start
    ```
2. 61616为默认的服务端口，可以进入/conf/activemq.xml配置
3. 访问以下地址，进入后台管理页面，登录账号密码admin/admin
    ```
    http://127.0.0.1:8161/admin
    ```

### 原生API

1. pom配置
    ```
    <dependency>
        <groupId>org.apache.activemq</groupId>
        <artifactId>activemq-all</artifactId>
        <version>5.8.0</version>
    </dependency>
    ```
2. 生产者
    ```
    public static void main(String[] args) {
        /* 连接工厂*/
        ConnectionFactory connectionFactory;
        /* 连接*/
        Connection connection = null;
        /* 会话*/
        Session session;
        /* 消息的目的地*/
        Destination destination;
        /* 消息的生产者*/
        MessageProducer messageProducer;

        /* 实例化连接工厂*/
        connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER,
                ActiveMQConnection.DEFAULT_PASSWORD,
                ActiveMQConnection.DEFAULT_BROKER_URL);
        try {
            /* 通过连接工厂获取连接*/
            connection = connectionFactory.createConnection();
            /* 启动连接*/
            connection.start();
            /* 创建session
             * 第一个参数表示是否使用事务，第二次参数表示是否自动确认*/
            session = connection.createSession(false,
                    Session.AUTO_ACKNOWLEDGE);
            /* 创建一个名为HelloWorld消息队列*/
            destination = session.createTopic("HelloActiveMq");
            //destination = session.createQueue("HelloActiveMqQueue");
            /* 创建消息生产者*/
            messageProducer = session.createProducer(destination);
            /* 循环发送消息*/
            for (int i = 0; i < 3; i++) {
                String msg = "发送消息" + i + " " + System.currentTimeMillis();
                TextMessage textMessage = session.createTextMessage(msg);
                System.out.println("标准用法:" + msg);
                messageProducer.send(textMessage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    ```
3. 消费者
    ```
    public static void main(String[] args) {
        /* 连接工厂*/
        ConnectionFactory connectionFactory;
        /* 连接*/
        Connection connection = null;
        /* 会话*/
        Session session;
        /* 消息的目的地*/
        Destination destination;
        /* 消息的消费者*/
        MessageConsumer messageConsumer;

        /* 实例化连接工厂*/
        connectionFactory
                = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER,
                ActiveMQConnection.DEFAULT_PASSWORD,
                ActiveMQConnection.DEFAULT_BROKER_URL);
        try {
            /* 通过连接工厂获取连接*/
            connection = connectionFactory.createConnection();
            /* 启动连接*/
            connection.start();
            /* 创建session*/
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            /* 创建一个名为HelloWorld消息队列*/
            destination = session.createTopic("HelloActiveMq");
            //destination = session.createQueue("HelloActiveMqQueue");

            /* 创建消息消费者*/
            messageConsumer = session.createConsumer(destination);
            /* 设置消费者监听器，监听消息，异步接收*/
            messageConsumer.setMessageListener(new MessageListener() {
                public void onMessage(Message message) {
                    try {
                        System.out.println(((TextMessage) message).getText());
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                }
            });
            //主动获取同步接收
            //messageConsumer = session.createConsumer(destination);
            //Message message;
            //while ((message = messageConsumer.receive()) != null) {
                //System.out.println(((TextMessage) message).getText());
            //}
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }
    ```

### 与springboot结合

1. pom文件
    ```
    //加上前面的mq的包
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-activemq</artifactId>
    </dependency>
    <dependency>
        <groupId>org.apache.activemq</groupId>
        <artifactId>activemq-pool</artifactId>
    </dependency>
    ```
2. 启动mq的配置类
    ```
    @Configuration
    @EnableJms
    public class ActiveMqConfiguration {

        @Value("${spring.activemq.user}")
        private String usrName;

        @Value("${spring.activemq.password}")
        private String password;

        @Value("${spring.activemq.broker-url}")
        private String brokerUrl;

        @Bean
        public ConnectionFactory connectionFactory(){
            ActiveMQConnectionFactory connectionFactory
                    = new ActiveMQConnectionFactory();
            connectionFactory.setBrokerURL(brokerUrl);
            connectionFactory.setUserName(usrName);
            connectionFactory.setPassword(password);
            return connectionFactory;
        }

        @Bean("jmsTopicListenerContainerFactory")
        public JmsListenerContainerFactory jmsTopicListenerContainerFactory(ConnectionFactory connectionFactory){
            DefaultJmsListenerContainerFactory factory
                    = new DefaultJmsListenerContainerFactory();
            factory.setConnectionFactory(connectionFactory);
            factory.setPubSubDomain(true);
            return factory;
        }

    }
    ```
3. 生产者
    ```
    //发送topic
    Destination destination = new ActiveMQTopic("springboot.topic");
    //发送queue
    //Destination destination = new ActiveMQQueue("springboot.queue");


    @Autowired
    private JmsMessagingTemplate jmsTemplate;
    @Autowired
    private JmsTemplate template;//可以做更细微的控制

    // 发送消息，destination是发送到的队列，message是待发送的消息
    public void sendMessage(Destination destination, final String message){
        jmsTemplate.convertAndSend(destination, message);
        template.send(destination, session -> {
            TextMessage msg = session.createTextMessage();
            msg.setText("othre information");
            return msg;
        });
    }
    ```
4. 消费者
    ```
    //queue默认是队列
    @JmsListener(destination = "springboot.queue")
    public void receiveQueue(String text) {
        System.out.println(this.getClass().getName()+" 收到的报文为:"+text);
    }
    //topic需要指定factory
    @JmsListener(destination = "springboot.topic",containerFactory = "jmsTopicListenerContainerFactory")
    public void receiveTopic(String text) {
        System.out.println(this.getClass().getName()+" 收到的报文为:"+text);
    }
    ```

### 其他问题

1. 消费者获取数据两种模式
    - receive，主动拉去，削峰推荐使用
    - 监听，mq推送的
2. 不建议传递太大的消息，一般来说说1k最好
    - 一个数字、字母占用一个字节
    - 一个汉字，一般占用三个字节
    - 8bit(位)=1Byte(字节) 、1024Byte(字节)=1KB、1024KB=1MB
3. 键一般不要超过1000个，超过之后对性能有影响