### 消息持久化

1. 定义，服务可能会不可用，如果不消费的话，可能会导致消息丢失，默认queue做持久化，topic不做持久化
2. 持久化方式
    - amq，基于文件存储方式，默认一个文件大小32M，当整个文件中的所有消息被消费之后，标记为可删除，activemq5.3以前的默认方式
    - kahaDB，基于文件存储方式，提供了容量的提升和恢复能力，他没有amq快，但是具有强扩展性，恢复时间比amq更短，activemq5.4之后的版本的默认方式
    - jdbc，基于jdbc存储，可以配置mysql等数据库
    - memory，基于内存的消息存储，不属于持久化范围
3. 更改使用jdbc-mysql方案
    ```
    //默认的kahaDB配置，注释掉，该为jdbc配置
    <persistenceAdapter>
        <kahaDB directory="${activemq.base}/data/kahadb"/>
    </persistenceAdapter>


    //jdbc配置
    <persistenceAdapter>
        <jdbcPersistenceAdapter  dataSource="#mysql-ds"/>
    </persistenceAdapter>

    //</broker>标签后，增加数据源的配置，relaxAutoCommit=true配置必须有，需要我们先创建好数据库
    <bean id="mysql-ds" class="org.apache.commons.dbcp2.BasicDataSource" destroy-method="close">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://localhost:3306/activemq?relaxAutoCommit=true&amp;useUnicode=true&amp;characterEncoding=utf-8&amp;serverTimezone=UTC"/>
        <property name="username" value="root"/>
        <property name="password" value="123456"/>
        <property name="poolPreparedStatements" value="true"/>
    </bean>
    ```
    - mysql-connector-java-5.1.34-bin.jar，mysql包放在mq的lib目录下
    - 启动后会在数据库中自动增加三个表
        - activemq_acks，用于存储订阅关系。如果是持久化Topic，订阅者和服务器的订阅关系在这个表保存
        - activemq_lock，集群环境中才有用，只有一个Broker可以获得消息，称为Master Broker，其他的只能作为备份等待Master Broker不可用，才可能成为下一个Master Broker。这个表用于记录哪个Broker是当前的Master Broker
        - activemq_msgs，用于存储消息，Queue和Topic都存储在这个表中
4. topic持久化，Topic消费者端
    - 需要设置客户端id：connection.setClientID("Mark");
    - 消息的destination变为 Topic 
    - 消费者类型变为TopicSubscriber
    - 消费者创建时变为session.createDurableSubscriber(destination,"任意名字，代表订阅名");
    - 运行一次消费者，将消费者在ActiveMQ上进行一次注册。然后在ActiveMQ的管理控制台subscribers页面可以看见我们的消费者
    > 将消息设置为非持久化，默认是消息是持久化的，messageProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);


### 消息可靠性（p2p）

1. 定义，可靠性分为两个方面
    - 消息生产者发送的消息可以被ActiveMQ收到
    - 消费者收到了ActiveMQ的消息
2. 保证生产者发送到mq
    - 事务中消息（无论是否持久化）是可靠的，send() 方法不会被阻塞。但是commit 方法会被阻塞，直到收到确认消息
    - 持久化消息，以同步方式发送，当持久化到磁盘后才会给生产者发送确认
    - 非持久化又不在事务中的消息，可能会有消息的丢失
3. 保证消费者消费了消息
    - 四种消息确认机制
        - AUTO_ACKNOWLEDGE = 1，自动确认，同步执行时，阻塞返回就自动确认，异步时，方法执行完没有异常就返回自动确认
        - CLIENT_ACKNOWLEDGE = 2，客户端手动确认   
            ```
            //消息逐个确认
            message.acknowledge()
            //或者
            session.acknowledge()
            ```
        - DUPS_OK_ACKNOWLEDGE = 3，自动批量确认，ActiveMQ会根据内部算法，在收到一定数量的消息自动进行确认，可能会出现重复消息（当consumer故障重启后，那些尚未ACK的消息会重新发送过来）
        - SESSION_TRANSACTED = 0，事务提交并确认
    - 创建连接时，会选择
        ```
        //第一个参数是否使用事务，如果为true，忽略第二个参数自动采用SESSION_TRANSACTED
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE)
        ```
    - springboot中的消息确认
        ```
        //生产者
        @Autowired
        private JmsMessagingTemplate jmsTemplate;

        // 发送消息，destination是发送到的队列，message是待发送的消息
        public void sendMessage(Destination destination, final String message){
            jmsTemplate.convertAndSend(destination, message);

        }

        @JmsListener(destination = "out.replyTo.queue")
        public void consumerMessage(String text){
            System.out.println("从out.replyTo.queue收到报文"+text);
        }

        //消费者，属于异步通知类型
        // 使用JmsListener配置消费者监听的队列，其中text是接收到的消息
        @JmsListener(destination = "springboot.replyto.queue")
        @SendTo("out.replyTo.queue")
        public String receiveQueue(String text) {
            System.out.println(this.getClass().getName()+" 收到的报文为:"+text);
            return "Hello,I watch you";
        }
        ```

### 通配符式订阅

1. 可以使用通配符来进行订阅，mq中.代表一个层级
    - "." 用于作为路径上名字间的分隔符
    - "*" 用于匹配路径上的任何名字
    - ">" 用于递归地匹配任何以这个名字开始的destination
2. 示例
    - xiangxue.>，以xiangxue开头的
    - xiangxue.*，以xiangxue开头的二级，第二级为任意
    - xiangxue.vip.*.redis.cache，第三级为任意匹配

### 死信队列

1. 定义，用来处理失败或者过期的消息
2. 出现下面的情况，消息会被重发
    - 事务会话被回滚
    - 事务会话在提交之前关闭
    - 会话使用CLIENT_ACKNOWLEDGE模式，并且Session.recover()被调用
    - 自动应答失败
3. 当一个消息被重发超过最大重发次数，默认6次，会进入死信队列
4. 死信队列DLQ.开头，预防死信队列过大影响性能，需要对其进行处理

### 消费者集群模式

1. 问题，当消息生产者和消费者都是集群时
    - 同一个queue消息的接收方可能有多个集群（A、B）进行消息的处理，一般来说A、B集群不属于同一个业务
    - 集群A、B分别要接收完整的消息
2. 现有的解决方案Topic、Queue
    - 使用topic，会对集群中所有的订阅者都会发送消息，不符合
    - 使用queue，会对集群A、B中的机器进行轮询，不能让A和B都接受到完整的queue
3. 虚拟Destination
    - 对于生产者，是一个topic，以VirtualTopic.开头，例如VirtualTopic.vtgroup
    - 对于消费者，是个队列，不同应用里使用不同的前缀作为队列的名称，即可表明自己的身份即可实现消费者应用分组，例如
        - Consumer.A.VirtualTopic.vtgroup，说明它是名称为A群组的消费者
        - Consumer.B.VirtualTopic.vtgroup，说明它是名称为B群组的消费者
        - 消费者的地址默认格式Consumer.*.VirtualTopic.vtgroup
4. 组合Destination
    - 在生产者中添加需要发往多个目的端
        ```
        //分别发往FOO.A,FOO.B,FOO.C这三个队列
        Queue queue = new ActiveMQQueue("FOO.A,FOO.B,FOO.C");
        //分别发往队列my-queue、my-queue2和主题cd.mark
        Destination destination = session.createQueue("my-queue,my-queue2,topic://cd.mark");
        ```

### 项目实战

1. 用户注册，不同的处理方式
    - 串行实现，阻塞式调用，强依赖，耗费时间长
    - 并行实现，异步调用，涉及到多线程，耗费资源
    - mq实现，异步实现，交由mq去处理，最好
    - 增加邮件通知、需要给数据中心和客户服务也发送消息，这样扩展的话，只需要订阅mq的消息就行，其他实现方式都会更改原有代码

2. 限时订单，下单后限时支付
    - 传统模式，数据库轮询，最糟糕的方案，浪费大量的资源，响应不及时
    - 利用DelayQueue延时队列
        - 下单后放进延时队列，例如延时半小时
        - 半小时后查询数据库该订单是否支付，如果没有支付就在修改订单状态已过期（一条sql执行）
        - 当用户支付的时候判断订单状态是否已过期
        - 算是比较优雅的实现，但是延时队列是存在内存中的，集群环境（需要定机器访问），宕机无法恢复问题
        - 宕机后，重启时，可以扫描数据库，扫描出符合放进延时队列的数据，集群环境下会出现一个订单被多个服务器扫描，被多次更改
        - 为了解决被多次更改，直接指定服务器，数据库字段中加入某个订单是某个服务器负责的，如果某个服务挂了，启动不了只能人工干预修改订单所配置的编号
    - 使用mq的延时队列，不是jms的规范，所以代码和之前有些差异
        - 配置activemq.xml
            ```
            //broker的标签加入属性
            schedulerSupport="true"
            ```
        - 生产者代码如下
            ```
            @Autowired
            private JmsTemplate jmsTemplate;	
            
            /**
            * 类说明：创建消息的类
            */
            private static class CreateMessage implements MessageCreator{
                
                private OrderExp order;
                private long expireTime;
                
                public CreateMessage(OrderExp order, long expireTime) {
                    super();
                    this.order = order;
                    this.expireTime = expireTime;
                }

                public Message createMessage(Session session) throws JMSException {
                    Gson gson = new Gson();
                    String txtMsg = gson.toJson(order);
                    Message message = session.createTextMessage(txtMsg);
                    //延时消息
                    message.setLongProperty(
                            ScheduledMessage.AMQ_SCHEDULED_DELAY, expireTime);
                    return message;
                }
            }
            //将需要进行延时订单处理的放在这里
            //AMQ_SCHEDULED_DELAY，延迟投递的时间
            //AMQ_SCHEDULED_PERIOD，重复投递的时间间隔
            //AMQ_SCHEDULED_REPEAT，重复投递次数
            //AMQ_SCHEDULED_CRON，Cron表达式
            public void orderDelay(OrderExp order, long expireTime) {
                logger.info("订单[超时时长："+expireTime+"秒] 将被发送给消息队列，详情："+order);
                jmsTemplate.send("order.delay", new CreateMessage(order,expireTime*1000));
                
            }
            ```
        - 消费者代码如下
            ```
            @Service
            public class MqConsume implements MessageListener {

                private Logger logger = LoggerFactory.getLogger(MqConsume.class);

                @Autowired
                private DlyOrderProcessor processDlyOrder;
                
                //延时队列到期，会自动进入这个方法
                public void onMessage(Message message) {
                    try {
                        String txtMsg = ((TextMessage)message).getText();
                        logger.info("接收到消息队列发出消息："+txtMsg);
                        Gson gson = new Gson();
                        OrderExp order = (OrderExp)gson.fromJson(txtMsg, OrderExp.class);
                        processDlyOrder.checkDelayOrder(order);
                    } catch (Exception e) {
                        logger.error("处理消费异常！",e);
                    }
                }

            }
            ```
        - 引来新问题，mq的可用性，推荐采取集群