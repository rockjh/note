### 集群方式

1. Master-Slave，只能保证消息的可靠性，Master提供服务，Slave是实时地备份Master的数据，当Master失效时，Slave会自动升级为Master，客户端会自动连接到Slave上工作，Master-Slave模式分为四类
    - Pure Master Slave，ActiveMQ5.8以前支持
    - Shared File System Master Slave，利用共享文件系统做ActiveMQ集群，kahaDB的底层是文件系统，如果各个ActiveMQ实例需要运行在不同的机器，就需要用到分布式文件系统了
    - JDBC Master Slave，所有实例的配置文件都换成了jdbc存储，更容易分布式部署，压力就放在了数据库中
        - 添加监控功能
            ```
            //broker标签内增加属性
            useJmx="true"
            ```
        - 配置jdbc，注释掉之前的kahadb
        - 增加数据源
        - 修改客户端的连接地址
            ```
            failover:(tcp://0.0.0.0:61616,tcp://0.0.0.0:61617,tcp://0.0.0.0:61618)?randomize=false
            ```
    - Replicated LevelDB Store，使用ZooKeeper协调选择一个node作为master，master挂了，得到了最新更新的slave被允许成为master，推荐运行至少3个replica nodes
        - 使用性能比较好的LevelDB替换掉默认的KahaDB
            ```
            <persistenceAdapter> 
                <replicatedLevelDB 
                    directory="${activemq.data}/leveldb" 
                    replicas="3" 
                    bind="tcp://0.0.0.0:62623" 
                    zkAddress="127.0.0.1:2181" 
                    hostname="localhost" 
                    zkPath="/activemq/leveldb-stores"/> 
            </persistenceAdapter>
            ```
            - •	directory，持久化数据存放地址
            - replicas，集群中节点的个数
            - bind，集群通信端口
            - zkAddress，ZooKeeper集群地址
            - hostname，当前服务器的IP地址，如果集群启动的时候报未知主机名错误，那么就需要配置主机名到IP地址的映射关系。
            - zkPath，ZooKeeper数据挂载点
        - 修改客户端的连接地址
            ```
            failover:(tcp://0.0.0.0:61616,tcp://0.0.0.0:61617,tcp://0.0.0.0:61618)?randomize=false
            ```
        - 可以看到只有一台MQ成为Master，其余两台成为slave。且两台slave的管理控制台将无法访问
    > LevelDB具有很高的随机写，顺序读/写性能，但是随机读的性能很一般，LevelDB很适合应用在查询较少，而写很多的场景，主从复制就是这种场景，从数据库只有写，主数据库只有重启才会读，删除也是写操作

2. Broker-Cluster，可以保证负载均衡，不能保证高可用
    - 静态模式，手动静态配置
        ```
        //61616和61617是生产消息的端口
        //这个配置是在集群mq配置的，客户端直接连接这个配置的mq就可以获取消息
        //既可以是消息生产者也可以配置这个配置
        //broker标签内添加标签
        <networkConnectors>
            <networkConnector uri="static(tcp://localhost:61616,tcp://0.0.0.0:61617)" duplex="true">
        </networkConnectors>
        ```
    - 动态模式，通过网络组播找到其他集群节点
        ```
        //在消息生产者或者其他集群添加这个配置，就可以实现负载均衡，自动发现消息生产者
        //transportConnector标签内添加属性
        discoveryUri="multicast://default"
        //broker标签内添加标签
        <networkConnectors>
            <networkConnector uri="multicast://default" duplex="false"/>
        </networkConnectors
        ```