### 原生客户端

1. maven引入包
    ```
    <dependency>
        <groupId>org.apache.zookeeper</groupId>
        <artifactId>zookeeper</artifactId>
        <version>3.6.1</version>
    </dependency>
    ```
2. 实例代码
    ```
    private final static String CONNECTION_IP_PORT = "127.0.0.1:2181";

    private static CountDownLatch countDownLatch = new CountDownLatch(1);

    public static void main(String[] args) throws Exception {
        //创建连接
        ZooKeeper zooKeeper = new ZooKeeper(CONNECTION_IP_PORT, 5000, new Watcher() {
            //watch监听
            public void process(WatchedEvent event) {
                if(event.getState()==Event.KeeperState.SyncConnected){
                    //等待连接创建成功后，再执行后面的
                    countDownLatch.countDown();
                    System.out.println("连接已经创建："+event.getState());
                }
                if(event.getType()==Event.EventType.NodeDataChanged){
                    System.out.println("数据已经修改");
                }
            }
        });
        //等待连接创建成功
        countDownLatch.await();

        //创建节点，所有人可以访问，永久节点
        zooKeeper.create("/zookeeper-","zookeeper-value".getBytes(),
                ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        //获取节点
        //节点的状态信息，会写进去
        Stat stat = new Stat();
        //true代表可以进入前面的watch监听
        byte[] data = zooKeeper.getData("/zookeeper-", true, stat);
        System.out.println("获取到的数据："+new String(data));

        //原生的客户端，watch具有一次性，例如上面的监听，每次set前都需要调用get

        //设置节点值、删除节点值、权限和上面类似
    }
    ```
3. 原生客户端的弊端
    - 会话连接是异步的，所以做了一个同步处理
    - watch需要重复注册，是一次性的
    - 开发复杂性高

### ZK-client

1. maven引入包，基于原生客户端的功能增强，一般常用
    ```
    <dependency>
        <groupId>com.101tec</groupId>
        <artifactId>zkclient</artifactId>
        <version>0.11</version>
    </dependency>
    ```
2. 实例代码
    ```
    private final static String CONNECTION_IP_PORT = "127.0.0.1:2181";

    public static void main(String[] args) throws Exception {
        //创建连接
        ZkClient zkClient = new ZkClient(CONNECTION_IP_PORT,5000);

        //递归创建
        zkClient.createPersistent("/a/b/c",true);

        //添加监听
        zkClient.subscribeDataChanges("/", new IZkDataListener() {
            public void handleDataChange(String dataPath, Object data){
                System.out.println("数据变化");
            }

            public void handleDataDeleted(String dataPath){
                System.out.println("数据删除");
            }
        });

        //级联删除
        zkClient.deleteRecursive("/a");
    }
    ```
### Curator

1. maven引入包，基于原生客户端的功能增强，常用
    ```
    <dependency>
        <groupId>org.apache.curator</groupId>
        <artifactId>curator-framework</artifactId>
        <version>5.1.0</version>
    </dependency>
    <dependency>
        <groupId>org.apache.curator</groupId>
        <artifactId>curator-recipes</artifactId>
        <version>5.1.0</version>
    </dependency>
    ```
2. 实例代码
    ```
    private final static String CONNECTION_IP_PORT = "127.0.0.1:2181";

    public static void main(String[] args) throws Exception {
        //获取连接
        CuratorFramework curatorFramework = CuratorFrameworkFactory.
                builder().
                connectString(CONNECTION_IP_PORT).
                sessionTimeoutMs(5000).
                //重试三次，失败一次，间隔时间累加
                retryPolicy(new ExponentialBackoffRetry(1000, 3)).
                build();
        curatorFramework.start();
        System.out.println("创建连接成功");

        //创建递归节点
        curatorFramework.
                create().
                creatingParentsIfNeeded().
                withMode(CreateMode.PERSISTENT).
                forPath("/a/b/c","value".getBytes());

        //查询节点
        Stat stat = new Stat();
        byte[] data = curatorFramework.
                getData().
                storingStatIn(stat).
                forPath("/a/b/c");
        System.out.println(new String(data));

        //更新
        Stat statUpdate = curatorFramework.
                setData().
                forPath("/a/b/c", "value-new".getBytes());

        //递归删除节点
        curatorFramework.
                delete().
                deletingChildrenIfNeeded().
                forPath("/a");

        //异步操作
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        curatorFramework.
                create().
                creatingParentsIfNeeded().
                withMode(CreateMode.PERSISTENT).
                inBackground(new BackgroundCallback() {
                    public void processResult(CuratorFramework curatorFramework, CuratorEvent curatorEvent) {
                        System.out.println("异步执行完成");
                        countDownLatch.countDown();
                    }
                },executorService).
                forPath("/1/2/3","values".getBytes());
        countDownLatch.await();
        executorService.shutdown();

        //事务操作
        curatorFramework.
                inTransaction().
                create().
                forPath("/x","x-value".getBytes()).
                and().
                setData().
                forPath("/x","x-update_value".getBytes()).
                and().
                commit();

        //watch
        //PatchCache监视一个路径下子节点的创建、删除、更新
        //NodeCache监视一个节点的创建、删除、更新
        //TreeCache=PatchCache+NodeCache的合体

    }
    ```