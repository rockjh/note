### 服务的注册与发现

1. 定义
    - 服务的注册，一个服务启动后，会向zk注册，发送服务的ip、端口信息
    - 服务的发现，一个服务要远程调用其他服务之前，会从zk获取最新的服务列表信息，获取服务列表时，会添加一个监听，监听到服务节点变化，zk主动推送最新的列表
2. 服务注册的实现逻辑
    - 服务项目启动后，向zk发起一个会话连接，准备ip+port
    - zk中如果不存在固定服务路径，就新建
    - 向zk新建一个固定服务路径有序的临时节点，值为ip+port
        - 临时节点，是因为如果该服务网络中断后，也就是会话会终止，节点会自动删除，就不在列表了
        - 有序节点，集群模式下，一个服务会有多个节点
    ```
    //服务基本路径
    private static final String BASE_SERVICES = "/services";
    //服务a的路径
    private static final String SERVICE_NAME = "/a";

    public void registerA(String address, int port) throws Exception{
        
        String path = BASE_SERVICES + SERVICE_NAME;
        ZooKeeper zooKeeper = new ZooKeeper("127.0.0.1:2181", 5000, watchedEvent -> {});
        Stat stat = zooKeeper.exists(path, false);
        if (stat == null) {
            //创建服务的根节点
            zooKeeper.create(path, "".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        }
        String server_path =address+":"+port;
        //注册临时有序的节点
        zooKeeper.create(path+"/child",server_path.getBytes(),ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.EPHEMERAL_SEQUENTIAL);
        System.out.println("产品服务创建成功");
    }
    ```
3. 服务发现的实现逻辑
    - 服务启动后，会向zk获取固定服务路径（其他服务路径），并添加监听，节点变化时，会收到通知，得到一个服务列表进行缓存
    - 在需要访问这些服务时，取出服务列表，进行一个负载均衡访问，如轮询、随机等
    ```
    //服务基本路径
    private static final String BASE_SERVICES = "/services";
    //服务a的路径
    private static final String SERVICE_NAME = "/a";
    private final String path = BASE_SERVICES+SERVICE_NAME;
    private List<String> serviceList;

    private ZooKeeper zooKeeper;

    public void discoverA(){
        try {
            zooKeeper = new ZooKeeper("127.0.0.1:2181", 5000, watchedEvent -> {
                if(watchedEvent.getType()==Watcher.Event.EventType.NodeChildrenChanged &&
                    watchedEvent.getPath().equals(path)){
                    updateServiceList();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        updateServiceList();
    }

    private void updateServiceList() {

        try {
            serviceList = new Vector<>();
            List<String> childrenList = null;
            childrenList = zooKeeper.getChildren(path,true);
            for (String children : childrenList) {
                byte[] data = zooKeeper.getData(path + "/" + children, false, null);
                serviceList.add(new String(data,"utf-8"));
            }
        } catch (KeeperException | InterruptedException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
    ```

### 分布式锁

1. 基于同名节点的分布式锁

    - 分布式锁，大致分为三个过程tryLock、waitLock、unLock
    - 基本思想，创建临时节点、添加临时节点的删除监听、删除锁释放客户端连接
    
    ```
    private final static String CONNECTION_IP_PORT = "127.0.0.1:2181";

    private final static String PATH = "/lock";

    //创建连接
    private ZkClient zkClient;
    private CountDownLatch countDownLatch;

    public boolean tryLock() {
        try {
            zkClient = new ZkClient(CONNECTION_IP_PORT, 5000);
            zkClient.createEphemeral(PATH);
            return true;
        } catch (Exception e) {
            //如果已经存在节点会抛出异常
            e.printStackTrace();
        }
        return false;
    }

    public void waitLock() {
        IZkDataListener iZkDataListener = new IZkDataListener() {
            @Override
            public void handleDataChange(String dataPath, Object data) {
                if (countDownLatch != null) {
                    countDownLatch.countDown();
                }
            }

            @Override
            public void handleDataDeleted(String dataPath) {

            }
        };
        //注册事件
        zkClient.subscribeDataChanges(PATH, iZkDataListener);
        //如果节点存在
        if (zkClient.exists(PATH)) {
            countDownLatch = new CountDownLatch(1);
            try {
                countDownLatch.await();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //删除监听
        zkClient.unsubscribeDataChanges(PATH, iZkDataListener);
    }

    public void unLock() {
        zkClient.delete(PATH);
        //重点，不关的话，临时节点删除有延迟
        zkClient.close();
        zkClient = null;
    }
    ```
2. 高性能分布式锁
    
    - 同名节点分布式锁有一个问题，在释放锁的一瞬间，如果有很多竞争锁，会导致羊群效应
    - 基本思想，创建临时有序节点，如果添加的是第一个，直接就获取锁、添加有序临时节点前一个节点的删除监听、删除锁释放客户端连接
    > 分布式的消息队列也是使用的这种思想

    ```
    private final static String CONNECTION_IP_PORT = "127.0.0.1:2181";

    private final static String PATH = "/lock";

    //创建连接
    private ZkClient zkClient;
    private CountDownLatch countDownLatch;
    private String currentPath;
    private String beforePath;

    public boolean tryLock() {
        zkClient = new ZkClient(CONNECTION_IP_PORT, 5000);
        //不存在根节点先创建
        if (!zkClient.exists(PATH)) {
            zkClient.createPersistent(PATH);
        }
        //为空则表示是第一次加锁
        if (currentPath == null || currentPath.length() <= 0) {
            currentPath = zkClient.createEphemeralSequential(PATH + "/", "lock");
        }
        //获取所有临时节点并排序，临时节点名称为自增的字符串
        List<String> childrenList = zkClient.getChildren(PATH);
        Collections.sort(childrenList);

        //如果当前节点排的就是第一，获取锁成功
        if (currentPath.equals(PATH + "/" + childrenList.get(0))) {
            return true;
        } else {
            //当前节点不是第一个节点，就找出当前节点的上一个节点
            int index = Collections.binarySearch(childrenList, currentPath.substring(PATH.length() + 1));
            beforePath = PATH + "/" + childrenList.get(index);
        }
        return false;
    }

    public void waitLock() {
        IZkDataListener iZkDataListener = new IZkDataListener() {
            @Override
            public void handleDataChange(String dataPath, Object data) {
                if (countDownLatch != null) {
                    countDownLatch.countDown();
                }
            }

            @Override
            public void handleDataDeleted(String dataPath) {

            }
        };
        //注册事件
        zkClient.subscribeDataChanges(beforePath, iZkDataListener);
        //如果节点存在
        if (zkClient.exists(beforePath)) {
            countDownLatch = new CountDownLatch(1);
            try {
                countDownLatch.await();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //删除监听
        zkClient.unsubscribeDataChanges(beforePath, iZkDataListener);
    }

    public void unLock() {
        zkClient.delete(PATH);
        //重点，不关的话，临时节点删除有延迟
        zkClient.close();
        zkClient = null;
    }
    ```

### 集群选举

1. 定义，leader领着一堆follower，如果leader挂了，那么所有的follower都会去竞争称为新的leader
2. 实现原理和基于同名节点的分布式锁一模一样，都是竞争然后出结果，这里不用考虑羊群效应，因为follower节点不会有很多

### 配置中心

1. 实例，mybatis访问数据库，在不重启项目的情况下，我需要动态的修改数据源
2. 实现原理，使用zk的监听，监听到存储数据源的节点变化后，就重新加载数据源
    - 获取spring中的datasource的bean
    - 重写datasource类，在数据源变化时，清空前面的连接池配置，重新加载新的数据源

### ZK注意事项

1. 数据和日志清理，dataDir和dataLogDir随着时间的推移变得庞大
    - 自己编写shell脚本，保留最新的n个脚本
    - 使用zk自带的zkClient.sh保留最新的n个文件zkClient.sh -n 15
    - 配置autopurge.snapRetainCount和autopurge.purgeInterval两个参数配合使用
2. zk服务端有最大客户端连接数限制，默认60，使用maxClientCnxns参数配置
3. 磁盘IO性能直接制约zk的更新操作速度，建议
    - 使用单独的磁盘
    - jvm堆内存设置要当心
4. 磁盘管理集群数量
    - 集群中机器的数量并非越多越好，一个写操作需要半数以上的节点ack，所以集群机器数量越多可以抗挂点的数量越多，越可靠，但是吞吐量越差，集群数量必须为奇数
    - zk是基于内存进行读写操作的，有时候会进行广播消息，不建议在节点存取容量比较大的数据