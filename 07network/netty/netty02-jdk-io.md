### BIO

1. IO类概述表
![IO类概述表.png](http://upload-images.jianshu.io/upload_images/7446289-412ce618a1b0ce01.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
2. 常见流
    - RandomAccessFile 跳跃读文件,原理是调用一个native方法移动指向当前操作字节数组指针的位置,所以一般对于流的操作我们就要按照顺序读写，就可以获得完整的文件
    - 管道流主要是为多个线程提供通信,一个输入管道，一个输出管道，一边输入一边输出
    - FileInputStream文件流
    - ByteArrayOutputStream允许我们以字节的形式获取值toByteArray()方法
    - ObjectOutputStream主要是对象序列化存储的一个东西，他需要一个将值输出在构造参数中的流中，相当于是一个包装流吧
    - BufferedOutputStream为输出流提供缓存，能提供很多IO性能，不用每次都去磁盘或者网络一次读取一个或者几个字节
    - DataInputStream包装输入流，可以使得输入流直接输入几种基本数据类型，而不是只能输入字节数据
    - System.in, System.out, System.error(注：Java标准输入、输出、错误输出)
    > 不管输入流还是输出流都要有一个数据源,最常见的一个列子就是实例化一个输入流，向其中写东西，然后输出流，输出到文件或者打印,输入流是read，输出流是write

3. 一个服务器ServerSocket和客户端Socket，每来一个Socket客户端，都会新建一个线程供这个客户端读写，一般来说有几个客户端连接，就会有几个线程，客户端断开，线程销毁

### NIO
1. 定义，NIO是面向通道和缓冲区的，数据总是从通道中读到buffer缓冲区内，或者从buffer缓冲区内写入通道，Channel通道和Buffer缓冲区是NIO的核心，几乎在每一个IO操作中使用它们，Selector选择器则允许单个线程操作多个通道，对于高并发多连接很有帮助

2. Buffer，缓冲区
    - 类型有ByteBuffer、MappedByteBuffer、CharBuffer、DoubleBuffer、FloatBuffer、IntBuffer、LongBuffer、ShortBuffer，对应了几大基本数据类型
    - 每个Buffer可以用DirectByteBuffer和HeapByteBuffer选择，分别是直接内存和堆内存，直接内存少一次内核拷贝，但销毁和创建成本更高，如果要使用建议采用池化
    - Buffer操作步骤
        - 将数据写入buffer，put方法
        - 调用flip将写模式改为读模式
        - 从buffer中读取数据，进行操作
        - 调用clear清除整个buffer数据或者调用compact清空已读数据
    - Buffer的属性
        - capacity容量，该buffer最多存储的字节数
        - position位置，写模式下，position从0到capacity-1，变更为读模式后，position归零，边读边移动
        - limit限制，写模式下，代表我们能写的最大量为capacity，读模式下，变更为原position位置，即有数据的位置
        ![buffer_modes.png](http://upload-images.jianshu.io/upload_images/7446289-9e2df5fac3a189e0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
    - Buffer Api
        - 通过allocate()方法为buffer分配内存大小
            ```
            //如开辟一个48字节的ByteBuffer buffer：ByteBuffer.allocate(48)
            ```
        - 写数据可以通过通道写，如：FileChannel.read(buffer)；也可以通过put方法来写数据，如：buffer.put(127)
        
        - flip()翻转方法，将写模式切换到读模式，position归零，设置limit为之前的position位置
        
        - 读数据可以读到通道，如：FileChannel.write(buffer)；也可以调用get()方法读取，byte aByte=buffer.get()
        
        - buffer.rewind()将position置0，limit不变，这样我们就可以重复读取数据啦
        
        - buffer.clear()将position置为0，limit设置为capacity，这里并没有删除buffer里面的数据，只是把标记位置改了；
        
        - buffer.compact()清除已读数据，这里也没有删除数据，将position设置为未读的个数，将后面几个未读的字节顺序的复制到前面的几个字节，limit设置为capacity，比如buffer容量3个字节，读取hello，在读取了2个字节后我就调用了compact()方法，那么此时position为1，limit为3，buffer内部存储的数据buff[0]='l',buff[1]='e',buff[2]='l'，因为有一个'l'没有读完，将'l'提取到最前面供下次读取
        
        - mark()可以标记当前的position位置，通过reset来恢复mark位置，可以用来实现重复读取满足条件的数据块
        
        - equals()两个buffer相等需满足，类型相同，buffer剩余（未读）字节数相同，所有剩余字节数相同
        
        - compareTo()比较buffer中的剩余元素，只不过此方法适合排序
3. Channel，通道
    - Channel的实现
        - FileChannel用于文件数据的读写，transferTo()方法可以将通道的数据传送至另外一个通道，完成数据的复制
        - DatagramChannel用于UDP数据的读写
        - SocketChannel用于TCP的数据读写，通常我们所说的客户端套接字通道
        - ServerSocketChannel允许我们监听TCP链接请求，通常我们所说的服务端套接字通道，每一个请求都会创建一个SocketChannel
    - Scatter和Gather
        - java nio在Channel实现类也实现了Scatter和Gather相关类
        - Scatter.read()是从通道读取的操作能把数据写入多个buffer，即一个通道向多个buffer写数据的过程，但是read必须写满一个buffer后才会向后移动到下一个buffer，因此read不适合大小会动态改变的数据。代码如下：
            ```
            ByteBuffer header = ByteBuffer.allocate(128);
            ByteBuffer body = ByteBuffer.allocate(1024);
            ByteBuffer[] bufferArray = { header, body };
            channel.read(bufferArray);
            ```
        - Gather.write()是从可以把多个buffer的数据写入通道，write是只会写position到limit之间的数据，因此写是可以适应大小动态改变的数据。代码如下
            ```
            ByteBuffer header = ByteBuffer.allocate(128);
            ByteBuffer body = ByteBuffer.allocate(1024);
            //write data into buffers
            ByteBuffer[] bufferArray = { header, body };
            channel.write(bufferArray);
            ```
        > 在有些场景会非常有用，比如处理多份需要分开传输的数据，举例来说，假设一个消息包含了header和body，我们可能会把header和body分别放在不同的buffer
4. Selector，选择器
    - 定义，Selector用于检查一个或多个NIO Channel的状态是否可读可写，这样就可以实现单线程管理多个Channels，也就是可以管理多个网络连接，NIO非阻塞主要就是通过Selector注册事件监听，监听通道将数据就绪后，就进行实际的读写操作，因此前面说的IO两个阶段，一阶段NIO仅仅是异步监听，二阶段就是同步实际操作数据
    - Selector监听事件类别
        - SelectionKey.OP_CONNECT是Channel和server连接成功后，连接就绪
        - SelectionKey.OP_ACCEPT是server Channel接收请求连接就绪
        - SelectionKey.OP_READ是Channel有数据可读时，处于读就绪
        - SelectionKey.OP_WRITE是Channel可以进行数据写入是，写就绪
    - 使用Selector的步骤
        - 创建一个Selector
            ```
            Selector selector = Selector.open()
            ```
        - 注册Channel到Selector上面
            ```
            //将Channel切换为非阻塞的
            channel.configureBlocking(false)
            //绑定事件
            Selector：SelectionKey key = channel.register(selector, SelectionKey.OP_READ)
            ```
        - SelectionKey.OP_READ为监听的事件类别，如果要监听多个事件，可利用位的或运算结合多个常量
            ```
            int interestSet = SelectionKey.OP_READ | SelectionKey.OP_WRITE;
            ```
        > 需要使用Selector的Channel必须是非阻塞的，FileChannel不能切换为非租塞的，因此FileChannel不适用于Selector
    - Selector Api
        - Selector.select()方法是阻塞的，因此可以放心的将它写在while(true)中，不用担心cpu会空转
        - Selector.wakeup()唤醒select()造成的阻塞，可能是有新的事件注册，优先级更高的事件触发（如定时器事件），希望及时处理。其原理是向通道或者连接中写入一个字节，阻塞的select因为有IO事件就绪，立即返回

5. 示例程序

    - TCPServer
        ```
        public class TCPServer {

            private static final int bufferSize = 1024;
            private static final long timeOut = 3000;// 超时时间
            private static final int listenPort = 1993;// 本地监听端口

            public static void main(String[] args) throws Exception {
                Selector selector = Selector.open();
                ServerSocketChannel listenerChannel = ServerSocketChannel.open();// 创建监听通道，专门用来监听指定的本地端口
                listenerChannel.socket().bind(new InetSocketAddress(listenPort));// 将listenerChannel的socket绑定为本地服务器（IP+prot）绑定
                listenerChannel.configureBlocking(false);
                // 将选择器绑定到监听信道,只有非阻塞信道才可以注册选择器.并在注册过程中指出该信道可以进行Accept操作
                listenerChannel.register(selector, SelectionKey.OP_ACCEPT);
                TCPProtocolImpl protocol = new TCPProtocolImpl(bufferSize);

                while (true) {
                    if (selector.select(timeOut) == 0) {// 监听注册的通道，当其中有注册的IO时该函数返回（3000ms没有反应返回0），操作可以进行，并添加对应的SelectorKey
                        System.out.println("It haven't I/O now, please wait!");
                        continue;
                    }

                    Iterator<SelectionKey> keyIter = selector.selectedKeys().iterator();
                    while (keyIter.hasNext()) {
                        SelectionKey key = keyIter.next();
                        try {
                            if (key.isAcceptable()) {
                                protocol.handleAccept(key);
                            }
                            if (key.isReadable()) {
                                protocol.handleRead(key);
                            }
                        } catch (IOException e) {
                            keyIter.remove();
                            //抛出异常取消该客户端的通道在Selector上的注册
                            key.cancel();
                            continue;
                        }
                        //每次select之后就会把准备好的key放在selectedKeys中，操作完之后要删除，否则下一轮循环中他还会存在，但是又没有select会报NullPointException
                        keyIter.remove();
                    }
                }
            }
        }

        class TCPProtocolImpl{
            private int bufferSize;

            public TCPProtocolImpl() {
                super();
            }

            public TCPProtocolImpl(int bufferSize) {
                super();
                this.bufferSize = bufferSize;
            }

            public void handleAccept(SelectionKey key) throws IOException {
                // 返回创建此键的通道，接受客户端建立连接的请求，并返回SocketChannel对象
                SocketChannel clientChannel = ((ServerSocketChannel) key.channel()).accept();
                clientChannel.configureBlocking(false);
                // 将clientChannel注册到服务端的selector中
                clientChannel.register(key.selector(), SelectionKey.OP_READ, ByteBuffer.allocate(bufferSize));
            }

            public void handleRead(SelectionKey key) throws IOException {
                // 获取客户端通信的通道
                SocketChannel clientChannel = (SocketChannel) key.channel();
                ByteBuffer buffer = (ByteBuffer) key.attachment();
                buffer.clear();
                // 从客户端通道读取信息到buffer缓冲区中(并返回读到信息的字节数)
                long bytesRead = clientChannel.read(buffer);
                if (bytesRead == -1) {
                    clientChannel.close();
                } else {
                    buffer.flip();
                    // 将字节转化为为UTF-8的字符串
                    String receivedString = Charset.forName("UTF-8").newDecoder().decode(buffer).toString();
                    System.out.println("接收到来自：" + clientChannel.socket().getRemoteSocketAddress() + "发来的信息：" + receivedString);
                    String msgSendToClient = "已接收到你的信息：" + receivedString + "正在处理中";
                    buffer = ByteBuffer.wrap(msgSendToClient.getBytes("UTF-8"));
                    clientChannel.write(buffer);
                    // 设置为下一次读取或是写入做准备
                    key.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                }
            }
        }
        ```
    - TCPClient
        ```
        public class TCPClient {

            // 通道选择器，用于管理客户端的通道
            private Selector selector;

            // 与服务器通信的通道
            SocketChannel socketChannel;

            // 要连接的服务器的IP
            private String hostIp;

            // 要连接的远程服务器在监听的端口
            private int hostListeningPort;
            
            static TCPClient client;
            
            static boolean mFlag = true;

            public TCPClient(String hostIp, int hostPort) throws IOException {
                this.hostIp = hostIp;
                this.hostListeningPort = hostPort;
                init();
            }

            private void init() throws IOException {
                // 打开监听通道
                socketChannel = SocketChannel.open(new InetSocketAddress(hostIp, hostListeningPort));
                socketChannel.configureBlocking(false);
                
                // 创建选择器，并把通道注册到选择器中
                selector = Selector.open();
                socketChannel.register(selector, SelectionKey.OP_READ);
                
                new TCPClientReadThread(selector);
            }
            
            /**
            * 发送字符串到服务器
            * @param message
            * @throws IOException
            */
            public void sendMsg(String message) throws IOException{
                ByteBuffer writeBuffer = ByteBuffer.wrap(message.getBytes("UTF-8"));
                socketChannel.write(writeBuffer);
            }
            
            public static void main(String[] args) throws IOException {
                client = new TCPClient("127.0.0.1", 1993);
                new Thread(){
                    @Override
                    public void run(){
                        try{
                            client.sendMsg("test----~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                            while(mFlag){
                                Scanner scan = new Scanner(System.in);
                                String string = scan.next();
                                client.sendMsg(string);
                            }
                        }catch (Exception e) {
                            mFlag = false;
                        }finally{
                            mFlag = false;
                        }
                        super.run();
                    }
                }.start();
            }
        } 

        class TCPClientReadThread implements Runnable {
            private Selector selector;

            public TCPClientReadThread(Selector selector) {
                super();
                this.selector = selector;
                new Thread(this).start();
            }

            @Override
            public void run() {
                try {
                    while (selector.select() > 0) {// select()方法只能使用一次，用了之后就会自动删除,每个连接到服务器的选择器都是独立的
                        // 遍历每个有IO操作Channel对应的SelectionKey
                        for (SelectionKey sk : selector.selectedKeys()) {
                            if (sk.isReadable()) {
                                // 使用NIO读取Channel中的数据
                                SocketChannel sc = (SocketChannel) sk.channel();
                                ByteBuffer buffer = ByteBuffer.allocate(1024);
                                sc.read(buffer);
                                buffer.flip();
                                String receivedString = Charset.forName("UTF-8").newDecoder().decode(buffer).toString();
                                System.out.println("接收到来自服务器：" + sc.socket().getRemoteSocketAddress() + "的信息：" + receivedString);
                                sk.interestOps(SelectionKey.OP_READ);
                            }
                            selector.selectedKeys().remove(sk);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        ```
    - Issues
        - 在使用TcpClient和TcpServer时，最开始客户端结束之后，服务端catch住了异常，但是并没有处理，那个客户端的Channel还是可以从Selector出来，原因是注册了这个事件没有取消注册，在catch中取消注册该事件就行，SelectionKey.cancel()
        - localhost是一个域名，通常指向127.0.0.1可在host文件配置
        - 127.0.0.1指本机地址环回地址，只有自己的机器可以访问
        - 0.0.0.0表示网络中的本机地址，即如果本机器连接了两个网络，在注册服务端时使用0.0.0.0，这两个网络的其他主机都可以访问这个服务端
        > 我们可以理解为本机有三块网卡，一块网卡叫做 loopback（这是一块虚拟网卡）127.*整个网段用作loopback网络接口的默认地址，按照惯例通常设置为127.0.0.1这个地址和网络无关，这个是环回地址，直接找本地地址，如果服务端绑定这个地址，那就只有本地服务器才可以访问这个服务端，另外一块网卡叫做 ethernet （这是你的有线网卡），还有一块网卡叫做 wlan（这是你的无线网卡）

### AIO

1. AsynchronousServerSocketChannel
2. AsynchronousSocketChannel
3. CompletionHandler

### 路径

1. Object.class.getResource("/").getPath();获取classpath路径

2. Object.class.getResource("").getPath();获取当前类路径，即包括包