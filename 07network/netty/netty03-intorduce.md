### 为什么要用netty

1. netty提供异步、事件驱动的网络应用程序框架和工具
2. 屏蔽具体的细节，用以快速开发高性能高可靠的网络服务器和客户端程序
3. 原生的jdk nio如果去处理真实的网络通信，需要做的事情很多，类似客户端的权限、信息格式封装、各种类型的数据读取、断线重连、心跳等

### Reactor反应器
1. 单线程反应器模式，所有的事情都由这个反应器来处理，包括解码、编码、IO、发送数据等
2. 单线程反应器多线程工作者模式，接受连接、读写、网络数据交给反应器线程去处理，编码解码计算全部放在工作者线程池中去做，做完了之后交给反应器线程去处理
3. 多反应器多工作者模式，高并发多客户端时，反应器线程一般分为主反应器线程和子反应器线程（可以是一个线程池），主反应器线程主要负责接收客户端的连接请求，建立连接后，将socket整个交给子反应器线程去和客户端进行网络通信

### 核心组件

1. channel，比jdk中的channel范围更窄，只针对网络编程
2. callback， 注册一个方法，告诉操作系统，操作系统完成后，回调出发时间
3. future，netty的future继承了jdk中的future，向future中注册一个监听器，执行完成后，自动的调用成功或失败的业务逻辑
    ```
    Channel channel = ...;
    // Does not block
    ChannelFuture future = channel.connect(new InetSocketAddress("192.168.0.1", 25)); 
    //可以添加多个监听
    future.addListener(new ChannelFutureListener() {
        @Override
        public void operationComplete(ChannelFuture future) {
            //判断是否成功，成功进行处理
            if (future.isSuccess()){
                ByteBuf buffer = Unpooled.copiedBuffer(
                        "Hello",Charset.defaultCharset()); ChannelFuture wf = future.channel()
                        .writeAndFlush(buffer);
                // TODO: 2020/1/9  
            } else {
                //失败，找到失败原因，进行异常处理
                Throwable cause = future.cause();
                cause.printStackTrace(); 
            }
        });
    }
    ```
    > callback的关注点是后续的处理；future关注点是异步，他们两个是相辅相成的，future一般都是通过wait、notify或者锁机制来实现的
4. 事件，分别为入站事件（连接、建立连接、读取数据等）和出站事件（打开一个连接、写数据等），对于服务器客户端给服务器的就是入站，服务器给客户端就是出站
5. 事件发生后，会被分发，被很多个ChannelHandler组成的一个链上进行流通，一般进行网络开发也就是写各种ChannelHandler，netty有很多内置的常见的ChannelHandler

### Hello Netty

1. EchoServerHandlerAdapter
    ```
    //这个handler可以在多个channel中共享，这个类需要是线程安全的类
    @ChannelHandler.Sharable
    public class EchoServerHandlerAdapter extends ChannelInboundHandlerAdapter {


        /**
        * 服务器读取到网络数据后的处理
        */
        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) {
            ByteBuf in = (ByteBuf) msg;
            System.out.println("Server received: " + in.toString(CharsetUtil.UTF_8));
            //将接收到的消息写给发送者，而不冲刷出站消息
            ctx.write(in);
        }

        /**
        * 服务器读取完网络数据后的处理，可能channelRead一次性读取不完数据
        */
        @Override
        public void channelReadComplete(ChannelHandlerContext ctx) {
            //将未决消息冲刷到远程节点，并且关闭该Channel
            ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
            //打印异常
            cause.printStackTrace();
            //关闭通道
            ctx.close();
        }
    }
    ```
2. EchoServer
    ```
    public class EchoServer {

        private EchoServerHandlerAdapter echoServerHandlerAdapter = new EchoServerHandlerAdapter();

        private int port = 9999;

        public static void main(String[] args) throws InterruptedException {
            new EchoServer().start();
        }

        private void start() throws InterruptedException {
            //线程组
            EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
            try {
                //服务器启动必备
                ServerBootstrap serverBootstrap = new ServerBootstrap();
                serverBootstrap.
                        group(eventLoopGroup).
                        //指定所使用的的NIO传输channel
                        channel(NioServerSocketChannel.class).
                        //使用指定的端口设置套接字地址
                        localAddress(new InetSocketAddress(port)).
                        childHandler(new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel(SocketChannel socketChannel) {
                                //添加一个EchoServerHandler到子channelPipeline
                                //当有多个客户端接进来的时候，这里会初始化多次
                                socketChannel.pipeline().addLast(echoServerHandlerAdapter);
                            }
                        });
                //异地绑定服务器，调用sync方法阻塞等待直到绑定完成
                ChannelFuture sbChannelFuture = serverBootstrap.bind().sync();
                //获取channel的closeFuture,并且阻塞直到当前线程完成
                sbChannelFuture.channel().closeFuture().sync();
            } finally {
                //关闭EventLoopGroup释放所有资源
                eventLoopGroup.shutdownGracefully().sync();
            }

        }
    }
    ```
3. EchoClientHandlerAdapter
    ```
    public class EchoClientHandlerAdapter extends SimpleChannelInboundHandler<ByteBuf> {

        @Override
        public void channelActive(ChannelHandlerContext ctx) {
            //当被通知channel是活跃的时候，发送一条消息
            ctx.writeAndFlush(Unpooled.copiedBuffer("hello netty", CharsetUtil.UTF_8));
        }

        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
            //打印异常，关闭channel
            cause.printStackTrace();
            ctx.close();
        }

        @Override
        protected void channelRead0(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf) {
            //记录已经接受消息的转储
            System.out.println("Client received: " + byteBuf.toString(CharsetUtil.UTF_8));
        }
    }
    ```
4. EchoClient
    ```
    public class EchoClient {

        private int serverPort = 9999;

        public static void main(String[] args) throws InterruptedException {
            new EchoClient().start();
        }

        private void start() throws InterruptedException {
            //线程组
            EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
            try {
                //创建Bootstrap
                Bootstrap bootstrap = new Bootstrap();
                bootstrap.
                        group(eventLoopGroup).
                        //指定所使用的的NIO传输channel
                        channel(NioSocketChannel.class).
                        //使用指定的端口设置套接字地址
                        remoteAddress(new InetSocketAddress("127.0.0.1", serverPort)).
                        handler(new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel(SocketChannel socketChannel) {
                                //添加一个EchoServerHandler到子channelPipeline
                                socketChannel.pipeline().addLast(new EchoClientHandlerAdapter());
                            }
                        });
                //连接远程节点服务器，调用sync方法阻塞等待直到绑定完成
                ChannelFuture bChannelFuture = bootstrap.connect().sync();
                //获取channel的closeFuture,并且阻塞直到当前线程完成
                bChannelFuture.channel().closeFuture().sync();
            } finally {
                //关闭EventLoopGroup释放所有资源
                eventLoopGroup.shutdownGracefully().sync();
            }

        }
    }
    ```