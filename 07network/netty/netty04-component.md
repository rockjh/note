### Channel

1. 定义，基本的IO操作（bind,connect,read,write）依赖于底层网络传输所提供的原语，在基于java的网络编程中，其基本构造是class socket，netty的channel接口所提供的api大大降低了直接使用socket类的复杂性。此外channel也是拥有许多预定义
2. 内置的传输
    - NIO，使用java.nio.channels包作为基础——基于 选择器的方式
    - Epoll，由JNI驱动的epoll()和非阻塞IO。这个传输支持只有在Linux上可用的多种特性，如SO_REUSEPORT，比NIO传输更快，而且是完全非阻塞的，底层使用的IO复用
    - OIO，使用java.net包作为基础——使用阻塞流
    - Local，可以在VM内部通过管道进行通信的本地传输
    - Embedded，允许使用ChannelHandler而又不需要一个真正的基于网络的传输。这在测试你的ChannelHandler实现时非常有用
3. 预定义Channel，一般每个内置的传输都会有服务端和客户端使用的
    - EmbeddedChannel
    - LocalServerChannel
    - NioDatagramChannel
    - NioStcpChannel
    - NioSocketChannel

### ChannelFuture

1. 定义，netty所有操作都是异步的，因为一个操作不会立即返回，所以我们需要一种用于在之后某个时间点确定其结果的方法，ChannelFuture可以实现，他的addListener方法注册了一个ChannelFutureListener，以便在某个操作完成时（无论是否成功）得到通知

2. ChannelFuture究竟什么时候被执行可能取决于若干因素，因此不能准确的预测，但是可以肯定的是他将会执行

### EventLoop

1. 定义，EventLoop定义了netty核心抽象，用于处理连接的生命周期所发生的的事件
2. Channel，Thread，EventLoop，EventLoopGroup之间的关系如下：
    - 一个EventLoopGroup包含多个EventLoop
    - 一个EventLoop在他的生命周期中只和一个Thread绑定
    - 所有由EventLoop处理的I/O事件都将在他专有的Thread上处理
    - 一个Channel在他的生命周期内只注册一个EventLoop
    - 一个EventLoop可能会被分配给一个或多个Channel
3. NioEventLoopGroup中会创建是一个线程组，里面包含了很多的EventLoop
    ```
    //eventLoopGroup中的属性
    private final EventExecutor[] children;
    private final AtomicInteger childIndex = new AtomicInteger();
    private final AtomicInteger terminatedChildren = new AtomicInteger();
    private final Promise<?> terminationFuture = new DefaultPromise(GlobalEventExecutor.INSTANCE);
    private final EventExecutorChooser chooser;
    
    //eventLoop的抽象基类
    SingleThreadEventLoop，相当于一个单线程的线程池
    ```
> 一个给定的Channel的I/O操作都是由相同的Thread执行的，实际上消除了对于同步的需要

> ServerBootStrap中可以传入主线程和子线程Reactor
BootStrap不能

> ChannelInitializer也是一个channelHandler，添加后自己会移除

### ChannelHandler

1. 定义，它充当了所有处理入站和出站数据的应用程序逻辑的容器，ChannelHandler的方法是由网络事件触发的，ChannelHandler主要分为ChannelInboundHandler（入站）和ChannelOutboundHandler（出站）两大类，分别是从网络战接收和从往网络中发送数据时调用
    - ChannelInboundHandler也称为解码
    - ChannelOutboundHandler也称为编码
    - 自定义编码或解码器时，注意类型匹配
2. 用途

    - 将数据从一种格式转换为另一种格式
    - 提供异常通知
    - 提供Channel变为活动的或者非活动的通知
    - 提供当Channel注册到EventLoop或者从EventLoop注销时的通知
    - 提供有关用户自定义事件的通知
3. 多个handler时，需要调用消息传递，否则后续handler收不到消息
    ```
    ctx.fireChannelRead(msg)
    ```
4. 多个handler时，没有进行消息传递时，buffer用了池化，不释放一直在内存需要释放
    ```
    ReferenceCountUtil.release(msg);
    ```
5. 手写一个HTTP服务器，会用到netty中下面的这些内置类
    ```
    HttpResponseDecoder
    HttpResponseEncoder
    HttpObjectAggregator（请求聚合）
    HttpContentCompressor（增加压缩）
    FullHttpRequest
    FullHttpResponse
    ```
### ChannelPipeline

1. 定义，ChannelPipeline提供了ChannelHandler链的容器，并定义了用于在该链上传播入站和出站事件流的API，当Channel被创建时，它会被自动地分配到它专属的ChannelPipeline，ChannelHandler注册到ChannelPipeline的流程如下

    - 一个ChannelInitializer的实现被注册到ServerBootstrap或者Bootstrap中
    - 当ChannelInitializer.initChannel()方法被调用时，ChannelInitializer将在ChannelPipeline中安装一组自定义的ChannelHandler
    - ChannelInitializer将他自己从ChannelPipeline中移除
    - 在入站时，不会触发ChannelOutboundHandler的事件，出站时同理
    - 入站时，会从ChannelPipeline的头部开始顺序处理，一个ChannelHandler处理完接着下一个，直到ChannelPipeline末端
    出站时，会从ChannelPipeline的末端开始顺序处理，一个ChannelHandler处理完接着下一个，直到ChannelPipelime头部
    - 当ChannelHandler被添加到ChannelPipeline时，它将会被分配一个ChannelHandlerContext，其代表了ChannelHandler和ChannelPipeline之间的绑定，他可以用来直接获取底层的Channel，也可以用于写出站数据


2. 关于写出站数据有三个方案：

    - 在ChannelHandler的事件中直接调用ChannelHandlerContext.wiriteAndFlush()方法，这种方式会遵循当前ChannelHandler在ChannelPipeline中的顺序，往头部顺序处理
    - 在ChannelHandler的事件中直接调用ChannelHandlerContext.channel().writeAndFlush()方法，这种方式会从ChannelPipeline的尾部开始，往头部处理；或者在开始注册的时候取到Channel进行缓存，可以直接发消息
    - 在ChannelHandler的事件中直接调用ChannelHandlerContext.pipeline().writeAndFlush()方法，这种方式会从ChannelPipeline的尾部开始，往头部处理

    > ChannelPipeline实现了一种常见的设计模式，拦截过滤器。多个命令被链接在一起，其中一个命令的输出端将连接到命令行中下一个命令的输入端

    > 可以通过添加或者删除ChannelHandler实例来修改ChannelPipeline，可以通过这个特性来构建高度灵活的程序，每当某些特殊协议连接时，加上相关的ChannelHandler，断开之后再删除ChannelHandler

### ByteBuf

1. 向通道写数据的时候，只能写ByteBuf，不能直接写字符串，直接写字符串Channel不会处理也不会抛出异常
2. 分配方式
    ```
    ctx.alloc();
    ctx.channel().alloc()
    UnpooledByteBufAllocator
    PooledByteBufAllocator
    ```
3. 查找
    - 简单查找调用indexOf方法
    - 复杂查询forEachByte，ByteBufProcessor有内置简单的一些常量
4. 派生缓冲区，源ByteBuf的一个视图，派生缓冲区=源ByteBuf中的一部分，用的同一个内存地址
5. 引用计数，池化之后，当前这个内存区域有几个引用了，调用ReferenceCountUtil.release(msg)，引用计数减一
6. 工具类ByteBufUtil.hexDump()把ByteBuf中的数据转为16进制
7. bytebuf和JDK的byteBuffer对比的优点
    - 可以被用户自定义的缓冲区类型扩展
    - 通过内置的复合缓冲区类型实现了透明的零拷贝
    - 容量可以按需增长类似StringBuilder
    - 在读和写之间不需要调用flip方法
    - 读和写是用了不同的索引
    - 支持方法的链式调用
    - 支持引用计数
    - 支持池化

### 粘包，拆包

1. 粘包，客户端发送100个消息，服务端两次就接收完了
2. 拆包，因为长度限制问题，客户端一次性发送的消息，服务端分为了多次才接收完
3. TooLongFrameException，当遇到超长消息时，抛出异常，考虑网卡受到恶意攻击
4. 解决方案
    - 加分隔符，netty提供了DelimiterBasedFrameDecoder（自定义分隔符）、LineBasedFrameDecoder（系统换行符）
    - 消息定长，netty提供FixedLengthFrameDecoder（消息定长）
    - 消息带上长度字段，netty提供LengthFieldBasedFrameDecoder

### 对象序列化

1. 通过jdk自带的序列化的缺点，慢，占用内存大，必须使用jdk才能反序列化
2. netty可以通过引用自身的序列化或者引入第三方的序列化来实现
    - 默认有PotoBuf、jboss公司自带的
    - 只需要加入对应的ChannelHandler就行，然后在读取数据中，可以直接强转类型