### UDP

1. udp没有粘包拆包的说法
2. udp单播，udp和tcp的不同点
    - 使用udp的channel，NioDatagramChannel
    - 发送方直接绑定端口0，然后发送数据的时候使用udp报文包装
        ```
        bootstrap.bind(0).sync().channel();
        //发往目的地的地址和端口
        channel.writeAndFlush(new DatagramPacket(byteBuf),new InetSocketAddress("127.0.0.1",port))
        ```
    - 接收方，绑定端口，获取发送数据
        ```
        bootstrap.bind(port).sync();
        (DatagramPacket)msg.content().toString(CharsetUtil.UTF_8)
        ```
3. udp广播

### 服务器推送技术

1. 常见的ajax短轮询的特征
    - 服务器基本不用改造
    - 服务区沉重压力和资源的浪费
    - 数据同步不及时
2. websocket特征
    - 长连接，耗费服务器资源
    - 适合客户与业务频繁交互的场景，如实时共享、多人协作、直播、共享桌面
    - 采用新的协议，后端需要单独实现
    - 客户端并不是所有浏览器都支持

### 自定义协议传输

1. 直接端口进行二进制通信，通过各种ChannelHandler来对数据流进行修饰
    - 可以采用三方的直接系列化为实体
    - 也可以采用netty自身的序列化框架
2. 客户端设置重连机制，服务端设置登录机制，心跳机制（在连接激活后，放入一个定时任务重）
3. 如果是自己channelHandler需要处理的消息，处理完之后释放
    ```
    ReferenceCountUtil.release(msg);
    ```
4. 如果不是自己的channelHandler需要处理的消息，直接传给下一个channelHandler
    ```
    ctx.fireChannelRead(msg)
    ```
5. 处理消息有一个链路pipeline，和他加入的顺序有关系