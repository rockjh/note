### 架构图

![tomcat-frame](http://assets.processon.com/chart_image/5febe6fae401fd661a0539be.png?_=1609296701037)

### 访问流程

#### 浏览器访问流程

1. http请求只是定义了数据的组织格式（通信格式），是一个应用层协议，数据传输依靠的是TCP/IP协议

2. 服务器通过socket接收到的数据=tcp头+tcp数据，tcp数据=http请求头+http请求体

   ![浏览器访问流程](http://assets.processon.com/chart_image/5fec17ba5653bb6b1b504202.png?_=1609309009653)

#### Tomcat处理流程

1. HTTP服务器（连接器）会把请求信息使用ServletRequest对象封装起来

2. 进一步去调用Servlet（Container）容器中某个具体的Servlet

3. 在第2步中，Servlet容器拿到请求后，根据URL和Servlet的映射关系，找到相应的Servlet

4. 如果Servlet还没有被加载，就用反射机制创建这个Servlet，并调用Servlet的init方法来完成初始化

5. 接着调用这个具体Servlet的service方法来处理请求，请求处理结果使用ServletResponse对象封装

6. 把ServletResponse对象返回给HTTP服务器，HTTP服务器会把响应发送给客户端

   ![网络请求流程](http://assets.processon.com/chart_image/5febfa335653bb6b1b4ffb0e.png?_=1609301203351)

### 组件

1. Tomcat/Catalina/Server都可以称为一个tomcat实例
   - Tomcat，启动的时候会初始化Catalina实例
   - Catalina，加载解析server.xml，创建并管理一个Server
   - Server，表示整个Catalina Servlet容器以及其它组件，负责组装并启动Servlet引擎，Tomcat连接器
2. Service，是Server内部的组件，一个Server包含多个Service，它将若干个Connector绑定到一个Container
3. Connector，连接器，服务器请求的协议和端口，支持HTTP、HTTPS、AJP，配置多个就是多个端口都可以进入这个Container
4. Container，主要功能是多容器的加载、处理业务逻辑，包含Engine、Host、Context和Wrapper组件，他们是父子关系
   - Engine，表示整个Catalina的Servlet引擎，用来管理多个虚拟站点，一个Service最多只能有一个Engine， 一个引擎可包含多个Host
   - Host，表示一个虚拟主机/站点，一个虚拟主机下可包含多个Context
   - Context，表示一个Web应用程序，一个Web应用可包含多个Wrapper
   - Wrapper，表示一个Servlet，Wrapper作为容器中的最底层，不能包含子容器

### 连接器Connector/Coyote

1. Coyote功能

   - 封装了底层的网络通信（Socket 请求及响应处理）

   - 使Catalina 容器（容器组件）与具体的请求协议及IO操作方式完全解耦

   - 将Socket输入转换封装为Request对象，进一步封装后交由Catalina容器进行处理，处理请求完成后, Catalina 通过Coyote提供的Response对象将结果写入输出流

   - 负责的是具体协议（应用层）和IO（传输层）相关内容

2. 支持的应用层协议
   - HTTP/1.1，大部分WEB应用采用的协议（默认）
   - AJP，用于和WX集成（如Apache），以实现对静态资源的优化和集群部署
   - HTTP/2，大幅度提升WEB性能，下一代的HTTP协议
3. 传输层的IO模型
   - NIO，非阻塞IO，采用java NIO类库实现（默认）
   - NIO2，异步IO
   - APR，采用Apache可移植运行库的实现，是C/C++编写的本地库，需要单独安装APR库（优化的方向）

4. 流程

   ![Coyote流程](http://assets.processon.com/chart_image/5fec45515653bb6b1b514be0.png?_=1609320729781)

5. Coyote组件

   - EndPoint，Socket通信监听的接口，对传输层进行了抽象，实现TCP/IP协议的接收和发送数据
   - Processor，是对应用层协议的抽象，用来实现HTTP/AJP协议，接收来自EndPoint的Socket数据，读取字节流解析成Tomcat Request和Response
   - ProtocolHandler，Coyote 协议接口， 封装Endpoint 和 Processor ， Tomcat 按照协议和I/O 提供了6个实现类 : AjpNioProtocol、 AjpAprProtocol、 AjpNio2Protocol、 Http11NioProtocol、 Http11Nio2Protocol、Http11AprProtocol
   - Adapter，Tomcat的Request不是标准的ServletRequest，Container容器需要标准的ServletRequest，Adapter做一个适配转换

### 手写Tomcat要点

1. 传输的内容是，tcp数据 = http请求头+http请求体，使用socket的输入输出流传输tcp数据的byte[]
2. 访问静态资源文件，根据请求路径找到服务器中的静态资源文件，将http请求头+文件输入流读取的byte[]
3. 访问动态资源，判断如果根据请求路径没有找到静态资源文件，就去HttpServlet中根据请求路径找到对应的映射，反射执行动态方法
4. 多线程支持，使用一个线程来接收请求，使用另外的一个线程池来处理请求业务逻辑

