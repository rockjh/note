### 声明周期

Tomcat中几乎都是容器嵌套容器，各个容器的组件都会涉及创建、销毁等，因此设计了生命周期接口Lifecycle进行统一规范，每个容器都实现该接口

![Lifecycle接口](http://assets.processon.com/chart_image/5fed44cc07912977bed9ddf1.png?_=1609385275584)

### 初始化

1. Tomcat脚本start.sh启动入口Bootstrap#main

2. 调用层级

   ```java
   //init初始化
   //从启动层级可以看出来，容器之间的关系，和config配置文件对照
   Bootstrap.init()
   Bootstrap.load()
   		Catalin.load()
     			StandardServer.init()
     					StandardService.init()
     							StandardEngine.init()
     									StandardHost.init()
     											StandardContext.init()
     					Connector.init()
     							Http11NioProtocol.init()
     					StandardThreadExecutor.init()
     
   //start
   //从启动层级可以看出来，容器之间的关系，和config配置文件对照
   Bootstrap.start()
   		Catalin.start()
     			StandardServer.start()
     					StandardService.start()
     							StandardEngine.start()
     									StandardHost.start()
     											StandardContext.start()
     					Connector.start()
     							Http11NioProtocol.start()
     					StandardThreadExecutor.start()
   ```

3. 连接器的启动核心代码

   ```java
   Connector#start
   Http11NioProtocol#start
   NioEndpoint#start
   //通信端点，接收socket连接通信
   NioEndpoint#startInternal
       //检查NIO的Selector是否准备好了读或者写
       poller = new Poller();
       Thread pollerThread = new Thread(poller, getName() + "-ClientPoller");
       pollerThread.setPriority(threadPriority);
       pollerThread.setDaemon(true);
       pollerThread.start();
       //创建一个专门接收通信连接的线程，接收到连接之后进行封装转交给poller
       startAcceptorThread();
   NioEndpoint#startAcceptorThread
     	//Acceptor是一个线程，run方法中ServerSocketChannel.accept()等待连接
   		acceptor = new Acceptor<>(this);
       String threadName = getName() + "-Acceptor";
       acceptor.setThreadName(threadName);
       Thread t = new Thread(acceptor, threadName);
       t.setPriority(getAcceptorThreadPriority());
       t.setDaemon(getDaemon());
       t.start();
   ```

### 处理请求

1. 请求处理代码流程

   ```java
   //方法中ServerSocketChannel.accept()接收到连接
   Acceptor#run
   //封装后转交给Poller，Poller是一个线程，监听NIO事件
   Poller#run
   //事件到来，处理事件类型
   Poller#processKey
     	//根据读还是写分别传递不同的参数
     	processSocket(socketWrapper, SocketEvent.OPEN_READ, true)
     	processSocket(socketWrapper, SocketEvent.OPEN_WRITE, true)
   AbstractEndpoint#processSocket
     	//是一个Runnable的实现类
     	//run方法会调用自己的doRun
   		SocketProcessorBase<S> sc = null;
       if (processorCache != null) {
         sc = processorCache.pop();
       }
       if (sc == null) {
         //创建一个Processor
         sc = createSocketProcessor(socketWrapper, event);
       } else {
         sc.reset(socketWrapper, event);
       }
   		//取得线程池
       Executor executor = getExecutor();
       if (dispatch && executor != null) {
         //进入线程池执行
         executor.execute(sc);
       } else {
         sc.run();
       }
   //处理握手消息
   SocketProcessor#doRun
     	//具体处理Socket事件
   		getHandler().process(socketWrapper, SocketEvent.OPEN_READ)
   //获取或者创建Processor，也就是我们架构图中的Processor，用来处理应用层协议
   ConnectionHandler#process
   //根据状态选择Processor的执行方法
   AbstractProcessor#process
   		if(status == SocketEvent.OPEN_READ) {
       	state = service(socketWrapper);
       }
   Http11Processor#service
     	//省略一些代码，封装Request和Response...
     	//获取到Coyote适配器调用service
     	getAdapter().service(request, response);
   CoyoteAdapter#service
     	//根据请求路径将host、context、wrapper封装到request中的MappingData对象中
   		postParseSuccess = postParseRequest(req, request, res, response);
   		//调用Engine的invoke方法
   		connector.getService().getContainer().getPipeline().getFirst().invoke(request, response)
   StandardEngineValve#invoke
       //调用host的invoke方法
       host.getPipeline().getFirst().invoke(request, response)
   StandardHostValve#invoke
       //调用context的invoke方法
       context.getPipeline().getFirst().invoke(request, response);
   StandardContextValve#invoke
     	//调用wrapper的invoke方法
   		wrapper.getPipeline().getFirst().invoke(request, response);
   StandardWrapperValve#invoke
     	//获取servlet
     	servlet = wrapper.allocate();
   		//将servlet封装到一个拦截链中
   		ApplicationFilterChain filterChain = ApplicationFilterFactory.createFilterChain(request,wrapper, servlet);
   		//执行这个拦截链
   		filterChain.doFilter(request.getRequest(), response.getResponse());
   ApplicationFilterChain#doFilter
     	//真正的执行filter
     	internalDoFilter(request,response);
   ApplicationFilterChain#internalDoFilter
     	//执行封装的servlet的service方法
     	servlet.service(request, response);
   ```

2. 请求处理流程图

   ![请求处理流程图](http://assets.processon.com/chart_image/5fed911de401fd661a09f47c.png?_=1609404713616)

### Springboot内嵌Tomcat

1. 初始化流程

   ```java
   //前面流程查看springboot-main相关的文章
   TomcatWebServer#initialize
       //省略其他代码...
       //初始化tomcat
       this.tomcat.start();
   Tomcat#start
     	//初始化变量server = new StandardServer()
     	getServer();
   		//调用StandardServer的start方法
       server.start();
   //会去判断是否调用了init方法，如果没有会先调用init方法
   //就和上面的Tomcat初始化类似了
   LifecycleBase#start
     	
   ```

2. 嵌入mvc

   ```java
   //在第一次发起请求，处理请求流程走到这一步时
   StandardWrapperValve#invoke
     	//获取servlet
     	servlet = wrapper.allocate();
   StandardWrapper#allocate
     	//第一次调用，servlet没有被初始化，调用初始化
     	initServlet(instance);
   StandardWrapper#initServlet
     	//servlet初始化
     	//具体和mvc的关联查看mvc相关的文章
     	servlet.init(facade);
   ```

   

