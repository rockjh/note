### JVM类加载器

1. 采用双亲委派模型进行类加载
2. 引导类加载器（BootstrapClassLoader），c++编写，加载rt.jar中的类，构造ExtClassLoader和AppClassLoader
3. 扩展类加载器（ExtClassLoader），加载JAVA_HOME/lib/ext目录下的jar中的类，如classpath中的jre、javax.*、java.ext.dir指定位置中的类
4. 系统类加载器（AppClassLoader），加载环境变量classpath中指明的路径
5. 自定义类加载器，自定义加载器加载不到上面指定路径的类，除非破坏双亲委派模型

### Tomcat类加载器

1. 违背的双亲委派模型，因为一个Tomcat可以部署多个应用，如果多个应用中有两个相同路径的类，这两个路径相同的类内容不同，就会出现问题

2. 引导类加载器（BootstrapClassLoader）和扩展类加载器（ExtClassLoader），功能保持不变

3. 系统类加载器（AppClassLoader），加载tomcat启动的类，位于CATALINA_HOME/bin下

4. Common通用类加载器，加载Tomcat使用以及应用通用的一些类，位于CATALINA_HOME/lib下， 比如servlet-api.jar

5. Catalina ClassLoader，加载服务器内部可⻅类，这些类应用程序不能访问

6. Shared ClassLoader，加载应用程序共享类，这些类服务器不会依赖

7. Webapp ClassLoader，每个应用程序都会有一个独一无二的Webapp ClassLoader，加载本应用程序/WEB-INF/classes和/WEB-INF/lib下的类

8. tomcat 8.5 默认改变了严格的双亲委派机制

   - 首先从Bootstrap Classloader和ExtClassLoader加载指定的类
   - 如果未加载到则从/WEB-INF/classes加载如果未加载到（Webapp ClassLoader）
   - 则从 /WEB-INF/lib/*.jar 加载（Webapp ClassLoader）

   - 如果未加载到，则依次从AppClassLoader、Common、Shared 加载（在这最后一步遵从双亲委派 机制）

### HTTPS

1. HTTP和HTTPS的区别

   - HTTPS协议使用时需要到电子商务认证授权机构(CA)申请SSL证书
   - HTTP默认使用8080端口，HTTPS默认使用8443端口
   - HTTPS则是具有SSL加密的安全性传输协议，对数据的传输进行加密，效果上相当于HTTP的升级版
   - HTTP的连接是无状态的，不安全的，HTTPS协议是由SSL+HTTP协议构建的可进行加密传输、身份认证的网络协议，比HTTP协议安全

2. HTTPS工作原理

   ![HTTPS流程](http://assets.processon.com/chart_image/5fed34c85653bb21c1b103b6.png?_=1609382379072)

3. Tomcat中使用HTTPS的配置

   ```xml
    
   <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol" maxThreads="150" schema="https" secure="true" SSLEnabled="true">
     	<SSLHostConfig>
           <Certificate
   certificateKeystoreFile="/Users/yingdian/workspace/servers/apache-tomcat- 8.5.50/conf/lagou.keystore" certificateKeystorePassword="lagou123" type="RSA"/>
       </SSLHostConfig>
   </Connector>
   ```

   

### 优化方向

1. JVM虚拟机优化（优化内存模型）
2. Tomcat自身配置的优化
   - 共享线程池
   - IO模型，默认使用NIO，当并发要求高或出现瓶颈时，可以考虑使用APR
   - 动静分离，Nginx+Tomcat，Nginx负责静态资源访问，Tomcat负责动态资源处理