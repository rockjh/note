### 大数据

1. 定义，指无法在一定时间范围内用常规软件工具进行捕捉、管理和处理的数据集合，是需要新处理模式才能具有更强的决策力、洞察发现力和流程优化能力的海量、高增长率和多样化的信息资产
2. 特点
    - 大量，数据量大
    - 高速，数据产生快
    - 多样，结构化和非结构化数据（日志、音频、视频、图片、地理位置）
    - 低价值密度，需要从大量数据中筛选出小部分有用的数据
3. 应用场景
    - 物流仓库，上午下单下午到达，帮助商家精细化运营
    - 零售，分析用户群体的消费习惯，为用户购买提供便利（摆放位置）+ 捆绑销售（纸尿布和啤酒）
    - 旅游，数据分析后，共建旅游产业智慧管理、智慧服务、智慧营销
    - 商品广告推荐，给用户推荐可能喜欢的商品
    - 保险，海量数据挖掘及风险预测，助力保险行业精准营销和精细定价
    - 金融，多维度分析用户特征，帮助金融机构推荐优质客户
    - 房产，大数据全面助力房地产，打造精准投策与营销、选更合适的地、建更合适的楼、卖更合适的客户
    - 人工智能，大量的行为分析造就人工智能

### 大数据部门组织结构

1. 平台组
    - Hadoop、Flume、Kafka、HBase、Spark等框架平台的搭建
    - 集群性能监控
    - 集群性能测试
2. 数据仓库组，数据链路分析，主要是写sql（hive）
    - ETL工程师（数据清洗）
    - 数据分析、数据仓库建模
3. 实时组，实时指标分析性能调优（Spark、Flink）
4. 数据挖掘组
    - 算法工程师
    - 推荐系统工程师
    - 用户画像工程师
5. 报表开发组
    - JavaEE工程师
    - 前端工程师

### Hadoop介绍

1. Hadoop是一个有apache基金会所开发的分布式系统框架，主要解决<font color="green">海量数据的存储和分析问题</font>
2. Hadoop的三大发行版本
    - Apache Hadoop，最原始（最基础）的版本，对于入门学习最好
    - Cloudera Hadoop，内部集成了很多大数据框架，对应产品CDH（大多数公司实际使用，因为构建集群这些很方便）
    - Hortonworks Hadoop，文档较好，对应产品HDP
3. Hadoop的优势
    - 高可靠性，底层维护多个数据副本，即时某个副本出错，也不会导致数据丢失
    - 高扩展性，在集群间分配任务数据，可方便扩展数以千计的节点
    - 高效性，在MapReduce的思想下，Hadoop是并行工作的，可以加快任务执行速度
    - 高容错性，可以自动将失败的任务重新分配

### Hadoop架构

![hadoop架构图](http://assets.processon.com/chart_image/5fa114e97d9c081baf10f6f3.png?_=1604396293681)

### Hadoop生态

1. Sqoop，可以将关系型数据库（MySQL、Oracle）中的数据和Hadoop的HDFS中的数据相互导入导出
2. Flume，是一个高可用、高可靠、分布式的海量日志采集、聚合和传输的系统，Flume支持在日志系统中定制各类数据发送方，用于收集数据
3. Kafka，是一种高吞吐量的分布式发布订阅消息系统
4. Storm，用于连续计算，对数据流做连续查询，在计算时就将结果以流的形式输出给用户
5. Spark，是当前最流行的开源大数据内存计算框架，可以基于Hadoop上存储的大数据进行计算
6. Flink，是当前最流行的开源大数据内存计算框架，大多用于实时计算场景
7. Oozie，是一个管理Hdoop作业（job）的工作流程调度管理系统
8. Hbase，HBase是一个分布式的、面向列的开源数据库，HBase适用于非结构化数据存储
9. Hive，是基于Hadoop的一个数据仓库工具，可以将结构化的数据文件映射为一张数据库表，并提供简单的SQL查询功能，可以将SQL语句转换为MapReduce任务进行运行，可以通过类SQL语句快速实现简单的MapReduce统计，不必开发专门的MapReduce应用，十分适合数据仓库的统计分析
10. ZooKeeper，是一个针对大型分布式系统的可靠协调系统，提供的功能包括：配置维护、名字服务、分布式同步、组服务等

### 推荐系统框架图

![推荐系统架构图](http://assets.processon.com/chart_image/5facad271e08535073b172cd.png?_=1605154391808)

### 运行环境搭建（完全分布式运行模式）

#### 虚拟机准备

1. 准备三台虚拟机，虚拟机配置要求如下
    - 单台虚拟机，内存4G，硬盘50G，安装必要环境,centos
        ```
        sudo yum install -y epel-release sudo yum install -y psmisc nc net-tools rsync vim lrzsz ntp libzstd openssl-static tree iotop git
        ```
    - 修改克隆虚拟机的静态IP
        ```
        sudo vim /etc/sysconfig/network-scripts/ifcfg-ens33
        ```
        ```
        #网关、ip网关、子网掩码根据mac机器配置
        DEVICE=ens33
        TYPE=Ethernet
        ONBOOT=yes
        BOOTPROTO=static
        NAME="ens33"
        IPADDR=192.168.1.101
        PREFIX=24
        GATEWAY=192.168.1.2
        DNS1=192.168.1.2
        ```
2. 修改主机名
    - 修改主机名称
        ```
        sudo hostnamectl --static set-hostname hadoop101
        ```
    - 配置主机名称映射，打开/etc/hosts
        ```
        sudo vim /etc/hosts

        添加hosts内容
        192.168.1.100 hadoop100
        192.168.1.101 hadoop101
        192.168.1.102 hadoop102
        ```
    - 修改mac机器hosts文件
        ```
        sudo vim /etc/hosts

        添加hosts内容
        192.168.1.100 hadoop100
        192.168.1.101 hadoop101
        192.168.1.102 hadoop102
        ```
3. 关闭防火墙
    ```
    sudo systemctl stop firewalld
    sudo systemctl disable firewalld
    ```
4. 创建hadoop用户
    ```
    sudo useradd hadoop
    sudo passwd hadoop
    ```
5. 重启虚拟机
    ```
    reboot
    ```
6. 配置hadoop用户具有root权限，修改/etc/sudoers文件，找到下面一行（91行），在root下面添加一行
    ```
    ## Allow root to run any commands anywhere
    root    ALL=(ALL)     ALL
    hadoop   ALL=(ALL)     ALL
    ```
7. 在/opt目录下创建文件夹
    - 在/opt目录下创建module、software文件夹
        ```
        sudo mkdir module
        sudo mkdir software
        ```
    - 修改module、software文件夹的所有者cd
        ```
        sudo chown hadoop:hadoop /opt/module /opt/software
        ```

8. 卸载现有的jdk
    ```
    rpm -qa | grep -i java | xargs -n1 sudo rpm -e --nodeps
    ```
9. 将jdk和hadoop分别解压到对应的目录
10. 配置jdk和hadoop的环境变量
    ```
    #配置环境变量只需要在这个目录下创建预执行文件就行
    sudo vim /etc/profile.d/my_env.sh
    ```
    ```
    #JAVA_HOME
    export JAVA_HOME=/opt/module/jdk1.8.0_212
    export PATH=$PATH:$JAVA_HOME/bin

    ##HADOOP_HOME
    export HADOOP_HOME=/opt/module/hadoop-3.1.3
    export PATH=$PATH:$HADOOP_HOME/bin
    export PATH=$PATH:$HADOOP_HOME/sbin
    ```
11. rsync远程同步工具，rsync只对差异文件做更新
    - 基本语法
        ```
        # a归档拷贝
        # v显示复制过程
        #将当前机器的software目录，复制到hadoop102
        rsync -av /opt/software/ hadoop@hadoop102:/opt/software
        ```
    - xsync集群分发脚本
        - 需求，循环复制文件到所有节点的相同目录下
        - rsync命令原始拷贝
            ```
            rsync -av /opt/module root@hadoop103:/opt/
            ```
        - 期望脚本
            ```
            xsync 要同步的文件名称
            ```
        - 脚本实现，
            ```
            cd /home/hadoop
            vim xsync
            ```
            ```
            #!/bin/bash
            #1. 判断参数个数
            if [ $# -lt 1 ]
            then
                echo Not Enough Arguement!
                exit;
            fi
            #2. 遍历集群所有机器
            for host in hadoop102 hadoop103 hadoop104
            do
                echo ====================  $host  ====================
                #3. 遍历所有目录，挨个发送
                for file in $@
                do
                    #4 判断文件是否存在
                    if [ -e $file ]
                    then
                        #5. 获取父目录
                        pdir=$(cd -P $(dirname $file); pwd)
                        #6. 获取当前文件的名称
                        fname=$(basename $file)
                        ssh $host "mkdir -p $pdir"
                        rsync -av $pdir/$fname $host:$pdir
                    else
                        echo $file does not exists!
                    fi
                done
            done
            ```
        - 修改脚本xsync具有执行权限，将脚本移动到/bin中，以便全局调用，测试脚本
        ```
        chmod +x xsync
        sudo mv xsync /bin/
        #将这个脚本同步到上面遍历的机器中
        sudo xsync /bin/xsync
        ```

> 将上面的虚拟机进行快照存储，可以通过快照克隆虚拟机

#### Hadoop集群环境搭建

1. SSH无密登录配置
    - 配置ssh，输入命令后，输入yes登入目标主机
        ```
        ssh 用户@hadoop103
        ```
    - 无密钥配置
        - 在hadoop100中，生成公钥和私钥，输入命令后连续敲三个回车说明没有密码
            ```
            ssh-keygen -t rsa
            ```
        - 将公钥拷贝到要免密登录的目标机器上，然后下面这些机器就可以免密登录hadoop100了
            ```
            ssh-copy-id hadoop102
            ssh-copy-id hadoop103
            ssh-copy-id hadoop104
            ```
        - .ssh文件夹下（~/.ssh）的文件功能解释
            - known_hosts，记录ssh访问过计算机的公钥(public key)
            - id_rsa，生成的私钥
            - id_rsa.pub，生成的公钥
            - authorized_keys，存放授权过的无密登录服务器公钥
2. 集群配置
    - 集群部署规划
        - hadoop102配置NameNode、DataNode、NodeManager
        - hadoop103配置DataNode、ResourceManager、NodeManager
        - hadoop104配置SecondaryNameNode、DataNode、NodeManager
        > NameNode和SecondaryNameNode不要安装在同一台服务器
        
        > ResourceManager也很消耗内存，不要和NameNode、SecondaryNameNode
    - core-site.xml，核心配置文件
        ```
        <configuration>
            <!-- NameNode端口 -->
            <property>
                <name>fs.defaultFS</name>
                <value>hdfs://hadoop102:8020</value>
            </property>
            <!-- hadoop数据存放的位置，这里是声明的一个变量 -->
            <property>
                <name>hadoop.data.dir</name>
                <value>/opt/module/hadoop-3.1.3/data</value>
            </property>
        </configuration>
        ```
    - hdfs-site.xml，HDFS配置文件
        ```
        <configuration>
            <!-- namenode文件的存储路径 -->
            <property>
                <name>dfs.namenode.name.dir</name>
                <value>file://${hadoop.data.dir}/name</value>
            </property>
            <!-- datanode文件的存储路径 -->
            <property>
                <name>dfs.namenode.data.dir</name>
                <value>file://${hadoop.data.dir}/data</value>
            </property>
            <!-- secondarynode文件的存储路径 -->
            <property>
                <name>dfs.namenode.checkpoint.dir</name>
                <value>file://${hadoop.data.dir}/namesecondary</value>
            </property>
            <!-- 指定secondarynode地址 -->
            <property>
                <name>dfs.namenode.secondary.http-address</name>
                <value>hadoop104:9868</value>
            </property>
        </configuration>
        ```
    - yarn-site.xml，Yarn配置文件
        ```
        <configuration>
            <property>
                <name>yarn.nodemanager.aux-services</name>
                <value>mapreduce_shuffle</value>
            </property>
            <!-- 指定resourcemanager路径 -->
            <property>
                <name>yarn.resourcemanager.hostname</name>
                <value>hadoop103</value>
            </property>
            <property>
                <name>yarn.nodemanager.env-whitelist</name>
                <value>JAVA_HOME,HADOOP_COMMON_HOME,HADOOP_HDFS_HOME,HADOOP_CONF_DIR,CLASSPATH_PREPEND_DISTCACHE,HADOOP_YARN_HOME,HADOOP_MAPRED_HOME</value>
            </property>
        </configuration>
        ```
    - mapred-site.xml，MapReduce配置文件
        ```
        <configuration>
            <!-- 说明MapReduce程序运行在Yarn上 -->
            <property>
                <name>mapreduce.framework.name</name>
                <value>yarn</value>
            </property>
        </configuration>
        ```
3. 分发集群配置
    ```
    xsync /opt/module/hadoop-3.1.3/etc/hadoop/
    ```
4. 群起集群
    - 配置works，分发该配置文件
        ```
        vim /opt/module/hadoop-3.1.3/etc/hadoop/workers

        hadoop102
        hadoop103
        hadoop104
        ```
    - 集群第一次启动需要格式化HDFS，会生成一些初始化文件
        ```
        hdfs namenode -format
        ```
    - 启动HDFS和Yarn（会启动集群中所有的机器）
        ```
        sbin/start-dfs.sh
        sbin/start-yarn.sh
        ```
5. 启动停止总结
    - 各个服务组件逐一启动/停止
        ```
        hdfs --daemon start/stop namenode/datanode/secondarynamenode

        yarn --daemon start/stop  resourcemanager/nodemanager
        ```
    - 集群各个模块分开启动/停止（配置ssh是前提）常用
        ```
        start-dfs.sh/stop-dfs.sh

        start-yarn.sh/stop-yarn.sh
        ```
6. 配置历史服务器，mapred-site.xml，新增如下配置
    ```
    <!-- 历史服务器端地址 -->
    <property>
        <name>mapreduce.jobhistory.address</name>
        <value>hadoop102:10020</value>
    </property>

    <!-- 历史服务器web端地址 -->
    <property>
        <name>mapreduce.jobhistory.webapp.address</name>
        <value>hadoop102:19888</value>
    </property>
    ```
    ```
    #启动历史服务器
    mapred --daemon start historyserver
    ```
7. 配置日志的聚集，yarn-site.xml，新增如下配置
    ```
    <property>
        <name>yarn.log-aggregation-enable</name>
        <value>true</value>
    </property>
    <property>  
        <name>yarn.log.server.url</name>  
        <value>http://${yarn.timeline-service.webapp.address}/applicationhistory/logs</value>
    </property>
    <property>
        <name>yarn.log-aggregation.retain-seconds</name>
        <value>604800</value>
    </property>
    ```