### 定义

1. 背景，随着数据量越来越大，在一个系统中存不下所有的数据，迫切的需要一种系统来管理多台机器上的文件，这就是分布式文件系统，HDFS只是分布式文件系统中的一种
2. 定义，HDFS是一个分布式文件系统，用于存储文件，通过目录树来定位文件，由很多服务器联合起来实现其功能，集群中的服务器各自有各的角色
3. 使用场景，一次写，多次读，且不支持文件修改，适合用于做数据分析

### 优缺点

1. 优点
    - 高容错性，数据自动保存多个副本，某一个副本丢失后，可以自动恢复
    - 适合处理大数据
    - 可以构建在廉价的机器上
2. 缺点
    - 不适合做低延时的访问
    - 无法高效的对大量小文件进行存储
        - 大量小文件会占用NameNode大量的内存来存储文件目录和块信息
        - 小文件存储寻址时间会超过读取时间，违反了HDFS的设计目标
    - 不支持并发写入，文件随机修改
        - 一个文件只能有一个写，不允许多个线程同时写
        - 仅支持数据追加，不支持修改

### 文件的存储方式

1. 在HDFS中，文件在物理上以块的形式存储，块的大小可以配置（默认128M）
2. 块大小配置的原则，块大小设置取决于磁盘的传输速率，磁盘传输越快块的划分就可以越大，一般来说寻址时间为传输时间的1%为最佳
    - 设置太大，会导致磁盘传输数据时间明显大于定位块位置的时间
    - 设置太小，会增加寻址时间

### HDFS的Shell命令操作

1. 语法
    ```
    #下面两个二选一，都一样
    bin/hadoop fs 具体命令
    bin/hdfs dfs 具体命令
    ```
2. 命令
    - 本地到HDFS（上传）
        - put，上传
        - copyFromLocal=put，从本地复制到HDFS
        - moveFromLocal，从本地移动到HDFS
        - appendToFile，文件追加
    - HDFS到HDFS，类似linux中的命令
        - cp、mv、chown、chmod、mkdir、du、df、cat、rm
    - HDFS到本地（下载）
        - get，下载
        - getmerge，下载合并
        - copyToLocal=get，拷贝到本地
    ```
    #命令帮助
    hadoop fs -help moveFromLocal

    hadoop fs -moveFromLocal  ./kongming.txt  /sanguo/shuguo

    #合并/user/atguigu/test/目录下所有文件并下载到本地的zaiyiqi.txt文件
    hadoop fs -getmerge /user/atguigu/test/* ./zaiyiqi.txt
    ```

### HDFS客户端操作

1. 新建maven项目，pom.xml
    ```
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
    </dependency>
    <dependency>
        <groupId>org.apache.logging.log4j</groupId>
        <artifactId>log4j-slf4j-impl</artifactId>
        <version>2.12.0</version>
    </dependency>
    <dependency>
        <groupId>org.apache.hadoop</groupId>
        <artifactId>hadoop-client-api</artifactId>
        <version>3.1.3</version>
    </dependency>
    <dependency>
        <groupId>org.apache.hadoop</groupId>
        <artifactId>hadoop-client-runtime</artifactId>
        <version>3.1.3</version>
    </dependency>
    ```
2. Resource目录中log4j2.xml
    ```
    <?xml version="1.0" encoding="UTF-8"?>
    <Configuration status="error" strict="true" name="XMLConfig">
        <Appenders>
            <!-- 类型名为Console，名称为必须属性 -->
            <!--  -->
            <Appender type="Console" name="STDOUT">
                <!-- 布局为PatternLayout的方式，
                输出样式为[INFO] [2018-01-22 17:34:01][org.test.Console]I'm here -->
                <Layout type="PatternLayout"
                        pattern="[%p] [%d{yyyy-MM-dd HH:mm:ss}][%c{10}]%m%n" />
            </Appender>

        </Appenders>

        <Loggers>
            <!-- 可加性为false -->
            <Logger name="test" level="info" additivity="false">
                <AppenderRef ref="STDOUT" />
            </Logger>

            <!-- root loggerConfig设置 -->
            <Root level="info">
                <AppenderRef ref="STDOUT" />
            </Root>
        </Loggers>

    </Configuration>
    ```
3. 新建一个Test类
    ```
    /**
     * 上传
     * @throws IOException
     * @throws InterruptedException
     */
    @Test
    public void put() throws IOException, InterruptedException {
        Configuration configuration = new Configuration();
        configuration.set("dfs.replication", "1");
        configuration.set("dfs.blocksize", "67108864");


        //1. 新建HDFS对象
        FileSystem fileSystem = FileSystem.get(URI.create("hdfs://hadoop01:8082"),
                configuration);


        //2. 操作集群
        fileSystem.copyFromLocalFile(
                new Path("/Volumes/file/note/linux-command.md"),
                new Path("/1/linux-command.md"));

        //3. 关闭资源
        fileSystem.close();
    }
    ```
4. 设置虚拟机启动参数，-DHADOOP_USER_NAME=hadoop，hadoop是hadoop服务器的用户名
5. <font color="red">可能需要c将ore-site.xml和hdfs-site.xml放在Resource中</font>