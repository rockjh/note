### 架构图

![HDFS架构图](http://assets.processon.com/chart_image/5fad0310e401fd7d35c5592e.png?_=1605175461487)

### 写数据流

1. 客户端通过Distributed FileSystem模块向NameNode请求上传文件，NameNode检查目标文件是否已存在，父目录是否存在
2. NameNode返回是否可以上传
3. 客户端请求第一block上传到哪几个DataNode服务器上
4. NameNode返回3个DataNode节点，分别为dn1、dn2、dn3
5. 客户端通过FSDataOutputStream模块请求dn1上传数据，dn1收到请求会继续调用dn2，然后dn2调用dn3，将这个通信管道建立完成
6. dn1、dn2、dn3逐级应答客户端
7. 客户端开始往dn1上传第一个block（先从磁盘读取数据放到一个本地内存缓存）；以Packet为单位，dn1收到一个Packet就会传给dn2，dn2传给dn3；dn1每传一个packet会放入一个应答队列等待应答
8. 当一个block传输完成之后，客户端再次请求NameNode上传第二个block的服务器（重复执行3-7步）

![HDFS写数据流程](http://assets.processon.com/chart_image/5fade2621e0853373ece84e8.png?_=1605232551792)

### 读数据流

1. 客户端通过Distributed FileSystem向NameNode请求下载文件，NameNode通过查询元数据，找到文件块所在的DataNode地址
2. 挑选一台DataNode（就近原则，然后随机）服务器，请求读取数据
3. DataNode开始传输数据给客户端（从磁盘里面读取数据输入流，以Packet为单位来做校验）
4. 客户端以Packet为单位接收，先在本地缓存，然后写入目标文件

![HDFS读数据流程](http://assets.processon.com/chart_image/5fade7c11e0853373ece992e.png?_=1605233208712)

### NameNode选择DataNode机制

1. 在HDFS写数据的过程中，NameNode会选择距离待上传数据最近距离的DataNode接收数据（作为第一个接收数据的节点），<font color="red">因为操作的数据都在HDFS文件系统上，就算使用API也会先去连接NameNode的机器，NameNode的机器中也会存储DataNode</font>
2. 节点距离，两个节点到达最近的共同祖先的距离总和
3. 副本节点选择
    - 第一个副本在client所处的节点上，如果客户端在集群外，随机选择一个
    - 第二个副本和第一个副本位于相同的机架，随机节点
    - 第三个副本位于不同机架，随机节点
4. 节点距离示例
    - Distance(/d1/r1/n0, /d1/r1/n0)=0（同一节点上的进程）
    - Distance(/d1/r2/n0, /d1/r3/n2)=4（同一数据中心不同机架上的节点）
    - Distance(/d1/r1/n1, /d1/r1/n2)=2（同一机架上的不同节点）
    - Distance(/d1/r2/n1, /d2/r4/n1)=6（不同数据中心的节点）

![HDFS网络拓扑结构](http://assets.processon.com/chart_image/5fadea5ef346fb2d03b28e92.png?_=1605236455706)

### NameNode和SecondaryNameNode

1. 2nn出现的背景，因为nn是加载到内存中的，如果随机访问磁盘存储效率低下，所以采用了edits编辑日志的形式追加内容改变（类似mysql的redo），一直写文件会导致edits文件过大，恢复缓慢，如果在nn中另开一个线程去处理合并的事情，会耗费大量的资源，所以就出来了2nn来辅助nn做这件事
2. NameNode启动
    - 第一次启动NameNode格式化后，创建Fsimage（nn的文件内容）和Edits文件。如果不是第一次启动，直接加载编辑日志和镜像文件到内存
    - 客户端对元数据进行增删改的请求
    - NameNode记录操作日志，更新滚动日志
    - NameNode在内存中对元数据进行增删改
3. Secondary NameNode工作
    - Secondary NameNode询问NameNode是否需要CheckPoint。直接带回NameNode是否检查结果
    - Secondary NameNode请求执行CheckPoint
    - NameNode滚动正在写的Edits日志
    - 将滚动前的编辑日志和镜像文件拷贝到Secondary NameNode
    - Secondary NameNode加载编辑日志和镜像文件到内存并合并
    - 生成新的镜像文件fsimage.chkpoint
    - 拷贝fsimage.chkpoint到NameNode
    - NameNode将fsimage.chkpoint重新命名成fsimage

![NN和2NN工作机制](http://assets.processon.com/chart_image/5fadf7fb1e0853373eceeba2.png?_=1605239300592)

### 集群安全模式

1. 定义，集群处于安全模式，不能执行重要操作（写操作），集群启动完成后，自动退出安全模式

2. NameNode启动，首先将镜像文件（Fsimage）载入内存，并执行编辑日志（Edits）中的各项操作，一旦在内存中成功建立文件系统元数据的映像，则创建一个新的Fsimage文件和一个空的编辑日志。此时，NameNode开始监听DataNode请求。<font color="red">这个过程期间，NameNode一直运行在安全模式，即NameNode的文件系统对于客户端来说是只读的</font>

3. DataNode启动，<font color="red">系统的数据块的位置并不是NameNode维护的，而是以块列表的形式存储在DataNode中</font>，DataNode启动后，在安全模式下，各个DataNode会向NameNode发送最新的块列表信息，系统运行期间块列表信息一直存放NameNode内存中，
    - 因为在NameNode停止后，集群不知道那些数据块有问题，所以NameNode不存储块列表信息

4. 安全模式退出判断，如果满足最小副本条件，NameNode会在30秒钟之后就退出安全模式
    - 最小副本，每一个块有一个副本上报了块信息
    - 在启动一个刚刚格式化的HDFS集群时，因为系统中还没有任何块，所以NameNode不会进入安全模式

5. 命令
    ```
    #查看安全模式状态
    bin/hdfs dfsadmin -safemode get
    #进入安全模式状态
    bin/hdfs dfsadmin -safemode enter
    #离开安全模式状态
    bin/hdfs dfsadmin -safemode leave
    #等待安全模式状态
    bin/hdfs dfsadmin -safemode wait
    ```
### DataNode

1. 工作机制

    - 数据块在DataNode上以文件形式存储，包括两个文件，一个是数据本身，一个是元数据包括数据块的长度，块数据的校验和，以及时间戳
    - DataNode启动后向NameNode注册，通过后，周期性（1小时）的向NameNode上报所有的块信息
    - 心跳是每3秒一次，心跳返回结果带有NameNode给该DataNode的命令（比如删除，会先删除NN中元数据，并将这个文件标记为删除，在下次DN心跳过来时，NN返回给DN删除文件命令），如果超过10分钟没有收到某个DataNode的心跳，则认为该节点不可用
    - 集群运行中可以安全加入和退出一些机器

    ![DataNode工作机制](http://assets.processon.com/chart_image/5fae0ea6e0b34d0d2233568a.png?_=1605243486624)

2. 数据完整性，hdfs使用crc校验

    - 当DataNode读取Block的时候，它会计算CheckSum
    - 如果计算后的CheckSum，与Block创建时值不一样，说明Block已经损坏
    - Client读取其他DataNode上的Block
    - DataNode在其文件创建后周期验证CheckSum

3. 新增数据节点

    - 配置好一台新的节点
    - 启动节点
        ```
        hdfs --daemon start datanode
        sbin/yarn-daemon.sh start nodemanager
        ```

4. 删除数据节点，可以通过黑白名单来实现
    - 白名单，添加到白名单的主机节点，都允许访问NameNode，不在白名单的主机节点，都会被退出
    - 黑名单，在黑名单上面的主机都会被强制退出

5. 多目录配置，
    - 支持多硬盘，将来数据分布时，会将两个两个存储目录进行平均存储
    - 在hdfs-site.xml中配置
        ```
        <property>
            <name>dfs.datanode.data.dir</name>
            <value>file:///${hadoop.tmp.dir}/dfs/data1,file:///${hadoop.tmp.dir}/dfs/data2</value>
        </property>
        ```