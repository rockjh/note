### 概述

Yarn是一个资源调度平台，负责为运算程序提供服务器运算资源，相当于一个分布式的操作系统平台，而MapReduce等运算程序则相当于运行于操作系统之上的应用程序

### 基本架构

1. ResourceManager
    - 处理客户端请求
    - 启动或者监控ApplicationMaster
    - 监控NodeManager
    - 资源的分配与调度
2. NodeManager
    - 单个节点上的资源管理
    - 处理来自ResourceManager上的命令
    - 处理来自ApplicationMaster上的命令
3. ApplicationMaster
    - 负责数据的切分
    - 为应用程序申请资源并分配给内部的任务
    - 任务的监控与容错
4. Container
    - 对任务运行环境进行抽象，封装CPU、内存等多维度的资源以及环境变量、启动命令等任务运行相关的信息
    - RM为AM分配的初始资源就是Container，然后再由AM为Container分配具体的任务

![Yarn基本架构](http://assets.processon.com/chart_image/5fae20a2e401fd7d35c739bf.png?_=1605249559794)

### 作业提交全过程

1. 作业提交
    - Client调用job.waitForCompletion方法，向整个集群提交MapReduce作业
    - Client向RM申请一个作业id
    - RM给Client返回该job资源的提交路径和作业id
    - Client提交jar包、切片信息和配置文件到指定的资源提交路径
    - Client提交完资源后，向RM申请运行MrAppMaster
2. 作业初始化
    - 当RM收到Client的请求后，将该job添加到容量调度器中
    - 某一个空闲的NM领取到该Job
    - 该NM创建Container，并产生MRAppmaster
    - 下载Client提交的资源到本地
3. 任务分配
    - MrAppMaster向RM申请运行多个MapTask任务资源
    - RM将运行MapTask任务分配给另外两个NodeManager，另两个NodeManager分别领取任务并创建容器
4. 任务运行
    - MR向两个接收到任务的NodeManager发送程序启动脚本，这两个NodeManager分别启动MapTask，MapTask对数据分区排序
    - MrAppMaster等待所有MapTask运行完毕后，向RM申请容器，运行ReduceTask
    - ReduceTask向MapTask获取相应分区的数据
    - 程序运行完毕后，MR会向RM申请注销自己
5. 进度和状态更新，客户端每秒向应用管理器请求进度更新，展示给用户
6. 作业完成，除了向应用管理器请求作业进度外，客户端每5秒都会通过调用waitForCompletion()来检查作业是否完成

![Yarn作业提交全过程](http://assets.processon.com/chart_image/5fae20cfe0b34d0d22337b4d.png?_=1605253222067)

### 资源调度器

1. 先进先出调度器FIFO，按照达到时间先后顺序，可能会造成任务饥饿
2. 容量调度器Capacity Scheduler（默认），支持多个队列，每个队列可配置一定的资源量，每个队列采用FIFO调度策略
3. 公平调度器Fair Scheduler，支持多个队列，每个队列可配置一定的资源量，并发执行每个任务（有最小资源限定），动态公平