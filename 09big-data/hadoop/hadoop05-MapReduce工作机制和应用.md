### 核心思想

1. 分布式的运算程序往往需要分成至少2个阶段
2. 第一个阶段的MapTask并发实例，完全并行运行，互不相干
3. 第二个阶段的ReduceTask并发实例互不相干，但是他们的数据依赖于上一个阶段的所有MapTask并发实例的输出
4. MapReduce编程模型只能包含一个Map阶段和一个Reduce阶段，如果用户的业务逻辑非常复杂，那就只能多个MapReduce程序，串行运行

### Map阶段

#### InputFormat数据输入

1. 文件传输之前，会先进行切片，切的片数等于运行的MapTask数量
2. FileInputFormat切片规则解析（TextInputFormat默认的输入是他的子类）
    - 遍历每一个文件
    - 对文件进行切片，文件大于块大小（128M），切片之后会判断剩余文件大小是否大于块大小的1.1倍，不大于就划分为一块
    - 切片文件只记录了切片的元数据信息，比如起始位置、长度以及所在的节点列表等
    - 提交切片规划文件到Yarn上，Yarn上的MrAppMaster就可以根据切片规划文件计算开启MapTask个数
3. CombineTextInputFormat切片规则（专门处理小文件），它可以将多个小文件从逻辑上规划到一个切片中，这样，多个小文件就可以交给一个MapTask处理
4. KeyValueTextInputFormat（父类是FileInputFormat），设置切割符，文件的每一行都会从第一个切割符分成两段，分别为key、value
5. NLineInputFormat（父类是FileInputFormat），根据每个输入文件的行数来规定输出多少个切片，他的key是当前行的偏移量，value是当前行的内容
6. TextInputFormat（父类是FileInputFormat），他的key是当前行的偏移量，value是当前行的内容

#### Shuffle机制

1. Partition分区，分区为几个就会有几个ReduceTask，最后会输出不同的文件，用户没有办法控制哪个key存储到哪个分区，默认分区如下
    ```
    public class HashPartitioner<K2, V2> implements Partitioner<K2, V2> {

        public void configure(JobConf job) {}

        //通过设置的numReduceTasks分区数量，对key进行hash计算
        public int getPartition(K2 key, V2 value,
                                int numReduceTasks) {
            return (key.hashCode() & Integer.MAX_VALUE) % numReduceTasks;
        }

    }
    ```
2. WritableComparable排序
    - MapTask和ReduceTask均会对数据按照key进行排序
    - 对于MapTask，它会将处理的结果暂时放到环形缓冲区中，当环形缓冲区使用率达到一定阈值后，再对缓冲区中的数据进行一次快速排序
    - ReduceTask统一对内存和磁盘上的所有数据进行一次归并排序（如果文件太大，会归并很多次）
3. Combiner合并
    - Combiner组件的父类就是Reducer
    - Combiner和Reducer的区别在于运行的位置
        - Combiner是在每一个MapTask所在的节点运行
        - Reducer是接收全局所有Mapper的输出结果
    - Combiner的意义就是对每一个MapTask的输出进行局部汇总，以减小网络传输量
4. GroupingComparator分组（辅助排序），在同一个分区中先分组，然后再通过组相同的进行key排序，输出时，key就是相同组，value集合就是这个组中的一系列数据

#### Map工作机制

1. MapTask通过用户编写的RecordReader，从输入InputSplit中解析出一个个key/value。
2. Map阶段，该节点主要是将解析出的key/value交给用户编写map()函数处理，并产生一系列新的key/value
3. Collect收集阶段，在用户编写map()函数中，当数据处理完成后，一般会调用OutputCollector.collect()输出结果。在该函数内部，它会将生成的key/value分区（调用Partitioner），并写入一个环形内存缓冲区中
4. Spill阶段，即“溢写”，当环形缓冲区即将满时，MapReduce会对数据进行一次本地排序、合并、压缩等操作，在磁盘上生成一个临时文件
    - 利用快排，对key进行分区后再排序，做到区内有序
    - 将文件按照分区号写入output/spillN.out（N表示当前溢写次数）中，Combiner会在写入文件之前进行一次聚集操作
    - 将分区数据的元信息（文件偏移量、压缩前后数据大小）写到内存索引数据结构SpillRecord中。当前内存索引大小超过1MB，则将内存索引写到文件output/spillN.out.index中
5. 当所有数据处理完成后，MapTask会将所有临时文件合并成一个大文件，并保存到文件output/file.out中，同时生成相应的索引文件output/file.out.index
	- MapTask以分区为单位进行合并对于某个分区
	- 让每个MapTask最终只生成一个数据文件，可避免同时打开大量文件和同时读取大量小文件产生的随机读取带来的开销

![MapReduce之Map工作机制](http://assets.processon.com/chart_image/5fb1df52e401fd7d35cc0554.png?_=1605495466809)

### Reduce阶段

#### OutputFormat数据输出

1. TextOutputFormat（文本输出、默认），它把每条记录写为文本行，它的键和值可以是任意类型，因为TextOutputFormat调用toString()方法把它们转换为字符串
2. SequenceFileOutputFormat（二进制输出），可以将两个文件顺序输出，达到追加的效果

#### Reduce工作机制

1. copy阶段：ReduceTask从各个MapTask上远程属于自己分片的数据，如果数据大小超过一定阈值，则写到磁盘上，否则直接放到内存中
2. Merge阶段：在拷贝数据的同时，ReduceTask启动了两个后台线程对内存和磁盘上的文件进行合并，以防止内存使用过多或磁盘上文件过多
3. Sort阶段：将获取到的数据进行归并排序，在reduce()中输出时就会按照key的有序输出
4. Reduce阶段：reduce()函数将计算结果写到HDFS上


![MapReduce之Reduce工作机制](http://assets.processon.com/chart_image/5fb1df52e401fd7d35cc0554.png?_=1605496447027)

### 实例

1. 需求，有两个数据源，要求得到如下结果，将所有的订单按照每年一个文件输出，要求每个文件中只输出每个小时成交金额最大的订单，数据源如下
	```
    orderId goodIds totalPrice createTime
    2020011 1,3     10.1       1605498416745
    2020013 1,2     20.5       1605498416746
    2020014 3,1     30.6       1605498416747
    2020015 1,5     11.4       1605498416748
    ```
	```
    goodId goodName
    1      香蕉
    2      苹果
    3      葡萄
    ```
    ```
    //希望得到的数据字段
    orderId goodNames totalPrice time
    2020011 香蕉,葡萄  10.01      2020-1-2:11
    ```
2. 实现方案
    - 设置10个reduceTask
    - 为实体key添加排序（时间比较到小时，然后金额）、序列化方法，toString方法
    - 按小时添加分组比较器，reduce时，只取第一个
    - 添加自定义OutputFormat，根据时间每年生成一个方法
    - 指定默认分区

### 其他应用

1. join，主要是针对两个数据来源，有关联id，需要联合的将两个数据源放在一起输出，例如id	pid	amount是一个数据源，pid	pname是另一个数据源，最终要得到输出的字段id pname amount

    - Reduce join，定义一个实体，包含两个数据源的所有字段，同时加载两个数据源（没有值的设置为空字符串），实体按照pid排序然后按照pname排序+添加一个分组比较器，相同的pid为一组，最终的结果就是<font color="red">reduce输出，每次都是一个组的pid，在第一个的一定是pid	pname数据源，后面的数据进行替换写入</font> 
    - Map join，在job中添加分布式缓存，然后在Mapper.setup()中开流，对pid	pname数据源进行缓存在内存中，在Mapper.map()中正常对另外一个数据源循环，替换对应的pid<font color="red">适用于其中一个数据源的数据量不大的情况</font>
2. 计数器，计数结果在程序运行后的控制台上查看，在Mapper或者Reduce方法中调用
    ```
    #使用枚举定义的
    context.getCounter(MyCounter.MALFORORMED).increment(1)
    #采用计数器组、计数器名称的方式统计
    context.getCounter("counterGroup", "counter").increment(1);
    ```