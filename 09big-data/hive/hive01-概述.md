### 概念

1. 定义，Hive是基于Hadoop的一个数据仓库工具，可以将结构化的数据文件映射为一张表，并提供类SQL查询功能
2. 本质，将HQL转换为MapReduce（Tez）程序，Tez是一个强化版的MapReduce
    - Hive处理的数据存储在HDFS
    - Hive分析数据底层的实现是MapReduce
    - 执行程序运行在Yarn上
    > Tez是一个Hive的运行引擎，Tez的性能优于MapReduce，因为MapReduce在工作过程中，会多次在磁盘中写数据，Tez不会，因为Tez需要更高的内存消耗
3. 优点
    - 操作接口采用类SQL语法，简单、容易上手
    - 避免了去写MapReduce，减少开发人员的学习成本
    - Hive支持用户自定义函数，用户可以根据自己的需求来实现自己的函数
    - Hive的执行延迟比较高，因此Hive常用于数据分析，处理大数据
4. 缺点
    - Hive的HQL表达能力有限
        - 迭代算法无法表达
        - 高效算法无法实现
    - Hive的效率比较低
        - 自动生成的MapReduce，通常情况下，不够只能
        - 因为是自动生成调优困难

### Hive的架构图

1. 客户端，CLI（hive shell 命令行、JDBC/ODBC（java访问hive）、WEBUI（浏览器访问hive）
2. 元数据（Metastore），包括有，表名、表所属数据库、表的拥有者、列/分区字段、表的类型(是否是外部表)、表的数据所在目录等
3. hive是HDFS进行存储，MapReduce计算
4. 驱动器（Driver）
    - 解析器（SQL Parser），将SQL字符转换成抽象语法树AST，对AST进行语法分析，比如表是否存在、字段是否存在、SQL语句是否有误
    - 编译器（Physical Plan），将AST编译生成逻辑执行计划
    - 优化器（Query Optimizer），对逻辑执行计划进行优化
    - 执行器（Execution）:把逻辑执行计划转换成可以运行的物理计划,对于Hive来说,就是MR/Spark

![Hive架构图]("http://assets.processon.com/chart_image/5fb635ff1e085368771e1fa1.png?_=1605865395995")

### Hive运行机制

Hive通过给用户提供的交互接口，接收用户的sql，使用自己的driver和结合元数据，将这些指令翻译成MapReduce，打包称为jar，提交到Hadoop中执行，最后将返回结果输出到用户交互接口

![Hive运行机制]("http://assets.processon.com/chart_image/5fb78d9b6376895bf9784694.png?_=1605865760458")

### Hive的安装

1. 安装mysql
2. 将hive二进制安装包解压，配置如下环境变量
    ```
    #HIVE_HOME
    export HIVE_HOME=/opt/module/hive
    export PATH=$PATH:$HIVE_HOME/bin
    ```
3. 删除hive的日志包，避免日志冲突
    ```
    mv $HIVE_HOME/lib/log4j-slf4j-impl-2.10.0.jar $HIVE_HOME/lib/log4j-slf4j-impl-2.10.0.bak
    ```
4. 复制mysql驱动包到hive的目录中
    ```
    cp /opt/software/mysql-connector-java-5.1.48.jar $HIVE_HOME/lib
    ```
5. hive配置文件$HIVE_HOME/conf/hive-site.xml
    ```xml
    <?xml version="1.0"?>
    <?xml-stylesheet type="text/xsl" href="configuration.xsl"?>
    <configuration>
        <property>
            <name>javax.jdo.option.ConnectionURL</name>
            <value>jdbc:mysql://hadoop01:3306/metastore?useSSL=false</value>
        </property>

        <property>
            <name>javax.jdo.option.ConnectionDriverName</name>
            <value>com.mysql.jdbc.Driver</value>
        </property>

        <property>
            <name>javax.jdo.option.ConnectionUserName</name>
            <value>root</value>
        </property>

        <property>
            <name>javax.jdo.option.ConnectionPassword</name>
            <value>root</value>
        </property>

        <property>
            <name>hive.metastore.warehouse.dir</name>
            <value>/user/hive/warehouse</value>
        </property>

        <property>
            <name>hive.metastore.schema.verification</name>
            <value>false</value>
        </property>

        <property>
            <name>hive.metastore.uris</name>
            <value>thrift://hadoop01:9083</value>
        </property>

        <property>
        <name>hive.server2.thrift.port</name>
        <value>10000</value>
        </property>

        <property>
            <name>hive.server2.thrift.bind.host</name>
            <value>hadoop01</value>
        </property>

        <property>
            <name>hive.metastore.event.db.notification.api.auth</name>
            <value>false</value>
        </property>

    </configuration>
    ```
6. 初始化元数据
    ```
    #进入mysql创建数据库
    create database metastore
    #在hive中初始化数据库
    schematool -initSchema -dbType mysql -verbose
    ```
7. 在hadoop中添加hive账号访问权限vim core-site.xml
    ```
    <property>
        <name>hadoop.proxyuser.atguigu.hosts</name>
        <value>*</value>
    </property>
    <property>
        <name>hadoop.proxyuser.atguigu.groups</name>
        <value>*</value>
    </property>
    <property>
        <name>hadoop.http.staticuser.user</name>
        <value>atguigu</value>
    </property>
    ```
8. 编写hive启动脚本vim $HIVE_HOME/bin/hiveservices.sh
    ```
    #!/bin/bash
    HIVE_LOG_DIR=$HIVE_HOME/logs
    META_PID=/tmp/meta.pid
    SERVER_PID=/tmp/server.pid

    function hive_start()
    {
        nohup hive --service metastore >$HIVE_LOG_DIR/metastore.log 2>&1 &
    echo $! > $META_PID
    sleep 8
        nohup hive --service hiveserver2 >$HIVE_LOG_DIR/hiveserver2.log 2>&1 &
        echo $! > $SERVER_PID
    }

    function hive_stop()
    {
        if [ -f $META_PID ]
        then
            cat $META_PID | xargs kill -9
            rm $META_PID
        else
            echo "Meta PID文件丢失，请手动关闭服务"
        fi
        if [ -f $SERVER_PID ]
        then
            cat $SERVER_PID | xargs kill -9
            rm $SERVER_PID
        else
            echo "Server2 PID文件丢失，请手动关闭服务"
        fi

    }

    case $1 in
    "start")
        hive_start
        ;;
    "stop")
        hive_stop
        ;;
    "restart")
    hive_stop
    sleep 2
        hive_start
        ;;
    *)
        echo Invalid Args!
        echo 'Usage: '$(basename $0)' start|stop|restart'
        ;;
    esac
    ```
9. 启动hdfs、yarn、hive
    ```
    start-dfs.sh
    start-yarn.sh
    #启动时间比较久
    hiveservices.sh start
    ```
10. 使用beeline访问hive
    ```
    beeline -u jdbc:hive2://hadoop01:10000 -n atguigu
    ```