### 数据类型

1. 基本数据类型
    - TINYINT 对应java的byte 长度1byte有符号整数 示例20
    - SMALINT short 2byte有符号整数 20
    - INT int 4byte有符号整数 20
    - BIGINT long 8byte有符号整数 20
    - BOOLEAN boolean
    - FLOAT	float 单精度浮点数 3.14159
    - DOUBLE double	双精度浮点数 3.14159
    - STRING string
    - TIMESTAMP 时间类型
    - BINARY 字节数组
2. 集合数据类型
    - STRUCT，都可以通过“点”符号访问元素内容
        ```java
        "address": {
            "street": "hui long guan",
            "city": "beijing"
        }

        //访问
        address.street
        ```
    - ARRAY，是一组具有相同类型和名称的变量的集合
        ```java
        "friends": ["bingbing","lili"]

        //访问
        friends[0]
        ```
    - MAP，是一组键-值对元组集合
        ```java
        "children": {
            "xiao song": 18,
            "xiaoxiao song": 19
        }

        //访问
        children["xiao song"]
        ```
3. 类型转换
    - 隐式转换，大致定义就是可以小的数据类型隐式转换转换为大的类型
    - 强制转换
        ```java
        //将字符串“1”转换为INT的1
        CAST('1' AS INT)
        ```

### 库的DDL

1. 常用sql
    ```mysql
    --建库
    CREATE DATABASE [IF NOT EXISTS] database_name
    [COMMENT database_comment]
    [LOCATION hdfs_path]
    [WITH DBPROPERTIES (property_name=property_value, ...)];

    --例
    create database test
    comment "Just for test"
    location '/test.db'
    with dbproperties("aaa"="bbb");
    ```

    ```mysql
    --查看所有库
    show databases;
    --查看库信息
    desc database test;
    --查看详细信息
    desc database extended test;
    ```

    ```mysql
    --删库
    drop database test;
    --强制删库
    drop database test cascade;
    ```

2. 不常用sql
    ```mysql
    --修改库
    alter database test set dbproperties("aaa"="ccc");
    ```

### 表的DDL

1. 常用sql

    ```mysql
    CREATE [EXTERNAL] TABLE [IF NOT EXISTS] table_name 
    [(col_name data_type [COMMENT col_comment], ...)] 
    [COMMENT table_comment] 
    [PARTITIONED BY (col_name data_type [COMMENT col_comment], ...)] 
    [CLUSTERED BY (col_name, col_name, ...) 
    [SORTED BY (col_name [ASC|DESC], ...)] INTO num_buckets BUCKETS] 
    [ROW FORMAT row_format] 
    [STORED AS file_format] 
    [LOCATION hdfs_path]
    [TBLPROPERTIES (property_name=property_value, ...)]
    [AS select_statement]
    ```

    ```mysql
    --创建表
    create table test
    (id int comment "ID" , name string comment "Name")
    comment "Test Table"
    row format delimited fields terminated by '\t'
    location "/xxx"
    tblproperties("aaa"="bbb");
    ```

    ```mysql
    --查表
    desc test;
    desc formatted test;
    ```

    ```mysql
    --删表
    drop table test2;
    ```
2. 不常用sql

    ```mysql
    --修改一列
    alter table test2 change id id string;
    --添加一列或多列
    alter table test2 add columns(class string comment "class NO.");
    --替换多列信息
    alter table test2 replace columns(id double comment "ID" , name string);
    ```

    ```mysql
    --用查询结果建表
    create table stu_result as select * from stu_par where id=1001;
    ```

### 外部表

1. 定义，将数据和表结构进行关联，删除表，不会删除数据（默认建立内部表）
2. 用途，引用其他数据做查询操作时
3. 常用sql
    ```mysql
    --建立外部表
    create external table test
    (id int, name string)
    row format delimited fields terminated by '\t';
    ```

    ```mysql
    --插入数据
    load data local inpath "/opt/module/datas/student.txt" into table test;
    ```

    ```mysql
    --删表后查看HDFS，数据还在
    drop table test;
4. 不常用sql
    ```mysql
    --建立错成内部表
    create table test
    (id int, name string)
    row format delimited fields terminated by '\t';
    ```

    ```mysql
    --转换成外部表
    alter table test set tblproperties("EXTERNAL"="TRUE");
    ```

    ```mysql
    --转换成内部表
    alter table test set tblproperties("EXTERNAL"="FALSE");
    ```
### 分区表

1. 定义，在对应的HDFS文件系统上的独立的文件夹，该文件夹下是该分区所有的数据文件
2. 用途，用来查询指定的分区，这样可以少扫描很多数据
3. 常用sql
    ```mysql
    --建立一张分区表
    create table stu_par
    (id int, name string)
    partitioned by (class string)
    row format delimited fields terminated by '\t';
    ```

    ```mysql
    --向表中插入数据
    load data local inpath '/opt/module/datas/student.txt' into table stu_par
    partition (class='01');
    load data local inpath '/opt/module/datas/student.txt' into table stu_par
    partition (class='02');
    load data local inpath '/opt/module/datas/student.txt' into table stu_par
    partition (class='03');
    ```

    ```mysql
    --建立二级分区表
    create table stu_par2
    (id int, name string)
    partitioned by (grade string, class string)
    row format delimited fields terminated by '\t';
    ```

    ```mysql
    --插入数据，指定到二级分区
    load data local inpath '/opt/module/datas/student.txt' into table stu_par2
    partition (grade='01', class='03');
    ```

    ```mysql
    --查表时，选择分区，可以减小数据扫描量
    select * from stu_par where class="01";
    select * from stu_par where id=1001;
    ```

    ```mysql
    --查询分区表的分区
    show partitions stu_par;
    ```
4. 不常用sql

    ```mysql
    --如果提前准备数据，但是没有元数据，修复方式
    --1. 添加分区
    alter table stu_par add partition(class="03");
    --2. 直接修复
    msck repair table stu_par;
    --3. 上传时候直接带分区
    load data local inpath '/opt/module/datas/student.txt' into table stu_par
    partition (class='03');
    ```

    ```mysql
    --增加分区
    alter table stu_par add partition(class="05");
    --一次增加多个分区
    alter table stu_par add partition(class="06") partition(class="07");
    --删除分区
    alter table stu_par drop partition(class="05");
    --一次删除多个分区
    alter table stu_par drop partition(class="06"), partition(class="07");
    ```

### 分桶表

1. 定义，分桶是将数据集分解成更容易管理的若干部分的另一个技术
2. 分桶和分区的区别，分区是针对的数据的存储路径，分桶针对的是数据文件
3. 用途，主要用于筛选的时候可以少扫描数据，例如分为4个区，查询id=1，就只会扫描其中一个桶
4. sql，查询分桶的表，不需要额外指定

    ```mysql
    --创建分桶表
    create table stu_buck(id int, name string)
    clustered by(id) 
    into 4 buckets
    row format delimited fields terminated by '\t';
    ```

    ```mysql
    --向分桶表中插入数据
    load data local inpath '/opt/module/datas/student.txt' into table stu_buck;
    ```

### 导入导出

1. 数据导入

    ```mysql
    --从本地磁盘或者DHFS导入数据
    load data [local] inpath '/opt/module/datas/student.txt' [overwrite] into table student [partition (partcol1=val1,…)];

    --例子
    load data local inpath '/opt/module/datas/student.txt' overwrite into table student;

    --先在hdfs://hadoop102:8020/xxx文件夹上传一份student.txt
    --HDFS的导入是移动，而本地导入是复制
    load data inpath '/xxx/student.txt' overwrite into table student;
    ```

    ```mysql
    --Insert导入
    insert into table student select id, name from stu_par where class="01";
    ```

    ```mysql
    --建表时候通过location加载
    --先在hdfs://hadoop102:8020/xxx文件夹上传一份student.txt
    create external table student2
    (id int, name string)
    row format delimited fields terminated by '\t'
    location '/xxx';
    ```

2. 数据导出，不常用

    ```mysql
    --Insert导出
    insert overwrite local directory '/opt/module/datas/export/student'
    select * from student;

    --带格式导出
    insert overwrite local directory '/opt/module/datas/export/student1'
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
    select * from student;
    ```

    ```bash
    #bash命令行导出
    hive -e "select * from default.student;" > /opt/module/datas/export/test.txt
    ```

    ```mysql
    --整张表export到HDFS
    export table student to '/export/student';

    --从导出结果导入到Hive
    import table student3 from '/export/student';
    ```

3. 数据删除

    ```mysql
    --只删表数据，不删表本身
    truncate table student;
    ```