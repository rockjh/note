### 要点

1. hive的查询都是全表扫描，优化仅仅有分区和分桶
2. hvie建立数据库，如果不指定会在配置的默认路径建立文件夹，再建立表，会在数据库的文件夹下，再次建立表的文件夹
3. select查询执行的顺序，
    - from
    - where
    - group by
    - select
    - having
    - **over()
    - order by
    - limit

### 数据初始化

```mysql
--建立员工表
create table if not exists emp(
empno int,
ename string,
job string,
mgr int,
hiredate string, 
sal double, 
comm double,
deptno int)
row format delimited fields terminated by '\t';
```

```mysql
--建立部门表
create table if not exists dept(
deptno int,
dname string,
loc int
)
row format delimited fields terminated by '\t';
```

```mysql
--导入数据
load data local inpath '/opt/module/datas/dept.txt' into table
dept;
load data local inpath '/opt/module/datas/emp.txt' into table emp;
```

### limit

```mysql
--limit，取前几行
select * from emp limit 5;
```

### where

```mysql
--查询工资大于1000的人
select * from emp where sal > 1000;
```

```mysql
--通配符字符串匹配 % _
--以A开头的员工
select * from emp where ename like "A%";
```

```mysql
--正则匹配
--以A开头的员工
select * from emp where ename rlike "^A";
```

```mysql
--与或非
select * from emp where empno = 30 and sal > 1000;
```

```mysql
--计算emp表每个部门的平均工资
select deptno, avg(sal) aa from emp group by deptno;
```

### group

```mysql
--分组过滤
--计算部门平均工资大于2000的部门
select deptno, avg(sal) aa from emp group by deptno having aa>2000;
```

### join

```mysql
--查询员工编号，姓名以及部门所在名称
select
    e.empno,
    e.ename,
    d.dname
from
    emp e
join
    dept d
on
    e.deptno=d.deptno;
```

```mysql
--多表连接
SELECT e.ename, d.dname, l.loc_name
FROM   emp e 
JOIN   dept d
ON     d.deptno = e.deptno 
JOIN   location l
ON     d.loc = l.loc;
```

### order

```mysql
--按照工资降序排序(全局排序)
select *
from emp
order by sal desc;
```

```mysql
--多条件排序，先按部门排序，再按工资排序
select *
from emp
order by
deptno asc,
sal desc;
```

```mysql
--一般需求不会要求给所有的数据排序，而要求求前几
--求工资前10的人,Map会先求局部前10
select *
from emp
order by sal desc
limit 10;
```

```mysql
--还有一种可能，我们只需要看大概的数据趋势，不需要全排序
--Hive的局部排序
select *
from emp
sort by empno desc;
```

```mysql
--指定局部排序的分区字段
select * from emp
distribute by empno
sort by sal desc;
```

```
--如果分区和排序的字段一样，我们可以用cluster by代替
select * from emp distribute by empno sort by empno;
select * from emp cluster by empno;
```
