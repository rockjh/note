### 系统内置函数

1. 查看系统内置函数
    ```mysql
    show functions;
    ```
2. 查看函数的用法
    ```mysql
    desc function upper;
    ```
3. 详细显示自带的函数的用法
    ```mysql
    desc function extended upper;
    ```

### 常用函数

1. 简单函数

    ```mysql
    --nvl空字段赋值
    select comm, nvl(comm, -1) from emp;
    ```

    ```mysql
    --case when
    --统计不同部门男女各有多少人
    select
        dept_id,
        count(*) total,
        sum(case sex when '男' then 1 else 0 end) male,
        sum(case sex when '女' then 1 else 0 end) female
    from
        emp_sex
    group by
        dept_id;
    ```
2. 行转列（本质是做了一个聚合，将很多行转换成了并排的一列），用到的函数
    - CONCAT(string A/col, string B/col…)，字符串连接
    - CONCAT_WS(separator, str1, str2,...)，字符串连接，固定的分隔符
    - COLLECT_SET(col)，将某字段的值进行去重汇总，产生array类型字段，配合聚合使用

    ```
    --输入
    name	constellation	blood_type
    孙悟空	白羊座	A
    大海	射手座	A
    宋宋	白羊座	B
    猪八戒	白羊座	A
    凤姐	射手座	A
    苍老师	白羊座	B

    --输出，星座和血型一样的人归类到一起
    射手座,A            大海|凤姐
    白羊座,A            孙悟空|猪八戒
    白羊座,B            宋宋|苍老师
    ```

    ```mysql
    --行转列
    select
        concat(constellation,",",blood_type) xzxx,
        concat_ws("|", collect_list(name)) rentou
    from
        person_info
    group by
        constellation,blood_type;
    ```
3. 列转行（本质是拆分，将一行的数据炸开，分别显示为多行）
    - EXPLODE(col)，将hive一列中复杂的array或者map结构拆分成多行
    - LATERAL VIEW，用于和split, explode等UDTF一起使用，它能够将一列数据拆成多行数据，在此基础上可以对拆分后的数据进行聚合，语法
        ```
        LATERAL VIEW udtf(expression) tableAlias AS columnAlias
        ```
    
    ```
     --输入
     movie   category
    《疑犯追踪》	悬疑,动作,科幻,剧情
    《Lie to me》	悬疑,警匪,动作,心理,剧情
    《战狼2》	战争,动作,灾难

     --输出，星座和血型一样的人归类到一起
    《疑犯追踪》      悬疑
    《疑犯追踪》      动作
    《疑犯追踪》      科幻
    《疑犯追踪》      剧情
    《Lie to me》   悬疑
    《Lie to me》   警匪
    《Lie to me》   动作
    《Lie to me》   心理
    《Lie to me》   剧情
    《战狼2》        战争
    《战狼2》        动作
    《战狼2》        灾难
    ```
    ```mysql
    --列转行
    select
        m.movie,
        tbl.cate
    from
        movie_info m
    lateral view
        explode(split(category, ",")) tbl as cate;
    ```

### 窗口函数

1. 定义，相当于在查询中另外开一个窗口，加入分组排序之类的，进行统计
2. 相关函数说明
    - OVER()，指定分析函数工作的数据窗口大小，这个数据窗口大小可能会随着行的变而变化
    - CURRENT ROW，当前行
    - n PRECEDING，往前n行数据
    - n FOLLOWING，往后n行数据
    - UNBOUNDED，起点
    - UNBOUNDED PRECEDING，从前面的起点
    - UNBOUNDED FOLLOWING，到后面的终点
    - LAG(col,n,default_val)，往前第n行数据
    - LEAD(col,n, default_val)，往后第n行数据
    - NTILE(n)，把有序窗口的行分发到指定数据的组中，各个组有编号，编号从1开始，对于每一行，NTILE返回此行所属的组的编号，n必须为int类型
3. 示例
    ```mysql
    --各种聚合
    select name,orderdate,cost, 
    sum(cost) over() as sample1,--所有行相加 
    sum(cost) over(partition by name) as sample2,--按name分组，组内数据相加 
    sum(cost) over(partition by name order by orderdate) as sample3,--按name分组，组内数据累加 
    sum(cost) over(partition by name order by orderdate rows between UNBOUNDED PRECEDING and current row ) as sample4 ,--和sample3一样,由起点到当前行的聚合 
    sum(cost) over(partition by name order by orderdate rows between 1 PRECEDING and current row) as sample5, --当前行和前面一行做聚合 
    sum(cost) over(partition by name order by orderdate rows between 1 PRECEDING AND 1 FOLLOWING ) as sample6,--当前行和前边一行及后面一行 
    sum(cost) over(partition by name order by orderdate rows between current row and UNBOUNDED FOLLOWING ) as sample7 --当前行及后面所有行 
    from business;
    ```

    ```mysql
    --结合其他函数使用
    select
        name, orderdate, cost, 
        lag(orderdate, 1) 
        over(partition by name order by orderdate) last_order,
        lead(orderdate, 1) 
        over(partition by name order by orderdate) next_order
    from
        business;
    ```

    ```mysql
    --ntile
    SELECT
        *
    FROM
        (
            select name,
            orderdate,
            cost,
            ntile(5) over(
            order by orderdate) n
        from
            business) t1
    WHERE
        n = 1;
    ```

### 排名函数

1. 函数
    - RANK() 排序相同时会重复（排序号会重复），总数不会变
    - DENSE_RANK() 排序相同时会重复，总数会减少
    - ROW_NUMBER() 会根据顺序计算
2. 示例
    ```mysql
    --percent_rank
    select
        name,
        orderdate,
        cost,
        PERCENT_RANK() over(
        order by orderdate) pr
    from
        business;
    ```

    ```mysql
    --rank
    SELECT
        *,
        rank() OVER(partition by subject
    order by
        score desc) r,
        DENSE_RANK() OVER(partition by subject
    order by
        score desc) dr,
        ROW_NUMBER() OVER(partition by subject
    order by
        score desc) rn
    from
        score;
    ```

### 日期函数

1. 函数
    - current_date返回当前日期
    - date_add，date_sub日期的加减
    - 两个日期之间的日期差
2. 示例

    ```mysql
    --current_date 返回当前日期
    select current_date();
    ```

    ```mysql
    --日期的加减
    --今天开始90天以后的日期
    select date_add(current_date(), 90);
    --今天开始90天以前的日期
    select date_sub(current_date(), 90);
    ```

    ```mysql
    --日期差
    SELECT datediff(CURRENT_DATE(), "1990-06-04");
    ```
> 可以自定义函数，一般是自己写MapReduce然后打包jar，上传到hive的lib包中