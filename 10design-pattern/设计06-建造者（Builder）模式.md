### 定义

将一个复杂对象的构造与他的表示分离，使同样的构建过程可以创建不同的表示，他是将以复杂的对象分解为多个简单的对象，然后一步一步构建而成。


### 实现

HttpClient包中的简单使用

```
CloseableHttpClient client = HttpClientBuilder.create().
		setProxy(new HttpHost("127.0.0.1", 8080)).
		build();
```

### 扩展

- 建站者（Builder）模式在应用过程中可以根据需要改变，如果创建的产品种类只有一种，那么只要一个具体的建造者，否则可以和工厂模式一起使用