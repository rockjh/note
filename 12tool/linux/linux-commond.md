linux 命令

xdg-open
tar
unzpi
zip
rar
unrar
dpkg
gedit
vi
vim

tar.bz2 tar -jxvf

sudo chmod +x *.* 
echo

### 命令的组成

command [-options] parameter1 parameter2 ...

- command为指令的名称，例如cd,mv等
- [-options]指命令带的其他功能，比如--help是弹出命令的帮助信息，ls -l就会比原有的ls多文件自恋的详细信息；也有可能是带+号的，date +%Y/%m/%d
- parameter1 parameter2为依附在选项后面的参数，或者是common的参数，例如mv /sourceFile /targetFile 中/sourceFile和/targetFile就是参数，参数是根据命令来的

### 命令行热键

- tab 命令、文档名、参数补全
- ctrl+c中断当前正在执行的命令
- ctrl+d可以用来取代exit的输入，比如bc，进入计算器之后，可以直接按ctrl+d结束，效果相当于输入quit结束当前的命令

### 命令帮助

- 命令带参数--help，可以查看参数使用说明，例如：ls --help
- 使用manual操作说明可以看到命令详细的说明，例如：man ls
- 使用info可以获得在线求助，它里面有一些超链接，分页，分段的更详细的说明，例如：info ls

### 命令快捷执行的秘密

主要归功与PATH变量，通过执行echo $PATH查看变量值，可以看到其中有/bin,/usr/bin等等路径，说明在该路径下的可执行文件能够直接执行，如果不在配置的路径中需要执行的话，可以分别使用相对路径或者绝对路径执行./install或/opt/install

### 文本编辑器

- vi
- vim
- nano
- gedit

### 关机指令

- shutdown
- reboot
- half
- poweroff

### linux内核指令

uname用于显示电脑及操作系统的信息，加上-a显示所有信息

### 网络命令

-lsof