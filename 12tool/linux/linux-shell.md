linux shell

### 初步介绍

- linux预设的shell是/bin/bash
- bash shell拥有记忆功能，上下键可以找出曾经使用过的命令，可以在～/.bash_history中找到上一次注销系统前使用的指令
- tab命令补全，文件补全

### bash shell语法

- 查看变量值，echo $PATH
- 设定变量值，XXXNAME = XXXVALUE，如果变量内容有空格符可以使用双引号，或者单引号，但是双引号中的$XXX会识别为XXX的变量值，单引号中会原样识别为$XXX
- 可以使用跳脱字符\将特殊符号，Enter，$,\,空格等变成一般字符
- 变量值追加内容PATH=${PATH}content
- 变量需要在其他子程序执行，则需要以export来使变量变成环境变量，export PATH
- 取消变量的方法为使用unset，unset PATH