### 数据类型
- Byte、Short、Int、Long、Float、Double、Char、String、Boolean
- Unit，表示无值，和其他语言中void等同。用作不返回任何结果的方法的结果类型。Unit只有一个实例值，写成()
- Null，null或空引用
- Nothing	Nothing类型在scala的类层级的最底端；它是任何其他类型的子类型
- Any，Any是所有其他类的超类
- AnyRef，AnyRef类是scala里所有引用类(reference class)的基类

### 符号引用

- 变量，使用var声明，除了在abstract class和trait中可以不用初始化值，其他时候都要初始化
    ```
    var s: Stirng = "String"
    var s = "String"
    ```
- 常量，使用val声明，初始化后不可更改，除了在abstract class和trait中可以不用初始化值，其他时候都要初始化
    ```
    val s: String = "String"
    val s = "String"
    ```

### 访问修饰符

1. public，默认访问级别，当不写访问级别时，默认这个级别，任何地方都可以访问
2. protocol，比java更加严格，只允许子类访问，同包路径下不可访问
3. private，比java更加严格
    - 允许外部类访问内部类的私有成员
    - 对象的内部访问
    ```
    class Outer {
        class Inner {
            private def f() {
                println("f")
            }
            class InnerMost {
                f() // 正确
            }
        }
        (new Inner).f() //错误
    }
    ```

### 运算符（按顺序排名）

- ()、[]
- !、~
- *、/、%
- +、-、>>、>>>、<<、>、>=、<、<=
- ==、!=、&、^、|、&&、||
- =、+=、-=、*=、/=、%=、>>=、<<=、&=、^=、|=
- ,

### 流程语句

1. if、else if、else

2. while、do....while
    ```
    while(condition){
        statement(s);
    }

    do {
        statement(s);
    } while( condition );
    ```
3. for
    ```
    //to也可以替换为util
    for( var x <- i to j){
        statement(x);
    }

    val numList: List[Int] = List(1, 2, 3, 4, 5, 6);
    //迭代集合
    for (element <- numList) {
        println(element)
    }

    val a = 1
    val b = 2
    //会迭代a,b所有的值范围，类似双重循环
    //x=1,y=2;x=1,y=3;x=2;y=2....
    for( x <- a to 3; y <- b to 3){
        println( "Value of x: " + x )
        println( "Value of y: " + y )
    }

    val numList = List(1,2,3,4,5,6,7,8,9,10);
    //加入条件筛选
    for( a <- numList if a != 3; if a < 8 ){
        println( "Value of a: " + a );
    }

    val numList = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    // 使用yield，将循环结果保存到常量retVal中
    val retVal = for {a <- numList if a != 3; if a < 8} yield a
    // 输出返回值
    for (a <- retVal) {
        println("Value of a: " + a);
    }
    ```

### 方法

1. 方法是组成类的一部分，使用def定义
2. 方法声明，如果不写等号和方法主体，这个方法被隐式的认为是abstract
    ```
    def methodName ([参数列表]): [return type]
    ```
3. 方法定义
    ```
    def methodName ([参数列表]): [return type] = {
        method body
        return [expr]
    }

    def xxx(x: Int, y: Int): Int = {
		println(x);println(y)
        //scala会将最后出现的变量作为return的值
        x * y
    }

    //当没有返回值时，可以将: Unit =省略
    def main(args: Array[String]) {
        ...
    }

    //当方法中没有参数时，如果定义时没有括号
    //调用时加上括号会报错
    //当定义有括号，调用时随意
    def xxx {
		println("xxx")
	}
    ```
4. 方法调用
    ```
    methodName(参数列表)
    [instance.]methodName(参数列表)
    ```

### 函数
     
1. 函数是一个对象，可以赋值给变量，scala中的函数其实就是继承了trait的类的对象，使用val声明
    ```
    val f1 = (x: Int, y: Int) => x * y
    ```


### String

1. scala中的String是不可变的
2. 创建一个可以修改的字符串可以使用StringBuilder
    ```
    val buf = new StringBuilder;
    buf += 'a'
    buf ++= "bcde"
	buf.append("f")
    //abcdef
    println("buf is : " + buf.toString)
    ```
3. 因为scala中的Stirng是直接引用的java.lang.String，所以java.lang.String中的方法scala中都可以使用
    ```
    type String = java.lang.String
    ```
4. 调用java.lang.String中的方法
    ```
    "1234".length()
    "1234".isEmpty()
    ...
    ```

### 数组

### 集合

1. List，类似于数组，所有的元素类型一致，但是列表是不可变的，可以具有链接表结构
    - 创建
        ```
        val site: List[String] = List("Runoob", "Google", "Baidu")
		//同上创建列表
		val site = "Runoob" :: ("Google" :: ("Baidu" :: Nil))
		
		//二维列表
		val dim: List[List[Int]] = List(
			List(1, 0, 0),
			List(0, 1, 0),
			List(0, 0, 1)
		)
        ```
    - 列表连接
        ```
        val site1 = "Runoob" :: ("Google" :: ("Baidu" :: Nil))
		val site2 = "Facebook" :: ("Taobao" :: Nil)
		
		// 使用 ::: 运算符
		var fruit = site1 ::: site2
		
		// 使用 List.:::() 方法
		fruit = site1.:::(site2)
		
		// 使用 concat 方法
		fruit = List.concat(site1, site2)
        ```
    - 其他api，查看源码
        ```
        //获取第几个元素
        apply(n: Int)
        ```
2. Set
    - 
3. Map
4. 元组

    - 索引从1开始
5. Option
6. Iterator


### 类和对象




### Trait（特征）

1. scala中只能继承单一的父类，trait可以被继承多个
2. trait类似java中的接口，但是他可以定义属性和方法的实现，在功能上又和抽象类差不多
3. 实例
    ```
    trait Equal {
        def isEqual(x: Any): Boolean
        def isNotEqual(x: Any): Boolean = !isEqual(x)
    }

    class Point(xc: Int, yc: Int) extends Equal {
        var x: Int = xc
        var y: Int = yc
        def isEqual(obj: Any) =
            obj.isInstanceOf[Point] &&
            obj.asInstanceOf[Point].x == x
    }
    ```
4. trait的构造顺序
    - 调用超类的构造器
    - 特征构造器在超类构造器之后、类构造器之前执行
    - 特征由左到右被构造
    - 每个特征当中，父特征先被构造
    - 如果多个特征共有一个父特征，父特征不会被重复构造
    - 所有特征被构造完毕，子类被构造

### 模式匹配

1. match case类似于switch case，找到一个case立即退出
2. 示例
    ```
    def matchTest(x: Any): Any = x match {
        //对应整型数值1
        case 1 => "one"
        //对应字符串值two
        case "two" => 2
        //对应类型模式Int
        case y: Int => "scala.Int"
        //在0或空字符串的情况下
        case 0 | "" => false
        //在模式匹配中使用if
        case x if x == 2 || x == 3 => println("two's company, three's a crowd")
        //即没有找到其他匹配时的匹配项
        case _ => "many"
   }
    ```
3. 使用样例类，在模式匹配中，使用了case关键字的类定义就属于样例类，可以不使用new关键字
    - 构造器的每个参数都成为val，除非显式被声明为var，但是并不推荐这么做
    - 在伴生对象中提供了apply方法，所以可以不使用new关键字就可构建对象
    - 提供unapply方法使模式匹配可以工作
    - 生成toString、equals、hashCode和copy方法，除非显示给出这些方法的定义
    ```
    object Test {
        def main(args: Array[String]) {
            val alice = Person("Alice", 25)
            val bob = Person("Bob", 32)
            val charlie = Person("Charlie", 32)
            
            for (person <- List(alice, bob, charlie)) {
                person match {
                    case Person("Alice", 25) => println("Hi Alice!")
                    case Person("Bob", 32) => println("Hi Bob!")
                    case Person(name, age) =>
                        println("Age: " + age + " year, name: " + name + "?")
                }
            }
        }
        //样例类
        case class Person(name: String, age: Int)
    }
    ```

### 异常处理

1. 抛出异常
    ```
    throw new IllegalArgumentException
    ```
2. 捕获异常，case异常捕捉是按次序，范围小的异常写在前面，如果把普遍的异常Throwable写在最前面，那么直接就在最前面捕捉了，后面的就没有用了
    ```
    try {
        val f = new FileReader("input.txt")
    } catch {
        case ex: FileNotFoundException =>{
            println("Missing file exception")
        }
        case ex: IOException => {
            println("IO Exception")
        }
    } finally {
        println("Exiting finally...")
    }
    ```

### Extractor（提取器）

### IO

1. 文件写操作，直接使用java中的IO类
    ```
    val writer = new java.io.PrintWriter(new java.io.File("test.txt" ))
    writer.write("菜鸟教程")
    writer.close()
    ```
2. 获取用户的输入
    ```
    print("请输入菜鸟教程官网 : " )
    val line = scala.io.StdIn.readLine()
    println("谢谢，你输入的是: " + line)
    ```
3. 读取文件内容
    ```
    println("文件内容为:" )
    scala.io.Source.fromFile("test.txt" ).foreach{
        print
    }
    ```