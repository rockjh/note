### Mybatis

1. 从架构设计方向思考，有API接口层、数据处理层、基础支撑层（一些工具类）
   - API接口层：提供给外部使用的接口API，开发人员通过这些本地API来操纵数据库。接口层一接收到调用请求就会调用数据处理层来完成具体的数据处理。
   - 数据处理层：负责具体的SQL查找、SQL解析、SQL执行和执行结果映射处理等。它主要的目的是根据调用的请求完成一次数据库操作。
   - 基础支撑层：负责最基础的功能支撑，包括连接管理、事务管理、配置加载和缓存处理，这些都是共用的东西，将他们抽取出来作为最基础的组件。为上层的数据处理层提供最基础的支撑

2. 针对比较复杂的数据处理层又是按照流程进行设计的
   - SqlSession--->Executor--->StatementHandler这里是串行流程，属于Mybatis的一个框架设计，主要功能是在保证扩展性的同时，如何保证对接入JDBC操作，Executor中是引用的StatementHandler，在调用StatementHandler之前会将Connection、Statement都实例化好
   - StatementHandler就是完整的阐述了JDBC的操作，主要包含了ParameterHander（参数）、TypeHandler（类型转换）、Statement、ResultSet、ResultSetHandler（返回值），每一个处理器中都带上了Statement引用，直接设置在Statement中，最终会直接执行

3. springboot+mybatis，针对API接口层，兼容spring的IOC，做到springboot+mybatis的整合
   - springboot利用MybatisAutoConfiguration自动注入，默认配置
   - 利用后置处理器，加载配置好的Mybatis中的Configuration
   - 利用FactoryBean，直接注入mapper接口的动态代理实现

### Spring

1. IOC是Context的体现，如果需要加入Web容器，Web容器就会将IOC进行扩展变成Web Context
2. 事务和AOP的结合，AOP是基础，事务是AOP的一个体现
   - 拦截动态代理这些都是事务tx的jar包引用的AOP的jar
   - 对于事务的解析（TransactionAttributeSource）、执行的拦截方法（TransactionInterceptor）、事务管理（TransactionSynchronizationManager）都是在tx的jar中执行的

3. MVC将spring中注册的bean进行了一个整合，和嵌入式tomcat进行了一个整合，分成了九大组件，按照执行流程顺序执行九大组件

### Springboot

1. 